(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _test_test_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./test/test.component */ "./src/app/test/test.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./not-found/not-found.component */ "./src/app/not-found/not-found.component.ts");
/* harmony import */ var _avis_avis_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./avis/avis.component */ "./src/app/avis/avis.component.ts");
/* harmony import */ var _video_video_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./video/video.component */ "./src/app/video/video.component.ts");









const routes = [
    { path: '', redirectTo: '/', pathMatch: 'full' },
    { path: '', component: _video_video_component__WEBPACK_IMPORTED_MODULE_6__["VideoComponent"] },
    { path: 'avis', component: _avis_avis_component__WEBPACK_IMPORTED_MODULE_5__["AvisComponent"] },
    { path: 'home', component: _home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"] },
    { path: 'test', component: _test_test_component__WEBPACK_IMPORTED_MODULE_2__["TestComponent"] },
    { path: 'cpn', loadChildren: () => __webpack_require__.e(/*! import() | cpn-cpn-module */ "cpn-cpn-module").then(__webpack_require__.bind(null, /*! ./cpn/cpn.module */ "./src/app/cpn/cpn.module.ts")).then(m => m.CpnModule) },
    { path: '**', component: _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_4__["NotFoundComponent"] },
];
class AppRoutingModule {
}
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); }, imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
                exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services/cpn/auth.service */ "./src/app/services/cpn/auth.service.ts");
/* harmony import */ var _services_token_storage_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./services/token-storage.service */ "./src/app/services/token-storage.service.ts");







class AppComponent {
    constructor(router, auth, tokenStorage) {
        this.auth = auth;
        this.tokenStorage = tokenStorage;
        this.title = 'frontCrm';
        router.events.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["filter"])((event) => event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"])).subscribe((event) => {
            this.previousUrl = this.currentUrl;
            this.currentUrl = event.url;
        });
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_token_storage_service__WEBPACK_IMPORTED_MODULE_4__["TokenStorageService"])); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 1, vars: 0, template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "router-outlet");
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]], styles: [".loader[_ngcontent-%COMP%] {\r\n    display: inline-block;\r\n    width: 40px;\r\n    height: 40px;\r\n    position: absolute;\r\n    left: 0;\r\n    right: 0;\r\n    margin-left: auto;\r\n    margin-right: auto;\r\n    top: calc(50% - 50px);\r\n    transform: translateY(-50%);\r\n  }\r\n  \r\n  .loader[_ngcontent-%COMP%]:after {\r\n    content: ' ';\r\n    display: block;\r\n    width: 30px;\r\n    height: 30px;\r\n    border-radius: 50%;\r\n    border: 5px solid #fff;\r\n    border-color: #fff transparent #fff transparent;\r\n    animation: loader 1.2s linear infinite;\r\n  }\r\n  \r\n  @keyframes loader {\r\n    0% {\r\n      transform: rotate(0deg);\r\n    }\r\n    100% {\r\n      transform: rotate(360deg);\r\n    }\r\n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxxQkFBcUI7SUFDckIsV0FBVztJQUNYLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsT0FBTztJQUNQLFFBQVE7SUFDUixpQkFBaUI7SUFDakIsa0JBQWtCO0lBQ2xCLHFCQUFxQjtJQUNyQiwyQkFBMkI7RUFDN0I7O0VBRUE7SUFDRSxZQUFZO0lBQ1osY0FBYztJQUNkLFdBQVc7SUFDWCxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLHNCQUFzQjtJQUN0QiwrQ0FBK0M7SUFDL0Msc0NBQXNDO0VBQ3hDOztFQUVBO0lBQ0U7TUFDRSx1QkFBdUI7SUFDekI7SUFDQTtNQUNFLHlCQUF5QjtJQUMzQjtFQUNGIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubG9hZGVyIHtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbGVmdDogMDtcclxuICAgIHJpZ2h0OiAwO1xyXG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgICB0b3A6IGNhbGMoNTAlIC0gNTBweCk7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XHJcbiAgfVxyXG4gIFxyXG4gIC5sb2FkZXI6YWZ0ZXIge1xyXG4gICAgY29udGVudDogJyAnO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB3aWR0aDogMzBweDtcclxuICAgIGhlaWdodDogMzBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIGJvcmRlcjogNXB4IHNvbGlkICNmZmY7XHJcbiAgICBib3JkZXItY29sb3I6ICNmZmYgdHJhbnNwYXJlbnQgI2ZmZiB0cmFuc3BhcmVudDtcclxuICAgIGFuaW1hdGlvbjogbG9hZGVyIDEuMnMgbGluZWFyIGluZmluaXRlO1xyXG4gIH1cclxuICBcclxuICBAa2V5ZnJhbWVzIGxvYWRlciB7XHJcbiAgICAwJSB7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xyXG4gICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgIHRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7XHJcbiAgICB9XHJcbiAgfSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-root',
                templateUrl: './app.component.html',
                styleUrls: ['./app.component.css']
            }]
    }], function () { return [{ type: _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"] }, { type: _services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] }, { type: _services_token_storage_service__WEBPACK_IMPORTED_MODULE_4__["TokenStorageService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/animations.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _material_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./material-module */ "./src/app/material-module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _security_token_interceptor_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./security/token-interceptor.service */ "./src/app/security/token-interceptor.service.ts");
/* harmony import */ var ngx_countdown__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-countdown */ "./node_modules/ngx-countdown/__ivy_ngcc__/fesm2015/ngx-countdown.js");
/* harmony import */ var _test_test_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./test/test.component */ "./src/app/test/test.component.ts");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");
/* harmony import */ var _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @fullcalendar/angular */ "./node_modules/@fullcalendar/angular/__ivy_ngcc__/fesm2015/fullcalendar-angular.js");
/* harmony import */ var _fullcalendar_daygrid__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @fullcalendar/daygrid */ "./node_modules/@fullcalendar/daygrid/main.js");
/* harmony import */ var _fullcalendar_interaction__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @fullcalendar/interaction */ "./node_modules/@fullcalendar/interaction/main.js");
/* harmony import */ var _map_french_map_french_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./map-french/map-french.component */ "./src/app/map-french/map-french.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./not-found/not-found.component */ "./src/app/not-found/not-found.component.ts");
/* harmony import */ var _baseUrl__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./baseUrl */ "./src/app/baseUrl.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/__ivy_ngcc__/fesm2015/ng-select-ng-select.js");
/* harmony import */ var _angular_slider_ngx_slider__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular-slider/ngx-slider */ "./node_modules/@angular-slider/ngx-slider/__ivy_ngcc__/fesm2015/angular-slider-ngx-slider.js");
/* harmony import */ var ng_lazyload_image__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ng-lazyload-image */ "./node_modules/ng-lazyload-image/__ivy_ngcc__/fesm2015/ng-lazyload-image.js");
/* harmony import */ var _avis_avis_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./avis/avis.component */ "./src/app/avis/avis.component.ts");
/* harmony import */ var _video_video_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./video/video.component */ "./src/app/video/video.component.ts");


/**************** library      **********************************/





/**************** component      **********************************/

















 // <-- import it

_fullcalendar_angular__WEBPACK_IMPORTED_MODULE_12__["FullCalendarModule"].registerPlugins([
    _fullcalendar_daygrid__WEBPACK_IMPORTED_MODULE_13__["default"],
    _fullcalendar_interaction__WEBPACK_IMPORTED_MODULE_14__["default"]
]);
class AppModule {
}
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({ factory: function AppModule_Factory(t) { return new (t || AppModule)(); }, providers: [
        { provide: 'baseUrl', useValue: _baseUrl__WEBPACK_IMPORTED_MODULE_18__["baseUrl"] },
        { provide: _angular_common__WEBPACK_IMPORTED_MODULE_19__["APP_BASE_HREF"], useValue: '/' },
        {
            provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HTTP_INTERCEPTORS"],
            useClass: _security_token_interceptor_service__WEBPACK_IMPORTED_MODULE_8__["TokenInterceptorService"],
            multi: true,
        },
        { provide: ng_lazyload_image__WEBPACK_IMPORTED_MODULE_22__["LAZYLOAD_IMAGE_HOOKS"], useClass: ng_lazyload_image__WEBPACK_IMPORTED_MODULE_22__["ScrollHooks"] }
    ], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
            _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
            _material_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModule"],
            ngx_countdown__WEBPACK_IMPORTED_MODULE_9__["CountdownModule"],
            _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_12__["FullCalendarModule"],
            _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_20__["NgSelectModule"],
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_11__["MatIconModule"],
            _angular_slider_ngx_slider__WEBPACK_IMPORTED_MODULE_21__["NgxSliderModule"],
            ng_lazyload_image__WEBPACK_IMPORTED_MODULE_22__["LazyLoadImageModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
        _test_test_component__WEBPACK_IMPORTED_MODULE_10__["TestComponent"],
        _map_french_map_french_component__WEBPACK_IMPORTED_MODULE_15__["MapFrenchComponent"],
        _home_home_component__WEBPACK_IMPORTED_MODULE_16__["HomeComponent"],
        _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_17__["NotFoundComponent"],
        _avis_avis_component__WEBPACK_IMPORTED_MODULE_23__["AvisComponent"],
        _video_video_component__WEBPACK_IMPORTED_MODULE_24__["VideoComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
        _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
        _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
        _material_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModule"],
        ngx_countdown__WEBPACK_IMPORTED_MODULE_9__["CountdownModule"],
        _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_12__["FullCalendarModule"],
        _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_20__["NgSelectModule"],
        _angular_material_icon__WEBPACK_IMPORTED_MODULE_11__["MatIconModule"],
        _angular_slider_ngx_slider__WEBPACK_IMPORTED_MODULE_21__["NgxSliderModule"],
        ng_lazyload_image__WEBPACK_IMPORTED_MODULE_22__["LazyLoadImageModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"],
        args: [{
                declarations: [
                    _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
                    _test_test_component__WEBPACK_IMPORTED_MODULE_10__["TestComponent"],
                    _map_french_map_french_component__WEBPACK_IMPORTED_MODULE_15__["MapFrenchComponent"],
                    _home_home_component__WEBPACK_IMPORTED_MODULE_16__["HomeComponent"],
                    _not_found_not_found_component__WEBPACK_IMPORTED_MODULE_17__["NotFoundComponent"],
                    _avis_avis_component__WEBPACK_IMPORTED_MODULE_23__["AvisComponent"],
                    _video_video_component__WEBPACK_IMPORTED_MODULE_24__["VideoComponent"]
                ],
                imports: [
                    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                    _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
                    _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                    _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                    _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"],
                    _material_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModule"],
                    ngx_countdown__WEBPACK_IMPORTED_MODULE_9__["CountdownModule"],
                    _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_12__["FullCalendarModule"],
                    _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_20__["NgSelectModule"],
                    _angular_material_icon__WEBPACK_IMPORTED_MODULE_11__["MatIconModule"],
                    _angular_slider_ngx_slider__WEBPACK_IMPORTED_MODULE_21__["NgxSliderModule"],
                    ng_lazyload_image__WEBPACK_IMPORTED_MODULE_22__["LazyLoadImageModule"]
                ],
                providers: [
                    { provide: 'baseUrl', useValue: _baseUrl__WEBPACK_IMPORTED_MODULE_18__["baseUrl"] },
                    { provide: _angular_common__WEBPACK_IMPORTED_MODULE_19__["APP_BASE_HREF"], useValue: '/' },
                    {
                        provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HTTP_INTERCEPTORS"],
                        useClass: _security_token_interceptor_service__WEBPACK_IMPORTED_MODULE_8__["TokenInterceptorService"],
                        multi: true,
                    },
                    { provide: ng_lazyload_image__WEBPACK_IMPORTED_MODULE_22__["LAZYLOAD_IMAGE_HOOKS"], useClass: ng_lazyload_image__WEBPACK_IMPORTED_MODULE_22__["ScrollHooks"] }
                ],
                bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]],
                schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/avis/avis.component.ts":
/*!****************************************!*\
  !*** ./src/app/avis/avis.component.ts ***!
  \****************************************/
/*! exports provided: AvisComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AvisComponent", function() { return AvisComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var src_app_services_cpn_avis_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/cpn/avis.service */ "./src/app/services/cpn/avis.service.ts");






class AvisComponent {
    constructor(fb, avisSevice) {
        this.fb = fb;
        this.avisSevice = avisSevice;
        this.AvisForm = this.fb.group({
            email: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            username: ['',],
            phone: ['',],
            site: ['',],
            finance: ['',],
            buget: ['',],
            comment: ['',],
            proj: ['',],
            note: ['',],
        });
    }
    onSubmit() {
        console.log('avis', this.AvisForm.value);
        const formData = new FormData();
        formData.append('email', this.AvisForm.get('email').value);
        formData.append('username', this.AvisForm.get('username').value);
        formData.append('phone', this.AvisForm.get('phone').value);
        formData.append('site', this.AvisForm.get('site').value);
        formData.append('finance', this.AvisForm.get('finance').value);
        formData.append('buget', this.AvisForm.get('buget').value);
        formData.append('comment', this.AvisForm.get('comment').value);
        formData.append('proj', this.AvisForm.get('proj').value);
        formData.append('note', this.AvisForm.get('note').value);
        if (formData) {
            this.avisSevice.addAvis(formData).subscribe(res => {
                if (!res.error) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                        icon: 'success',
                        title: 'save reussie',
                        showConfirmButton: false,
                        timer: 2000
                    });
                    location.href = '/';
                }
                else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: res.message + ' !',
                    });
                }
            }, error => {
                console.log(error);
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'error 500 !',
                });
            });
        }
    }
    rating(val) {
        this.AvisForm.get('note').setValue(val);
    }
    ngOnInit() {
    }
}
AvisComponent.ɵfac = function AvisComponent_Factory(t) { return new (t || AvisComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_cpn_avis_service__WEBPACK_IMPORTED_MODULE_3__["AvisService"])); };
AvisComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AvisComponent, selectors: [["app-avis"]], decls: 132, vars: 1, consts: [[1, "container"], [3, "formGroup", "ngSubmit"], [1, "myCard"], [1, "row"], [1, "col-md-6"], [1, "myLeftCtn"], ["novalidate", "", 1, "myForm", "text-center", "needs-validation"], [1, "block"], [1, "circle"], ["src", "assets/cpnimages/logo/logo-cpn-blanc.png", "alt", "", 1, "logo1"], [1, "left"], [1, "col"], [1, "form-group"], [1, "fas", "fa-user"], ["type", "text", "formControlName", "username", "placeholder", "Username", "id", "username", "required", "", 1, "myInput"], [1, "invalid-feedback"], [1, "fas", "fa-envelope"], ["formControlName", "email", "placeholder", "Email", "type", "text", "id", "email", "required", "", 1, "myInput"], [1, "fas", "fa-phone"], ["type", "text", "formControlName", "phone", "id", "phone", "placeholder", "telephone", "required", "", 1, "myInput"], [1, "fas", "fa-link"], ["type", "text", "formControlName", "site", "id", "url", "placeholder", "lien site web", "required", "", 1, "myInput"], ["type", "submit", "value", "Envoyer", 1, "butt"], ["src", "assets/cpnimages/logo/logo-cpn-blanc.png", "alt", "", 1, "background"], [1, "myRightCtn"], [1, "form-check-label"], [1, "radio"], [1, "form-check"], ["type", "radio", "formControlName", "finance", "id", "finance1", "name", "finance", "value", "oui", "checked", "", 1, "form-check-input"], ["for", "finance1", 1, "form-check-label"], ["type", "radio", "formControlName", "finance", "id", "finance2", "name", "finance", "value", "non", 1, "form-check-input"], ["for", "finance2", 1, "form-check-label"], [1, "rate"], ["type", "radio", "id", "star1", "name", "rate", "value", "1", 3, "click"], ["for", "star1", "title", "text"], ["type", "radio", "id", "star2", "name", "rate", "value", "2", 3, "click"], ["for", "star2", "title", "text"], ["type", "radio", "id", "star3", "name", "rate", "value", "3", 3, "click"], ["for", "star3", "title", "text"], ["type", "radio", "id", "star4", "name", "rate", "value", "4", 3, "click"], ["for", "star4", "title", "text"], ["type", "radio", "id", "star5", "name", "rate", "value", "5", 3, "click"], ["for", "star5", "title", "text"], ["type", "radio", "id", "star6", "name", "rate", "value", "6", 3, "click"], ["for", "star6", "title", "text"], ["type", "radio", "formControlName", "buget", "id", "buget1", "name", "buget", "value", "oui", "checked", "", 1, "form-check-input"], ["for", "buget1", 1, "form-check-label"], ["type", "radio", "formControlName", "buget", "id", "buget2", "name", "buget", "value", "non", 1, "form-check-input"], ["for", "buget2", 1, "form-check-label"], ["formControlName", "comment", "type", "text", "id", "commentaire", "placeholder", "commentaire", "required", "", 1, "myInput"], [1, "form-group", "end"], ["type", "radio", "formControlName", "proj", "id", "proj1", "name", "proj", "value", "financ\u00E9 mon d\u00E9veloppement application", "checked", "", 1, "form-check-input"], ["for", "proj1", 1, "form-check-label"], ["type", "radio", "formControlName", "proj", "id", "proj2", "name", "proj", "value", "financ\u00E9 mon CRM -ERP d\u2019entreprise", 1, "form-check-input"], ["for", "proj2", 1, "form-check-label"], ["type", "radio", "formControlName", "proj", "id", "proj3", "name", "proj", "value", "financ\u00E9 ma communication r\u00E9seaux sociaux", 1, "form-check-input"], ["for", "proj3", 1, "form-check-label"], ["type", "radio", "formControlName", "proj", "id", "proj4", "name", "proj", "value", "financ\u00E9 un projet d\u2019innovation digitale", 1, "form-check-input"], ["for", "proj4", 1, "form-check-label"], ["type", "radio", "formControlName", "proj", "id", "proj5", "name", "proj", "value", "non", 1, "form-check-input"], ["for", "proj5", 1, "form-check-label"]], template: function AvisComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "form", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function AvisComponent_Template_form_ngSubmit_1_listener() { return ctx.onSubmit(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "img", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "header", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Identifi\u00E9 vous");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "i", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "input", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Please fill out this field.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](22, "i", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "input", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Please fill out this field.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "i", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "input", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Please fill out this field.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "i", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](36, "input", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "Please fill out this field.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](39, "input", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "img", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "header");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "Enqu\u00EAte de satisfaction");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "votre avis a un r\u00E9\u00E9l int\u00E9ret pour nous dans le cadre de l\u2019am\u00E9lioration de notre service");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "label", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, " Avez-vous b\u00E9n\u00E9ficier d\u2019un financement attribuer par le CPN ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](56, "input", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](57, "Oui ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](58, "label", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](60, "input", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "Non ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](62, "label", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "label", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, " Note sur l\u2019agent du CPN.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "input", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AvisComponent_Template_input_click_68_listener() { return ctx.rating(6); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "label", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](70, "1 stars");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "input", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AvisComponent_Template_input_click_71_listener() { return ctx.rating(5); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "label", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](73, "2 stars");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "input", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AvisComponent_Template_input_click_74_listener() { return ctx.rating(4); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "label", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](76, "3 stars");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "input", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AvisComponent_Template_input_click_77_listener() { return ctx.rating(3); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "label", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](79, "4 stars");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "input", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AvisComponent_Template_input_click_80_listener() { return ctx.rating(2); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "label", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "5 star");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "input", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AvisComponent_Template_input_click_83_listener() { return ctx.rating(1); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "label", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](85, "6 star");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "label", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](90, " Grace au financement le Reste a charge n\u2019a pas eu de cons\u00E9quence sur ma tr\u00E9sorerie il a \u00E9t\u00E9 adapt\u00E9 \u00E0 votre Budget ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](93, "input", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](94, "Oui ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](95, "label", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](97, "input", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](98, "Non ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](99, "label", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "label", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](104, "le Cabinet de propulsion Num\u00E9rique vous a t\u2019il apport\u00E9 une aide majeur pour r\u00E9alis\u00E9 votre projet digital en terme de financement, d\u2019accompagnement ou d\u2019expertise ?");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](105, "textarea", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "div", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](109, "label", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](110, " Avez-vous d\u2019autre projet num\u00E9rique \u00E0 financer au cours de l\u2019ann\u00E9e ? (si oui lequel)");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](113, "input", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](114, "financ\u00E9 mon d\u00E9veloppement application ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](115, "label", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](117, "input", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](118, "financ\u00E9 mon CRM -ERP d\u2019entreprise ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](119, "label", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](120, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](121, "input", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](122, "financ\u00E9 ma communication r\u00E9seaux sociaux. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](123, "label", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](125, "input", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](126, "financ\u00E9 un projet d\u2019innovation digitale. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](127, "label", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](128, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](129, "input", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](130, "Non ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](131, "label", 60);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.AvisForm);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RadioControlValueAccessor"]], styles: ["body[_ngcontent-%COMP%]\r\n{\r\n    background: #fbf3ff;\r\n}\r\n.logo[_ngcontent-%COMP%]{\r\n    width: 66px;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-end;\r\n    align-items: flex-end;\r\n    position: fixed;\r\n    top: 547px;\r\n    left: 549px;;\r\n}\r\n.form-check[_ngcontent-%COMP%] {\r\n    display: block;\r\n    min-height: 1.5rem;\r\n    padding-left: 2em;\r\n    margin-bottom: 0.125rem;\r\n}\r\n.background[_ngcontent-%COMP%]{\r\n    position: absolute;\r\n    width: 76%;\r\n    height: 65%;\r\n    z-index: 1;\r\n    opacity: 1px;\r\n    opacity: 0.13;\r\n    display: flex;\r\n    justify-content: center;\r\n    align-items: center;\r\n    flex-direction: column;\r\n    text-align: center;\r\n    left: 74px;\r\n    top: 97px;\r\n}\r\n.container[_ngcontent-%COMP%]\r\n{\r\n    position: absolute;\r\n    max-width: 800px;\r\n    height: 618px;\r\n    margin: auto;\r\n    top: 50%;\r\n    left: 35%;\r\n    transform: translate(-50%,-50%);\r\n}\r\n.myRightCtn[_ngcontent-%COMP%]\r\n{\r\n    position: relative;\r\n    background-image: linear-gradient(45deg, #6246ff, #111D5E );\r\n    border-radius: 25px;\r\n    height: 100%;\r\n    padding: 25px;\r\n    color: rgb(192, 192, 192);\r\n    font-size: 12px;\r\n    display: flex;\r\n  justify-content: center;\r\n  align-items: initial;\r\n\r\n}\r\n.logo1[_ngcontent-%COMP%]{\r\n    width: 55%;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nalign-items: center;\r\ntop: 25px;\r\nposition: absolute;\r\nleft: 20px;\r\n}\r\n.circle[_ngcontent-%COMP%]{\r\n    display: inline-block;\r\n    position: relative;\r\n    width: 80px;\r\n    height: 80px;\r\n    overflow: hidden;\r\n    border-radius: 50%;\r\n    background: #111D5E;\r\n    margin-right: 10px;\r\n}\r\n.myRightCtn[_ngcontent-%COMP%]   header[_ngcontent-%COMP%]\r\n{\r\n    color: #ffffff;\r\n    margin-bottom: 50px;\r\n    text-align: initial;\r\n    height: 86px;\r\n}\r\n.myRightCtn[_ngcontent-%COMP%]   header[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]\r\n{\r\n    color: #ffffff;\r\n   \r\n}\r\n.myLeftCtn[_ngcontent-%COMP%]\r\n{\r\n   position: relative;\r\n    background: #fff;\r\n    border-radius: 25px;\r\n    height: 100%;\r\n    padding: 25px;\r\n    padding-left: 50px;\r\n}\r\n.myLeftCtn[_ngcontent-%COMP%]   header[_ngcontent-%COMP%]\r\n{\r\n    color: #696969;\r\n    margin-bottom: 50px;\r\n    height: 94px;\r\n    text-align: initial;\r\n    display: flex;\r\n    flex-direction: column;\r\n    align-items: center;\r\n    justify-content: center;\r\n}\r\n.myLeftCtn[_ngcontent-%COMP%]   header[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]\r\n{\r\n    color: #111D5E;\r\n   \r\n}\r\n.row[_ngcontent-%COMP%]\r\n{\r\n    height: 100%;\r\n \r\n}\r\n.myCard[_ngcontent-%COMP%]\r\n{\r\n    position: relative;\r\n    background: #fff;\r\n    height: 100%;\r\n    border-radius: 25px;\r\n    box-shadow: 0px 10px 40px -10px rgba(0,0,0,0.7);\r\n    width: 159%;\r\n\r\n}\r\n.box[_ngcontent-%COMP%]\r\n{\r\n    position: relative;\r\n    margin: 20px;\r\n    margin-bottom: 100px;\r\n   \r\n}\r\n.end[_ngcontent-%COMP%]{\r\n   margin-bottom: 3rem;\r\n   text-align: left;\r\n   margin-left: 15px;\r\n}\r\n.block[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: initial; \r\n    margin-bottom: 25px;\r\n}\r\n.myLeftCtn[_ngcontent-%COMP%]   .myInput[_ngcontent-%COMP%]\r\n{\r\n    width: 236px;\r\n    border-radius: 25px;\r\n    padding: 10px;\r\n    padding-left: 50px;\r\n    border: none;\r\n    box-shadow: 0px 10px 49px -14px rgba(0,0,0,0.7);\r\n}\r\n.myLeftCtn[_ngcontent-%COMP%]   .myInput[_ngcontent-%COMP%]:focus\r\n{\r\n    outline: none;\r\n}\r\n.myForm[_ngcontent-%COMP%]\r\n{\r\n    position: relative;\r\n    margin-top: 5px;\r\n    height: 20%;\r\n    width: 100%;\r\n    z-index: 1;\r\n}\r\n.myRightCtn[_ngcontent-%COMP%]   .radio[_ngcontent-%COMP%]{\r\n    display: flex;\r\njustify-content: space-evenly;\r\nflex-direction: row;\r\n}\r\n.myRightCtn[_ngcontent-%COMP%]   .form-group[_ngcontent-%COMP%]{\r\n    margin-bottom: 1rem;\r\ntext-align: initial;\r\nwidth: -moz-fit-content;\r\nwidth: fit-content;\r\n}\r\n.myRightCtn[_ngcontent-%COMP%]   .myInput[_ngcontent-%COMP%]\r\n{\r\n    width: 100%;\r\n    border-radius: 25px;\r\n    padding: 10px;\r\n    padding-left: 50px;\r\n    border: none;\r\n    box-shadow: 0px 10px 49px -14px rgba(0,0,0,0.7);\r\n}\r\n.myRightCtn[_ngcontent-%COMP%]   .myInput[_ngcontent-%COMP%]:focus\r\n{\r\n    outline: none;\r\n}\r\n.myLeftCtn[_ngcontent-%COMP%]   .butt[_ngcontent-%COMP%]\r\n{\r\n    background:  linear-gradient(45deg, #fd3636, #e80000 );\r\n    color: #fff;\r\n    width: 230px;\r\n    border: none;\r\n    border-radius: 25px;\r\n    padding: 10px;\r\nbox-shadow: 0px 10px 41px -11px rgba(0,0,0,0.7);\r\n}\r\n.myLeftCtn[_ngcontent-%COMP%]   .butt[_ngcontent-%COMP%]:hover\r\n{\r\n    background:  linear-gradient(45deg, #ff5b5b, #ff2626 );\r\n\r\n}\r\n.myLeftCtn[_ngcontent-%COMP%]   .butt[_ngcontent-%COMP%]:focus\r\n{\r\n    outline: none;\r\n}\r\n.myRightCtn[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\r\n    margin-bottom: 8px;\r\n}\r\n.myLeftCtn[_ngcontent-%COMP%]   .fas[_ngcontent-%COMP%]\r\n{\r\n    position: relative;\r\n    color: #111D5E;\r\n    left: 36px;\r\n}\r\n.butt_out[_ngcontent-%COMP%]\r\n{\r\n    background:  transparent;\r\n    color: #fff;\r\n    width: 120px;\r\n    border: 2px solid#fff;\r\n    border-radius: 25px;\r\n    padding: 10px;\r\nbox-shadow: 0px 10px 49px -14px rgba(0,0,0,0.7);\r\n}\r\n.butt_out[_ngcontent-%COMP%]:hover\r\n{\r\n    border: 2px solid#eecbff;\r\n}\r\n.butt_out[_ngcontent-%COMP%]:focus\r\n{\r\n    outline: none;\r\n}\r\n.myRightCtn[_ngcontent-%COMP%]   .end[_ngcontent-%COMP%]\r\n{\r\n    margin-top: 26px;    \r\n}\r\n\r\n.rating[_ngcontent-%COMP%] {\r\n    position: relative;\r\n    width: 180px;\r\n    background: transparent;\r\n    display: flex;\r\n    justify-content: center;\r\n    align-items: center;\r\n    gap: .3em;\r\n    padding: 5px;\r\n    overflow: hidden;\r\n    border-radius: 20px;\r\n    box-shadow: 0 0 2px #b3acac;\r\n }\r\n.rating__result[_ngcontent-%COMP%] {\r\n    position: absolute;\r\n    top: 0;\r\n    left: 0;\r\n    transform: translateY(-10px) translateX(-5px);\r\n    z-index: -9;\r\n    font: 3em Arial, Helvetica, sans-serif;\r\n    color: #ebebeb8e;\r\n    pointer-events: none;\r\n }\r\n.rating__star[_ngcontent-%COMP%] {\r\n    font-size: 1.3em;\r\n    cursor: pointer;\r\n    color: #dabd18b2;\r\n    transition: filter linear .3s;\r\n }\r\n.rating__star[_ngcontent-%COMP%]:hover {\r\n    filter: drop-shadow(1px 1px 4px gold);\r\n }\r\n.starActive[_ngcontent-%COMP%]{\r\n    color: gold;\r\n    \r\n}\r\n\r\n.rate[_ngcontent-%COMP%] {\r\n    float: left;\r\n    height: 38px;\r\n    padding: 0 10px;\r\n    position: relative;\r\n    width: 180px;\r\n    background: transparent;\r\n    display: flex;\r\n    justify-content: center;\r\n    align-items: center;\r\n   \r\n    padding: 5px;\r\n      padding-bottom: 5px;\r\n      flex-flow: row-reverse;\r\n    overflow: hidden;\r\n    border-radius: 20px;\r\n    box-shadow: 0 0 2px #b3acac;\r\n    padding-bottom: 0px;\r\n}\r\n.rate[_ngcontent-%COMP%]:not(:checked)    > input[_ngcontent-%COMP%] {\r\n    position:absolute;\r\n    top:-9999px;\r\n}\r\n.rate[_ngcontent-%COMP%]:not(:checked)    > label[_ngcontent-%COMP%] {\r\n    float:right;\r\n    width:1em;\r\n    overflow:hidden;\r\n    white-space:nowrap;\r\n    cursor:pointer;\r\n    font-size:30px;\r\n    color:#ccc;\r\n}\r\n.rate[_ngcontent-%COMP%]:not(:checked)    > label[_ngcontent-%COMP%]:before {\r\n    content: '\u2605 ';\r\n}\r\n.rate[_ngcontent-%COMP%]    > input[_ngcontent-%COMP%]:checked    ~ label[_ngcontent-%COMP%] {\r\n    color: #ffc700;    \r\n}\r\n.rate[_ngcontent-%COMP%]:not(:checked)    > label[_ngcontent-%COMP%]:hover, .rate[_ngcontent-%COMP%]:not(:checked)    > label[_ngcontent-%COMP%]:hover    ~ label[_ngcontent-%COMP%] {\r\n    color: #deb217;  \r\n}\r\n.rate[_ngcontent-%COMP%]    > input[_ngcontent-%COMP%]:checked    + label[_ngcontent-%COMP%]:hover, .rate[_ngcontent-%COMP%]    > input[_ngcontent-%COMP%]:checked    + label[_ngcontent-%COMP%]:hover    ~ label[_ngcontent-%COMP%], .rate[_ngcontent-%COMP%]    > input[_ngcontent-%COMP%]:checked    ~ label[_ngcontent-%COMP%]:hover, .rate[_ngcontent-%COMP%]    > input[_ngcontent-%COMP%]:checked    ~ label[_ngcontent-%COMP%]:hover    ~ label[_ngcontent-%COMP%], .rate[_ngcontent-%COMP%]    > label[_ngcontent-%COMP%]:hover    ~ input[_ngcontent-%COMP%]:checked    ~ label[_ngcontent-%COMP%] {\r\n    color: #c59b08;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXZpcy9hdmlzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0lBRUksbUJBQW1CO0FBQ3ZCO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix5QkFBeUI7SUFDekIscUJBQXFCO0lBQ3JCLGVBQWU7SUFDZixVQUFVO0lBQ1YsV0FBVztBQUNmO0FBRUE7SUFDSSxjQUFjO0lBQ2Qsa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQix1QkFBdUI7QUFDM0I7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsV0FBVztJQUNYLFVBQVU7SUFDVixZQUFZO0lBQ1osYUFBYTtJQUNiLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLHNCQUFzQjtJQUN0QixrQkFBa0I7SUFDbEIsVUFBVTtJQUNWLFNBQVM7QUFDYjtBQUNBOztJQUVJLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsYUFBYTtJQUNiLFlBQVk7SUFDWixRQUFRO0lBQ1IsU0FBUztJQUNULCtCQUErQjtBQUNuQztBQUVBOztJQUVJLGtCQUFrQjtJQUNsQiwyREFBMkQ7SUFDM0QsbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixhQUFhO0lBQ2IseUJBQXlCO0lBQ3pCLGVBQWU7SUFDZixhQUFhO0VBQ2YsdUJBQXVCO0VBQ3ZCLG9CQUFvQjs7QUFFdEI7QUFDQTtJQUNJLFVBQVU7QUFDZCxhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkIsU0FBUztBQUNULGtCQUFrQjtBQUNsQixVQUFVO0FBQ1Y7QUFFQTtJQUNJLHFCQUFxQjtJQUNyQixrQkFBa0I7SUFDbEIsV0FBVztJQUNYLFlBQVk7SUFDWixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixrQkFBa0I7QUFDdEI7QUFFQTs7SUFFSSxjQUFjO0lBQ2QsbUJBQW1CO0lBQ25CLG1CQUFtQjtJQUNuQixZQUFZO0FBQ2hCO0FBRUE7O0lBRUksY0FBYzs7QUFFbEI7QUFFQTs7R0FFRyxrQkFBa0I7SUFDakIsZ0JBQWdCO0lBQ2hCLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osYUFBYTtJQUNiLGtCQUFrQjtBQUN0QjtBQUNBOztJQUVJLGNBQWM7SUFDZCxtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLG1CQUFtQjtJQUNuQix1QkFBdUI7QUFDM0I7QUFFQTs7SUFFSSxjQUFjOztBQUVsQjtBQUVBOztJQUVJLFlBQVk7O0FBRWhCO0FBRUE7O0lBRUksa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixZQUFZO0lBQ1osbUJBQW1CO0lBR25CLCtDQUErQztJQUMvQyxXQUFXOztBQUVmO0FBR0E7O0lBRUksa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixvQkFBb0I7O0FBRXhCO0FBRUE7R0FDRyxtQkFBbUI7R0FDbkIsZ0JBQWdCO0dBQ2hCLGlCQUFpQjtBQUNwQjtBQUVBO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQix3QkFBd0I7SUFDeEIsbUJBQW1CO0FBQ3ZCO0FBRUE7O0lBRUksWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2Isa0JBQWtCO0lBQ2xCLFlBQVk7SUFHWiwrQ0FBK0M7QUFDbkQ7QUFFQTs7SUFFSSxhQUFhO0FBQ2pCO0FBRUE7O0lBRUksa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixXQUFXO0lBQ1gsV0FBVztJQUNYLFVBQVU7QUFDZDtBQUNBO0lBQ0ksYUFBYTtBQUNqQiw2QkFBNkI7QUFDN0IsbUJBQW1CO0FBQ25CO0FBRUE7SUFDSSxtQkFBbUI7QUFDdkIsbUJBQW1CO0FBQ25CLHVCQUFrQjtBQUFsQixrQkFBa0I7QUFDbEI7QUFJQTs7SUFFSSxXQUFXO0lBQ1gsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixrQkFBa0I7SUFDbEIsWUFBWTtJQUdaLCtDQUErQztBQUNuRDtBQUVBOztJQUVJLGFBQWE7QUFDakI7QUFFQTs7SUFFSSxzREFBc0Q7SUFDdEQsV0FBVztJQUNYLFlBQVk7SUFDWixZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLGFBQWE7QUFHakIsK0NBQStDO0FBQy9DO0FBRUE7O0lBRUksc0RBQXNEOztBQUUxRDtBQUVBOztJQUVJLGFBQWE7QUFDakI7QUFFQTtJQUNJLGtCQUFrQjtBQUN0QjtBQUVBOztJQUVJLGtCQUFrQjtJQUNsQixjQUFjO0lBQ2QsVUFBVTtBQUNkO0FBRUE7O0lBRUksd0JBQXdCO0lBQ3hCLFdBQVc7SUFDWCxZQUFZO0lBQ1oscUJBQXFCO0lBQ3JCLG1CQUFtQjtJQUNuQixhQUFhO0FBR2pCLCtDQUErQztBQUMvQztBQUVBOztJQUVJLHdCQUF3QjtBQUM1QjtBQUdBOztJQUVJLGFBQWE7QUFDakI7QUFFQTs7SUFFSSxnQkFBZ0I7QUFDcEI7QUFDQSxvREFBb0Q7QUFFcEQ7SUFDSSxrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLHVCQUF1QjtJQUN2QixhQUFhO0lBQ2IsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixTQUFTO0lBQ1QsWUFBWTtJQUNaLGdCQUFnQjtJQUNoQixtQkFBbUI7SUFDbkIsMkJBQTJCO0NBQzlCO0FBRUE7SUFDRyxrQkFBa0I7SUFDbEIsTUFBTTtJQUNOLE9BQU87SUFDUCw2Q0FBNkM7SUFDN0MsV0FBVztJQUNYLHNDQUFzQztJQUN0QyxnQkFBZ0I7SUFDaEIsb0JBQW9CO0NBQ3ZCO0FBRUE7SUFDRyxnQkFBZ0I7SUFDaEIsZUFBZTtJQUNmLGdCQUFnQjtJQUNoQiw2QkFBNkI7Q0FDaEM7QUFFQTtJQUNHLHFDQUFxQztDQUN4QztBQUVEO0lBQ0ksV0FBVzs7QUFFZjtBQUVBLHNCQUFzQjtBQUV0QjtJQUNJLFdBQVc7SUFDWCxZQUFZO0lBQ1osZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osdUJBQXVCO0lBQ3ZCLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsbUJBQW1CO0dBQ3BCLGNBQWM7SUFDYixZQUFZO01BQ1YsbUJBQW1CO01BQ25CLHNCQUFzQjtJQUN4QixnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLDJCQUEyQjtJQUMzQixtQkFBbUI7QUFDdkI7QUFDQTtJQUNJLGlCQUFpQjtJQUNqQixXQUFXO0FBQ2Y7QUFDQTtJQUNJLFdBQVc7SUFDWCxTQUFTO0lBQ1QsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixjQUFjO0lBQ2QsY0FBYztJQUNkLFVBQVU7QUFDZDtBQUNBO0lBQ0ksYUFBYTtBQUNqQjtBQUNBO0lBQ0ksY0FBYztBQUNsQjtBQUNBOztJQUVJLGNBQWM7QUFDbEI7QUFDQTs7Ozs7SUFLSSxjQUFjO0FBQ2xCO0FBRUEsMkVBQTJFIiwiZmlsZSI6InNyYy9hcHAvYXZpcy9hdmlzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJib2R5XHJcbntcclxuICAgIGJhY2tncm91bmQ6ICNmYmYzZmY7XHJcbn1cclxuLmxvZ297XHJcbiAgICB3aWR0aDogNjZweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIHRvcDogNTQ3cHg7XHJcbiAgICBsZWZ0OiA1NDlweDs7XHJcbn1cclxuXHJcbi5mb3JtLWNoZWNrIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgbWluLWhlaWdodDogMS41cmVtO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAyZW07XHJcbiAgICBtYXJnaW4tYm90dG9tOiAwLjEyNXJlbTtcclxufVxyXG5cclxuLmJhY2tncm91bmR7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB3aWR0aDogNzYlO1xyXG4gICAgaGVpZ2h0OiA2NSU7XHJcbiAgICB6LWluZGV4OiAxO1xyXG4gICAgb3BhY2l0eTogMXB4O1xyXG4gICAgb3BhY2l0eTogMC4xMztcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbGVmdDogNzRweDtcclxuICAgIHRvcDogOTdweDtcclxufVxyXG4uY29udGFpbmVyXHJcbntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIG1heC13aWR0aDogODAwcHg7XHJcbiAgICBoZWlnaHQ6IDYxOHB4O1xyXG4gICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgdG9wOiA1MCU7XHJcbiAgICBsZWZ0OiAzNSU7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLC01MCUpO1xyXG59XHJcblxyXG4ubXlSaWdodEN0blxyXG57XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoNDVkZWcsICM2MjQ2ZmYsICMxMTFENUUgKTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBwYWRkaW5nOiAyNXB4O1xyXG4gICAgY29sb3I6IHJnYigxOTIsIDE5MiwgMTkyKTtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGluaXRpYWw7XHJcblxyXG59XHJcbi5sb2dvMXtcclxuICAgIHdpZHRoOiA1NSU7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG50b3A6IDI1cHg7XHJcbnBvc2l0aW9uOiBhYnNvbHV0ZTtcclxubGVmdDogMjBweDtcclxufVxyXG5cclxuLmNpcmNsZXtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHdpZHRoOiA4MHB4O1xyXG4gICAgaGVpZ2h0OiA4MHB4O1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIGJhY2tncm91bmQ6ICMxMTFENUU7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbn1cclxuXHJcbi5teVJpZ2h0Q3RuIGhlYWRlclxyXG57XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIG1hcmdpbi1ib3R0b206IDUwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBpbml0aWFsO1xyXG4gICAgaGVpZ2h0OiA4NnB4O1xyXG59XHJcblxyXG4ubXlSaWdodEN0biBoZWFkZXIgaDFcclxue1xyXG4gICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgIFxyXG59XHJcblxyXG4ubXlMZWZ0Q3RuXHJcbntcclxuICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBwYWRkaW5nOiAyNXB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiA1MHB4O1xyXG59XHJcbi5teUxlZnRDdG4gaGVhZGVyXHJcbntcclxuICAgIGNvbG9yOiAjNjk2OTY5O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNTBweDtcclxuICAgIGhlaWdodDogOTRweDtcclxuICAgIHRleHQtYWxpZ246IGluaXRpYWw7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5cclxuLm15TGVmdEN0biBoZWFkZXIgaDFcclxue1xyXG4gICAgY29sb3I6ICMxMTFENUU7XHJcbiAgIFxyXG59XHJcblxyXG4ucm93XHJcbntcclxuICAgIGhlaWdodDogMTAwJTtcclxuIFxyXG59XHJcblxyXG4ubXlDYXJkXHJcbntcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMTBweCA0MHB4IC0xMHB4IHJnYmEoMCwwLDAsMC43KTtcclxuICAgIC1tb3otYm94LXNoYWRvdzogMHB4IDEwcHggNDBweCAtMTBweCByZ2JhKDAsMCwwLDAuNyk7XHJcbiAgICBib3gtc2hhZG93OiAwcHggMTBweCA0MHB4IC0xMHB4IHJnYmEoMCwwLDAsMC43KTtcclxuICAgIHdpZHRoOiAxNTklO1xyXG5cclxufVxyXG5cclxuXHJcbi5ib3hcclxue1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgbWFyZ2luOiAyMHB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMTAwcHg7XHJcbiAgIFxyXG59XHJcblxyXG4uZW5ke1xyXG4gICBtYXJnaW4tYm90dG9tOiAzcmVtO1xyXG4gICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICBtYXJnaW4tbGVmdDogMTVweDtcclxufVxyXG5cclxuLmJsb2Nre1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGluaXRpYWw7IFxyXG4gICAgbWFyZ2luLWJvdHRvbTogMjVweDtcclxufVxyXG5cclxuLm15TGVmdEN0biAubXlJbnB1dFxyXG57XHJcbiAgICB3aWR0aDogMjM2cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIHBhZGRpbmctbGVmdDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDEwcHggNDlweCAtMTRweCByZ2JhKDAsMCwwLDAuNyk7XHJcbiAgICAtbW96LWJveC1zaGFkb3c6IDBweCAxMHB4IDQ5cHggLTE0cHggcmdiYSgwLDAsMCwwLjcpO1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDEwcHggNDlweCAtMTRweCByZ2JhKDAsMCwwLDAuNyk7XHJcbn1cclxuXHJcbi5teUxlZnRDdG4gLm15SW5wdXQ6Zm9jdXNcclxue1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxufVxyXG5cclxuLm15Rm9ybVxyXG57XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgei1pbmRleDogMTtcclxufVxyXG4ubXlSaWdodEN0biAucmFkaW97XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWV2ZW5seTtcclxuZmxleC1kaXJlY3Rpb246IHJvdztcclxufVxyXG5cclxuLm15UmlnaHRDdG4gLmZvcm0tZ3JvdXB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxcmVtO1xyXG50ZXh0LWFsaWduOiBpbml0aWFsO1xyXG53aWR0aDogZml0LWNvbnRlbnQ7XHJcbn1cclxuXHJcblxyXG5cclxuLm15UmlnaHRDdG4gLm15SW5wdXRcclxue1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIHBhZGRpbmctbGVmdDogNTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDEwcHggNDlweCAtMTRweCByZ2JhKDAsMCwwLDAuNyk7XHJcbiAgICAtbW96LWJveC1zaGFkb3c6IDBweCAxMHB4IDQ5cHggLTE0cHggcmdiYSgwLDAsMCwwLjcpO1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDEwcHggNDlweCAtMTRweCByZ2JhKDAsMCwwLDAuNyk7XHJcbn1cclxuXHJcbi5teVJpZ2h0Q3RuIC5teUlucHV0OmZvY3VzXHJcbntcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbn1cclxuXHJcbi5teUxlZnRDdG4gLmJ1dHRcclxue1xyXG4gICAgYmFja2dyb3VuZDogIGxpbmVhci1ncmFkaWVudCg0NWRlZywgI2ZkMzYzNiwgI2U4MDAwMCApO1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICB3aWR0aDogMjMwcHg7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIC13ZWJraXQtYm94LXNoYWRvdzogMHB4IDEwcHggNDFweCAtMTFweCByZ2JhKDAsMCwwLDAuNyk7XHJcbi1tb3otYm94LXNoYWRvdzogMHB4IDEwcHggNDFweCAtMTFweCByZ2JhKDAsMCwwLDAuNyk7XHJcbmJveC1zaGFkb3c6IDBweCAxMHB4IDQxcHggLTExcHggcmdiYSgwLDAsMCwwLjcpO1xyXG59XHJcblxyXG4ubXlMZWZ0Q3RuIC5idXR0OmhvdmVyXHJcbntcclxuICAgIGJhY2tncm91bmQ6ICBsaW5lYXItZ3JhZGllbnQoNDVkZWcsICNmZjViNWIsICNmZjI2MjYgKTtcclxuXHJcbn1cclxuXHJcbi5teUxlZnRDdG4gLmJ1dHQ6Zm9jdXNcclxue1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxufVxyXG5cclxuLm15UmlnaHRDdG4gbGFiZWx7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA4cHg7XHJcbn1cclxuXHJcbi5teUxlZnRDdG4gLmZhc1xyXG57XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBjb2xvcjogIzExMUQ1RTtcclxuICAgIGxlZnQ6IDM2cHg7XHJcbn1cclxuXHJcbi5idXR0X291dFxyXG57XHJcbiAgICBiYWNrZ3JvdW5kOiAgdHJhbnNwYXJlbnQ7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIHdpZHRoOiAxMjBweDtcclxuICAgIGJvcmRlcjogMnB4IHNvbGlkI2ZmZjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgLXdlYmtpdC1ib3gtc2hhZG93OiAwcHggMTBweCA0OXB4IC0xNHB4IHJnYmEoMCwwLDAsMC43KTtcclxuLW1vei1ib3gtc2hhZG93OiAwcHggMTBweCA0OXB4IC0xNHB4IHJnYmEoMCwwLDAsMC43KTtcclxuYm94LXNoYWRvdzogMHB4IDEwcHggNDlweCAtMTRweCByZ2JhKDAsMCwwLDAuNyk7XHJcbn1cclxuXHJcbi5idXR0X291dDpob3ZlclxyXG57XHJcbiAgICBib3JkZXI6IDJweCBzb2xpZCNlZWNiZmY7XHJcbn1cclxuXHJcblxyXG4uYnV0dF9vdXQ6Zm9jdXNcclxue1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxufVxyXG5cclxuLm15UmlnaHRDdG4gLmVuZFxyXG57XHJcbiAgICBtYXJnaW4tdG9wOiAyNnB4OyAgICBcclxufVxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKipyYXRpbmcqKioqKioqKioqKioqKioqL1xyXG5cclxuLnJhdGluZyB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB3aWR0aDogMTgwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBnYXA6IC4zZW07XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgIGJveC1zaGFkb3c6IDAgMCAycHggI2IzYWNhYztcclxuIH1cclxuIFxyXG4gLnJhdGluZ19fcmVzdWx0IHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTEwcHgpIHRyYW5zbGF0ZVgoLTVweCk7XHJcbiAgICB6LWluZGV4OiAtOTtcclxuICAgIGZvbnQ6IDNlbSBBcmlhbCwgSGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xyXG4gICAgY29sb3I6ICNlYmViZWI4ZTtcclxuICAgIHBvaW50ZXItZXZlbnRzOiBub25lO1xyXG4gfVxyXG4gXHJcbiAucmF0aW5nX19zdGFyIHtcclxuICAgIGZvbnQtc2l6ZTogMS4zZW07XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBjb2xvcjogI2RhYmQxOGIyO1xyXG4gICAgdHJhbnNpdGlvbjogZmlsdGVyIGxpbmVhciAuM3M7XHJcbiB9XHJcbiBcclxuIC5yYXRpbmdfX3N0YXI6aG92ZXIge1xyXG4gICAgZmlsdGVyOiBkcm9wLXNoYWRvdygxcHggMXB4IDRweCBnb2xkKTtcclxuIH1cclxuXHJcbi5zdGFyQWN0aXZle1xyXG4gICAgY29sb3I6IGdvbGQ7XHJcbiAgICBcclxufVxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKi9cclxuXHJcbi5yYXRlIHtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgaGVpZ2h0OiAzOHB4O1xyXG4gICAgcGFkZGluZzogMCAxMHB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgd2lkdGg6IDE4MHB4O1xyXG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAvKiBnYXA6IC4zZW07Ki9cclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcclxuICAgICAgZmxleC1mbG93OiByb3ctcmV2ZXJzZTtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgYm94LXNoYWRvdzogMCAwIDJweCAjYjNhY2FjO1xyXG4gICAgcGFkZGluZy1ib3R0b206IDBweDtcclxufVxyXG4ucmF0ZTpub3QoOmNoZWNrZWQpID4gaW5wdXQge1xyXG4gICAgcG9zaXRpb246YWJzb2x1dGU7XHJcbiAgICB0b3A6LTk5OTlweDtcclxufVxyXG4ucmF0ZTpub3QoOmNoZWNrZWQpID4gbGFiZWwge1xyXG4gICAgZmxvYXQ6cmlnaHQ7XHJcbiAgICB3aWR0aDoxZW07XHJcbiAgICBvdmVyZmxvdzpoaWRkZW47XHJcbiAgICB3aGl0ZS1zcGFjZTpub3dyYXA7XHJcbiAgICBjdXJzb3I6cG9pbnRlcjtcclxuICAgIGZvbnQtc2l6ZTozMHB4O1xyXG4gICAgY29sb3I6I2NjYztcclxufVxyXG4ucmF0ZTpub3QoOmNoZWNrZWQpID4gbGFiZWw6YmVmb3JlIHtcclxuICAgIGNvbnRlbnQ6ICfimIUgJztcclxufVxyXG4ucmF0ZSA+IGlucHV0OmNoZWNrZWQgfiBsYWJlbCB7XHJcbiAgICBjb2xvcjogI2ZmYzcwMDsgICAgXHJcbn1cclxuLnJhdGU6bm90KDpjaGVja2VkKSA+IGxhYmVsOmhvdmVyLFxyXG4ucmF0ZTpub3QoOmNoZWNrZWQpID4gbGFiZWw6aG92ZXIgfiBsYWJlbCB7XHJcbiAgICBjb2xvcjogI2RlYjIxNzsgIFxyXG59XHJcbi5yYXRlID4gaW5wdXQ6Y2hlY2tlZCArIGxhYmVsOmhvdmVyLFxyXG4ucmF0ZSA+IGlucHV0OmNoZWNrZWQgKyBsYWJlbDpob3ZlciB+IGxhYmVsLFxyXG4ucmF0ZSA+IGlucHV0OmNoZWNrZWQgfiBsYWJlbDpob3ZlcixcclxuLnJhdGUgPiBpbnB1dDpjaGVja2VkIH4gbGFiZWw6aG92ZXIgfiBsYWJlbCxcclxuLnJhdGUgPiBsYWJlbDpob3ZlciB+IGlucHV0OmNoZWNrZWQgfiBsYWJlbCB7XHJcbiAgICBjb2xvcjogI2M1OWIwODtcclxufVxyXG5cclxuLyogTW9kaWZpZWQgZnJvbTogaHR0cHM6Ly9naXRodWIuY29tL211a3Vsa2FudC9TdGFyLXJhdGluZy11c2luZy1wdXJlLWNzcyAqLyJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AvisComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-avis',
                templateUrl: './avis.component.html',
                styleUrls: ['./avis.component.css']
            }]
    }], function () { return [{ type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }, { type: src_app_services_cpn_avis_service__WEBPACK_IMPORTED_MODULE_3__["AvisService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/baseUrl.ts":
/*!****************************!*\
  !*** ./src/app/baseUrl.ts ***!
  \****************************/
/*! exports provided: baseUrl */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "baseUrl", function() { return baseUrl; });
const baseUrl = "https://www.cpn-aide-aux-entreprise.jobid.fr";


/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/token-storage.service */ "./src/app/services/token-storage.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/cpn/auth.service */ "./src/app/services/cpn/auth.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var ng_lazyload_image__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng-lazyload-image */ "./node_modules/ng-lazyload-image/__ivy_ngcc__/fesm2015/ng-lazyload-image.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");








function HomeComponent_a_8_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Acceuil");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/home");
} }
function HomeComponent_a_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Acceuil");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/Home_tpe_pme");
} }
function HomeComponent_a_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Acceuil");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/agence");
} }
function HomeComponent_a_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Acceuil");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/Home_collectivite");
} }
function HomeComponent_a_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Agenda +");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/agenda");
} }
function HomeComponent_a_14_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Agenda +");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/calendar");
} }
function HomeComponent_a_22_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, " Connexion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/Connexion");
} }
function HomeComponent_li_23_Template(rf, ctx) { if (rf & 1) {
    const _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "li", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 128);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "a", 129);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 130);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "a", 131);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "profile");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "a", 132);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function HomeComponent_li_23_Template_a_click_7_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9); const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r8.logout(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "d\u00E9connexion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r7.user.first_name, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/cpn/profile");
} }
class HomeComponent {
    constructor(tokenStorage, route, auth) {
        this.tokenStorage = tokenStorage;
        this.route = route;
        this.auth = auth;
        this.token = "";
        this.user = null;
        this.connect = false;
        this.role = "logout";
    }
    ngOnInit() {
        this.auth.getFellower().subscribe(res => {
            this.follow = res;
        });
        if (this.tokenStorage.getUser() != false) {
            this.token = this.tokenStorage.getUser();
            this.user = JSON.parse(this.token);
            this.role = this.user.role;
            this.connect = true;
        }
        this.serachbar();
        this.compteur();
    }
    serachbar() {
        $(document).mousemove(function (e) {
            $('#info-box').css('top', e.pageY - $('#info-box').height() - 30);
            $('#info-box').css('left', e.pageX - ($('#info-box').width()) / 2);
        }).mouseover();
        $('.search').mouseenter(function () {
            $(this).addClass('search--show');
            $(this).removeClass('search--hide');
        });
        $('.search').mouseleave(function () {
            $(this).addClass('search--hide');
            $(this).removeClass('search--show');
        });
    }
    compteur() {
        $(document).ready(function () {
            $('.item_num').counterUp({
                time: 2000
            });
        });
    }
    logout() {
        this.tokenStorage.signOut();
        this.connect = false;
        this.role = "logout";
        location.href = '/home';
    }
}
HomeComponent.ɵfac = function HomeComponent_Factory(t) { return new (t || HomeComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"])); };
HomeComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: HomeComponent, selectors: [["app-home"]], decls: 279, vars: 18, consts: [[1, "navbar", "navbar-expand-lg", "nav_g"], [1, "navbar-brand", 3, "routerLink"], ["src", "assets/cpnimages/logo/logo-cpn-blanc.png", "alt", "Logo", 1, "brand_logo", "d-inline-block", "align-text-top", "nav_img"], ["type", "button", "data-toggle", "collapse", "data-target", "#navbarSupportedContent", "aria-controls", "navbarSupportedContent", "aria-expanded", "false", "aria-label", "Toggle navigation", 1, "navbar-toggler", 2, "box-shadow", "none"], [1, "far", "fa-bars", "navbar-toggler-icon", 2, "color", "white", "z-index", "1"], ["id", "navbarSupportedContent", 1, "collapse", "navbar-collapse"], [1, "navbar-nav", "ml-auto", "topnav"], [1, "nav-item"], ["class", "nav-link", 3, "routerLink", 4, "ngIf"], [1, "nav-link", 3, "routerLink"], [1, "nav-item", 2, "margin-right", "5px"], [1, "nav-link", "con", 3, "routerLink"], ["class", "nav-link con", 3, "routerLink", 4, "ngIf"], ["class", "nav-item", 4, "ngIf"], [1, "primary_body", "mb-5"], [1, "home_container"], [1, "section_heading", "mb-3"], [1, "heading_wrapper", "container-fluid", "g-0"], [1, "row", "g-0"], [1, "col-md-8", "col-12"], [1, "row", "g-0", "justify-content-center"], [1, "col-auto"], [1, "img_wrapper", "content"], ["alt", "", 1, "heading_img", 3, "defaultImage", "lazyLoad"], [1, "col-md-4", "p-3", "block0"], [1, "row", "g-0", "justify-content-start"], [1, "col-md-auto", "transtion"], [1, "title_heading"], [1, "desc_heading"], [1, "search_bloc"], [1, "search"], ["placeholder", "Quel type de subvention souhaitez vous", 1, "search__input"], [1, "carre"], [1, "row"], [1, "col", 2, "display", "initial", "flex-direction", "row", "padding", "8px", "text-align", "start"], ["src", "assets/cpnimages/home/I.png", "alt", "", 2, "width", "10%", "margin", "0 0 0 20px"], [2, "margin-left", "4px", "font-size", "13px"], [2, "text-align", "center", "margin", "0 90px 0 0"], [2, "text-align", "center", "font-size", "12px", "margin", "0 0px 0 -45px"], [2, "color", "#00FF00"], [1, "our_success", "mb-3"], [1, "success_wrapper", "container", "px-4", "g-0"], [1, "col-md-6", "py-2", "title"], [1, "success_txt"], [1, "success_desc"], [1, "col-md-6", "py-2", "chiffre"], [1, "success_list"], [1, "success_item"], [1, "k"], [1, "item_num"], [1, "item_desc"], [1, "actuality", "mb-5"], [1, "float_actions"], [1, "actions_content"], [1, "action_items"], ["href", "cpn/Home_tpe_pme", 1, "item_href"], [1, "ihref_logo"], ["width", "40%", "src", "assets/cpnimages/sidebar/Entreprise.png", "alt", ""], [1, "ihref_text"], [1, "testmegi", 3, "routerLink"], ["href", "/cpn/agence", 1, "item_href"], ["width", "40%", "src", "assets/cpnimages/sidebar/Agence.png"], ["href", "/cpn/Home_collectivite", 1, "item_href"], ["width", "40%", "src", "assets/cpnimages/sidebar/Collectivit\u00E9.png"], [1, "card-blog", "mb-5"], [1, "card_wrapper", "container", "px-4", "g-0"], [1, "row", "py-3"], [1, "col-md-4"], [1, "card", "d-flex", "flex-column", "align-items-center", "justify-content-center", "block2"], ["src", "assets/cpnimages/home/agent.png", "alt", "...", 1, "card-img-top", 2, "height", "50%", "width", "50%"], [1, "card-body", "py-0", "d-flex", "flex-column", "justify-content-center", "align-items-center"], [1, "card-title", "home"], [1, "card-text", "home"], [1, "card-footer", "py-0"], [1, "test-btn1", "btn", "btn-light", 3, "routerLink"], ["src", "assets/cpnimages/home/casque.png", "alt", "...", 1, "card-img-top", 2, "height", "50%", "width", "50%"], [1, "card-text", "home", 2, "text-align", "center"], ["href", "#", 1, "test-btn2", "btn", "btn-light"], ["src", "assets/cpnimages/home/money.png", "alt", "...", 1, "card-img-top", 2, "height", "50%", "width", "50%"], ["href", "#", 1, "test-btn3", "btn", "btn-light"], [1, "text-bloc", "g-0", "mb-5", "block3"], [1, "container", "px-4"], [1, "text_body"], [1, "text_content"], [1, "divider", "g-0", "mb-5"], [1, "divider_ligne"], [1, "block4"], [1, "container", "px-4", "block1"], [1, "wavy"], [1, "outer-div", "one"], [1, "inner-div"], [1, "front"], [1, "front__face-photo1"], ["src", "assets/cpnimages/home/icone-2.png", "alt", "", 2, "width", "100%"], [1, "front__text"], [1, "front__text-header"], [1, "front__text-para"], [1, "outer-div", "two"], [1, "front__face-photo2"], ["src", "assets/cpnimages/home/icone-1.png", "alt", "", 2, "width", "100%"], [1, "outer-div", "three"], [1, "front__face-photo3"], ["src", "assets/cpnimages/home/icone-3.png", "alt", "", 2, "width", "100%"], [1, "third-bloc", "g-0", "mb-5"], [1, "container", "px-4", "text-center"], [2, "color", "#111d5e"], [1, "subvention-text", "text-center", 2, "color", "gray"], [1, "col-6", "block5"], ["src", "assets/cpnimages/home/old.PNG", "alt", "..."], [1, "col-6", "block6"], [1, "third-bloc-border"], [1, "block7"], [1, "container", "px-4", "lastB"], [1, "text-center"], [1, "row", "row-cols-1", "row-cols-md-3", "g-4", "justify-content-center"], [1, "col-md-3", "card1"], [1, "card", "h-100", "box1"], [1, "card-body"], [1, "card-text"], [1, "image"], ["src", "assets/cpnimages/home/avis3.png", "alt", " avis 1", 1, "img-fluid"], [1, "col-md-3", "card2"], [1, "card", "h-100", "box2"], ["src", "assets/cpnimages/home/avis1.png", "alt", " avis 2", 1, "img-fluid"], [1, "col-md-3", "card3"], [1, "card", "h-100", "box3"], [1, "card-text", 2, "margin-right", "-43px"], ["src", "assets/cpnimages/home/avis2.png", "alt", "avis 3", 1, "img-fluid"], [1, "dropdown"], ["role", "button", "id", "dropdownMenuLink", "data-toggle", "dropdown", "aria-haspopup", "true", "aria-expanded", "false", 1, "nav-link", "dropdown-toggle"], ["aria-labelledby", "dropdownMenuLink", 1, "dropdown-menu", "drop"], [1, "dropdown-item", 3, "routerLink"], [1, "dropdown-item", 3, "click"]], template: function HomeComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "nav", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "a", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "i", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "ul", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, HomeComponent_a_8_Template, 2, 1, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, HomeComponent_a_9_Template, 2, 1, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, HomeComponent_a_10_Template, 2, 1, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, HomeComponent_a_11_Template, 2, 1, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, HomeComponent_a_13_Template, 2, 1, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, HomeComponent_a_14_Template, 2, 1, "a", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "a", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "aide-aux-entreprises");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "li", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "a", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, " Financement");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "li", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](22, HomeComponent_a_22_Template, 2, 1, "a", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](23, HomeComponent_li_23_Template, 9, 2, "li", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "section", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "img", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "h2", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "Transition Num\u00E9rique");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "h4", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "2021");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "form", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](43, "input", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "img", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "span", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "Followers");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "h4", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "p", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "a", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "2.1%");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "vs last 7 days");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "section", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "div", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "h5", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "Notre succ\u00E9s");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "h3", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "Peut importe votre secteur d'acitivit\u00E9 nous vous offrons une subvention");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "div", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "ul", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "li", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "h3", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "span", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "562");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](70, "K");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "p", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](72, "Entreprises");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "li", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "h3", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "span", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](76, "10");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](77, "K");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "p", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](79, "Subventions");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "li", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "h3", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "span", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](83, "200");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](84, "K+ ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "p", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](86, "Adh\u00E9rants");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "section", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "div", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "ul", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "li", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "a", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "i", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](93, "img", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "p", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](95, "Entreprise");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "a", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](97, "Testez mon \u00E9gibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "li", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "a", 60);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "i", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](101, "img", 61);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "p", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](103, "Agence");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "a", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](105, "Testez mon \u00E9gibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "li", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "a", 62);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "i", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](109, "img", 63);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "p", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](111, "Collectivites");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "a", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](113, "Testez mon \u00E9gibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "section", 64);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](115, "div", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "div", 66);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "div", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](118, "div", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](119, "img", 69);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](120, "div", 70);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "h5", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](122, "Subvention imm\u00E9diate");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](123, "p", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](124, "Besoin d'un ch\u00E8que Num\u00E9rique");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](125, "div", 73);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](126, "a", 74);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](127, "Testez Votre \u00E9ligibilit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](128, "div", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](129, "div", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](130, "img", 75);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](131, "div", 70);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](132, "h5", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](133, "Accompagnement");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](134, "p", 76);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](135, "Des Experts \u00E0 votre disposition pour digitaliser votre entreprise");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](136, "div", 73);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](137, "a", 77);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](138, "En savoir plus");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](139, "div", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](140, "div", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](141, "img", 78);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](142, "div", 70);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](143, "h5", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](144, "Financement");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "p", 76);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](146, "Obtenez le imm\u00E9diatement sur votre devis ou facture");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](147, "div", 73);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](148, "a", 79);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](149, "En savoir plus");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](150, "section", 80);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](151, "div", 81);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](152, "ul", 82);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](153, "li", 83);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](154, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](155, " Conseils et Accompagnement");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](156, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](157, "Le CPN est un acteur majeur dans la transition digital des entreprises nous subventionnons et accompagnons tous type de projet de d\u00E9veloppement informatique nous vous orientons au-pr\u00E8s d\u2019agence de d\u00E9veloppement v\u00E9rifier et d\u00E9sign\u00E9.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](158, "section", 84);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](159, "div", 81);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](160, "span", 85);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](161, "section", 86);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](162, "div", 87);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](163, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](164, "Facilit\u00E9 & Rapidit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](165, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](166, " Tous les services du CPN Aide aux entreprises sont destin\u00E9e aux petites et moyennes entreprises et Start-up. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](167, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](168, "Nous faisons un suivie et donnons acc\u00E8s a notre r\u00E9seau de partenaires et d\u2019entreprise pour favoris\u00E9 leurs croissance. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](169, "div", 88);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](170, "div", 89);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](171, "div", 90);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](172, "div", 91);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](173, "div", 92);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](174, "img", 93);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](175, "div", 94);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](176, "h3", 95);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](177, "Agence");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](178, "p", 96);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](179, "Grace au Cpn Soyez accompagn\u00E9 au pr\u00E9s d'agence garentie et referenc\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](180, "div", 97);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](181, "div", 90);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](182, "div", 91);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](183, "div", 98);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](184, "img", 99);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](185, "div", 94);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](186, "h3", 95);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](187, "Transformation ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](188, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](189, " digitale");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](190, "p", 96);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](191, "Grace au Cpn Soyez accompagn\u00E9 au pr\u00E9s d'agence garentie et referenc\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](192, "div", 100);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](193, "div", 90);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](194, "div", 91);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](195, "div", 101);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](196, "img", 102);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](197, "div", 94);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](198, "h3", 95);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](199, "Financement ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](200, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](201, " Im\u00E9diat");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](202, "p", 96);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](203, "Grace au Cpn Soyez accompagn\u00E9 au pr\u00E9s d'agence garentie et referenc\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](204, "section", 103);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](205, "div", 104);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](206, "h1", 105);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](207, "Pour obtenir votre subvention");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](208, "p", 106);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](209, " Le CPN s'engage \u00E0 vous mettre en relation avec une agence. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](210, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](211, " Le cabinet vous permet d'obtenir une subvention sur votre ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](212, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](213, " d\u00E9veloppement informatique, et vous met a disposition \u00E9galement ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](214, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](215, " sont r\u00E9seaux d'entreprises et de partenaire international. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](216, "div", 107);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](217, "img", 108);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](218, "div", 109);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](219, "div", 110);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](220, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](221, "Economiser");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](222, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](223, "nos subventions sont calcul\u00E9es par rapport \u00E0 votre investissement quel que soit le poids de votre projet digital. Nos aides varient de 1000 \u00E0 10 000 euro de subvention imm\u00E9diate.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](224, "div", 110);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](225, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](226, "Gagner du temps");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](227, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](228, " Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](229, "div", 110);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](230, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](231, "Service de qualit\u00E9");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](232, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](233, "Tous nos conseillers d\u00E9tiennent un domaine d\u2019expertise qui leurs est propre et pourront vous accompagner dans la r\u00E9alisation de vos projets.");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](234, "section", 111);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](235, "div", 112);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](236, "div", 113);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](237, "h1", 105);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](238, "Avis D'entreprises");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](239, "p", 106);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](240, " Ils nous ont fait confiance , ont \u00E9t\u00E9 accompagn\u00E9s par le CPN et ont r\u00E9ussi \u00E0 avoir leur ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](241, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](242, " subventions ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](243, "div", 114);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](244, "div", 115);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](245, "div", 116);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](246, "div", 117);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](247, "p", 118);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](248, "\"J\u2019ai \u00E9t\u00E9 accompagn\u00E9 par le CPN et j\u2019ai eu ma subvention, excellent service.\"");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](249, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](250, "Justin Rhodes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](251, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](252, "Marketing officer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](253, "div", 119);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](254, "img", 120);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](255, "div", 121);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](256, "div", 122);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](257, "div", 117);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](258, "p", 118);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](259, "\"Rapidit\u00E9, efficacit\u00E9 et un tr\u00E8s bon service client, le CPN m\u2019a aid\u00E9 \u00E0 num\u00E9riser mon entreprise.\"");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](260, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](261, "Justin Rhodes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](262, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](263, "Marketing officer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](264, "div", 119);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](265, "img", 123);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](266, "div", 124);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](267, "div", 125);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](268, "div", 117);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](269, "p", 126);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](270, "\"Gr\u00E2ce \u00E0 la digitalisation de mon entreprise j\u2019ai pu augmenter mon chiffre d\u2019affaire, et c\u2019est gr\u00E2ce au CPN que j\u2019ai pu \u00EAtre bien accompagn\u00E9 et conseill\u00E9.\" ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](271, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](272, "Justin Rhodes");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](273, "h6");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](274, "Marketing officer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](275, "div", 119);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](276, "img", 127);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](277, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](278, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/home");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.role == "logout");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.role == "tpe");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.role == "age");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.role == "col");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.connect);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.connect);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/test");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "https://cpn-aide-aux-entreprises.com/?fbclid=IwAR1VWDNOUhRXSXxHooNwUGd-plDWM67gVj8DwCUk7zTbGxROqRq0FZDVYqk");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.connect);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.connect);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("defaultImage", "assets/cpnimages/home/33.png")("lazyLoad", "assets/cpnimages/home/33.png");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", ctx.follow, "K");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/test");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/test");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/test");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/test");
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterLinkWithHref"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgIf"], ng_lazyload_image__WEBPACK_IMPORTED_MODULE_5__["LazyLoadImageDirective"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgForm"]], styles: ["footer[_ngcontent-%COMP%]{\r\n  background: #111D5E !important;\r\n  color: white !important;\r\n\r\n}\r\nfooter[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]{\r\n  color: white;\r\n}\r\n.drop[_ngcontent-%COMP%]{\r\n  min-width: -moz-available;\r\n  margin-left: -70px;\r\n}\r\n.navbar-nav[_ngcontent-%COMP%]{\r\n  flex-direction: row;\r\n  justify-content: space-between;\r\n  display: flex;\r\n  font-size: 14px;\r\n  height: 40px\r\n  }\r\n.nav-link[_ngcontent-%COMP%]{\r\n      color: white !important;\r\n  }\r\n.navlinkwhit[_ngcontent-%COMP%]{\r\n      color: #111D5E !important;\r\n  }\r\n.con[_ngcontent-%COMP%]{\r\n      color: white !important;\r\n      border: none;\r\n      background: red;\r\n      border-radius: 25px;\r\n      width: 120px;\r\n      text-align: center;\r\n  height: 100%;\r\n  display: flex;\r\n  justify-content: center;\r\n  align-items: center;\r\n  }\r\n.topnav[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\r\n      border-bottom: 0.1px solid red;\r\n\r\n  }\r\n.nav_t[_ngcontent-%COMP%]{\r\n      color: white !important;\r\n  }\r\n.nav_g[_ngcontent-%COMP%]{\r\n      background-color: #111D5E;\r\n  }\r\n.navwhit[_ngcontent-%COMP%]{\r\n      background-color: #EBECF0;\r\n\r\n  }\r\n.nav_img[_ngcontent-%COMP%]{\r\n      width: 80px;\r\n      margin-bottom: 10px;\r\n  }\r\n.navbar-brand[_ngcontent-%COMP%] {\r\n      display: inline-block;\r\n      padding-top: .3125rem;\r\n      padding-bottom: .3125rem;\r\n      margin-right: 1rem;\r\n      font-size: 1.25rem;\r\n      line-height: inherit;\r\n      white-space: nowrap;\r\n      margin-left: 75px;\r\n      z-index: 5;\r\n  }\r\n\r\n.float_actions[_ngcontent-%COMP%] {\r\nposition: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 10px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 6%;\r\nz-index:5900;\r\n}\r\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\r\npadding: 0;\r\nmargin: 0 0 -30px 0;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: space-between;\r\n}\r\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%] {\r\npadding: 5px;\r\nwidth: 120px;\r\nheight: 120px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-content: center;\r\nposition: relative;\r\n}\r\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%] {\r\ntext-decoration: none;\r\ndisplay: block;\r\nflex-direction: column;\r\njustify-content: center;\r\nfont-size: 14px;\r\n}\r\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_logo[_ngcontent-%COMP%] {\r\nwidth: 100px;\r\nheight: 100px;\r\nmargin-left: 25px;\r\n}\r\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_text[_ngcontent-%COMP%] {\r\ntext-align: center;\r\nmargin: 0;\r\ncolor: white;\r\nmargin-top: 7px;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]::before{\r\n content: \">\";\r\n position: absolute;\r\n right: -10px;\r\n top: 15%;\r\n color: white;\r\n font-size: 20px;\r\n width: 40%;\r\n font-weight: bold;\r\n }\r\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .testmegi[_ngcontent-%COMP%]{\r\n  text-decoration: none;\r\n   position: absolute;\r\n   right: -180px;\r\n   top: 15%;\r\n   color: black;\r\n   font-size: 17px;\r\n   width: 40%;\r\n   background: white;\r\n   width: 180px;\r\n   border-radius: 25px;\r\n   text-align: center;\r\n   height: 40px;\r\n   display: none;\r\n   justify-content: center;\r\n   align-items: center;\r\n }\r\n.float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]:hover   .testmegi[_ngcontent-%COMP%]{\r\n display: flex;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%] {\r\n  text-decoration: none;\r\n  display: block;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  font-size: 14px;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_logo[_ngcontent-%COMP%] {\r\n  width: 100px;\r\n  height: 100px;\r\n  margin-left: 25px;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_text[_ngcontent-%COMP%] {\r\n  text-align: center;\r\n  margin: 0;\r\n  color: white;\r\n  margin-top: 7px;\r\n }\r\n\r\n\r\n.primary_body[_ngcontent-%COMP%]   .divider[_ngcontent-%COMP%]   .divider_ligne[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: 5px;\r\n  background: #111d5e;\r\n  display: block;\r\n  position: relative;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .divider[_ngcontent-%COMP%]   .divider_ligne[_ngcontent-%COMP%]::before {\r\n  content: \"\";\r\n  position: absolute;\r\n  width: 30%;\r\n  top: 0;\r\n  left: 0;\r\n  height: 5px;\r\n  background: red;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .divider[_ngcontent-%COMP%]   .divider_ligne[_ngcontent-%COMP%]::after {\r\n  content: \"\";\r\n  position: absolute;\r\n  width: 30%;\r\n  top: 0;\r\n  right: 0;\r\n  height: 5px;\r\n  background: red;\r\n }\r\n\r\n.content[_ngcontent-%COMP%]{\r\n  width: max-content;\r\n  }\r\n.transtion[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    position: absolute;\r\n    margin-left: -350px;\r\n    width: max-content;\r\n    flex-direction: column;\r\n    }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%] {\r\n  background:#111d5e;\r\n  min-height: -moz-fit-content;\r\n  min-height: fit-content;\r\n  border-bottom-right-radius: 100px;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\r\n  height: 1000px;\r\n  max-height: 640px;\r\n  margin-top: 1;\r\n  width: 1050px;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\r\n  font-size: 60px;\r\n  font-weight: 800;\r\n  color:  #ffffff;\r\n  width: max-content;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .desc_heading[_ngcontent-%COMP%] {\r\n  font-size: 60px;\r\n  font-weight: 700;\r\n  color:  #ffffff;\r\n  width: max-content;\r\n }\r\n.carre[_ngcontent-%COMP%] {\r\n  width: 200px;\r\n  height: 90px;\r\n  background: white;\r\n  border-radius: 18px;\r\n  margin-left: -290px;\r\n  margin-top: 385px;\r\n  }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   *[_ngcontent-%COMP%], .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   *[_ngcontent-%COMP%]:before, .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   *[_ngcontent-%COMP%]:after {\r\n    box-sizing: border-box;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   body[_ngcontent-%COMP%] {\r\n   background: #f5f5f5;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%] {\r\n   display: flex;\r\n   flex-direction: row;\r\n   justify-content: flex-start;\r\n   margin-left: unset;\r\n   width: 650px;\r\n   margin-left: 100px;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   main[_ngcontent-%COMP%] {\r\n    left: 50%;\r\n    position: absolute;\r\n    top: 50%;\r\n    transform: translateX(-50%) translateY(-50%);\r\n    width: 300px;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:before, .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:after {\r\n    content: \"\";\r\n    display: block;\r\n    position: absolute;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:before {\r\n    border: 5px solid #ffffff ;\r\n    border-radius: 20px;\r\n    height: 40px;\r\n    transition: all 0.3s ease-out;\r\n    transition-delay: 0.3s;\r\n    width: 40px;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%]:after {\r\n    background: #ffffff;\r\n    border-radius: 3px;\r\n    height: 5px;\r\n    transform: rotate(-45deg);\r\n    transform-origin: 0% 100%;\r\n    transition: all 0.3s ease-out;\r\n    width: 15px;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\r\n    background: transparent;\r\n    border: none;\r\n    border-radius: 20px;\r\n    display: block;\r\n    font-size: 20px;\r\n    height: 40px;\r\n    line-height: 40px;\r\n    opacity: 0;\r\n    outline: none;\r\n    padding: 0 15px;\r\n    position: relative;\r\n    transition: all 0.3s ease-out;\r\n    transition-delay: 0.6s;\r\n    width: 40px;\r\n    z-index: 1;\r\n    color: rgb(223, 223, 223);\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--hide[_ngcontent-%COMP%]:before {\r\n    transition-delay: 0.3s;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--hide[_ngcontent-%COMP%]:after {\r\n    transition-delay: 0.6s;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--hide[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\r\n    transition-delay: 0s;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:after {\r\n    transform: rotate(45deg) translateX(15px) translateY(-2px);\r\n    width: 0;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\r\n    border: 5px solid #ffffff;\r\n    border-radius: 20px;\r\n    height: 40px;\r\n    width: 500px;\r\n   }\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\r\n    opacity: 1;\r\n    width: 500px;\r\n   }\r\ninput[type=\"text\"][_ngcontent-%COMP%] {\r\n  height: 50px;\r\n  font-size: 30px;\r\n  display: inline-block;\r\n  \r\n  font-weight: 100;\r\n  border: none;\r\n  outline: none;\r\n  color: white;\r\n  padding: 3px;\r\n  padding-right: 60px;\r\n  width: 0px;\r\n  position: absolute;\r\n  top: 0;\r\n  right: 0;\r\n  background: none;\r\n  z-index: 3;\r\n  transition: width 0.4s cubic-bezier(0, 0.795, 0, 1);\r\n  cursor: pointer;\r\n  }\r\ninput[type=\"text\"][_ngcontent-%COMP%]:focus:hover {\r\n  border-bottom: 1px solid white;\r\n  }\r\ninput[type=\"text\"][_ngcontent-%COMP%]:focus {\r\n  width: 700px;\r\n  z-index: 1;\r\n  border-bottom: 1px solid white;\r\n  cursor: text;\r\n  }\r\ninput[type=\"submit\"][_ngcontent-%COMP%] {\r\n  height: 50px;\r\n  width: 50px;\r\n  display: inline-block;\r\n  color: white;\r\n  float: right;\r\n  background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAMAAABg3Am1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAADNQTFRFU1NT9fX1lJSUXl5e1dXVfn5+c3Nz6urqv7+/tLS0iYmJqampn5+fysrK39/faWlp////Vi4ZywAAABF0Uk5T/////////////////////wAlrZliAAABLklEQVR42rSWWRbDIAhFHeOUtN3/ags1zaA4cHrKZ8JFRHwoXkwTvwGP1Qo0bYObAPwiLmbNAHBWFBZlD9j0JxflDViIObNHG/Do8PRHTJk0TezAhv7qloK0JJEBh+F8+U/hopIELOWfiZUCDOZD1RADOQKA75oq4cvVkcT+OdHnqqpQCITWAjnWVgGQUWz12lJuGwGoaWgBKzRVBcCypgUkOAoWgBX/L0CmxN40u6xwcIJ1cOzWYDffp3axsQOyvdkXiH9FKRFwPRHYZUaXMgPLeiW7QhbDRciyLXJaKheCuLbiVoqx1DVRyH26yb0hsuoOFEPsoz+BVE0MRlZNjGZcRQyHYkmMp2hBTIzdkzCTc/pLqOnBrk7/yZdAOq/q5NPBH1f7x7fGP4C3AAMAQrhzX9zhcGsAAAAASUVORK5CYII=)\r\n    center center no-repeat;\r\n  text-indent: -10000px;\r\n  border: none;\r\n  position: absolute;\r\n  top: 0;\r\n  right: 0;\r\n  z-index: 2;\r\n  cursor: pointer;\r\n  opacity: 0.4;\r\n  cursor: pointer;\r\n  transition: opacity 0.4s ease;\r\n  }\r\ninput[type=\"submit\"][_ngcontent-%COMP%]:hover {\r\n  opacity: 0.8;\r\n  }\r\n\r\n.item_num[_ngcontent-%COMP%] {\r\n  margin: 0;\r\n  font-weight: 700;\r\n  color: #111d5e;\r\n}\r\n.success_item[_ngcontent-%COMP%] {\r\n  list-style: none;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: center;\r\n}\r\n.item_desc[_ngcontent-%COMP%] {\r\n  margin: 0;\r\n  color: #111d5e;\r\n}\r\n.success_list[_ngcontent-%COMP%] {\r\n  display: flex;\r\n  justify-content: space-between;\r\n  margin: 0;\r\n  padding: 0;\r\n}\r\n.success_txt[_ngcontent-%COMP%] {\r\n  text-transform: uppercase;\r\n  font-weight: 400;\r\n  font-size: 14px;\r\n  color: #111d5e;\r\n}\r\n.success_desc[_ngcontent-%COMP%] {\r\n  font-size: 28px;\r\n  color: #111d5e;\r\n}\r\n.container_box[_ngcontent-%COMP%] {\r\n  width: 100%;\r\n  height: auto;\r\n  display: flex;\r\n  flex-direction: row;\r\n\r\n}\r\n.k[_ngcontent-%COMP%]{\r\n  font-weight: bold;\r\n  margin: inherit;\r\n  }\r\n.chiffre[_ngcontent-%COMP%]{\r\n  display: flex;\r\n              flex-direction: column;\r\n              justify-content: center;\r\n              width: 40%;\r\n              align-items: inherit;\r\n  }\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_txt[_ngcontent-%COMP%] {\r\n  text-transform: uppercase;\r\n  font-weight: 400;\r\n  font-size: 14px;\r\n  color: #111d5e;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_desc[_ngcontent-%COMP%] {\r\n  font-size: 28px;\r\n  color: #111d5e;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_list[_ngcontent-%COMP%] {\r\n  display: flex;\r\n  justify-content: space-between;\r\n  margin: 0;\r\n  padding: 0;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_list[_ngcontent-%COMP%]   .success_item[_ngcontent-%COMP%] {\r\n  list-style: none;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: center;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_list[_ngcontent-%COMP%]   .success_item[_ngcontent-%COMP%]   .item_num[_ngcontent-%COMP%] {\r\n  margin: 0;\r\n  font-weight: 700;\r\n  color: #111d5e;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .success_list[_ngcontent-%COMP%]   .success_item[_ngcontent-%COMP%]   .item_desc[_ngcontent-%COMP%] {\r\n  margin: 0;\r\n  color: #111d5e;\r\n }\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%] {\r\n  background-color: #111d5e;\r\n  border-radius: 30px;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-body[_ngcontent-%COMP%] {\r\n  margin-bottom: 14px;\r\n  margin-top: -22px;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-body[_ngcontent-%COMP%]   .test-btn1[_ngcontent-%COMP%] {\r\n  border-radius: 20px;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-body[_ngcontent-%COMP%]   .test-btn2[_ngcontent-%COMP%] {\r\n  border: none;\r\n  background: red;\r\n  border-radius: 25px;\r\n  color: white;\r\n  padding: 8px 30px;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-body[_ngcontent-%COMP%]   .test-btn3[_ngcontent-%COMP%] {\r\n  border-radius: 20px;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-group[_ngcontent-%COMP%] {\r\n  text-align: center;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-group[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%] {\r\n  border: none;\r\n  position: relative;\r\n  display: flex;\r\n  flex-direction: column;\r\n  min-width: 0;\r\n  word-wrap: break-word;\r\n  background-color: #111d5e;\r\n  border-radius: 12.25rem;\r\n  color: white;\r\n }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-group[_ngcontent-%COMP%]   .card[_ngcontent-%COMP%]   .card-img-top[_ngcontent-%COMP%] {\r\n  width: 50%;\r\n  margin-left: 91px;\r\n }\r\n.block2[_ngcontent-%COMP%]{\r\n  background: transparent;\r\n   color: white;\r\n   padding: 19px;\r\n  }\r\n.block2[_ngcontent-%COMP%]:hover{\r\n   border: 5px solid white;\r\n  }\r\n.block2[_ngcontent-%COMP%]   .test-btn1[_ngcontent-%COMP%] {\r\n    border-radius: 25px;\r\n    margin-top: 30px;\r\n  }\r\n.block2[_ngcontent-%COMP%]   .test-btn2[_ngcontent-%COMP%] {\r\n  border-radius: 25px;\r\n  }\r\n.block2[_ngcontent-%COMP%]   .test-btn3[_ngcontent-%COMP%] {\r\n    border-radius: 25px;\r\n  }\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn1[_ngcontent-%COMP%] {\r\n    border: none;\r\n    margin-top: 30px;\r\n    color: white;\r\n    background-color:red; \r\n  }\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn2[_ngcontent-%COMP%] {\r\n    color: white;\r\n    border: none;\r\n    background-color:red; \r\n  }\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn3[_ngcontent-%COMP%] {\r\n    border: none;\r\n    color: white;\r\n    background-color:red; \r\n  }\r\n.home[_ngcontent-%COMP%]{\r\n    color: white;\r\n    font-size: 18px;\r\n    }\r\n\r\n.block3[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n  padding-right: 25rem;\r\n  width: 800px;\r\n}\r\n.text-bloc[_ngcontent-%COMP%]   .text_body[_ngcontent-%COMP%]{\r\n  margin-left: 15px;\r\n}\r\n.text-bloc[_ngcontent-%COMP%]   .text_body[_ngcontent-%COMP%]   .text_content[_ngcontent-%COMP%]{\r\n  font-size: 70px;\r\n  color:#111d5e\r\n}\r\n.text-bloc[_ngcontent-%COMP%]   .text_body[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  margin-top: -75px;\r\n  margin-left: -17px;\r\n  color:#111d5e\r\n}\r\n\r\n.block4[_ngcontent-%COMP%]{\r\n  background-image: url('sin.png');\r\n  background-repeat: no-repeat;\r\n  background-position:658px 234px;\r\n  margin-top:100px;\r\n}\r\n.block4[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n  color:#111d5e;\r\n  font-weight: bold;\r\n}\r\n.block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{padding-right: 48rem;}\r\n.outer-div[_ngcontent-%COMP%], .inner-div[_ngcontent-%COMP%] {\r\n  height: 378px;\r\n  max-width: 300px;\r\n  margin: 0 auto;\r\n  position: relative;\r\n}\r\n.outer-div[_ngcontent-%COMP%] {\r\n  perspective: 900px;\r\n  perspective-origin: 50% calc(50% - 18em);\r\n}\r\n.one[_ngcontent-%COMP%]{\r\nmargin: 11px 0px 0px 556px\r\n}\r\n.two[_ngcontent-%COMP%]{\r\nmargin: -580px 0 0 1065px\r\n}\r\n.three[_ngcontent-%COMP%]{\r\nmargin: -12px 0 0 1059px\r\n}\r\n.inner-div[_ngcontent-%COMP%] {\r\n  margin: 0 auto;\r\n  border-radius: 5px;\r\n  font-weight: 400;\r\n  color: black;\r\n  font-size: 1rem;\r\n  text-align: center;\r\n \r\n}\r\n.front[_ngcontent-%COMP%] {\r\n  cursor: pointer;\r\n  height: 85%;\r\n  background: white;\r\n  -webkit-backface-visibility: hidden;\r\n          backface-visibility: hidden;\r\n  box-shadow: 0 0 40px rgba(0, 0, 0, 0.1) inset;\r\n  box-shadow: 0px 1px 15px grey;\r\n  border-radius: 25px;\r\n  position: relative;\r\n  top: 0;\r\n  left: 0;\r\n}\r\n.front__face-photo1[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  top: 10px;\r\n  height: 120px;\r\n  width: 120px;\r\n  margin: 0 auto;\r\n  border-radius: 50%;\r\n\r\n  background-size: contain;\r\n  overflow: hidden;\r\n  \r\n}\r\n.front__face-photo2[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  top: 10px;\r\n  height: 120px;\r\n  width: 120px;\r\n  margin: 0 auto;\r\n  border-radius: 50%;\r\n  background-size: contain;\r\n  overflow: hidden;\r\n  \r\n}\r\n.front__face-photo3[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  top: 10px;\r\n  height: 120px;\r\n  width: 120px;\r\n  margin: 0 auto;\r\n  border-radius: 50%;\r\n\r\n  background-size: contain;\r\n  overflow: hidden;\r\n  \r\n}\r\n.front__text[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  top: 35px;\r\n  margin: 0 auto;\r\n  font-family: \"Montserrat\";\r\n  font-size: 18px;\r\n  -webkit-backface-visibility: hidden;\r\n          backface-visibility: hidden;\r\n}\r\n.front__text-header[_ngcontent-%COMP%] {\r\n  font-weight: 700;\r\n  font-family: \"Oswald\";\r\n  text-transform: uppercase;\r\n  font-size: 20px;\r\n}\r\n.front__text-para[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  top: -5px;\r\n  color: #000;\r\n  font-size: 14px;\r\n  letter-spacing: 0.4px;\r\n  font-weight: 400;\r\n  font-family: \"Montserrat\", sans-serif;\r\n}\r\n.front-icons[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  top: 0;\r\n  font-size: 14px;\r\n  margin-right: 6px;\r\n  color: gray;\r\n}\r\n.front__text-hover[_ngcontent-%COMP%] {\r\n  position: relative;\r\n  top: 10px;\r\n  font-size: 10px;\r\n  color: red;\r\n  -webkit-backface-visibility: hidden;\r\n          backface-visibility: hidden;\r\n\r\n  font-weight: 700;\r\n  text-transform: uppercase;\r\n  letter-spacing: .4px;\r\n\r\n  border: 2px solid red;\r\n  padding: 8px 15px;\r\n  border-radius: 30px;\r\n\r\n  background: red;\r\n  color: white;\r\n}\r\n\r\n.block5[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  margin-left: 190px;\r\n  height:80%;\r\n  width:80%\r\n}\r\n\r\n.block6[_ngcontent-%COMP%]{\r\n  padding-top: 30px; \r\n  padding-left: 110px;\r\n  margin-top: -501px; \r\n  float:right\r\n}\r\n.block6[_ngcontent-%COMP%]   .third-bloc-border[_ngcontent-%COMP%]{\r\n  border-radius: 20px;\r\n   border: 2px solid #C5C5C5;\r\n    margin-top: 20px; \r\n    width: 90%; \r\n  padding: 20px;\r\n}\r\n.block6[_ngcontent-%COMP%]   .third-bloc-border[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%]{\r\n  font-size: 26px;\r\n  font-weight: 700;\r\n}\r\n.block6[_ngcontent-%COMP%]   .third-bloc-border[_ngcontent-%COMP%]:hover{\r\nborder: 2px solid blue;\r\n}\r\n.block6[_ngcontent-%COMP%]   .third-bloc-border[_ngcontent-%COMP%]:hover   h3[_ngcontent-%COMP%]{\r\n  color: blue;  \r\n}\r\n\r\n.lastB[_ngcontent-%COMP%]{\r\n  width: 100%;\r\n \r\n}\r\n.block7[_ngcontent-%COMP%]{\r\n  padding-top: 100px\r\n}\r\n.card1[_ngcontent-%COMP%]{\r\n  margin-right: 50px;height: 345px;width: 315px;\r\n}\r\n.card1[_ngcontent-%COMP%]   .box1[_ngcontent-%COMP%]{\r\n  box-shadow: 0px 1px 15px grey;\r\n  border: none;\r\n  border-radius: 71px 14px 71px 14px;\r\n  background-color: #ffffff;\r\n  padding: 40px;\r\n}\r\n.card2[_ngcontent-%COMP%]{\r\n  margin-right: 50px;\r\n  height: 345px;\r\n  width: 315px;\r\n}\r\n.card2[_ngcontent-%COMP%]   .box2[_ngcontent-%COMP%]{\r\n  box-shadow: 0px 1px 15px grey;\r\n  border: none;\r\n  border-radius: 71px 14px 71px 14px;\r\n  background-color: #ffffff;\r\n  padding: 40px;\r\n}\r\n.card3[_ngcontent-%COMP%]{\r\n  margin-right: 50px;\r\n  height: 345px;\r\n  width: 315px;\r\n}\r\n.card3[_ngcontent-%COMP%]   .box3[_ngcontent-%COMP%]{\r\n  box-shadow: 0px 1px 15px grey;\r\n  border: none;\r\n  border-radius: 71px 14px 71px 14px;\r\n  background-color: #ffffff;\r\n  padding: 40px;\r\n}\r\n.card1[_ngcontent-%COMP%]   .box1[_ngcontent-%COMP%]   h4[_ngcontent-%COMP%]{\r\n  padding: 69px 0 0 26px;\r\n  font-size: 20px;\r\n  color:#00BFFF\r\n}\r\n.card1[_ngcontent-%COMP%]   .box1[_ngcontent-%COMP%]   h6[_ngcontent-%COMP%]{\r\n  padding: 0px 0 0px 54px;\r\n  font-size: 10px;\r\n  color:#c7c7c7\r\n}\r\n.card1[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  position: relative;\r\n  top: -132px;\r\n  left: -116px;\r\n  height: 100px;\r\n  width: 100px;\r\n  margin: 0 auto;\r\n  border-radius: 50%;\r\n  background-size: contain;\r\n  overflow: hidden;\r\n}\r\n.card1[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  width: 120%;\r\n  height: 120%\r\n}\r\n.card2[_ngcontent-%COMP%]   .box2[_ngcontent-%COMP%]   h4[_ngcontent-%COMP%]{\r\n  padding: 54px 0 0 26px;\r\n  font-size: 20px;\r\n   color:#00BFFF\r\n}\r\n.card2[_ngcontent-%COMP%]   .box2[_ngcontent-%COMP%]   h6[_ngcontent-%COMP%]{\r\n  padding: 0px 0 0px 54px;\r\n  font-size: 10px;\r\n  color:#c7c7c7\r\n}\r\n.card2[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  position: relative;\r\n   top: -132px;\r\n  left: -116px;\r\n  height: 100px;\r\n  width: 100px;\r\n  margin: 0 auto;\r\n  border-radius: 50%;\r\n   background-size: contain;\r\n   overflow: hidden;\r\n}\r\n.card2[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  width: 120%;\r\n  height: 120%\r\n}\r\n.card3[_ngcontent-%COMP%]   .box3[_ngcontent-%COMP%]   h4[_ngcontent-%COMP%]{\r\n  padding: 44px 0 0 26px;\r\n  font-size: 20px;\r\n   color:#00BFFF\r\n}\r\n.card3[_ngcontent-%COMP%]   .box3[_ngcontent-%COMP%]   h6[_ngcontent-%COMP%]{\r\n  padding: 0px 0 0px 54px;\r\n  font-size: 10px;\r\n  color:#c7c7c7\r\n}\r\n.card3[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  position: relative;\r\n  top: -132px;\r\n  left: -116px;\r\n  height: 100px;\r\n  width: 100px;\r\n  margin: 0 auto;\r\n  border-radius: 50%;\r\n  background-size: contain;\r\n  overflow: hidden;\r\n}\r\n.card3[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  width: 120%;\r\n  height: 120%\r\n}\r\n\r\nh1[_ngcontent-%COMP%]{\r\nfont-weight: bold;\r\ncolor: #111d5e;\r\nmargin: 50px 0 50px 0;\r\n}\r\nh5[_ngcontent-%COMP%]{\r\ncolor: #111d5e;\r\nfont-size: 15px;\r\n}\r\np[_ngcontent-%COMP%]{\r\nfont-size: 12px;\r\ncolor: #111d5e;\r\n\r\n}\r\n.footer[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n  font-size: 12px;\r\ncolor: #ffffff;\r\n}\r\n.copyright[_ngcontent-%COMP%]{\r\n  background-color: #0c133a;\r\n  color:#fff;\r\n  font-size:13px;\r\n  text-align: center;\r\n}\r\n\r\n\r\n@media only screen and (min-width : 320px) and (max-width : 480px)  {\r\n     \r\n  \r\n    .navbar-brand[_ngcontent-%COMP%] {\r\n        display: inline-block;\r\n        padding-top: .3125rem;\r\n        padding-bottom: .3125rem;\r\n        margin-right: 1rem;\r\n        font-size: 1.25rem;\r\n        line-height: inherit;\r\n        white-space: nowrap;\r\n        margin-left: 0;\r\n        z-index: 5;\r\n  }\r\n  \r\n  .drop[_ngcontent-%COMP%]{\r\n    min-width: -moz-available;\r\n    margin-left: 0px;\r\n  }\r\n  \r\n.navbar-nav[_ngcontent-%COMP%]{\r\n  flex-direction: column;\r\n  justify-content: space-between;\r\n  display: flex;\r\n  font-size: 14px;\r\n  height:max-content;\r\n  }\r\n  \r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]{\r\n  position: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 10px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 3%;\r\nheight: 5%;\r\nz-index:5900;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]::after{\r\n  content: \">\";\r\n  color: white;\r\n  position: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 0px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 5%;\r\nheight: 5%;\r\nfont-weight: bold;\r\nfont-size: 20px;\r\n\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover:after{\r\n  content: \">\";\r\n  color: white;\r\n  position: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 10px;\r\ndisplay: none;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 3%;\r\nheight: 5%;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\r\n  padding: 0;\r\n  margin: 0 0 -30px 0;\r\n  display: none;\r\n  flex-direction: column;\r\n  justify-content: space-between;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover{\r\n  position: fixed;\r\n  background: red;\r\n  border-radius: 10px;\r\n  left: 0;\r\n  top: 35%;\r\n  padding: 10px;\r\n  display: flex;\r\n  justify-content: center;\r\n  align-items: center;\r\n  width: 25%;\r\n  height: auto;\r\n  z-index: 99999;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover   .actions_content[_ngcontent-%COMP%] {\r\npadding: 0;\r\nmargin: 0 0 -30px 0;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: space-between;\r\n}\r\n    \r\n  .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]{\r\n    border-bottom-right-radius:0;\r\n  }\r\n  .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\r\n    border: 5px solid #111d5e;\r\n    border-radius: 20px;\r\n    height: 40px;\r\n    width: 300px;\r\n  }\r\n  .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\r\n    opacity: 1;\r\n    width: 300px;\r\n  }\r\n\r\n  .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\r\n    font-size: 40px;\r\n    font-weight: 800;\r\n    color: #ffffff;\r\n    width: max-content;\r\n    text-align: center;\r\n    margin-top: auto;\r\n    height: auto;\r\n }\r\n\r\n\r\n .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%] {\r\n  display: flex;\r\n  width: 300px;\r\n\r\n  flex-direction: row;\r\n  justify-content: center;\r\n  margin-left: unset;\r\n  width: -moz-available;\r\n  align-items: center;\r\n  text-align: center;\r\n}\r\n.transtion[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  width: -moz-available;\r\n  width: -moz-fit-content;\r\n  width: fit-content;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: center;\r\n  margin: 0;\r\n}\r\n.carre[_ngcontent-%COMP%] {\r\n  width: 200px;\r\n  height: 90px;\r\n  background: white;\r\n  border-radius: 18px;\r\n  margin-left: auto;\r\n  margin-top: 250px;\r\n  align-items: center;\r\n}\r\n.content[_ngcontent-%COMP%]{\r\n  width: -moz-fit-content;\r\n  width: fit-content;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\r\n  font-size: 30px;\r\n  font-weight: 800;\r\n  color: #ffffff;\r\n  width: -moz-available;\r\n  text-align: center;\r\n}\r\n\r\n\r\n\r\n\r\n\r\n\r\n.block2[_ngcontent-%COMP%]   .test-btn1[_ngcontent-%COMP%] {\r\n  border-radius: 25px;\r\n  margin-top: 30px;\r\n  border: 0;\r\n}\r\n.block2[_ngcontent-%COMP%]   .test-btn2[_ngcontent-%COMP%] {\r\nborder-radius: 25px;\r\nbackground-color: white;\r\ncolor: black;\r\nborder: 0;\r\n}\r\n.block2[_ngcontent-%COMP%]   .test-btn3[_ngcontent-%COMP%] {\r\n  border-radius: 25px;\r\n  border: 0;\r\n}\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn1[_ngcontent-%COMP%] {\r\n  border: none;\r\n  margin-top: 30px;\r\n  color: white;\r\n  background-color:red; \r\n}\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn2[_ngcontent-%COMP%] {\r\n  color: white;\r\n  border: none;\r\n  background-color:red; \r\n}\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn3[_ngcontent-%COMP%] {\r\n  border: none;\r\n  color: white;\r\n  background-color:red; \r\n}\r\n\r\n\r\n\r\n\r\n.chiffre[_ngcontent-%COMP%]{\r\n  display: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nwidth: 100%;\r\nalign-items: center;\r\n}\r\n\r\n.k[_ngcontent-%COMP%]{\r\n  margin: initial;\r\n}\r\n.success_item[_ngcontent-%COMP%]{\r\n  margin-left: 20px;\r\n  list-style: none;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: center;\r\n}\r\n \r\n .block3[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n  padding-right: 0;\r\n  width: 250px;\r\n  }\r\n\r\n  \r\n\r\n.block4[_ngcontent-%COMP%]{\r\n  background-image: url('sin.png');\r\n  background-repeat: no-repeat;\r\n  background-position:658px 234px;\r\n  margin-top:100px;\r\n  }\r\n  .block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\n  display: flex;\r\n  flex-direction: column;\r\n  align-items: flex-start;\r\n  }\r\n  .block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n  padding-right: 0;\r\n  }\r\n  \r\n   .one[_ngcontent-%COMP%]{\r\n  margin: 40px 0 0 0 ;\r\n  }\r\n   .two[_ngcontent-%COMP%]{\r\n  margin: 0;\r\n  }\r\n   .three[_ngcontent-%COMP%]{\r\n  margin: 0;\r\n  }\r\n  .outer-div[_ngcontent-%COMP%]{\r\n    display: contents;\r\n  }\r\n  \r\n  \r\n  .block5[_ngcontent-%COMP%]{\r\n  max-width: 100%;\r\n  width: 100%;\r\n  }\r\n  .block5[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  margin-left: 0;\r\n  height:100%;\r\n  width:100%\r\n  }\r\n  \r\n  .block6[_ngcontent-%COMP%]{\r\n  padding-top: 30px; \r\n  padding-left:0;\r\n  margin-top: 0; \r\n  max-width: 100%;\r\n  display: contents;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  align-items: center;\r\n  }\r\n  \r\n  .block6[_ngcontent-%COMP%]   .third-bloc-border[_ngcontent-%COMP%]{\r\n    margin: 20px auto 0 auto;\r\n  }\r\n  \r\n  \r\n \r\n .block7[_ngcontent-%COMP%]{\r\n  padding-top: 10px;\r\n}\r\n\r\n    .card1[_ngcontent-%COMP%]{\r\n      margin-right: 0px;\r\n      height: 345px;\r\n      width: 315px;\r\n      }\r\n      .card1[_ngcontent-%COMP%]   .box1[_ngcontent-%COMP%]{\r\n      box-shadow: 0px 1px 15px grey;\r\n      border: none;\r\n      border-radius: 71px 14px 71px 14px;\r\n      background-color: #ffffff;\r\n      padding: 40px;\r\n      }\r\n      .card2[_ngcontent-%COMP%]{\r\n        margin-right: 0px;\r\n        height: 345px;\r\n        width: 315px;\r\n      }\r\n      .card2[_ngcontent-%COMP%]   .box2[_ngcontent-%COMP%]{\r\n        box-shadow: 0px 1px 15px grey;\r\n        border: none;\r\n        border-radius: 71px 14px 71px 14px;\r\n        background-color: #ffffff;\r\n        padding: 40px;\r\n        }\r\n      .card3[_ngcontent-%COMP%]{\r\n        margin-right: 0px;\r\n        height: 345px;\r\n        width: 315px;\r\n      }\r\n      .card3[_ngcontent-%COMP%]   .box3[_ngcontent-%COMP%]{\r\n        box-shadow: 0px 1px 15px grey;\r\n        border: none;\r\n        border-radius: 71px 14px 71px 14px;\r\n        background-color: #ffffff;\r\n        padding: 40px;\r\n        }\r\n        \r\n      .lastB[_ngcontent-%COMP%]{\r\n      width: 80%;\r\n      margin: 50px;\r\n      }\r\n\r\n\r\n      .text-center[_ngcontent-%COMP%]{\r\n      margin-bottom: 50px;\r\n      }\r\n\r\n    }\r\n\r\n@media only screen and (min-width : 480px) and (max-width : 768px)  {\r\n            \r\n    \r\n  \r\n  .navbar-brand[_ngcontent-%COMP%] {\r\n    display: inline-block;\r\n    padding-top: .3125rem;\r\n    padding-bottom: .3125rem;\r\n    margin-right: 1rem;\r\n    font-size: 1.25rem;\r\n    line-height: inherit;\r\n    white-space: nowrap;\r\n    margin-left: 0;\r\n    z-index: 5;\r\n}\r\n.nav_img[_ngcontent-%COMP%] {\r\n  width: 80px;\r\n  margin-bottom: 10px;\r\n  margin-left: 38px;\r\n}\r\n\r\n.drop[_ngcontent-%COMP%]{\r\n  min-width: -moz-available;\r\n  margin-left: 0px;\r\n}\r\n.navbar-nav[_ngcontent-%COMP%]{\r\n  flex-direction: column;\r\n  justify-content: space-between;\r\n  display: flex;\r\n  font-size: 14px;\r\n  height:max-content;\r\n  }\r\n\r\n  \r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]{\r\nposition: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 10px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 3%;\r\nheight: 5%;\r\nz-index:5900;\r\n\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]::after{\r\ncontent: \">\";\r\ncolor: white;\r\nposition: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 0px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 5%;\r\nheight: 5%;\r\nfont-weight: bold;\r\nfont-size: 20px;\r\n\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover:after{\r\ncontent: \">\";\r\ncolor: white;\r\nposition: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 10px;\r\ndisplay: none;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 3%;\r\nheight: 5%;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\r\npadding: 0;\r\nmargin: 0 0 -30px 0;\r\ndisplay: none;\r\nflex-direction: column;\r\njustify-content: space-between;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover{\r\nposition: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 10px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 18%;\r\nheight: auto;\r\nz-index: 99999;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover   .actions_content[_ngcontent-%COMP%] {\r\npadding: 0;\r\nmargin: 0 0 -30px 0;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: space-between;\r\n}\r\n  \r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]{\r\nborder-bottom-right-radius:0;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\r\nborder: 5px solid #111d5e;\r\nborder-radius: 20px;\r\nheight: 40px;\r\nwidth: 300px;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\r\nopacity: 1;\r\nwidth: 300px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%] {\r\n  width: auto;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\r\n  height: 100%;\r\n  max-height: 585px;\r\n  margin-top: 0;\r\n  width: 750px;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%] {\r\ndisplay: flex;\r\nflex-direction: row;\r\njustify-content: center;\r\nmargin-left: unset;\r\nwidth: -moz-available;\r\nalign-items: center;\r\ntext-align: center;\r\n}\r\n.transtion[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nwidth: -moz-available;\r\nflex-direction: column;\r\njustify-content: center;\r\nalign-items: center;\r\nmargin: 0;\r\n}\r\n.carre[_ngcontent-%COMP%] {\r\nwidth: 200px;\r\nheight: 90px;\r\nbackground: white;\r\nborder-radius: 18px;\r\nmargin-left: auto;\r\nmargin-top: 250px;\r\nalign-items: center;\r\n}\r\n.content[_ngcontent-%COMP%]{\r\nwidth: -moz-fit-content;\r\nwidth: fit-content;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\r\nfont-size: 30px;\r\nfont-weight: 800;\r\ncolor: #ffffff;\r\nwidth: -moz-available;\r\ntext-align: center;\r\n}\r\n\r\n\r\n\r\n\r\n\r\n.block2[_ngcontent-%COMP%]   .test-btn1[_ngcontent-%COMP%] {\r\nborder-radius: 25px;\r\nmargin-top: 30px;\r\nborder: 0;\r\n}\r\n.block2[_ngcontent-%COMP%]   .test-btn2[_ngcontent-%COMP%] {\r\nborder-radius: 25px;\r\nbackground-color: white;\r\ncolor: black;\r\nborder: 0;\r\n}\r\n.block2[_ngcontent-%COMP%]   .test-btn3[_ngcontent-%COMP%] {\r\nborder-radius: 25px;\r\nborder: 0;\r\n}\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn1[_ngcontent-%COMP%] {\r\nborder: none;\r\nmargin-top: 30px;\r\ncolor: white;\r\nbackground-color:red; \r\n}\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn2[_ngcontent-%COMP%] {\r\ncolor: white;\r\nborder: none;\r\nbackground-color:red; \r\n}\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn3[_ngcontent-%COMP%] {\r\nborder: none;\r\ncolor: white;\r\nbackground-color:red; \r\n}\r\n\r\n\r\n\r\n\r\n.chiffre[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nwidth: 100%;\r\nalign-items: center;\r\n}\r\n\r\n.k[_ngcontent-%COMP%]{\r\nmargin: initial;\r\n}\r\n.success_item[_ngcontent-%COMP%]{\r\nmargin-left: 20px;\r\nlist-style: none;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nalign-items: center;\r\n}\r\n\r\n.block3[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\npadding-right: 0;\r\nwidth: 250px;\r\n}\r\n\r\n\r\n\r\n.block4[_ngcontent-%COMP%]{\r\nbackground-image: url('sin.png');\r\nbackground-repeat: no-repeat;\r\nbackground-position:0px 576px;\r\nmargin-top:100px;\r\n}\r\n.block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nflex-direction: column;\r\nalign-items: flex-start;\r\n}\r\n.block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\npadding-right: 0;\r\n}\r\n\r\n.one[_ngcontent-%COMP%]{\r\nmargin: 40px 0 0 0 ;\r\n}\r\n.two[_ngcontent-%COMP%]{\r\nmargin: 0;\r\n}\r\n.three[_ngcontent-%COMP%]{\r\nmargin: 0;\r\n}\r\n.outer-div[_ngcontent-%COMP%]{\r\ndisplay: contents;\r\n}\r\n\r\n\r\n.block5[_ngcontent-%COMP%]{\r\nmax-width: 100%;\r\nwidth: 100%;\r\n}\r\n.block5[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\nmargin-left: 0;\r\nheight:100%;\r\nwidth:100%\r\n}\r\n\r\n.block6[_ngcontent-%COMP%]{\r\npadding-top: 30px; \r\npadding-left:0;\r\nmargin-top: 0; \r\nmax-width: 100%;\r\ndisplay: contents;\r\nflex-direction: column;\r\njustify-content: center;\r\nalign-items: center;\r\n}\r\n\r\n.block6[_ngcontent-%COMP%]   .third-bloc-border[_ngcontent-%COMP%]{\r\nmargin: 20px auto 0 auto;\r\n}\r\n\r\n\r\n\r\n.block7[_ngcontent-%COMP%]{\r\npadding-top: 10px;\r\n}\r\n\r\n.card1[_ngcontent-%COMP%]{\r\n  margin-right: 0px;\r\n  height: 345px;\r\n  width: 315px;\r\n  }\r\n  .card1[_ngcontent-%COMP%]   .box1[_ngcontent-%COMP%]{\r\n  box-shadow: 0px 1px 15px grey;\r\n  border: none;\r\n  border-radius: 71px 14px 71px 14px;\r\n  background-color: #ffffff;\r\n  padding: 40px;\r\n  }\r\n  .card2[_ngcontent-%COMP%]{\r\n    margin-right: 0px;\r\n    height: 345px;\r\n    width: 315px;\r\n  }\r\n  .card2[_ngcontent-%COMP%]   .box2[_ngcontent-%COMP%]{\r\n    box-shadow: 0px 1px 15px grey;\r\n    border: none;\r\n    border-radius: 71px 14px 71px 14px;\r\n    background-color: #ffffff;\r\n    padding: 40px;\r\n    }\r\n  .card3[_ngcontent-%COMP%]{\r\n    margin-right: 0px;\r\n    height: 345px;\r\n    width: 315px;\r\n  }\r\n  .card3[_ngcontent-%COMP%]   .box3[_ngcontent-%COMP%]{\r\n    box-shadow: 0px 1px 15px grey;\r\n    border: none;\r\n    border-radius: 71px 14px 71px 14px;\r\n    background-color: #ffffff;\r\n    padding: 40px;\r\n    }\r\n    \r\n  .lastB[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    margin: none;\r\n  }\r\n\r\n\r\n  .text-center[_ngcontent-%COMP%]{\r\n  margin-bottom: 50px;\r\n  }\r\n\r\n\r\n    }\r\n\r\n@media only screen and (min-width : 768px) and (max-width : 992px)  {\r\n    \r\n       \r\n  .navbar-brand[_ngcontent-%COMP%] {\r\n    display: inline-block;\r\n    padding-top: .3125rem;\r\n    padding-bottom: .3125rem;\r\n    margin-right: 1rem;\r\n    font-size: 1.25rem;\r\n    line-height: inherit;\r\n    white-space: nowrap;\r\n    margin-left: 0;\r\n    z-index: 5;\r\n}\r\n.nav_img[_ngcontent-%COMP%] {\r\n  width: 80px;\r\n  margin-bottom: 10px;\r\n  margin-left: 38px;\r\n}\r\n.navbar-nav[_ngcontent-%COMP%]{\r\n  flex-direction: column;\r\n  justify-content: space-between;\r\n  display: flex;\r\n  font-size: 14px;\r\n  height:max-content;\r\n\r\n  }\r\n.drop[_ngcontent-%COMP%]{\r\n  min-width: -moz-available;\r\n  margin-left: 0px;\r\n}\r\n  \r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]{\r\nposition: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 10px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 3%;\r\nheight: 5%;\r\nz-index:5900;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]::after{\r\ncontent: \">\";\r\ncolor: white;\r\nposition: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 0px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 5%;\r\nheight: 5%;\r\nfont-weight: bold;\r\nfont-size: 20px;\r\n\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover:after{\r\ncontent: \">\";\r\ncolor: white;\r\nposition: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 10px;\r\ndisplay: none;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 3%;\r\nheight: 5%;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\r\npadding: 0;\r\nmargin: 0 0 -30px 0;\r\ndisplay: none;\r\nflex-direction: column;\r\njustify-content: space-between;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover{\r\nposition: fixed;\r\nbackground: red;\r\nborder-radius: 10px;\r\nleft: 0;\r\ntop: 35%;\r\npadding: 10px;\r\ndisplay: flex;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 12%;\r\nheight:auto;\r\nz-index: 99999;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]:hover   .actions_content[_ngcontent-%COMP%] {\r\npadding: 0;\r\nmargin: 0 0 -30px 0;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: space-between;\r\n}\r\n  \r\n.block0[_ngcontent-%COMP%]{\r\n  flex: 0 0 auto;\r\nwidth: -moz-available;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]{\r\nborder-bottom-right-radius:0;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\r\nborder: 5px solid #111d5e;\r\nborder-radius: 20px;\r\nheight: 40px;\r\nwidth: 300px;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\r\nopacity: 1;\r\nwidth: 300px;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%] {\r\n  width: auto;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\r\n  height: 100%;\r\n  max-height: 585px;\r\n  margin-top: 0;\r\n  width: 750px;\r\n}\r\n.primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%] {\r\ndisplay: flex;\r\nflex-direction: row;\r\njustify-content: center;\r\nmargin-left: unset;\r\nwidth: -moz-available;\r\nalign-items: center;\r\ntext-align: center;\r\n}\r\n.transtion[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nwidth: -moz-available;\r\nflex-direction: column;\r\njustify-content: center;\r\nalign-items: center;\r\nmargin: 0;\r\n}\r\n.carre[_ngcontent-%COMP%] {\r\nwidth: 200px;\r\nheight: 90px;\r\nbackground: white;\r\nborder-radius: 18px;\r\nmargin-left: auto;\r\nmargin-top: 250px;\r\nalign-items: center;\r\n}\r\n.content[_ngcontent-%COMP%]{\r\nwidth: -moz-fit-content;\r\nwidth: fit-content;\r\n}\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\r\nfont-size: 30px;\r\nfont-weight: 800;\r\ncolor: #ffffff;\r\nwidth: -moz-available;\r\ntext-align: center;\r\n}\r\n\r\n\r\n\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .card-blog[_ngcontent-%COMP%]   .card_wrapper[_ngcontent-%COMP%]   .row[_ngcontent-%COMP%]   .card-body[_ngcontent-%COMP%] {\r\n  margin-bottom: 14px;\r\n  margin-top: -22px;\r\n  text-align: center;\r\n }\r\n\r\n.block2[_ngcontent-%COMP%]   .test-btn1[_ngcontent-%COMP%] {\r\nborder-radius: 25px;\r\nmargin-top: 0px;\r\nborder: 0;\r\n}\r\n.block2[_ngcontent-%COMP%]   .test-btn2[_ngcontent-%COMP%] {\r\nborder-radius: 25px;\r\nbackground-color: white;\r\ncolor: black;\r\nborder: 0;\r\n}\r\n.block2[_ngcontent-%COMP%]   .test-btn3[_ngcontent-%COMP%] {\r\nborder-radius: 25px;\r\nborder: 0;\r\n}\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn1[_ngcontent-%COMP%] {\r\nborder: none;\r\nmargin-top: 30px;\r\ncolor: white;\r\nbackground-color:red; \r\n}\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn2[_ngcontent-%COMP%] {\r\ncolor: white;\r\nborder: none;\r\nbackground-color:red; \r\n}\r\n.block2[_ngcontent-%COMP%]:hover   .test-btn3[_ngcontent-%COMP%] {\r\nborder: none;\r\ncolor: white;\r\nbackground-color:red; \r\n}\r\n\r\n\r\n\r\n\r\n.chiffre[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nwidth: 100%;\r\n}\r\n\r\n.k[_ngcontent-%COMP%]{\r\nmargin: initial;\r\n}\r\n.success_item[_ngcontent-%COMP%]{\r\nmargin-left: 20px;\r\nlist-style: none;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nalign-items: center;\r\n}\r\n.our_success[_ngcontent-%COMP%]   .success_wrapper[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%]{\r\n  width: -moz-available;\r\n}\r\n\r\n.block3[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\npadding-right: 0;\r\nwidth: -moz-available;\r\n}\r\n\r\n\r\n\r\n.block4[_ngcontent-%COMP%]{\r\nbackground-image: url('sin.png');\r\nbackground-repeat: no-repeat;\r\nbackground-position:658px 234px;\r\nmargin-top:0px;\r\n}\r\n.block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nflex-direction: column;\r\nalign-items: flex-start;\r\n}\r\n.block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\npadding-right: 0px;\r\n}\r\n\r\n.one[_ngcontent-%COMP%]{\r\nmargin: 40px 0 0 0 ;\r\n}\r\n.two[_ngcontent-%COMP%]{\r\nmargin: 0;\r\n}\r\n.three[_ngcontent-%COMP%]{\r\nmargin: 0;\r\n}\r\n.outer-div[_ngcontent-%COMP%]{\r\ndisplay: contents;\r\n}\r\n\r\n\r\n.block5[_ngcontent-%COMP%]{\r\nmax-width: 100%;\r\nwidth: 100%;\r\n}\r\n.block5[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\nmargin-left: 0;\r\nheight:100%;\r\nwidth:100%\r\n}\r\n\r\n.block6[_ngcontent-%COMP%]{\r\npadding-top: 30px; \r\npadding-left:0;\r\nmargin-top: 0; \r\nmax-width: 100%;\r\ndisplay: contents;\r\nflex-direction: column;\r\njustify-content: center;\r\nalign-items: center;\r\n}\r\n\r\n.block6[_ngcontent-%COMP%]   .third-bloc-border[_ngcontent-%COMP%]{\r\nmargin: 20px auto 0 auto;\r\n}\r\n\r\n\r\n\r\n.block7[_ngcontent-%COMP%]{\r\npadding-top: 10px;\r\n}\r\n\r\n.card1[_ngcontent-%COMP%]{\r\n  margin-right: 0px;\r\n  height: 345px;\r\n  width: 315px;\r\n  }\r\n  .card1[_ngcontent-%COMP%]   .box1[_ngcontent-%COMP%]{\r\n  box-shadow: 0px 1px 15px grey;\r\n  border: none;\r\n  border-radius: 71px 14px 71px 14px;\r\n  background-color: #ffffff;\r\n  padding: 40px;\r\n  }\r\n  .card2[_ngcontent-%COMP%]{\r\n    margin-right: 0px;\r\n    height: 345px;\r\n    width: 315px;\r\n  }\r\n  .card2[_ngcontent-%COMP%]   .box2[_ngcontent-%COMP%]{\r\n    box-shadow: 0px 1px 15px grey;\r\n    border: none;\r\n    border-radius: 71px 14px 71px 14px;\r\n    background-color: #ffffff;\r\n    padding: 40px;\r\n    }\r\n  .card3[_ngcontent-%COMP%]{\r\n    margin-right: 0px;\r\n    height: 345px;\r\n    width: 315px;\r\n  }\r\n  .card3[_ngcontent-%COMP%]   .box3[_ngcontent-%COMP%]{\r\n    box-shadow: 0px 1px 15px grey;\r\n    border: none;\r\n    border-radius: 71px 14px 71px 14px;\r\n    background-color: #ffffff;\r\n    padding: 40px;\r\n    }\r\n    \r\n  .lastB[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    margin: none;\r\n  }\r\n\r\n\r\n  .text-center[_ngcontent-%COMP%]{\r\n  margin-bottom: 50px;\r\n  }\r\n\r\n    }\r\n\r\n@media only screen and (min-width : 992px) and (max-width : 1200px)  {\r\n \r\n .drop[_ngcontent-%COMP%]{\r\n  min-width: -moz-available;\r\n  margin-left: -70px;\r\n }     \r\n\r\n.float_actions[_ngcontent-%COMP%] {\r\n  position: fixed;\r\n  background: red;\r\n  border-radius: 10px;\r\n  left: 0;\r\n  top: 35%;\r\n  padding: 10px;\r\n  display: flex;\r\n  justify-content: center;\r\n  align-items: center;\r\n  width: 8%;\r\n  z-index:5900;\r\n  }\r\n  \r\n  .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%] {\r\n  padding: 0;\r\n  margin: 0 0 -30px 0;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content: space-between;\r\n  }\r\n  .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%] {\r\n  padding: 5px;\r\n  width: 120px;\r\n  height: 120px;\r\n  display: flex;\r\n  justify-content: center;\r\n  align-content: center;\r\n  position: relative;\r\n  }\r\n  \r\n  .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%] {\r\n  text-decoration: none;\r\n  display: block;\r\n  flex-direction: column;\r\n  justify-content: center;\r\n  font-size: 14px;\r\n  }\r\n  .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_logo[_ngcontent-%COMP%] {\r\n  width: 100px;\r\n  height: 100px;\r\n  margin-left: 25px;\r\n  }\r\n  .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_text[_ngcontent-%COMP%] {\r\n  text-align: center;\r\n  margin: 0;\r\n  color: white;\r\n  margin-top: 7px;\r\n  }\r\n  \r\n  \r\n   .primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]::before{\r\n   content: \">\";\r\n   position: absolute;\r\n   right: -10px;\r\n   top: 15%;\r\n   color: white;\r\n   font-size: 20px;\r\n   width: 40%;\r\n   font-weight: bold;\r\n   }\r\n   \r\n   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .testmegi[_ngcontent-%COMP%]{\r\n    text-decoration: none;\r\n     position: absolute;\r\n     right: -180px;\r\n     top: 15%;\r\n     color: black;\r\n     font-size: 17px;\r\n     width: 40%;\r\n     background: white;\r\n     width: 180px;\r\n     border-radius: 25px;\r\n     text-align: center;\r\n     height: 40px;\r\n     display: none;\r\n     justify-content: center;\r\n     align-items: center;\r\n   }\r\n    .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]:hover   .testmegi[_ngcontent-%COMP%]{\r\n   display: flex;\r\n   }\r\n   .primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%] {\r\n    text-decoration: none;\r\n    display: block;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    font-size: 14px;\r\n   }\r\n   .primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_logo[_ngcontent-%COMP%] {\r\n    width: 100px;\r\n    height: 100px;\r\n    margin-left: 25px;\r\n   }\r\n   .primary_body[_ngcontent-%COMP%]   .float_actions[_ngcontent-%COMP%]   .actions_content[_ngcontent-%COMP%]   .action_items[_ngcontent-%COMP%]   .item_href[_ngcontent-%COMP%]   .ihref_text[_ngcontent-%COMP%] {\r\n    text-align: center;\r\n    margin: 0;\r\n    color: white;\r\n    margin-top: 7px;\r\n   }\r\n   \r\n      \r\n\r\n.content[_ngcontent-%COMP%]{\r\n  width: max-content;\r\n  }\r\n  .transtion[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    position: absolute;\r\n    margin-left: -140px;\r\n    width: max-content;\r\n    flex-direction: column;\r\n    }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%] {\r\n  background:#111d5e;\r\n  min-height: -moz-fit-content;\r\n  min-height: fit-content;\r\n  border-bottom-right-radius: 100px;\r\n }\r\n .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\r\n  height: 100%;\r\n  max-height: 585px;\r\n  margin-top: 1;\r\n  width: 993px;\r\n }\r\n .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\r\n  font-size: 40px;\r\n  font-weight: 800;\r\n  color:  #ffffff;\r\n  width: max-content;\r\n }\r\n .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .desc_heading[_ngcontent-%COMP%] {\r\n  font-size: 40px;\r\n  font-weight: 700;\r\n  color:  #ffffff;\r\n  width: max-content;\r\n }\r\n .carre[_ngcontent-%COMP%] {\r\n  width: 200px;\r\n  height: 90px;\r\n  background: white;\r\n  border-radius: 18px;\r\n  margin-left: 80px;\r\n  margin-top: 335px;\r\n  }\r\n  \r\n  .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%] {\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: flex-start;\r\n    margin-left: unset;\r\n    width: -moz-available;\r\n    margin-left: 0px;\r\n    }\r\n    \r\n   .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\r\n    border: 5px solid #ffffff;\r\n    border-radius: 20px;\r\n    height: 40px;\r\n    width: 400px;\r\n   }\r\n   .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\r\n    opacity: 1;\r\n    width: -moz-available;\r\n   }\r\n\r\n\r\n   \r\n   .home[_ngcontent-%COMP%]{\r\n    color: white;\r\n    font-size: 18px;\r\n    text-align: center;\r\n    }\r\n\r\n    .block2[_ngcontent-%COMP%]{\r\n      background: transparent;\r\n       color: white;\r\n       padding: 22px;\r\n      }\r\n      .block2[_ngcontent-%COMP%]:hover{\r\n       border: 5px solid white;\r\n       padding: 18px;\r\n      }\r\n\r\n       \r\n \r\n       .block4[_ngcontent-%COMP%]{\r\n        background-image: url('sin.png');\r\n        background-repeat: no-repeat;\r\n        background-position:258px 220px;\r\n        margin-top:0px;\r\n        }\r\n        .block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: column;\r\n        align-items: flex-start;\r\n        }\r\n        .block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n          padding-right: 50%;\r\n           }\r\n\r\n        .one[_ngcontent-%COMP%]{\r\n          margin: 11px 0px 0px 263px\r\n          }\r\n           .two[_ngcontent-%COMP%]{\r\n          margin: -580px 0 0 650px\r\n          }\r\n          .three[_ngcontent-%COMP%]{\r\n          margin: -12px 0 0 650px\r\n          }\r\n\r\n          \r\n          .block5[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n            margin-left: 60px;\r\n            height:100%;\r\n            width:100%\r\n          }\r\n\r\n          \r\n \r\n          .block6[_ngcontent-%COMP%]{\r\n            padding-top: 30px; \r\n            padding-left: 110px;\r\n            margin-top: -490px; \r\n            float:right\r\n          }\r\n\r\n    }\r\n\r\n@media only screen and (min-width : 1200px) and (max-width : 1500px) {\r\n      \r\n\r\n.content[_ngcontent-%COMP%]{\r\n  width: max-content;\r\n  }\r\n  .transtion[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    position: absolute;\r\n   padding-left: 115px;\r\n    width: max-content;\r\n    flex-direction: column;\r\n    }\r\n.primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%] {\r\n  background:#111d5e;\r\n  min-height: -moz-fit-content;\r\n  min-height: fit-content;\r\n  border-bottom-right-radius: 100px;\r\n }\r\n .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .img_wrapper[_ngcontent-%COMP%]   .heading_img[_ngcontent-%COMP%] {\r\n  height: 100%;\r\n  max-height: 585px;\r\n  margin-top: 1;\r\n  width: 993px;\r\n }\r\n .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .title_heading[_ngcontent-%COMP%] {\r\n  font-size: 40px;\r\n  font-weight: 800;\r\n  color:  #ffffff;\r\n  width: max-content;\r\n }\r\n .primary_body[_ngcontent-%COMP%]   .home_container[_ngcontent-%COMP%]   .section_heading[_ngcontent-%COMP%]   .heading_wrapper[_ngcontent-%COMP%]   .desc_heading[_ngcontent-%COMP%] {\r\n  font-size: 40px;\r\n  font-weight: 700;\r\n  color:  #ffffff;\r\n  width: max-content;\r\n }\r\n .carre[_ngcontent-%COMP%] {\r\n  width: 200px;\r\n  height: 90px;\r\n  background: white;\r\n  border-radius: 18px;\r\n  margin-left: 80px;\r\n  margin-top: 335px;\r\n  }\r\n  \r\n  .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%] {\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: flex-start;\r\n    margin-left: unset;\r\n    width: -moz-available;\r\n    margin-left: 0px;\r\n    }\r\n    \r\n   .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]:before {\r\n    border: 5px solid #ffffff;\r\n    border-radius: 20px;\r\n    height: 40px;\r\n    width: 400px;\r\n   }\r\n   .primary_body[_ngcontent-%COMP%]   .search_bloc[_ngcontent-%COMP%]   .search--show[_ngcontent-%COMP%]   .search__input[_ngcontent-%COMP%] {\r\n    opacity: 1;\r\n    width: -moz-available;\r\n   }\r\n\r\n       \r\n \r\n        .block4[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: column;\r\n        align-items: flex-start;\r\n        }\r\n     \r\n\r\n        .one[_ngcontent-%COMP%]{\r\n          margin: 11px 0px 0px 456px\r\n          }\r\n           .two[_ngcontent-%COMP%]{\r\n          margin: -580px 0 0 792px\r\n          }\r\n          .three[_ngcontent-%COMP%]{\r\n          margin: -12px 0 0 792px\r\n          }\r\n          \r\n    }\r\n\r\n.elementor-element.elementor-element-4c89a748[_ngcontent-%COMP%]:not(.elementor-motion-effects-element-type-background), .elementor-element.elementor-element-4c89a748[_ngcontent-%COMP%]    > .elementor-motion-effects-container[_ngcontent-%COMP%]    > .elementor-motion-effects-layer[_ngcontent-%COMP%]{background-color:#FBFBFB;}\r\n.elementor-element.elementor-element-4c89a748[_ngcontent-%COMP%]{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;padding:96px 0px 96px 0px;}\r\n.elementor-element.elementor-element-4c89a748[_ngcontent-%COMP%]    > .elementor-background-overlay[_ngcontent-%COMP%]{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}\r\n.elementor-element.elementor-element-1439662d[_ngcontent-%COMP%]    > .elementor-element-populated[_ngcontent-%COMP%]{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}\r\n.elementor-element.elementor-element-1439662d[_ngcontent-%COMP%]    > .elementor-element-populated[_ngcontent-%COMP%]    > .elementor-background-overlay[_ngcontent-%COMP%]{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}\r\n.elementor-element.elementor-element-97fe02f[_ngcontent-%COMP%]{text-align:center;width:auto;max-width:auto;align-self:center;}\r\n.elementor-element.elementor-element-97fe02f[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{width:100%;max-width:100%;height:418px;object-fit:contain;}\r\n.elementor-element.elementor-element-97fe02f[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:-47px -147px -147px -147px;padding:0px 0px 0px 0px;}\r\n.elementor-element.elementor-element-ca76944[_ngcontent-%COMP%]{text-align:left;}\r\n.elementor-element.elementor-element-ca76944[_ngcontent-%COMP%]   .elementor-heading-title[_ngcontent-%COMP%]{color:#CF1B37;font-size:12px;font-weight:600;text-transform:uppercase;letter-spacing:2px;}\r\n.elementor-element.elementor-element-ca76944[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px 0px 24px 0px;}\r\n.elementor-element.elementor-element-b51e7b9[_ngcontent-%COMP%]{text-align:left;}\r\n.elementor-element.elementor-element-b51e7b9[_ngcontent-%COMP%]   .elementor-heading-title[_ngcontent-%COMP%]{color:#131b5c;font-family:var( --e-global-typography-primary-font-family ), Sans-serif;font-size:var( --e-global-typography-primary-font-size );font-weight:var( --e-global-typography-primary-font-weight );line-height:var( --e-global-typography-primary-line-height );}\r\n.elementor-element.elementor-element-b51e7b9[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px 0px 24px 0px;}\r\n.elementor-element.elementor-element-5b3a9ef0[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px 0px 16px 0px;}\r\n.elementor-element.elementor-element-48839a38[_ngcontent-%COMP%]   .elementskit_input_label[_ngcontent-%COMP%]{color:#000000;}\r\n.elementor-element.elementor-element-48839a38[_ngcontent-%COMP%]   .ekit_form_control[_ngcontent-%COMP%]{border-radius:75px 75px 75px 75px;border-style:solid;border-width:1px 1px 1px 1px;border-color:#DEDEDE;padding:12px 16px 12px 32px;}\r\n.elementor-element.elementor-element-48839a38[_ngcontent-%COMP%]   .elementskit_input_container[_ngcontent-%COMP%]{flex:0 0 66%;}\r\n.elementor-element.elementor-element-48839a38[_ngcontent-%COMP%]   .elementskit_inline_form[_ngcontent-%COMP%]   .elementskit_input_wraper[_ngcontent-%COMP%]:not(:last-child){margin-right:8px;}\r\n.elementor-element.elementor-element-48839a38[_ngcontent-%COMP%]   .ekit_form_control[_ngcontent-%COMP%]::-webkit-input-placeholder{color:#6B6B6B;font-size:14px;}\r\n.elementor-element.elementor-element-48839a38[_ngcontent-%COMP%]   .ekit_form_control[_ngcontent-%COMP%]::-moz-placeholder{color:#6B6B6B;font-size:14px;}\r\n.elementor-element.elementor-element-48839a38[_ngcontent-%COMP%]   .ekit_form_control[_ngcontent-%COMP%]:-ms-input-placeholder{color:#6B6B6B;font-size:14px;}\r\n.elementor-element.elementor-element-48839a38[_ngcontent-%COMP%]   .ekit_form_control[_ngcontent-%COMP%]:-moz-placeholder{color:#6B6B6B;font-size:14px;}\r\n.elementor-element.elementor-element-48839a38[_ngcontent-%COMP%]   .ekit-mail-submit[_ngcontent-%COMP%]{font-size:12px;font-weight:600;letter-spacing:1px;border-radius:88px 86px 88px 88px;padding:13px 0px 12px 0px;width:143px;margin:0px 0px 0px 0px;color:#fff;background-color:#131b5c;}\r\n.elementor-element.elementor-element-48839a38[_ngcontent-%COMP%]   .ekit-mail-submit[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%]   path[_ngcontent-%COMP%]{stroke:#fff;fill:#fff;}\r\n.elementor-element.elementor-element-48839a38[_ngcontent-%COMP%]   .ekit-mail-submit[_ngcontent-%COMP%]:hover{color:#fff;}\r\n.elementor-element.elementor-element-48839a38[_ngcontent-%COMP%]   .ekit-mail-submit[_ngcontent-%COMP%]:hover   svg[_ngcontent-%COMP%]   path[_ngcontent-%COMP%]{stroke:#fff;fill:#fff;}\r\n.elementor-element.elementor-element-48839a38[_ngcontent-%COMP%]   .ekit-mail-submit[_ngcontent-%COMP%]:before{background-color:#CF1B37;}\r\n.elementor-element.elementor-element-48839a38[_ngcontent-%COMP%]   .ekit-mail-submit[_ngcontent-%COMP%]    > i[_ngcontent-%COMP%], .elementor-element.elementor-element-48839a38[_ngcontent-%COMP%]   .ekit-mail-submit[_ngcontent-%COMP%]    > svg[_ngcontent-%COMP%]{margin-right:11px;}\r\n.elementor-element.elementor-element-48839a38[_ngcontent-%COMP%]   .elementskit_input_group_text[_ngcontent-%COMP%]{background-color:#FFFFFF;font-size:10px;}\r\n.elementor-element.elementor-element-48839a38[_ngcontent-%COMP%]   .elementskit_input_group_text[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{color:#131b5c;}\r\n.elementor-element.elementor-element-48839a38[_ngcontent-%COMP%]   .elementskit_input_group_text[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%]   path[_ngcontent-%COMP%]{stroke:#131b5c;fill:#131b5c;}\r\n.elementor-element.elementor-element-48839a38[_ngcontent-%COMP%]   .elementskit_input_group_text[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%]{max-width:10px;}\r\n.elementor-element.elementor-element-48839a38[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px 0px 16px 0px;}\r\n.elementor-element.elementor-element-8d2d471[_ngcontent-%COMP%]{color:#AEAEAE;font-size:14px;}\r\n.elementor-element.elementor-element-489576f2[_ngcontent-%COMP%]:not(.elementor-motion-effects-element-type-background), .elementor-element.elementor-element-489576f2[_ngcontent-%COMP%]    > .elementor-motion-effects-container[_ngcontent-%COMP%]    > .elementor-motion-effects-layer[_ngcontent-%COMP%]{background-color:#111E5D;}\r\n.elementor-element.elementor-element-489576f2[_ngcontent-%COMP%]    > .elementor-background-overlay[_ngcontent-%COMP%]{opacity:0.2;transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}\r\n.elementor-element.elementor-element-489576f2[_ngcontent-%COMP%]{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;padding:0px 0px 0px 0px;}\r\n.elementor-element.elementor-element-25dbdf9a[_ngcontent-%COMP%]    > .elementor-element-populated[_ngcontent-%COMP%]{padding:0px 0px 0px 0px;}\r\n.elementor-element.elementor-element-75716936[_ngcontent-%COMP%]    > .elementor-container[_ngcontent-%COMP%]{max-width:1251px;}\r\n.elementor-element.elementor-element-75716936[_ngcontent-%COMP%]{margin-top:32px;margin-bottom:90px;}\r\n.elementor-element.elementor-element-7bab91b6[_ngcontent-%COMP%]    > .elementor-element-populated[_ngcontent-%COMP%]{margin:0px 50px 0px 0px;--e-column-margin-right:50px;--e-column-margin-left:0px;padding:0px 0px 0px 0px;}\r\n.elementor-element.elementor-element-743589bc[_ngcontent-%COMP%]{text-align:left;}\r\n.elementor-element.elementor-element-743589bc[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{width:35%;border-radius:0px 0px 0px 0px;}\r\n.elementor-element.elementor-element-a3eb4cb[_ngcontent-%COMP%]{--e-icon-list-icon-size:14px;}\r\n.elementor-element.elementor-element-a3eb4cb[_ngcontent-%COMP%]   .elementor-icon-list-text[_ngcontent-%COMP%]{color:#FFFFFF;}\r\n.elementor-element.elementor-element-a3eb4cb[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]:hover   .elementor-icon-list-text[_ngcontent-%COMP%]{color:#cc1414;}\r\n.elementor-element.elementor-element-a3eb4cb[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]    > .elementor-icon-list-text[_ngcontent-%COMP%], .elementor-element.elementor-element-a3eb4cb[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]{font-size:12px;font-weight:600;letter-spacing:2px;}\r\n.elementor-element.elementor-element-305a976b[_ngcontent-%COMP%]    > .elementor-element-populated[_ngcontent-%COMP%]{padding:0px 0px 0px 0px;}\r\n.elementor-element.elementor-element-57814c0a[_ngcontent-%COMP%]   .elementor-heading-title[_ngcontent-%COMP%]{color:#FFFFFF;font-family:var( --e-global-typography-456d3d4-font-family ), Sans-serif;font-size:var( --e-global-typography-456d3d4-font-size );font-weight:var( --e-global-typography-456d3d4-font-weight );line-height:var( --e-global-typography-456d3d4-line-height );letter-spacing:var( --e-global-typography-456d3d4-letter-spacing );word-spacing:var( --e-global-typography-456d3d4-word-spacing );}\r\n.elementor-element.elementor-element-57814c0a[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px 0px 0px 0px;}\r\n.elementor-element.elementor-element-713053d[_ngcontent-%COMP%]   .elementor-icon-list-items[_ngcontent-%COMP%]:not(.elementor-inline-items)   .elementor-icon-list-item[_ngcontent-%COMP%]:not(:last-child){padding-bottom:calc(6px/2);}\r\n.elementor-element.elementor-element-713053d[_ngcontent-%COMP%]   .elementor-icon-list-items[_ngcontent-%COMP%]:not(.elementor-inline-items)   .elementor-icon-list-item[_ngcontent-%COMP%]:not(:first-child){margin-top:calc(6px/2);}\r\n.elementor-element.elementor-element-713053d[_ngcontent-%COMP%]   .elementor-icon-list-items.elementor-inline-items[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]{margin-right:calc(6px/2);margin-left:calc(6px/2);}\r\n.elementor-element.elementor-element-713053d[_ngcontent-%COMP%]   .elementor-icon-list-items.elementor-inline-items[_ngcontent-%COMP%]{margin-right:calc(-6px/2);margin-left:calc(-6px/2);}\r\nbody.rtl[_ngcontent-%COMP%]   .elementor-element.elementor-element-713053d[_ngcontent-%COMP%]   .elementor-icon-list-items.elementor-inline-items[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]:after{left:calc(-6px/2);}\r\nbody[_ngcontent-%COMP%]:not(.rtl)   .elementor-element.elementor-element-713053d[_ngcontent-%COMP%]   .elementor-icon-list-items.elementor-inline-items[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]:after{right:calc(-6px/2);}\r\n.elementor-element.elementor-element-713053d[_ngcontent-%COMP%]   .elementor-icon-list-icon[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]{color:#FFFFFF;}\r\n.elementor-element.elementor-element-713053d[_ngcontent-%COMP%]   .elementor-icon-list-icon[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%]{fill:#FFFFFF;}\r\n.elementor-element.elementor-element-713053d[_ngcontent-%COMP%]{--e-icon-list-icon-size:17px;}\r\n.elementor-element.elementor-element-713053d[_ngcontent-%COMP%]   .elementor-icon-list-text[_ngcontent-%COMP%]{color:#FFFFFF;}\r\n.elementor-element.elementor-element-713053d[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]:hover   .elementor-icon-list-text[_ngcontent-%COMP%]{color:var( --e-global-color-accent );}\r\n.elementor-element.elementor-element-713053d[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]    > .elementor-icon-list-text[_ngcontent-%COMP%], .elementor-element.elementor-element-713053d[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]{font-family:var( --e-global-typography-text-font-family ), Sans-serif;font-size:var( --e-global-typography-text-font-size );font-weight:var( --e-global-typography-text-font-weight );line-height:var( --e-global-typography-text-line-height );}\r\n.elementor-element.elementor-element-70f045d2[_ngcontent-%COMP%]    > .elementor-element-populated[_ngcontent-%COMP%]{padding:0px 0px 0px 0px;}\r\n.elementor-element.elementor-element-3997eee9[_ngcontent-%COMP%]   .elementor-heading-title[_ngcontent-%COMP%]{color:#FFFFFF;font-family:var( --e-global-typography-456d3d4-font-family ), Sans-serif;font-size:var( --e-global-typography-456d3d4-font-size );font-weight:var( --e-global-typography-456d3d4-font-weight );line-height:var( --e-global-typography-456d3d4-line-height );letter-spacing:var( --e-global-typography-456d3d4-letter-spacing );word-spacing:var( --e-global-typography-456d3d4-word-spacing );}\r\n.elementor-element.elementor-element-3997eee9[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px 0px 0px 0px;}\r\n.elementor-element.elementor-element-2a5cceb4[_ngcontent-%COMP%]   .elementor-icon-list-items[_ngcontent-%COMP%]:not(.elementor-inline-items)   .elementor-icon-list-item[_ngcontent-%COMP%]:not(:last-child){padding-bottom:calc(6px/2);}\r\n.elementor-element.elementor-element-2a5cceb4[_ngcontent-%COMP%]   .elementor-icon-list-items[_ngcontent-%COMP%]:not(.elementor-inline-items)   .elementor-icon-list-item[_ngcontent-%COMP%]:not(:first-child){margin-top:calc(6px/2);}\r\n.elementor-element.elementor-element-2a5cceb4[_ngcontent-%COMP%]   .elementor-icon-list-items.elementor-inline-items[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]{margin-right:calc(6px/2);margin-left:calc(6px/2);}\r\n.elementor-element.elementor-element-2a5cceb4[_ngcontent-%COMP%]   .elementor-icon-list-items.elementor-inline-items[_ngcontent-%COMP%]{margin-right:calc(-6px/2);margin-left:calc(-6px/2);}\r\nbody.rtl[_ngcontent-%COMP%]   .elementor-element.elementor-element-2a5cceb4[_ngcontent-%COMP%]   .elementor-icon-list-items.elementor-inline-items[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]:after{left:calc(-6px/2);}\r\nbody[_ngcontent-%COMP%]:not(.rtl)   .elementor-element.elementor-element-2a5cceb4[_ngcontent-%COMP%]   .elementor-icon-list-items.elementor-inline-items[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]:after{right:calc(-6px/2);}\r\n.elementor-element.elementor-element-2a5cceb4[_ngcontent-%COMP%]{--e-icon-list-icon-size:14px;}\r\n.elementor-element.elementor-element-2a5cceb4[_ngcontent-%COMP%]   .elementor-icon-list-text[_ngcontent-%COMP%]{color:#FFFFFF;}\r\n.elementor-element.elementor-element-2a5cceb4[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]:hover   .elementor-icon-list-text[_ngcontent-%COMP%]{color:var( --e-global-color-accent );}\r\n.elementor-element.elementor-element-2a5cceb4[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]    > .elementor-icon-list-text[_ngcontent-%COMP%], .elementor-element.elementor-element-2a5cceb4[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]{font-family:var( --e-global-typography-text-font-family ), Sans-serif;font-size:var( --e-global-typography-text-font-size );font-weight:var( --e-global-typography-text-font-weight );line-height:var( --e-global-typography-text-line-height );}\r\n.elementor-element.elementor-element-77a81dd6[_ngcontent-%COMP%]    > .elementor-element-populated[_ngcontent-%COMP%]{padding:0px 0px 0px 0px;}\r\n.elementor-element.elementor-element-1b9bbacc[_ngcontent-%COMP%]   .elementor-heading-title[_ngcontent-%COMP%]{color:#FFFFFF;font-family:var( --e-global-typography-456d3d4-font-family ), Sans-serif;font-size:var( --e-global-typography-456d3d4-font-size );font-weight:var( --e-global-typography-456d3d4-font-weight );line-height:var( --e-global-typography-456d3d4-line-height );letter-spacing:var( --e-global-typography-456d3d4-letter-spacing );word-spacing:var( --e-global-typography-456d3d4-word-spacing );}\r\n.elementor-element.elementor-element-1b9bbacc[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px 0px 0px 0px;}\r\n.elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .menu-item[_ngcontent-%COMP%]   a.hfe-menu-item[_ngcontent-%COMP%]{padding-left:0px;padding-right:0px;}\r\n.elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .menu-item[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%]{padding-left:calc( 0px + 20px );padding-right:0px;}\r\n.elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .hfe-nav-menu__layout-vertical[_ngcontent-%COMP%]   .menu-item[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%]{padding-left:calc( 0px + 40px );padding-right:0px;}\r\n.elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .hfe-nav-menu__layout-vertical[_ngcontent-%COMP%]   .menu-item[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%]{padding-left:calc( 0px + 60px );padding-right:0px;}\r\n.elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .hfe-nav-menu__layout-vertical[_ngcontent-%COMP%]   .menu-item[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   ul[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%]{padding-left:calc( 0px + 80px );padding-right:0px;}\r\n.elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .menu-item[_ngcontent-%COMP%]   a.hfe-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .menu-item[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%]{padding-top:3px;padding-bottom:3px;}\r\n.elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   a.hfe-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%]{font-family:var( --e-global-typography-text-font-family ), Sans-serif;font-size:var( --e-global-typography-text-font-size );font-weight:var( --e-global-typography-text-font-weight );line-height:var( --e-global-typography-text-line-height );}\r\n.elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .menu-item[_ngcontent-%COMP%]   a.hfe-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .sub-menu[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%]{color:#FFFFFF;}\r\n.elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .menu-item[_ngcontent-%COMP%]   a.hfe-menu-item[_ngcontent-%COMP%]:hover, .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .sub-menu[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%]:hover, .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .menu-item.current-menu-item[_ngcontent-%COMP%]   a.hfe-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .menu-item[_ngcontent-%COMP%]   a.hfe-menu-item.highlighted[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .menu-item[_ngcontent-%COMP%]   a.hfe-menu-item[_ngcontent-%COMP%]:focus{color:var( --e-global-color-accent );}\r\n.elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .menu-item.current-menu-item[_ngcontent-%COMP%]   a.hfe-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .menu-item.current-menu-ancestor[_ngcontent-%COMP%]   a.hfe-menu-item[_ngcontent-%COMP%]{color:#BCBCBC;}\r\n.elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .sub-menu[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .elementor-menu-toggle[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown-expandible[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown-expandible[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%]{color:var( --e-global-color-secondary );}\r\n.elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .sub-menu[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown-expandible[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown-expandible[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%]{font-family:var( --e-global-typography-text-font-family ), Sans-serif;font-size:var( --e-global-typography-text-font-size );font-weight:var( --e-global-typography-text-font-weight );line-height:var( --e-global-typography-text-line-height );}\r\n.elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .sub-menu[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown-expandible[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown-expandible[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%]{padding-top:15px;padding-bottom:15px;}\r\n.elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .sub-menu[_ngcontent-%COMP%]   li.menu-item[_ngcontent-%COMP%]:not(:last-child), .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown[_ngcontent-%COMP%]   li.menu-item[_ngcontent-%COMP%]:not(:last-child), .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown-expandible[_ngcontent-%COMP%]   li.menu-item[_ngcontent-%COMP%]:not(:last-child){border-bottom-style:solid;border-bottom-color:#c4c4c4;border-bottom-width:1px;}\r\n.elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:-5px 0px 0px 0px;}\r\n.elementor-element.elementor-element-301df4de[_ngcontent-%COMP%]    > .elementor-container[_ngcontent-%COMP%]{}\r\n.elementor-element.elementor-element-301df4de[_ngcontent-%COMP%]{border-style:solid;border-width:1px 0px 0px 0px;border-color:#FFFFFF17;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;padding:20px 0px 20px 0px;}\r\n.elementor-element.elementor-element-301df4de[_ngcontent-%COMP%]    > .elementor-background-overlay[_ngcontent-%COMP%]{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}\r\n.elementor-bc-flex-widget[_ngcontent-%COMP%]   .elementor-element.elementor-element-e6d2544.elementor-column[_ngcontent-%COMP%]   .elementor-widget-wrap[_ngcontent-%COMP%]{align-items:center;}\r\n.elementor-element.elementor-element-e6d2544.elementor-column.elementor-element[data-element_type=\"column\"][_ngcontent-%COMP%]    > .elementor-widget-wrap.elementor-element-populated[_ngcontent-%COMP%]{align-content:center;align-items:center;}\r\n.elementor-element.elementor-element-e6d2544[_ngcontent-%COMP%]    > .elementor-element-populated[_ngcontent-%COMP%]{padding:0px 0px 0px 0px;}\r\n.elementor-element.elementor-element-1a067344[_ngcontent-%COMP%]{color:#FFFFFF;font-family:\"Roboto\", Sans-serif;font-weight:400;}\r\n.elementor-element.elementor-element-1a067344[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px 0px -15px 0px;}\r\n.elementor-bc-flex-widget[_ngcontent-%COMP%]   .elementor-element.elementor-element-2ca75dd0.elementor-column[_ngcontent-%COMP%]   .elementor-widget-wrap[_ngcontent-%COMP%]{align-items:center;}\r\n.elementor-element.elementor-element-2ca75dd0.elementor-column.elementor-element[data-element_type=\"column\"][_ngcontent-%COMP%]    > .elementor-widget-wrap.elementor-element-populated[_ngcontent-%COMP%]{align-content:center;align-items:center;}\r\n.elementor-element.elementor-element-2ca75dd0.elementor-column[_ngcontent-%COMP%]    > .elementor-widget-wrap[_ngcontent-%COMP%]{justify-content:flex-end;}\r\n.elementor-element.elementor-element-2ca75dd0[_ngcontent-%COMP%]    > .elementor-element-populated[_ngcontent-%COMP%]{padding:0px 0px 0px 0px;}\r\n.elementor-element.elementor-element-2cc9afc[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]{font-family:var( --e-global-typography-text-font-family ), Sans-serif;font-size:var( --e-global-typography-text-font-size );font-weight:var( --e-global-typography-text-font-weight );line-height:var( --e-global-typography-text-line-height );fill:#FFFFFF;color:#FFFFFF;background-color:#0A6CFF00;border-style:solid;border-width:0px 0px 0px 0px;border-radius:0px 0px 0px 0px;padding:0px 0px 0px 0px;}\r\n.elementor-element.elementor-element-2cc9afc[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]:hover, .elementor-element.elementor-element-2cc9afc[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]:focus{color:var( --e-global-color-accent );}\r\n.elementor-element.elementor-element-2cc9afc[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]:hover   svg[_ngcontent-%COMP%], .elementor-element.elementor-element-2cc9afc[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]:focus   svg[_ngcontent-%COMP%]{fill:var( --e-global-color-accent );}\r\n.elementor-element.elementor-element-2cc9afc[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px 20px 0px 0px;}\r\n.elementor-element.elementor-element-2cc9afc[_ngcontent-%COMP%]{width:auto;max-width:auto;}\r\n.elementor-element.elementor-element-7e0af8af[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]{font-family:var( --e-global-typography-text-font-family ), Sans-serif;font-size:var( --e-global-typography-text-font-size );font-weight:var( --e-global-typography-text-font-weight );line-height:var( --e-global-typography-text-line-height );fill:#FFFFFF;color:#FFFFFF;background-color:#0A6CFF00;border-style:solid;border-width:0px 0px 0px 0px;border-radius:0px 0px 0px 0px;padding:0px 0px 0px 0px;}\r\n.elementor-element.elementor-element-7e0af8af[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]:hover, .elementor-element.elementor-element-7e0af8af[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]:focus{color:var( --e-global-color-accent );}\r\n.elementor-element.elementor-element-7e0af8af[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]:hover   svg[_ngcontent-%COMP%], .elementor-element.elementor-element-7e0af8af[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]:focus   svg[_ngcontent-%COMP%]{fill:var( --e-global-color-accent );}\r\n.elementor-element.elementor-element-7e0af8af[_ngcontent-%COMP%]{width:auto;max-width:auto;}\r\n[_ngcontent-%COMP%]:root{--page-title-display:none;}\r\n@media(max-width:1024px){ .elementor-element.elementor-element-b51e7b9[_ngcontent-%COMP%]   .elementor-heading-title[_ngcontent-%COMP%]{font-size:var( --e-global-typography-primary-font-size );line-height:var( --e-global-typography-primary-line-height );} .elementor-element.elementor-element-489576f2[_ngcontent-%COMP%]    > .elementor-background-overlay[_ngcontent-%COMP%]{background-image:url(\"http://askproject.net/wryter/wp-content/uploads/sites/36/2021/10/bg_8.png\");} .elementor-element.elementor-element-489576f2[_ngcontent-%COMP%]{padding:70px 30px 0px 30px;} .elementor-element.elementor-element-75716936[_ngcontent-%COMP%]{margin-top:0px;margin-bottom:70px;} .elementor-element.elementor-element-7bab91b6[_ngcontent-%COMP%]    > .elementor-element-populated[_ngcontent-%COMP%]{margin:0px 0px 0px 0px;--e-column-margin-right:0px;--e-column-margin-left:0px;padding:0px 0px 50px 0px;} .elementor-element.elementor-element-743589bc[_ngcontent-%COMP%]{text-align:center;} .elementor-element.elementor-element-743589bc[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{width:27%;} .elementor-element.elementor-element-743589bc[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px 0px 0px 0px;} .elementor-element.elementor-element-a3eb4cb[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;} .elementor-element.elementor-element-57814c0a[_ngcontent-%COMP%]   .elementor-heading-title[_ngcontent-%COMP%]{font-size:var( --e-global-typography-456d3d4-font-size );line-height:var( --e-global-typography-456d3d4-line-height );letter-spacing:var( --e-global-typography-456d3d4-letter-spacing );word-spacing:var( --e-global-typography-456d3d4-word-spacing );} .elementor-element.elementor-element-713053d[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]    > .elementor-icon-list-text[_ngcontent-%COMP%], .elementor-element.elementor-element-713053d[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]{font-size:var( --e-global-typography-text-font-size );line-height:var( --e-global-typography-text-line-height );} .elementor-element.elementor-element-70f045d2[_ngcontent-%COMP%]    > .elementor-element-populated[_ngcontent-%COMP%]{margin:0px 0px 0px 0px;--e-column-margin-right:0px;--e-column-margin-left:0px;padding:0px 0px 0px 0px;} .elementor-element.elementor-element-3997eee9[_ngcontent-%COMP%]   .elementor-heading-title[_ngcontent-%COMP%]{font-size:var( --e-global-typography-456d3d4-font-size );line-height:var( --e-global-typography-456d3d4-line-height );letter-spacing:var( --e-global-typography-456d3d4-letter-spacing );word-spacing:var( --e-global-typography-456d3d4-word-spacing );} .elementor-element.elementor-element-2a5cceb4[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]    > .elementor-icon-list-text[_ngcontent-%COMP%], .elementor-element.elementor-element-2a5cceb4[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]{font-size:var( --e-global-typography-text-font-size );line-height:var( --e-global-typography-text-line-height );} .elementor-element.elementor-element-1b9bbacc[_ngcontent-%COMP%]   .elementor-heading-title[_ngcontent-%COMP%]{font-size:var( --e-global-typography-456d3d4-font-size );line-height:var( --e-global-typography-456d3d4-line-height );letter-spacing:var( --e-global-typography-456d3d4-letter-spacing );word-spacing:var( --e-global-typography-456d3d4-word-spacing );} .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .menu-item[_ngcontent-%COMP%]   a.hfe-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .menu-item[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%]{padding-top:3px;padding-bottom:3px;} .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   a.hfe-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%]{font-size:var( --e-global-typography-text-font-size );line-height:var( --e-global-typography-text-line-height );}\r\n         .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .sub-menu[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown-expandible[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown-expandible[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%]{font-size:var( --e-global-typography-text-font-size );line-height:var( --e-global-typography-text-line-height );} .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:-5px 0px -10px 0px;} .elementor-element.elementor-element-301df4de[_ngcontent-%COMP%]{padding:20px 0px 20px 0px;} .elementor-element.elementor-element-2cc9afc[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]{font-size:var( --e-global-typography-text-font-size );line-height:var( --e-global-typography-text-line-height );} .elementor-element.elementor-element-7e0af8af[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]{font-size:var( --e-global-typography-text-font-size );line-height:var( --e-global-typography-text-line-height );}}\r\n@media(min-width:768px){ .elementor-element.elementor-element-1439662d[_ngcontent-%COMP%]{width:14.104%;} .elementor-element.elementor-element-65b95991[_ngcontent-%COMP%]{width:39.894%;} .elementor-element.elementor-element-42518aa6[_ngcontent-%COMP%]{width:45.334%;} .elementor-element.elementor-element-7bab91b6[_ngcontent-%COMP%]{width:40.851%;} .elementor-element.elementor-element-305a976b[_ngcontent-%COMP%]{width:23.868%;} .elementor-element.elementor-element-70f045d2[_ngcontent-%COMP%]{width:19.276%;} .elementor-element.elementor-element-77a81dd6[_ngcontent-%COMP%]{width:16.005%;} .elementor-element.elementor-element-e6d2544[_ngcontent-%COMP%]{width:55.61%;} .elementor-element.elementor-element-2ca75dd0[_ngcontent-%COMP%]{width:44.39%;}}\r\n@media(max-width:1024px) and (min-width:768px){ .elementor-element.elementor-element-1439662d[_ngcontent-%COMP%]{width:2%;} .elementor-element.elementor-element-65b95991[_ngcontent-%COMP%]{width:42%;} .elementor-element.elementor-element-42518aa6[_ngcontent-%COMP%]{width:54%;} .elementor-element.elementor-element-7bab91b6[_ngcontent-%COMP%]{width:100%;} .elementor-element.elementor-element-305a976b[_ngcontent-%COMP%]{width:44%;} .elementor-element.elementor-element-70f045d2[_ngcontent-%COMP%]{width:14%;} .elementor-element.elementor-element-77a81dd6[_ngcontent-%COMP%]{width:39%;}}\r\n@media(max-width:767px){ .elementor-element.elementor-element-4c89a748[_ngcontent-%COMP%]{padding:48px 0px 48px 0px;} .elementor-element.elementor-element-b51e7b9[_ngcontent-%COMP%]   .elementor-heading-title[_ngcontent-%COMP%]{font-size:var( --e-global-typography-primary-font-size );line-height:var( --e-global-typography-primary-line-height );} .elementor-element.elementor-element-b51e7b9[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px 0px 0px 0px;} .elementor-element.elementor-element-42518aa6[_ngcontent-%COMP%]    > .elementor-element-populated[_ngcontent-%COMP%]{padding:0px 30px 0px 30px;} .elementor-element.elementor-element-489576f2[_ngcontent-%COMP%]    > .elementor-background-overlay[_ngcontent-%COMP%]{background-image:url(\"http://askproject.net/floristy/wp-content/uploads/sites/35/2021/10/bg_7.png\");} .elementor-element.elementor-element-489576f2[_ngcontent-%COMP%]{padding:70px 20px 0px 20px;} .elementor-element.elementor-element-75716936[_ngcontent-%COMP%]{margin-top:0px;margin-bottom:50px;} .elementor-element.elementor-element-7bab91b6[_ngcontent-%COMP%]    > .elementor-element-populated[_ngcontent-%COMP%]{margin:0px 0px 50px 0px;--e-column-margin-right:0px;--e-column-margin-left:0px;padding:0px 0px 0px 0px;} .elementor-element.elementor-element-743589bc[_ngcontent-%COMP%]{text-align:center;} .elementor-element.elementor-element-743589bc[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{width:50%;} .elementor-element.elementor-element-743589bc[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px 0px 0px 0px;} .elementor-element.elementor-element-305a976b[_ngcontent-%COMP%]    > .elementor-element-populated[_ngcontent-%COMP%]{margin:0px 0px 40px 0px;--e-column-margin-right:0px;--e-column-margin-left:0px;} .elementor-element.elementor-element-57814c0a[_ngcontent-%COMP%]   .elementor-heading-title[_ngcontent-%COMP%]{font-size:var( --e-global-typography-456d3d4-font-size );line-height:var( --e-global-typography-456d3d4-line-height );letter-spacing:var( --e-global-typography-456d3d4-letter-spacing );word-spacing:var( --e-global-typography-456d3d4-word-spacing );} .elementor-element.elementor-element-57814c0a[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px 0px 0px 0px;} .elementor-element.elementor-element-713053d[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]    > .elementor-icon-list-text[_ngcontent-%COMP%], .elementor-element.elementor-element-713053d[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]{font-size:var( --e-global-typography-text-font-size );line-height:var( --e-global-typography-text-line-height );} .elementor-element.elementor-element-70f045d2[_ngcontent-%COMP%]    > .elementor-element-populated[_ngcontent-%COMP%]{margin:0px 0px 0px 0px;--e-column-margin-right:0px;--e-column-margin-left:0px;padding:0px 0px 0px 0px;} .elementor-element.elementor-element-3997eee9[_ngcontent-%COMP%]   .elementor-heading-title[_ngcontent-%COMP%]{font-size:var( --e-global-typography-456d3d4-font-size );line-height:var( --e-global-typography-456d3d4-line-height );letter-spacing:var( --e-global-typography-456d3d4-letter-spacing );word-spacing:var( --e-global-typography-456d3d4-word-spacing );} .elementor-element.elementor-element-3997eee9[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px 0px 0px 0px;} .elementor-element.elementor-element-2a5cceb4[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]    > .elementor-icon-list-text[_ngcontent-%COMP%], .elementor-element.elementor-element-2a5cceb4[_ngcontent-%COMP%]   .elementor-icon-list-item[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]{font-size:var( --e-global-typography-text-font-size );line-height:var( --e-global-typography-text-line-height );} .elementor-element.elementor-element-77a81dd6[_ngcontent-%COMP%]    > .elementor-element-populated[_ngcontent-%COMP%]{margin:0px 0px 50px 0px;--e-column-margin-right:0px;--e-column-margin-left:0px;} .elementor-element.elementor-element-1b9bbacc[_ngcontent-%COMP%]   .elementor-heading-title[_ngcontent-%COMP%]{font-size:var( --e-global-typography-456d3d4-font-size );line-height:var( --e-global-typography-456d3d4-line-height );letter-spacing:var( --e-global-typography-456d3d4-letter-spacing );word-spacing:var( --e-global-typography-456d3d4-word-spacing );} .elementor-element.elementor-element-1b9bbacc[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px 0px 0px 0px;} .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   a.hfe-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%]{font-size:var( --e-global-typography-text-font-size );line-height:var( --e-global-typography-text-line-height );}\r\n         .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .sub-menu[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown-expandible[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-menu-item[_ngcontent-%COMP%], .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown-expandible[_ngcontent-%COMP%]   li[_ngcontent-%COMP%]   a.hfe-sub-menu-item[_ngcontent-%COMP%]{font-size:var( --e-global-typography-text-font-size );line-height:var( --e-global-typography-text-line-height );} .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .sub-menu[_ngcontent-%COMP%]{border-radius:0px 0px 0px 0px;} .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .sub-menu[_ngcontent-%COMP%]   li.menu-item[_ngcontent-%COMP%]:first-child{border-top-left-radius:0px;border-top-right-radius:0px;overflow:hidden;} .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   .sub-menu[_ngcontent-%COMP%]   li.menu-item[_ngcontent-%COMP%]:last-child{border-bottom-right-radius:0px;border-bottom-left-radius:0px;overflow:hidden;} .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown[_ngcontent-%COMP%]{border-radius:0px 0px 0px 0px;} .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown[_ngcontent-%COMP%]   li.menu-item[_ngcontent-%COMP%]:first-child{border-top-left-radius:0px;border-top-right-radius:0px;overflow:hidden;} .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown[_ngcontent-%COMP%]   li.menu-item[_ngcontent-%COMP%]:last-child{border-bottom-right-radius:0px;border-bottom-left-radius:0px;overflow:hidden;} .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown-expandible[_ngcontent-%COMP%]{border-radius:0px 0px 0px 0px;} .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown-expandible[_ngcontent-%COMP%]   li.menu-item[_ngcontent-%COMP%]:first-child{border-top-left-radius:0px;border-top-right-radius:0px;overflow:hidden;} .elementor-element.elementor-element-be2cd1f[_ngcontent-%COMP%]   nav.hfe-dropdown-expandible[_ngcontent-%COMP%]   li.menu-item[_ngcontent-%COMP%]:last-child{border-bottom-right-radius:0px;border-bottom-left-radius:0px;overflow:hidden;} .elementor-element.elementor-element-e6d2544[_ngcontent-%COMP%]    > .elementor-element-populated[_ngcontent-%COMP%]{padding:0px 0px 0px 0px;} .elementor-element.elementor-element-1a067344[_ngcontent-%COMP%]{text-align:center;} .elementor-element.elementor-element-2ca75dd0.elementor-column[_ngcontent-%COMP%]    > .elementor-widget-wrap[_ngcontent-%COMP%]{justify-content:center;} .elementor-element.elementor-element-2ca75dd0[_ngcontent-%COMP%]    > .elementor-element-populated[_ngcontent-%COMP%]{padding:0px 0px 15px 0px;} .elementor-element.elementor-element-2cc9afc[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]{font-size:var( --e-global-typography-text-font-size );line-height:var( --e-global-typography-text-line-height );} .elementor-element.elementor-element-7e0af8af[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]{font-size:var( --e-global-typography-text-font-size );line-height:var( --e-global-typography-text-line-height );}}\r\n.elementor-element.elementor-element-1dc5bd4d[_ngcontent-%COMP%]{margin-top:0px;margin-bottom:0px;}\r\n.elementor-bc-flex-widget[_ngcontent-%COMP%]   .elementor-element.elementor-element-330ca371.elementor-column[_ngcontent-%COMP%]   .elementor-widget-wrap[_ngcontent-%COMP%]{align-items:center;}\r\n.elementor-element.elementor-element-330ca371.elementor-column.elementor-element[data-element_type=\"column\"][_ngcontent-%COMP%]    > .elementor-widget-wrap.elementor-element-populated[_ngcontent-%COMP%]{align-content:center;align-items:center;}\r\n.elementor-element.elementor-element-330ca371[_ngcontent-%COMP%]    > .elementor-element-populated[_ngcontent-%COMP%]{padding:32px 0px 32px 0px;}\r\n.elementor-element.elementor-element-58d1221d[_ngcontent-%COMP%]   .elementor-heading-title[_ngcontent-%COMP%]{color:#131b5c;}\r\n.elementor-element.elementor-element-58d1221d[_ngcontent-%COMP%]{color:#131b5c;margin-top:0px;margin-bottom:0px;padding:0px 0px 0px 0px;z-index:1;}\r\n.elementor-element.elementor-element-58d1221d.jet-sticky-section-sticky--stuck[_ngcontent-%COMP%]{z-index:99;margin-top:0px;margin-bottom:0px;padding:12px 0px 12px 0px;background-color:#FFFFFF;box-shadow:0px 0px 0px 1px #DEDEDE;}\r\n.elementor-element.elementor-element-58d1221d.jet-sticky-section-sticky--stuck.jet-sticky-transition-in[_ngcontent-%COMP%], .elementor-element.elementor-element-58d1221d.jet-sticky-section-sticky--stuck.jet-sticky-transition-out[_ngcontent-%COMP%]{transition:margin 0.5s, padding 0.5s, background 0.5s, box-shadow 0.5s;}\r\n.elementor-bc-flex-widget[_ngcontent-%COMP%]   .elementor-element.elementor-element-435289e6.elementor-column[_ngcontent-%COMP%]   .elementor-widget-wrap[_ngcontent-%COMP%]{align-items:center;}\r\n.elementor-element.elementor-element-435289e6.elementor-column.elementor-element[data-element_type=\"column\"][_ngcontent-%COMP%]    > .elementor-widget-wrap.elementor-element-populated[_ngcontent-%COMP%]{align-content:center;align-items:center;}\r\n.elementor-element.elementor-element-435289e6[_ngcontent-%COMP%]    > .elementor-element-populated[_ngcontent-%COMP%]{padding:0px 0px 0px 30px;}\r\n.elementor-element.elementor-element-269e61d[_ngcontent-%COMP%]{text-align:left;}\r\n.elementor-element.elementor-element-269e61d[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{width:100%;}\r\n.elementor-element.elementor-element-269e61d[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px -20px 0px 0px;border-style:solid;border-width:0px 0px 0px 0px;border-color:#FFFFFF63;}\r\n.elementor-bc-flex-widget[_ngcontent-%COMP%]   .elementor-element.elementor-element-4cb2f502.elementor-column[_ngcontent-%COMP%]   .elementor-widget-wrap[_ngcontent-%COMP%]{align-items:space-around;}\r\n.elementor-element.elementor-element-4cb2f502.elementor-column.elementor-element[data-element_type=\"column\"][_ngcontent-%COMP%]    > .elementor-widget-wrap.elementor-element-populated[_ngcontent-%COMP%]{align-content:space-around;align-items:space-around;}\r\n.elementor-element.elementor-element-4cb2f502.elementor-column[_ngcontent-%COMP%]    > .elementor-widget-wrap[_ngcontent-%COMP%]{justify-content:flex-end;}\r\n.elementor-element.elementor-element-4cb2f502[_ngcontent-%COMP%]    > .elementor-element-populated[_ngcontent-%COMP%]{padding:0px 6px 0px 0px;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-menu-container[_ngcontent-%COMP%]{height:40px;border-radius:0px 0px 0px 0px;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]{font-size:16px;font-weight:700;color:#131b5c;padding:0px 30px 0px 0px;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:hover, .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:focus, .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:active, .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]:hover    > a[_ngcontent-%COMP%]{background-color:rgba(255, 255, 255, 0);}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:hover{color:#131b5c;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:focus{color:#131b5c;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:active{color:#131b5c;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]:hover    > a[_ngcontent-%COMP%]{color:#131b5c;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]:hover    > a[_ngcontent-%COMP%]   .elementskit-submenu-indicator[_ngcontent-%COMP%]{color:#131b5c;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:hover   .elementskit-submenu-indicator[_ngcontent-%COMP%]{color:#131b5c;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:focus   .elementskit-submenu-indicator[_ngcontent-%COMP%]{color:#131b5c;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:active   .elementskit-submenu-indicator[_ngcontent-%COMP%]{color:#131b5c;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li.current-menu-item[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]{color:#131b5c;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li.current-menu-ancestor[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]{color:#131b5c;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li.current-menu-ancestor[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]   .elementskit-submenu-indicator[_ngcontent-%COMP%]{color:#131b5c;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]   .elementskit-submenu-indicator[_ngcontent-%COMP%]{color:#6B6B6B;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]   .elementskit-submenu-panel[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]{font-size:15px;font-weight:400;padding:8px 15px 8px 24px;color:#131b5c;background-color:#FFFFFF;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]   .elementskit-submenu-panel[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:hover{color:#C84040;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]   .elementskit-submenu-panel[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:focus{color:#C84040;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]   .elementskit-submenu-panel[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:active{color:#C84040;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]   .elementskit-submenu-panel[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]:hover    > a[_ngcontent-%COMP%]{color:#C84040;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]   .elementskit-submenu-panel[_ngcontent-%COMP%]    > li.current-menu-item[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]{color:#00AE9D !important;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-submenu-panel[_ngcontent-%COMP%]{padding:15px 0px 15px 0px;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]   .elementskit-submenu-panel[_ngcontent-%COMP%]{border-style:solid;border-width:0px 0px 0px 0px;border-color:rgba(2, 1, 1, 0);background-color:#FFFFFF;border-radius:2px 2px 2px 2px;min-width:220px;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-menu-hamburger[_ngcontent-%COMP%]{float:right;background-color:rgba(2, 1, 1, 0);}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-menu-hamburger[_ngcontent-%COMP%]   .elementskit-menu-hamburger-icon[_ngcontent-%COMP%]{background-color:#333333;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-menu-hamburger[_ngcontent-%COMP%]    > .ekit-menu-icon[_ngcontent-%COMP%]{color:#333333;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-menu-hamburger[_ngcontent-%COMP%]:hover   .elementskit-menu-hamburger-icon[_ngcontent-%COMP%]{background-color:#333333;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-menu-hamburger[_ngcontent-%COMP%]:hover    > .ekit-menu-icon[_ngcontent-%COMP%]{color:#333333;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-menu-close[_ngcontent-%COMP%]{background-color:rgba(2, 1, 1, 0);border-style:solid;color:rgba(51, 51, 51, 1);}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-menu-close[_ngcontent-%COMP%]:hover{color:rgba(0, 0, 0, 0.5);}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px 16px 0px 0px;}\r\n.elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]{width:auto;max-width:auto;}\r\n.elementor-element.elementor-element-11ad39b4[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]{font-size:15px;font-weight:500;fill:#FFFFFF;color:#FFFFFF;background-color:#C84040;border-radius:25px 25px 25px 25px;padding:10px 25px 10px 25px;}\r\n.elementor-element.elementor-element-11ad39b4[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]:hover, .elementor-element.elementor-element-11ad39b4[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]:focus{color:#FFFFFF;background-color:#9B0303;}\r\n.elementor-element.elementor-element-11ad39b4[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]:hover   svg[_ngcontent-%COMP%], .elementor-element.elementor-element-11ad39b4[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]:focus   svg[_ngcontent-%COMP%]{fill:#FFFFFF;}\r\n.elementor-element.elementor-element-11ad39b4[_ngcontent-%COMP%]{width:auto;max-width:auto;}\r\n.elementor-element.elementor-element-9f46611[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]{font-family:\"Nunito Sans\", Sans-serif;font-size:15px;font-weight:600;text-transform:none;letter-spacing:0px;fill:#131b5c;color:#131b5c;background-color:#131B5C00;border-style:solid;border-width:2px 2px 2px 2px;border-color:#131b5c;padding:8px 17px 8px 15px;}\r\n.elementor-element.elementor-element-9f46611[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]:hover, .elementor-element.elementor-element-9f46611[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]:focus{color:#7E7D7D;border-color:#C0C0C0;}\r\n.elementor-element.elementor-element-9f46611[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]:hover   svg[_ngcontent-%COMP%], .elementor-element.elementor-element-9f46611[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]:focus   svg[_ngcontent-%COMP%]{fill:#7E7D7D;}\r\n.elementor-element.elementor-element-9f46611[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px 0px 0px 7px;}\r\n.elementor-element.elementor-element-9f46611[_ngcontent-%COMP%]{width:auto;max-width:auto;}\r\n[_ngcontent-%COMP%]:root{--page-title-display:none;}\r\n@media(max-width:1024px){ .elementor-element.elementor-element-269e61d[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{width:100%;} .elementor-element.elementor-element-269e61d[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px -20px 0px 0px;border-width:0px 0px 0px 0px;}.elementor-bc-flex-widget[_ngcontent-%COMP%]   .elementor-element.elementor-element-4cb2f502.elementor-column[_ngcontent-%COMP%]   .elementor-widget-wrap[_ngcontent-%COMP%]{align-items:center;} .elementor-element.elementor-element-4cb2f502.elementor-column.elementor-element[data-element_type=\"column\"][_ngcontent-%COMP%]    > .elementor-widget-wrap.elementor-element-populated[_ngcontent-%COMP%]{align-content:center;align-items:center;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-nav-identity-panel[_ngcontent-%COMP%]{padding:10px 0px 10px 0px;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-menu-container[_ngcontent-%COMP%]{max-width:350px;border-radius:0px 0px 0px 0px;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]{background-size:0px auto;color:#333333;padding:8px 32px 8px 32px;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:hover, .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:focus, .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:active, .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]:hover    > a[_ngcontent-%COMP%]{background-size:0px auto;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:hover{color:#00BDAA;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:focus{color:#00BDAA;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:active{color:#00BDAA;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]:hover    > a[_ngcontent-%COMP%]{color:#00BDAA;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]:hover    > a[_ngcontent-%COMP%]   .elementskit-submenu-indicator[_ngcontent-%COMP%]{color:#00BDAA;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:hover   .elementskit-submenu-indicator[_ngcontent-%COMP%]{color:#00BDAA;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:focus   .elementskit-submenu-indicator[_ngcontent-%COMP%]{color:#00BDAA;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:active   .elementskit-submenu-indicator[_ngcontent-%COMP%]{color:#00BDAA;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li.current-menu-item[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]{color:#00BDAA;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li.current-menu-ancestor[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]{color:#00BDAA;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li.current-menu-ancestor[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]   .elementskit-submenu-indicator[_ngcontent-%COMP%]{color:#00BDAA;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]   .elementskit-submenu-panel[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]{padding:8px 8px 8px 8px;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]   .elementskit-submenu-panel[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:hover{color:#5A6CFF;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]   .elementskit-submenu-panel[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:focus{color:#5A6CFF;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]   .elementskit-submenu-panel[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:active{color:#5A6CFF;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]   .elementskit-submenu-panel[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]:hover    > a[_ngcontent-%COMP%]{color:#5A6CFF;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]   .elementskit-submenu-panel[_ngcontent-%COMP%]{border-width:0px 0px 0px 40px;border-radius:0px 0px 0px 0px;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-menu-hamburger[_ngcontent-%COMP%]{padding:8px 8px 8px 8px;width:45px;border-radius:3px;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-menu-close[_ngcontent-%COMP%]{padding:0px 8px 8px 8px;margin:12px 12px 12px 12px;width:45px;border-radius:3px;border-width:0px 0px 0px 0px;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-nav-logo[_ngcontent-%COMP%]    > img[_ngcontent-%COMP%]{max-width:70px;max-height:60px;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-nav-logo[_ngcontent-%COMP%]{margin:24px 24px 0px 24px;padding:5px 5px 5px 5px;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px 32px 0px 0px;}}\r\n@media(min-width:768px){ .elementor-element.elementor-element-435289e6[_ngcontent-%COMP%]{width:23.289%;} .elementor-element.elementor-element-4cb2f502[_ngcontent-%COMP%]{width:76.711%;}}\r\n@media(max-width:767px){ .elementor-element.elementor-element-435289e6[_ngcontent-%COMP%]{width:30%;} .elementor-element.elementor-element-269e61d[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{width:100%;} .elementor-element.elementor-element-269e61d[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px -30px 0px 0px;border-width:0px 0px 0px 0px;} .elementor-element.elementor-element-4cb2f502[_ngcontent-%COMP%]{width:70%;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:hover, .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:focus, .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:active, .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]:hover    > a[_ngcontent-%COMP%]{background-size:0px auto;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li.current-menu-item[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%], .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-navbar-nav[_ngcontent-%COMP%]    > li.current-menu-ancestor[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]{background-size:0px auto;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]   .elementskit-nav-logo[_ngcontent-%COMP%]    > img[_ngcontent-%COMP%]{max-width:70px;max-height:50px;} .elementor-element.elementor-element-8956bb7[_ngcontent-%COMP%]    > .elementor-widget-container[_ngcontent-%COMP%]{margin:0px 8px 0px 0px;} .elementor-element.elementor-element-11ad39b4[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]{padding:8px 12px 8px 12px;} .elementor-element.elementor-element-9f46611[_ngcontent-%COMP%]   .elementor-button[_ngcontent-%COMP%]{padding:0px 0px 0px 8px;}}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBO0VBQ0UsOEJBQThCO0VBQzlCLHVCQUF1Qjs7QUFFekI7QUFDQTtFQUNFLFlBQVk7QUFDZDtBQUVBO0VBQ0UseUJBQXlCO0VBQ3pCLGtCQUFrQjtBQUNwQjtBQUVBO0VBQ0UsbUJBQW1CO0VBQ25CLDhCQUE4QjtFQUM5QixhQUFhO0VBQ2IsZUFBZTtFQUNmO0VBQ0E7QUFDQTtNQUNJLHVCQUF1QjtFQUMzQjtBQUNBO01BQ0kseUJBQXlCO0VBQzdCO0FBQ0Y7TUFDTSx1QkFBdUI7TUFDdkIsWUFBWTtNQUNaLGVBQWU7TUFDZixtQkFBbUI7TUFDbkIsWUFBWTtNQUNaLGtCQUFrQjtFQUN0QixZQUFZO0VBQ1osYUFBYTtFQUNiLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkI7QUFDQTtNQUNJLDhCQUE4Qjs7RUFFbEM7QUFDQTtNQUNJLHVCQUF1QjtFQUMzQjtBQUNBO01BQ0kseUJBQXlCO0VBQzdCO0FBQ0E7TUFDSSx5QkFBeUI7O0VBRTdCO0FBQ0E7TUFDSSxXQUFXO01BQ1gsbUJBQW1CO0VBQ3ZCO0FBQ0c7TUFDQyxxQkFBcUI7TUFDckIscUJBQXFCO01BQ3JCLHdCQUF3QjtNQUN4QixrQkFBa0I7TUFDbEIsa0JBQWtCO01BQ2xCLG9CQUFvQjtNQUNwQixtQkFBbUI7TUFDbkIsaUJBQWlCO01BQ2pCLFVBQVU7RUFDZDtBQUdGLDRHQUE0RztBQUM1RztBQUNBLGVBQWU7QUFDZixlQUFlO0FBQ2YsbUJBQW1CO0FBQ25CLE9BQU87QUFDUCxRQUFRO0FBQ1IsYUFBYTtBQUNiLGFBQWE7QUFDYix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CLFNBQVM7QUFDVCxZQUFZO0FBQ1o7QUFFQTtBQUNBLFVBQVU7QUFDVixtQkFBbUI7QUFDbkIsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qiw4QkFBOEI7QUFDOUI7QUFDQTtBQUNBLFlBQVk7QUFDWixZQUFZO0FBQ1osYUFBYTtBQUNiLGFBQWE7QUFDYix1QkFBdUI7QUFDdkIscUJBQXFCO0FBQ3JCLGtCQUFrQjtBQUNsQjtBQUVBO0FBQ0EscUJBQXFCO0FBQ3JCLGNBQWM7QUFDZCxzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCLGVBQWU7QUFDZjtBQUNBO0FBQ0EsWUFBWTtBQUNaLGFBQWE7QUFDYixpQkFBaUI7QUFDakI7QUFDQTtBQUNBLGtCQUFrQjtBQUNsQixTQUFTO0FBQ1QsWUFBWTtBQUNaLGVBQWU7QUFDZjtBQUdDO0NBQ0EsWUFBWTtDQUNaLGtCQUFrQjtDQUNsQixZQUFZO0NBQ1osUUFBUTtDQUNSLFlBQVk7Q0FDWixlQUFlO0NBQ2YsVUFBVTtDQUNWLGlCQUFpQjtDQUNqQjtBQUNBO0VBQ0MscUJBQXFCO0dBQ3BCLGtCQUFrQjtHQUNsQixhQUFhO0dBQ2IsUUFBUTtHQUNSLFlBQVk7R0FDWixlQUFlO0dBQ2YsVUFBVTtHQUNWLGlCQUFpQjtHQUNqQixZQUFZO0dBQ1osbUJBQW1CO0dBQ25CLGtCQUFrQjtHQUNsQixZQUFZO0dBQ1osYUFBYTtHQUNiLHVCQUF1QjtHQUN2QixtQkFBbUI7Q0FDckI7QUFDQztDQUNELGFBQWE7Q0FDYjtBQUVBO0VBQ0MscUJBQXFCO0VBQ3JCLGNBQWM7RUFDZCxzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLGVBQWU7Q0FDaEI7QUFDQTtFQUNDLFlBQVk7RUFDWixhQUFhO0VBQ2IsaUJBQWlCO0NBQ2xCO0FBQ0E7RUFDQyxrQkFBa0I7RUFDbEIsU0FBUztFQUNULFlBQVk7RUFDWixlQUFlO0NBQ2hCO0FBSUQsZ0hBQWdIO0FBQ2hILG1IQUFtSDtBQUVuSDtFQUNFLFdBQVc7RUFDWCxXQUFXO0VBQ1gsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxrQkFBa0I7Q0FDbkI7QUFDQTtFQUNDLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLE1BQU07RUFDTixPQUFPO0VBQ1AsV0FBVztFQUNYLGVBQWU7Q0FDaEI7QUFDQTtFQUNDLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLE1BQU07RUFDTixRQUFRO0VBQ1IsV0FBVztFQUNYLGVBQWU7Q0FDaEI7QUFFRCxrSEFBa0g7QUFDbEg7RUFDRSxrQkFBa0I7RUFDbEI7QUFDQTtJQUNFLGFBQWE7SUFDYixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixzQkFBc0I7SUFDdEI7QUFDSjtFQUNFLGtCQUFrQjtFQUNsQiw0QkFBdUI7RUFBdkIsdUJBQXVCO0VBQ3ZCLGlDQUFpQztDQUNsQztBQUNBO0VBQ0MsY0FBYztFQUNkLGlCQUFpQjtFQUNqQixhQUFhO0VBQ2IsYUFBYTtDQUNkO0FBQ0E7RUFDQyxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixrQkFBa0I7Q0FDbkI7QUFDQTtFQUNDLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGtCQUFrQjtDQUNuQjtBQUNBO0VBQ0MsWUFBWTtFQUNaLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakI7QUFHQTtJQUNFLHNCQUFzQjtHQUN2QjtBQUNBO0dBQ0EsbUJBQW1CO0dBQ25CO0FBQ0E7R0FDQSxhQUFhO0dBQ2IsbUJBQW1CO0dBQ25CLDJCQUEyQjtHQUMzQixrQkFBa0I7R0FDbEIsWUFBWTtHQUNaLGtCQUFrQjtHQUNsQjtBQUNBO0lBQ0MsU0FBUztJQUNULGtCQUFrQjtJQUNsQixRQUFRO0lBQ1IsNENBQTRDO0lBQzVDLFlBQVk7R0FDYjtBQUNBO0lBQ0MsV0FBVztJQUNYLGNBQWM7SUFDZCxrQkFBa0I7R0FDbkI7QUFDQTtJQUNDLDBCQUEwQjtJQUMxQixtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLDZCQUE2QjtJQUM3QixzQkFBc0I7SUFDdEIsV0FBVztHQUNaO0FBQ0E7SUFDQyxtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCx5QkFBeUI7SUFDekIseUJBQXlCO0lBQ3pCLDZCQUE2QjtJQUM3QixXQUFXO0dBQ1o7QUFDQTtJQUNDLHVCQUF1QjtJQUN2QixZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLGNBQWM7SUFDZCxlQUFlO0lBQ2YsWUFBWTtJQUNaLGlCQUFpQjtJQUNqQixVQUFVO0lBQ1YsYUFBYTtJQUNiLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsNkJBQTZCO0lBQzdCLHNCQUFzQjtJQUN0QixXQUFXO0lBQ1gsVUFBVTtJQUNWLHlCQUF5QjtHQUMxQjtBQUNBO0lBQ0Msc0JBQXNCO0dBQ3ZCO0FBQ0E7SUFDQyxzQkFBc0I7R0FDdkI7QUFDQTtJQUNDLG9CQUFvQjtHQUNyQjtBQUNBO0lBQ0MsMERBQTBEO0lBQzFELFFBQVE7R0FDVDtBQUNBO0lBQ0MseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osWUFBWTtHQUNiO0FBQ0E7SUFDQyxVQUFVO0lBQ1YsWUFBWTtHQUNiO0FBSUg7RUFDRSxZQUFZO0VBQ1osZUFBZTtFQUNmLHFCQUFxQjs7RUFFckIsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtFQUNaLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsVUFBVTtFQUNWLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sUUFBUTtFQUNSLGdCQUFnQjtFQUNoQixVQUFVO0VBQ1YsbURBQW1EO0VBQ25ELGVBQWU7RUFDZjtBQUVBO0VBQ0EsOEJBQThCO0VBQzlCO0FBRUE7RUFDQSxZQUFZO0VBQ1osVUFBVTtFQUNWLDhCQUE4QjtFQUM5QixZQUFZO0VBQ1o7QUFDQTtFQUNBLFlBQVk7RUFDWixXQUFXO0VBQ1gscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixZQUFZO0VBQ1o7MkJBQ3lCO0VBQ3pCLHFCQUFxQjtFQUNyQixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLE1BQU07RUFDTixRQUFRO0VBQ1IsVUFBVTtFQUNWLGVBQWU7RUFDZixZQUFZO0VBQ1osZUFBZTtFQUNmLDZCQUE2QjtFQUM3QjtBQUVBO0VBQ0EsWUFBWTtFQUNaO0FBSUQsd0hBQXdIO0FBRXpIO0VBQ0UsU0FBUztFQUNULGdCQUFnQjtFQUNoQixjQUFjO0FBQ2hCO0FBQ0E7RUFDRSxnQkFBZ0I7RUFDaEIsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsbUJBQW1CO0FBQ3JCO0FBQ0E7RUFDRSxTQUFTO0VBQ1QsY0FBYztBQUNoQjtBQUNBO0VBQ0UsYUFBYTtFQUNiLDhCQUE4QjtFQUM5QixTQUFTO0VBQ1QsVUFBVTtBQUNaO0FBQ0E7RUFDRSx5QkFBeUI7RUFDekIsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixjQUFjO0FBQ2hCO0FBQ0E7RUFDRSxlQUFlO0VBQ2YsY0FBYztBQUNoQjtBQUNBO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixhQUFhO0VBQ2IsbUJBQW1COztBQUVyQjtBQUdBO0VBQ0UsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZjtBQUdGO0VBQ0UsYUFBYTtjQUNELHNCQUFzQjtjQUN0Qix1QkFBdUI7Y0FDdkIsVUFBVTtjQUNWLG9CQUFvQjtFQUNoQztBQUVELGtIQUFrSDtBQUVuSDtFQUNFLHlCQUF5QjtFQUN6QixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGNBQWM7Q0FDZjtBQUNBO0VBQ0MsZUFBZTtFQUNmLGNBQWM7Q0FDZjtBQUNBO0VBQ0MsYUFBYTtFQUNiLDhCQUE4QjtFQUM5QixTQUFTO0VBQ1QsVUFBVTtDQUNYO0FBQ0E7RUFDQyxnQkFBZ0I7RUFDaEIsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsbUJBQW1CO0NBQ3BCO0FBQ0E7RUFDQyxTQUFTO0VBQ1QsZ0JBQWdCO0VBQ2hCLGNBQWM7Q0FDZjtBQUNBO0VBQ0MsU0FBUztFQUNULGNBQWM7Q0FDZjtBQUVBLGtIQUFrSDtBQUNsSDtFQUNDLHlCQUF5QjtFQUN6QixtQkFBbUI7Q0FDcEI7QUFDQTtFQUNDLG1CQUFtQjtFQUNuQixpQkFBaUI7Q0FDbEI7QUFDQTtFQUNDLG1CQUFtQjtDQUNwQjtBQUNBO0VBQ0MsWUFBWTtFQUNaLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsWUFBWTtFQUNaLGlCQUFpQjtDQUNsQjtBQUNBO0VBQ0MsbUJBQW1CO0NBQ3BCO0FBQ0E7RUFDQyxrQkFBa0I7Q0FDbkI7QUFDQTtFQUNDLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLHNCQUFzQjtFQUN0QixZQUFZO0VBQ1oscUJBQXFCO0VBQ3JCLHlCQUF5QjtFQUN6Qix1QkFBdUI7RUFDdkIsWUFBWTtDQUNiO0FBQ0E7RUFDQyxVQUFVO0VBQ1YsaUJBQWlCO0NBQ2xCO0FBRUE7RUFDQyx1QkFBdUI7R0FDdEIsWUFBWTtHQUNaLGFBQWE7RUFDZDtBQUNBO0dBQ0MsdUJBQXVCO0VBQ3hCO0FBRUE7SUFDRSxtQkFBbUI7SUFDbkIsZ0JBQWdCO0VBQ2xCO0FBQ0E7RUFDQSxtQkFBbUI7RUFDbkI7QUFDQTtJQUNFLG1CQUFtQjtFQUNyQjtBQUNBO0lBQ0UsWUFBWTtJQUNaLGdCQUFnQjtJQUNoQixZQUFZO0lBQ1osb0JBQW9CO0VBQ3RCO0FBQ0E7SUFDRSxZQUFZO0lBQ1osWUFBWTtJQUNaLG9CQUFvQjtFQUN0QjtBQUNBO0lBQ0UsWUFBWTtJQUNaLFlBQVk7SUFDWixvQkFBb0I7RUFDdEI7QUFDQTtJQUNFLFlBQVk7SUFDWixlQUFlO0lBQ2Y7QUFDSCxrSEFBa0g7QUFFbkg7RUFDRSxvQkFBb0I7RUFDcEIsWUFBWTtBQUNkO0FBQ0E7RUFDRSxpQkFBaUI7QUFDbkI7QUFDQTtFQUNFLGVBQWU7RUFDZjtBQUNGO0FBQ0E7RUFDRSxpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCO0FBQ0Y7QUFFQyxrSEFBa0g7QUFFbkg7RUFDRSxnQ0FBMEQ7RUFDMUQsNEJBQTRCO0VBQzVCLCtCQUErQjtFQUMvQixnQkFBZ0I7QUFDbEI7QUFDQTtFQUNFLGFBQWE7RUFDYixpQkFBaUI7QUFDbkI7QUFFQSxrQkFBa0Isb0JBQW9CLENBQUM7QUFHdkM7O0VBRUUsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2Qsa0JBQWtCO0FBQ3BCO0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsd0NBQXdDO0FBQzFDO0FBQ0M7QUFDRDtBQUNBO0FBQ0M7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7RUFDRSxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQixZQUFZO0VBQ1osZUFBZTtFQUNmLGtCQUFrQjs7QUFFcEI7QUFJQTtFQUNFLGVBQWU7RUFDZixXQUFXO0VBQ1gsaUJBQWlCO0VBQ2pCLG1DQUEyQjtVQUEzQiwyQkFBMkI7RUFDM0IsNkNBQTZDO0VBQzdDLDZCQUE2QjtFQUM3QixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLE1BQU07RUFDTixPQUFPO0FBQ1Q7QUFHQTtFQUNFLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsYUFBYTtFQUNiLFlBQVk7RUFDWixjQUFjO0VBQ2Qsa0JBQWtCOztFQUVsQix3QkFBd0I7RUFDeEIsZ0JBQWdCO0VBQ2hCOzttQkFFaUI7QUFDbkI7QUFHQTtFQUNFLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsYUFBYTtFQUNiLFlBQVk7RUFDWixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLHdCQUF3QjtFQUN4QixnQkFBZ0I7RUFDaEI7O21CQUVpQjtBQUNuQjtBQUdBO0VBQ0Usa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxhQUFhO0VBQ2IsWUFBWTtFQUNaLGNBQWM7RUFDZCxrQkFBa0I7O0VBRWxCLHdCQUF3QjtFQUN4QixnQkFBZ0I7RUFDaEI7O21CQUVpQjtBQUNuQjtBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxjQUFjO0VBQ2QseUJBQXlCO0VBQ3pCLGVBQWU7RUFDZixtQ0FBMkI7VUFBM0IsMkJBQTJCO0FBQzdCO0FBRUE7RUFDRSxnQkFBZ0I7RUFDaEIscUJBQXFCO0VBQ3JCLHlCQUF5QjtFQUN6QixlQUFlO0FBQ2pCO0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsU0FBUztFQUNULFdBQVc7RUFDWCxlQUFlO0VBQ2YscUJBQXFCO0VBQ3JCLGdCQUFnQjtFQUNoQixxQ0FBcUM7QUFDdkM7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixNQUFNO0VBQ04sZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixXQUFXO0FBQ2I7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsZUFBZTtFQUNmLFVBQVU7RUFDVixtQ0FBMkI7VUFBM0IsMkJBQTJCOztFQUUzQixnQkFBZ0I7RUFDaEIseUJBQXlCO0VBQ3pCLG9CQUFvQjs7RUFFcEIscUJBQXFCO0VBQ3JCLGlCQUFpQjtFQUNqQixtQkFBbUI7O0VBRW5CLGVBQWU7RUFDZixZQUFZO0FBQ2Q7QUFPQyxrSEFBa0g7QUFDbEg7RUFDQyxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWO0FBQ0Y7QUFDQyxrSEFBa0g7QUFFbkg7RUFDRSxpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQjtBQUNGO0FBQ0E7RUFDRSxtQkFBbUI7R0FDbEIseUJBQXlCO0lBQ3hCLGdCQUFnQjtJQUNoQixVQUFVO0VBQ1osYUFBYTtBQUNmO0FBQ0E7RUFDRSxlQUFlO0VBQ2YsZ0JBQWdCO0FBQ2xCO0FBQ0E7QUFDQSxzQkFBc0I7QUFDdEI7QUFDQTtFQUNFLFdBQVc7QUFDYjtBQUdDLGtIQUFrSDtBQUNsSDtFQUNDLFdBQVc7O0FBRWI7QUFDQTtFQUNFO0FBQ0Y7QUFFQTtFQUNFLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxZQUFZO0FBQy9DO0FBQ0E7RUFDRSw2QkFBNkI7RUFDN0IsWUFBWTtFQUNaLGtDQUFrQztFQUNsQyx5QkFBeUI7RUFDekIsYUFBYTtBQUNmO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsYUFBYTtFQUNiLFlBQVk7QUFDZDtBQUNBO0VBQ0UsNkJBQTZCO0VBQzdCLFlBQVk7RUFDWixrQ0FBa0M7RUFDbEMseUJBQXlCO0VBQ3pCLGFBQWE7QUFDZjtBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixZQUFZO0FBQ2Q7QUFDQTtFQUNFLDZCQUE2QjtFQUM3QixZQUFZO0VBQ1osa0NBQWtDO0VBQ2xDLHlCQUF5QjtFQUN6QixhQUFhO0FBQ2Y7QUFDQTtFQUNFLHNCQUFzQjtFQUN0QixlQUFlO0VBQ2Y7QUFDRjtBQUNBO0VBQ0UsdUJBQXVCO0VBQ3ZCLGVBQWU7RUFDZjtBQUNGO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtFQUNaLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsd0JBQXdCO0VBQ3hCLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsV0FBVztFQUNYO0FBQ0Y7QUFFQTtFQUNFLHNCQUFzQjtFQUN0QixlQUFlO0dBQ2Q7QUFDSDtBQUNBO0VBQ0UsdUJBQXVCO0VBQ3ZCLGVBQWU7RUFDZjtBQUNGO0FBQ0E7RUFDRSxrQkFBa0I7R0FDakIsV0FBVztFQUNaLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtFQUNaLGNBQWM7RUFDZCxrQkFBa0I7R0FDakIsd0JBQXdCO0dBQ3hCLGdCQUFnQjtBQUNuQjtBQUNBO0VBQ0UsV0FBVztFQUNYO0FBQ0Y7QUFFQTtFQUNFLHNCQUFzQjtFQUN0QixlQUFlO0dBQ2Q7QUFDSDtBQUNBO0VBQ0UsdUJBQXVCO0VBQ3ZCLGVBQWU7RUFDZjtBQUNGO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFlBQVk7RUFDWixhQUFhO0VBQ2IsWUFBWTtFQUNaLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsd0JBQXdCO0VBQ3hCLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0UsV0FBVztFQUNYO0FBQ0Y7QUFDQSwrR0FBK0c7QUFHL0c7QUFDQSxpQkFBaUI7QUFDakIsY0FBYztBQUNkLHFCQUFxQjtBQUNyQjtBQUVBO0FBQ0EsY0FBYztBQUNkLGVBQWU7QUFDZjtBQUNBO0FBQ0EsZUFBZTtBQUNmLGNBQWM7O0FBRWQ7QUFDQTtFQUNFLGVBQWU7QUFDakIsY0FBYztBQUNkO0FBR0E7RUFDRSx5QkFBeUI7RUFDekIsVUFBVTtFQUNWLGNBQWM7RUFDZCxrQkFBa0I7QUFDcEI7QUFFRSwwR0FBMEc7QUFHMUcsMkJBQTJCO0FBQ3pCOztFQUVGLHNHQUFzRztJQUNwRztRQUNJLHFCQUFxQjtRQUNyQixxQkFBcUI7UUFDckIsd0JBQXdCO1FBQ3hCLGtCQUFrQjtRQUNsQixrQkFBa0I7UUFDbEIsb0JBQW9CO1FBQ3BCLG1CQUFtQjtRQUNuQixjQUFjO1FBQ2QsVUFBVTtFQUNoQjs7RUFFQTtJQUNFLHlCQUF5QjtJQUN6QixnQkFBZ0I7RUFDbEI7O0FBRUY7RUFDRSxzQkFBc0I7RUFDdEIsOEJBQThCO0VBQzlCLGFBQWE7RUFDYixlQUFlO0VBQ2Ysa0JBQWtCO0VBQ2xCO0FBQ0YsdUhBQXVIO0FBQ3ZIO0VBQ0UsZUFBZTtBQUNqQixlQUFlO0FBQ2YsbUJBQW1CO0FBQ25CLE9BQU87QUFDUCxRQUFRO0FBQ1IsYUFBYTtBQUNiLGFBQWE7QUFDYix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CLFNBQVM7QUFDVCxVQUFVO0FBQ1YsWUFBWTtBQUNaO0FBQ0E7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLGVBQWU7QUFDakIsZUFBZTtBQUNmLG1CQUFtQjtBQUNuQixPQUFPO0FBQ1AsUUFBUTtBQUNSLFlBQVk7QUFDWixhQUFhO0FBQ2IsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixTQUFTO0FBQ1QsVUFBVTtBQUNWLGlCQUFpQjtBQUNqQixlQUFlOztBQUVmO0FBQ0E7RUFDRSxZQUFZO0VBQ1osWUFBWTtFQUNaLGVBQWU7QUFDakIsZUFBZTtBQUNmLG1CQUFtQjtBQUNuQixPQUFPO0FBQ1AsUUFBUTtBQUNSLGFBQWE7QUFDYixhQUFhO0FBQ2IsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixTQUFTO0FBQ1QsVUFBVTtBQUNWO0FBQ0E7RUFDRSxVQUFVO0VBQ1YsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsOEJBQThCO0FBQ2hDO0FBQ0E7RUFDRSxlQUFlO0VBQ2YsZUFBZTtFQUNmLG1CQUFtQjtFQUNuQixPQUFPO0VBQ1AsUUFBUTtFQUNSLGFBQWE7RUFDYixhQUFhO0VBQ2IsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixVQUFVO0VBQ1YsWUFBWTtFQUNaLGNBQWM7QUFDaEI7O0FBRUE7QUFDQSxVQUFVO0FBQ1YsbUJBQW1CO0FBQ25CLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsOEJBQThCO0FBQzlCO0VBQ0UsaUdBQWlHO0VBQ2pHO0lBQ0UsNEJBQTRCO0VBQzlCO0VBQ0E7SUFDRSx5QkFBeUI7SUFDekIsbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixZQUFZO0VBQ2Q7RUFDQTtJQUNFLFVBQVU7SUFDVixZQUFZO0VBQ2Q7O0VBRUE7SUFDRSxlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGNBQWM7SUFDZCxrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixZQUFZO0NBQ2Y7OztDQUdBO0VBQ0MsYUFBYTtFQUNiLFlBQVk7O0VBRVosbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixrQkFBa0I7RUFDbEIscUJBQXFCO0VBQ3JCLG1CQUFtQjtFQUNuQixrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLGFBQWE7RUFDYixxQkFBcUI7RUFDckIsdUJBQWtCO0VBQWxCLGtCQUFrQjtFQUNsQixzQkFBc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQixTQUFTO0FBQ1g7QUFDQTtFQUNFLFlBQVk7RUFDWixZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsaUJBQWlCO0VBQ2pCLG1CQUFtQjtBQUNyQjtBQUNBO0VBQ0UsdUJBQWtCO0VBQWxCLGtCQUFrQjtBQUNwQjs7QUFFQTtFQUNFLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLHFCQUFxQjtFQUNyQixrQkFBa0I7QUFDcEI7OztBQUdBLHlHQUF5Rzs7OztBQUl6RztFQUNFLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsU0FBUztBQUNYO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkIsdUJBQXVCO0FBQ3ZCLFlBQVk7QUFDWixTQUFTO0FBQ1Q7QUFDQTtFQUNFLG1CQUFtQjtFQUNuQixTQUFTO0FBQ1g7QUFDQTtFQUNFLFlBQVk7RUFDWixnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLG9CQUFvQjtBQUN0QjtBQUNBO0VBQ0UsWUFBWTtFQUNaLFlBQVk7RUFDWixvQkFBb0I7QUFDdEI7QUFDQTtFQUNFLFlBQVk7RUFDWixZQUFZO0VBQ1osb0JBQW9CO0FBQ3RCOzs7O0FBSUEsZ0hBQWdIO0FBQ2hIO0VBQ0UsYUFBYTtBQUNmLHNCQUFzQjtBQUN0Qix1QkFBdUI7QUFDdkIsV0FBVztBQUNYLG1CQUFtQjtBQUNuQjs7QUFFQTtFQUNFLGVBQWU7QUFDakI7QUFDQTtFQUNFLGlCQUFpQjtFQUNqQixnQkFBZ0I7RUFDaEIsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsbUJBQW1CO0FBQ3JCO0NBQ0MsOEdBQThHO0NBQzlHO0VBQ0MsZ0JBQWdCO0VBQ2hCLFlBQVk7RUFDWjs7RUFFQSwrRkFBK0Y7O0FBRWpHO0VBQ0UsZ0NBQTBEO0VBQzFELDRCQUE0QjtFQUM1QiwrQkFBK0I7RUFDL0IsZ0JBQWdCO0VBQ2hCO0VBQ0E7RUFDQSxhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2QjtFQUNBO0VBQ0EsZ0JBQWdCO0VBQ2hCOztHQUVDO0VBQ0QsbUJBQW1CO0VBQ25CO0dBQ0M7RUFDRCxTQUFTO0VBQ1Q7R0FDQztFQUNELFNBQVM7RUFDVDtFQUNBO0lBQ0UsaUJBQWlCO0VBQ25COztFQUVBLGtHQUFrRztFQUNsRztFQUNBLGVBQWU7RUFDZixXQUFXO0VBQ1g7RUFDQTtFQUNBLGNBQWM7RUFDZCxXQUFXO0VBQ1g7RUFDQTtFQUNBLGtHQUFrRztFQUNsRztFQUNBLGlCQUFpQjtFQUNqQixjQUFjO0VBQ2QsYUFBYTtFQUNiLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsc0JBQXNCO0VBQ3RCLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkI7O0VBRUE7SUFDRSx3QkFBd0I7RUFDMUI7OztDQUdELGtIQUFrSDtDQUNsSDtFQUNDLGlCQUFpQjtBQUNuQjs7SUFFSTtNQUNFLGlCQUFpQjtNQUNqQixhQUFhO01BQ2IsWUFBWTtNQUNaO01BQ0E7TUFDQSw2QkFBNkI7TUFDN0IsWUFBWTtNQUNaLGtDQUFrQztNQUNsQyx5QkFBeUI7TUFDekIsYUFBYTtNQUNiO01BQ0E7UUFDRSxpQkFBaUI7UUFDakIsYUFBYTtRQUNiLFlBQVk7TUFDZDtNQUNBO1FBQ0UsNkJBQTZCO1FBQzdCLFlBQVk7UUFDWixrQ0FBa0M7UUFDbEMseUJBQXlCO1FBQ3pCLGFBQWE7UUFDYjtNQUNGO1FBQ0UsaUJBQWlCO1FBQ2pCLGFBQWE7UUFDYixZQUFZO01BQ2Q7TUFDQTtRQUNFLDZCQUE2QjtRQUM3QixZQUFZO1FBQ1osa0NBQWtDO1FBQ2xDLHlCQUF5QjtRQUN6QixhQUFhO1FBQ2I7O01BRUY7TUFDQSxVQUFVO01BQ1YsWUFBWTtNQUNaOzs7TUFHQTtNQUNBLG1CQUFtQjtNQUNuQjs7SUFFRjtBQUVILGdDQUFnQztBQUM3Qjs7O0VBR0Ysc0dBQXNHO0VBQ3RHO0lBQ0UscUJBQXFCO0lBQ3JCLHFCQUFxQjtJQUNyQix3QkFBd0I7SUFDeEIsa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixvQkFBb0I7SUFDcEIsbUJBQW1CO0lBQ25CLGNBQWM7SUFDZCxVQUFVO0FBQ2Q7QUFDQTtFQUNFLFdBQVc7RUFDWCxtQkFBbUI7RUFDbkIsaUJBQWlCO0FBQ25COztBQUVBO0VBQ0UseUJBQXlCO0VBQ3pCLGdCQUFnQjtBQUNsQjtBQUNBO0VBQ0Usc0JBQXNCO0VBQ3RCLDhCQUE4QjtFQUM5QixhQUFhO0VBQ2IsZUFBZTtFQUNmLGtCQUFrQjtFQUNsQjs7QUFFRix1SEFBdUg7QUFDdkg7QUFDQSxlQUFlO0FBQ2YsZUFBZTtBQUNmLG1CQUFtQjtBQUNuQixPQUFPO0FBQ1AsUUFBUTtBQUNSLGFBQWE7QUFDYixhQUFhO0FBQ2IsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixTQUFTO0FBQ1QsVUFBVTtBQUNWLFlBQVk7O0FBRVo7QUFDQTtBQUNBLFlBQVk7QUFDWixZQUFZO0FBQ1osZUFBZTtBQUNmLGVBQWU7QUFDZixtQkFBbUI7QUFDbkIsT0FBTztBQUNQLFFBQVE7QUFDUixZQUFZO0FBQ1osYUFBYTtBQUNiLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkIsU0FBUztBQUNULFVBQVU7QUFDVixpQkFBaUI7QUFDakIsZUFBZTs7QUFFZjtBQUNBO0FBQ0EsWUFBWTtBQUNaLFlBQVk7QUFDWixlQUFlO0FBQ2YsZUFBZTtBQUNmLG1CQUFtQjtBQUNuQixPQUFPO0FBQ1AsUUFBUTtBQUNSLGFBQWE7QUFDYixhQUFhO0FBQ2IsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixTQUFTO0FBQ1QsVUFBVTtBQUNWO0FBQ0E7QUFDQSxVQUFVO0FBQ1YsbUJBQW1CO0FBQ25CLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsOEJBQThCO0FBQzlCO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsZUFBZTtBQUNmLG1CQUFtQjtBQUNuQixPQUFPO0FBQ1AsUUFBUTtBQUNSLGFBQWE7QUFDYixhQUFhO0FBQ2IsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixVQUFVO0FBQ1YsWUFBWTtBQUNaLGNBQWM7QUFDZDs7QUFFQTtBQUNBLFVBQVU7QUFDVixtQkFBbUI7QUFDbkIsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qiw4QkFBOEI7QUFDOUI7QUFDQSxpR0FBaUc7QUFDakc7QUFDQSw0QkFBNEI7QUFDNUI7QUFDQTtBQUNBLHlCQUF5QjtBQUN6QixtQkFBbUI7QUFDbkIsWUFBWTtBQUNaLFlBQVk7QUFDWjtBQUNBO0FBQ0EsVUFBVTtBQUNWLFlBQVk7QUFDWjs7QUFFQTtFQUNFLFdBQVc7QUFDYjtBQUNBO0VBQ0UsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixhQUFhO0VBQ2IsWUFBWTtBQUNkO0FBQ0E7QUFDQSxhQUFhO0FBQ2IsbUJBQW1CO0FBQ25CLHVCQUF1QjtBQUN2QixrQkFBa0I7QUFDbEIscUJBQXFCO0FBQ3JCLG1CQUFtQjtBQUNuQixrQkFBa0I7QUFDbEI7QUFDQTtBQUNBLGFBQWE7QUFDYixxQkFBcUI7QUFDckIsc0JBQXNCO0FBQ3RCLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkIsU0FBUztBQUNUO0FBQ0E7QUFDQSxZQUFZO0FBQ1osWUFBWTtBQUNaLGlCQUFpQjtBQUNqQixtQkFBbUI7QUFDbkIsaUJBQWlCO0FBQ2pCLGlCQUFpQjtBQUNqQixtQkFBbUI7QUFDbkI7QUFDQTtBQUNBLHVCQUFrQjtBQUFsQixrQkFBa0I7QUFDbEI7O0FBRUE7QUFDQSxlQUFlO0FBQ2YsZ0JBQWdCO0FBQ2hCLGNBQWM7QUFDZCxxQkFBcUI7QUFDckIsa0JBQWtCO0FBQ2xCOztBQUVBLHlHQUF5Rzs7OztBQUl6RztBQUNBLG1CQUFtQjtBQUNuQixnQkFBZ0I7QUFDaEIsU0FBUztBQUNUO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkIsdUJBQXVCO0FBQ3ZCLFlBQVk7QUFDWixTQUFTO0FBQ1Q7QUFDQTtBQUNBLG1CQUFtQjtBQUNuQixTQUFTO0FBQ1Q7QUFDQTtBQUNBLFlBQVk7QUFDWixnQkFBZ0I7QUFDaEIsWUFBWTtBQUNaLG9CQUFvQjtBQUNwQjtBQUNBO0FBQ0EsWUFBWTtBQUNaLFlBQVk7QUFDWixvQkFBb0I7QUFDcEI7QUFDQTtBQUNBLFlBQVk7QUFDWixZQUFZO0FBQ1osb0JBQW9CO0FBQ3BCOzs7O0FBSUEsZ0hBQWdIO0FBQ2hIO0FBQ0EsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qix1QkFBdUI7QUFDdkIsV0FBVztBQUNYLG1CQUFtQjtBQUNuQjs7QUFFQTtBQUNBLGVBQWU7QUFDZjtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCLGdCQUFnQjtBQUNoQixhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkI7QUFDQSw4R0FBOEc7QUFDOUc7QUFDQSxnQkFBZ0I7QUFDaEIsWUFBWTtBQUNaOztBQUVBLCtGQUErRjs7QUFFL0Y7QUFDQSxnQ0FBMEQ7QUFDMUQsNEJBQTRCO0FBQzVCLDZCQUE2QjtBQUM3QixnQkFBZ0I7QUFDaEI7QUFDQTtBQUNBLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCO0FBQ0E7QUFDQSxnQkFBZ0I7QUFDaEI7O0FBRUE7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsa0dBQWtHO0FBQ2xHO0FBQ0EsZUFBZTtBQUNmLFdBQVc7QUFDWDtBQUNBO0FBQ0EsY0FBYztBQUNkLFdBQVc7QUFDWDtBQUNBO0FBQ0Esa0dBQWtHO0FBQ2xHO0FBQ0EsaUJBQWlCO0FBQ2pCLGNBQWM7QUFDZCxhQUFhO0FBQ2IsZUFBZTtBQUNmLGlCQUFpQjtBQUNqQixzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQjs7QUFFQTtBQUNBLHdCQUF3QjtBQUN4Qjs7O0FBR0Esa0hBQWtIO0FBQ2xIO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBO0VBQ0UsaUJBQWlCO0VBQ2pCLGFBQWE7RUFDYixZQUFZO0VBQ1o7RUFDQTtFQUNBLDZCQUE2QjtFQUM3QixZQUFZO0VBQ1osa0NBQWtDO0VBQ2xDLHlCQUF5QjtFQUN6QixhQUFhO0VBQ2I7RUFDQTtJQUNFLGlCQUFpQjtJQUNqQixhQUFhO0lBQ2IsWUFBWTtFQUNkO0VBQ0E7SUFDRSw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGtDQUFrQztJQUNsQyx5QkFBeUI7SUFDekIsYUFBYTtJQUNiO0VBQ0Y7SUFDRSxpQkFBaUI7SUFDakIsYUFBYTtJQUNiLFlBQVk7RUFDZDtFQUNBO0lBQ0UsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixrQ0FBa0M7SUFDbEMseUJBQXlCO0lBQ3pCLGFBQWE7SUFDYjs7RUFFRjtJQUNFLFdBQVc7SUFDWCxZQUFZO0VBQ2Q7OztFQUdBO0VBQ0EsbUJBQW1CO0VBQ25COzs7SUFHRTtBQUdILDBCQUEwQjtBQUN2Qjs7T0FFRyxzR0FBc0c7RUFDM0c7SUFDRSxxQkFBcUI7SUFDckIscUJBQXFCO0lBQ3JCLHdCQUF3QjtJQUN4QixrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLG9CQUFvQjtJQUNwQixtQkFBbUI7SUFDbkIsY0FBYztJQUNkLFVBQVU7QUFDZDtBQUNBO0VBQ0UsV0FBVztFQUNYLG1CQUFtQjtFQUNuQixpQkFBaUI7QUFDbkI7QUFDQTtFQUNFLHNCQUFzQjtFQUN0Qiw4QkFBOEI7RUFDOUIsYUFBYTtFQUNiLGVBQWU7RUFDZixrQkFBa0I7O0VBRWxCO0FBQ0Y7RUFDRSx5QkFBeUI7RUFDekIsZ0JBQWdCO0FBQ2xCO0FBQ0EsdUhBQXVIO0FBQ3ZIO0FBQ0EsZUFBZTtBQUNmLGVBQWU7QUFDZixtQkFBbUI7QUFDbkIsT0FBTztBQUNQLFFBQVE7QUFDUixhQUFhO0FBQ2IsYUFBYTtBQUNiLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkIsU0FBUztBQUNULFVBQVU7QUFDVixZQUFZO0FBQ1o7QUFDQTtBQUNBLFlBQVk7QUFDWixZQUFZO0FBQ1osZUFBZTtBQUNmLGVBQWU7QUFDZixtQkFBbUI7QUFDbkIsT0FBTztBQUNQLFFBQVE7QUFDUixZQUFZO0FBQ1osYUFBYTtBQUNiLHVCQUF1QjtBQUN2QixtQkFBbUI7QUFDbkIsU0FBUztBQUNULFVBQVU7QUFDVixpQkFBaUI7QUFDakIsZUFBZTs7QUFFZjtBQUNBO0FBQ0EsWUFBWTtBQUNaLFlBQVk7QUFDWixlQUFlO0FBQ2YsZUFBZTtBQUNmLG1CQUFtQjtBQUNuQixPQUFPO0FBQ1AsUUFBUTtBQUNSLGFBQWE7QUFDYixhQUFhO0FBQ2IsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixTQUFTO0FBQ1QsVUFBVTtBQUNWO0FBQ0E7QUFDQSxVQUFVO0FBQ1YsbUJBQW1CO0FBQ25CLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsOEJBQThCO0FBQzlCO0FBQ0E7QUFDQSxlQUFlO0FBQ2YsZUFBZTtBQUNmLG1CQUFtQjtBQUNuQixPQUFPO0FBQ1AsUUFBUTtBQUNSLGFBQWE7QUFDYixhQUFhO0FBQ2IsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixVQUFVO0FBQ1YsV0FBVztBQUNYLGNBQWM7QUFDZDs7QUFFQTtBQUNBLFVBQVU7QUFDVixtQkFBbUI7QUFDbkIsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qiw4QkFBOEI7QUFDOUI7QUFDQSxpR0FBaUc7QUFDakc7RUFDRSxjQUFjO0FBQ2hCLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0EsNEJBQTRCO0FBQzVCO0FBQ0E7QUFDQSx5QkFBeUI7QUFDekIsbUJBQW1CO0FBQ25CLFlBQVk7QUFDWixZQUFZO0FBQ1o7QUFDQTtBQUNBLFVBQVU7QUFDVixZQUFZO0FBQ1o7O0FBRUE7RUFDRSxXQUFXO0FBQ2I7QUFDQTtFQUNFLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsYUFBYTtFQUNiLFlBQVk7QUFDZDtBQUNBO0FBQ0EsYUFBYTtBQUNiLG1CQUFtQjtBQUNuQix1QkFBdUI7QUFDdkIsa0JBQWtCO0FBQ2xCLHFCQUFxQjtBQUNyQixtQkFBbUI7QUFDbkIsa0JBQWtCO0FBQ2xCO0FBQ0E7QUFDQSxhQUFhO0FBQ2IscUJBQXFCO0FBQ3JCLHNCQUFzQjtBQUN0Qix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CLFNBQVM7QUFDVDtBQUNBO0FBQ0EsWUFBWTtBQUNaLFlBQVk7QUFDWixpQkFBaUI7QUFDakIsbUJBQW1CO0FBQ25CLGlCQUFpQjtBQUNqQixpQkFBaUI7QUFDakIsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQSx1QkFBa0I7QUFBbEIsa0JBQWtCO0FBQ2xCOztBQUVBO0FBQ0EsZUFBZTtBQUNmLGdCQUFnQjtBQUNoQixjQUFjO0FBQ2QscUJBQXFCO0FBQ3JCLGtCQUFrQjtBQUNsQjs7QUFFQSx5R0FBeUc7O0FBRXpHO0VBQ0UsbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixrQkFBa0I7Q0FDbkI7O0FBRUQ7QUFDQSxtQkFBbUI7QUFDbkIsZUFBZTtBQUNmLFNBQVM7QUFDVDtBQUNBO0FBQ0EsbUJBQW1CO0FBQ25CLHVCQUF1QjtBQUN2QixZQUFZO0FBQ1osU0FBUztBQUNUO0FBQ0E7QUFDQSxtQkFBbUI7QUFDbkIsU0FBUztBQUNUO0FBQ0E7QUFDQSxZQUFZO0FBQ1osZ0JBQWdCO0FBQ2hCLFlBQVk7QUFDWixvQkFBb0I7QUFDcEI7QUFDQTtBQUNBLFlBQVk7QUFDWixZQUFZO0FBQ1osb0JBQW9CO0FBQ3BCO0FBQ0E7QUFDQSxZQUFZO0FBQ1osWUFBWTtBQUNaLG9CQUFvQjtBQUNwQjs7OztBQUlBLGdIQUFnSDtBQUNoSDtBQUNBLGFBQWE7QUFDYixXQUFXO0FBQ1g7O0FBRUE7QUFDQSxlQUFlO0FBQ2Y7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQixnQkFBZ0I7QUFDaEIsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CO0FBQ0E7RUFDRSxxQkFBcUI7QUFDdkI7QUFDQSw4R0FBOEc7QUFDOUc7QUFDQSxnQkFBZ0I7QUFDaEIscUJBQXFCO0FBQ3JCOztBQUVBLCtGQUErRjs7QUFFL0Y7QUFDQSxnQ0FBMEQ7QUFDMUQsNEJBQTRCO0FBQzVCLCtCQUErQjtBQUMvQixjQUFjO0FBQ2Q7QUFDQTtBQUNBLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEI7O0FBRUE7QUFDQSxtQkFBbUI7QUFDbkI7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7O0FBRUEsa0dBQWtHO0FBQ2xHO0FBQ0EsZUFBZTtBQUNmLFdBQVc7QUFDWDtBQUNBO0FBQ0EsY0FBYztBQUNkLFdBQVc7QUFDWDtBQUNBO0FBQ0Esa0dBQWtHO0FBQ2xHO0FBQ0EsaUJBQWlCO0FBQ2pCLGNBQWM7QUFDZCxhQUFhO0FBQ2IsZUFBZTtBQUNmLGlCQUFpQjtBQUNqQixzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQjs7QUFFQTtBQUNBLHdCQUF3QjtBQUN4Qjs7O0FBR0Esa0hBQWtIO0FBQ2xIO0FBQ0EsaUJBQWlCO0FBQ2pCOztBQUVBO0VBQ0UsaUJBQWlCO0VBQ2pCLGFBQWE7RUFDYixZQUFZO0VBQ1o7RUFDQTtFQUNBLDZCQUE2QjtFQUM3QixZQUFZO0VBQ1osa0NBQWtDO0VBQ2xDLHlCQUF5QjtFQUN6QixhQUFhO0VBQ2I7RUFDQTtJQUNFLGlCQUFpQjtJQUNqQixhQUFhO0lBQ2IsWUFBWTtFQUNkO0VBQ0E7SUFDRSw2QkFBNkI7SUFDN0IsWUFBWTtJQUNaLGtDQUFrQztJQUNsQyx5QkFBeUI7SUFDekIsYUFBYTtJQUNiO0VBQ0Y7SUFDRSxpQkFBaUI7SUFDakIsYUFBYTtJQUNiLFlBQVk7RUFDZDtFQUNBO0lBQ0UsNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixrQ0FBa0M7SUFDbEMseUJBQXlCO0lBQ3pCLGFBQWE7SUFDYjs7RUFFRjtJQUNFLFdBQVc7SUFDWCxZQUFZO0VBQ2Q7OztFQUdBO0VBQ0EsbUJBQW1CO0VBQ25COztJQUVFO0FBR0gsNkJBQTZCO0FBQzFCO0NBQ0gsbUVBQW1FO0NBQ25FO0VBQ0MseUJBQXlCO0VBQ3pCLGtCQUFrQjtDQUNuQjtBQUNELDRHQUE0RztBQUM1RztFQUNFLGVBQWU7RUFDZixlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLE9BQU87RUFDUCxRQUFRO0VBQ1IsYUFBYTtFQUNiLGFBQWE7RUFDYix1QkFBdUI7RUFDdkIsbUJBQW1CO0VBQ25CLFNBQVM7RUFDVCxZQUFZO0VBQ1o7O0VBRUE7RUFDQSxVQUFVO0VBQ1YsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixzQkFBc0I7RUFDdEIsOEJBQThCO0VBQzlCO0VBQ0E7RUFDQSxZQUFZO0VBQ1osWUFBWTtFQUNaLGFBQWE7RUFDYixhQUFhO0VBQ2IsdUJBQXVCO0VBQ3ZCLHFCQUFxQjtFQUNyQixrQkFBa0I7RUFDbEI7O0VBRUE7RUFDQSxxQkFBcUI7RUFDckIsY0FBYztFQUNkLHNCQUFzQjtFQUN0Qix1QkFBdUI7RUFDdkIsZUFBZTtFQUNmO0VBQ0E7RUFDQSxZQUFZO0VBQ1osYUFBYTtFQUNiLGlCQUFpQjtFQUNqQjtFQUNBO0VBQ0Esa0JBQWtCO0VBQ2xCLFNBQVM7RUFDVCxZQUFZO0VBQ1osZUFBZTtFQUNmOzs7R0FHQztHQUNBLFlBQVk7R0FDWixrQkFBa0I7R0FDbEIsWUFBWTtHQUNaLFFBQVE7R0FDUixZQUFZO0dBQ1osZUFBZTtHQUNmLFVBQVU7R0FDVixpQkFBaUI7R0FDakI7O0dBRUE7SUFDQyxxQkFBcUI7S0FDcEIsa0JBQWtCO0tBQ2xCLGFBQWE7S0FDYixRQUFRO0tBQ1IsWUFBWTtLQUNaLGVBQWU7S0FDZixVQUFVO0tBQ1YsaUJBQWlCO0tBQ2pCLFlBQVk7S0FDWixtQkFBbUI7S0FDbkIsa0JBQWtCO0tBQ2xCLFlBQVk7S0FDWixhQUFhO0tBQ2IsdUJBQXVCO0tBQ3ZCLG1CQUFtQjtHQUNyQjtJQUNDO0dBQ0QsYUFBYTtHQUNiO0dBQ0E7SUFDQyxxQkFBcUI7SUFDckIsY0FBYztJQUNkLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsZUFBZTtHQUNoQjtHQUNBO0lBQ0MsWUFBWTtJQUNaLGFBQWE7SUFDYixpQkFBaUI7R0FDbEI7R0FDQTtJQUNDLGtCQUFrQjtJQUNsQixTQUFTO0lBQ1QsWUFBWTtJQUNaLGVBQWU7R0FDaEI7OztBQUdILGtIQUFrSDtBQUNsSDtFQUNFLGtCQUFrQjtFQUNsQjtFQUNBO0lBQ0UsYUFBYTtJQUNiLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLHNCQUFzQjtJQUN0QjtBQUNKO0VBQ0Usa0JBQWtCO0VBQ2xCLDRCQUF1QjtFQUF2Qix1QkFBdUI7RUFDdkIsaUNBQWlDO0NBQ2xDO0NBQ0E7RUFDQyxZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLGFBQWE7RUFDYixZQUFZO0NBQ2I7Q0FDQTtFQUNDLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGtCQUFrQjtDQUNuQjtDQUNBO0VBQ0MsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2Ysa0JBQWtCO0NBQ25CO0NBQ0E7RUFDQyxZQUFZO0VBQ1osWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLGlCQUFpQjtFQUNqQjs7RUFFQTtJQUNFLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsMkJBQTJCO0lBQzNCLGtCQUFrQjtJQUNsQixxQkFBcUI7SUFDckIsZ0JBQWdCO0lBQ2hCOztHQUVEO0lBQ0MseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osWUFBWTtHQUNiO0dBQ0E7SUFDQyxVQUFVO0lBQ1YscUJBQXFCO0dBQ3RCOzs7R0FHQSwyRkFBMkY7R0FDM0Y7SUFDQyxZQUFZO0lBQ1osZUFBZTtJQUNmLGtCQUFrQjtJQUNsQjs7SUFFQTtNQUNFLHVCQUF1QjtPQUN0QixZQUFZO09BQ1osYUFBYTtNQUNkO01BQ0E7T0FDQyx1QkFBdUI7T0FDdkIsYUFBYTtNQUNkOztPQUVDLGtIQUFrSDs7T0FFbEg7UUFDQyxnQ0FBMEQ7UUFDMUQsNEJBQTRCO1FBQzVCLCtCQUErQjtRQUMvQixjQUFjO1FBQ2Q7UUFDQTtRQUNBLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsdUJBQXVCO1FBQ3ZCO1FBQ0E7VUFDRSxrQkFBa0I7V0FDakI7O1FBRUg7VUFDRTtVQUNBO1dBQ0M7VUFDRDtVQUNBO1VBQ0E7VUFDQTtVQUNBOztVQUVBLGtIQUFrSDtVQUNsSDtZQUNFLGlCQUFpQjtZQUNqQixXQUFXO1lBQ1g7VUFDRjs7VUFFQSxrSEFBa0g7O1VBRWxIO1lBQ0UsaUJBQWlCO1lBQ2pCLG1CQUFtQjtZQUNuQixrQkFBa0I7WUFDbEI7VUFDRjs7SUFFTjtBQUdDLDhCQUE4QjtBQUMvQjs7QUFFSixrSEFBa0g7QUFDbEg7RUFDRSxrQkFBa0I7RUFDbEI7RUFDQTtJQUNFLGFBQWE7SUFDYixrQkFBa0I7R0FDbkIsbUJBQW1CO0lBQ2xCLGtCQUFrQjtJQUNsQixzQkFBc0I7SUFDdEI7QUFDSjtFQUNFLGtCQUFrQjtFQUNsQiw0QkFBdUI7RUFBdkIsdUJBQXVCO0VBQ3ZCLGlDQUFpQztDQUNsQztDQUNBO0VBQ0MsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixhQUFhO0VBQ2IsWUFBWTtDQUNiO0NBQ0E7RUFDQyxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixrQkFBa0I7Q0FDbkI7Q0FDQTtFQUNDLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLGtCQUFrQjtDQUNuQjtDQUNBO0VBQ0MsWUFBWTtFQUNaLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixpQkFBaUI7RUFDakI7O0VBRUE7SUFDRSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDJCQUEyQjtJQUMzQixrQkFBa0I7SUFDbEIscUJBQXFCO0lBQ3JCLGdCQUFnQjtJQUNoQjs7R0FFRDtJQUNDLHlCQUF5QjtJQUN6QixtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLFlBQVk7R0FDYjtHQUNBO0lBQ0MsVUFBVTtJQUNWLHFCQUFxQjtHQUN0Qjs7T0FFSSxrSEFBa0g7O1FBRWpIO1FBQ0EsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkI7OztRQUdBO1VBQ0U7VUFDQTtXQUNDO1VBQ0Q7VUFDQTtVQUNBO1VBQ0E7VUFDQTs7SUFFTjtBQUlILGlGQUFpRjtBQUdqRiw2TkFBNk4sd0JBQXdCLENBQUM7QUFBRSw4Q0FBOEMsNEVBQTRFLENBQUMseUJBQXlCLENBQUM7QUFBRSw4RUFBOEUsNERBQTRELENBQUM7QUFBRSw2RUFBNkUsNEVBQTRFLENBQUM7QUFBRSw2R0FBNkcsNERBQTRELENBQUM7QUFBRSw2Q0FBNkMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQztBQUFFLGlEQUFpRCxVQUFVLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxrQkFBa0IsQ0FBQztBQUFFLDJFQUEyRSxpQ0FBaUMsQ0FBQyx1QkFBdUIsQ0FBQztBQUFFLDZDQUE2QyxlQUFlLENBQUM7QUFBRSxzRUFBc0UsYUFBYSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsd0JBQXdCLENBQUMsa0JBQWtCLENBQUM7QUFBRSwyRUFBMkUsdUJBQXVCLENBQUM7QUFBRSw2Q0FBNkMsZUFBZSxDQUFDO0FBQUUsc0VBQXNFLGFBQWEsQ0FBQyx3RUFBd0UsQ0FBQyx3REFBd0QsQ0FBQyw0REFBNEQsQ0FBQyw0REFBNEQsQ0FBQztBQUFFLDJFQUEyRSx1QkFBdUIsQ0FBQztBQUFFLDRFQUE0RSx1QkFBdUIsQ0FBQztBQUFFLHVFQUF1RSxhQUFhLENBQUM7QUFBRSxpRUFBaUUsaUNBQWlDLENBQUMsa0JBQWtCLENBQUMsNEJBQTRCLENBQUMsb0JBQW9CLENBQUMsMkJBQTJCLENBQUM7QUFBRSwyRUFBMkUsWUFBWSxDQUFDO0FBQUUsa0hBQWtILGdCQUFnQixDQUFDO0FBQUUsNEZBQTRGLGFBQWEsQ0FBQyxjQUFjLENBQUM7QUFBRSxtRkFBbUYsYUFBYSxDQUFDLGNBQWMsQ0FBQztBQUFFLHVGQUF1RixhQUFhLENBQUMsY0FBYyxDQUFDO0FBQUUsa0ZBQWtGLGFBQWEsQ0FBQyxjQUFjLENBQUM7QUFBRSxnRUFBZ0UsY0FBYyxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxpQ0FBaUMsQ0FBQyx5QkFBeUIsQ0FBQyxXQUFXLENBQUMsc0JBQXNCLENBQUMsVUFBVSxDQUFDLHdCQUF3QixDQUFDO0FBQUUseUVBQXlFLFdBQVcsQ0FBQyxTQUFTLENBQUM7QUFBRSxzRUFBc0UsVUFBVSxDQUFDO0FBQUUsK0VBQStFLFdBQVcsQ0FBQyxTQUFTLENBQUM7QUFBRSx1RUFBdUUsd0JBQXdCLENBQUM7QUFBRSw0SUFBNEksaUJBQWlCLENBQUM7QUFBRSw0RUFBNEUsd0JBQXdCLENBQUMsY0FBYyxDQUFDO0FBQUUsOEVBQThFLGFBQWEsQ0FBQztBQUFFLHFGQUFxRixjQUFjLENBQUMsWUFBWSxDQUFDO0FBQUUsZ0ZBQWdGLGNBQWMsQ0FBQztBQUFFLDRFQUE0RSx1QkFBdUIsQ0FBQztBQUFFLDZDQUE2QyxhQUFhLENBQUMsY0FBYyxDQUFDO0FBQUUsNk5BQTZOLHdCQUF3QixDQUFDO0FBQUUsOEVBQThFLFdBQVcsQ0FBQyw0REFBNEQsQ0FBQztBQUFFLDhDQUE4Qyw0RUFBNEUsQ0FBQyx1QkFBdUIsQ0FBQztBQUFFLDZFQUE2RSx1QkFBdUIsQ0FBQztBQUFFLHFFQUFxRSxnQkFBZ0IsQ0FBQztBQUFFLDhDQUE4QyxlQUFlLENBQUMsa0JBQWtCLENBQUM7QUFBRSw2RUFBNkUsdUJBQXVCLENBQUMsNEJBQTRCLENBQUMsMEJBQTBCLENBQUMsdUJBQXVCLENBQUM7QUFBRSw4Q0FBOEMsZUFBZSxDQUFDO0FBQUUsa0RBQWtELFNBQVMsQ0FBQyw2QkFBNkIsQ0FBQztBQUFFLDZDQUE2Qyw0QkFBNEIsQ0FBQztBQUFFLHVFQUF1RSxhQUFhLENBQUM7QUFBRSx1R0FBdUcsYUFBYSxDQUFDO0FBQUUsZ0xBQWdMLGNBQWMsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUM7QUFBRSw2RUFBNkUsdUJBQXVCLENBQUM7QUFBRSx1RUFBdUUsYUFBYSxDQUFDLHdFQUF3RSxDQUFDLHdEQUF3RCxDQUFDLDREQUE0RCxDQUFDLDREQUE0RCxDQUFDLGtFQUFrRSxDQUFDLDhEQUE4RCxDQUFDO0FBQUUsNEVBQTRFLHNCQUFzQixDQUFDO0FBQUUsZ0pBQWdKLDBCQUEwQixDQUFDO0FBQUUsaUpBQWlKLHNCQUFzQixDQUFDO0FBQUUseUhBQXlILHdCQUF3QixDQUFDLHVCQUF1QixDQUFDO0FBQUUsK0ZBQStGLHlCQUF5QixDQUFDLHdCQUF3QixDQUFDO0FBQUMseUlBQXlJLGlCQUFpQixDQUFDO0FBQUMsK0lBQStJLGtCQUFrQixDQUFDO0FBQUUseUVBQXlFLGFBQWEsQ0FBQztBQUFFLDJFQUEyRSxZQUFZLENBQUM7QUFBRSw2Q0FBNkMsNEJBQTRCLENBQUM7QUFBRSx1RUFBdUUsYUFBYSxDQUFDO0FBQUUsdUdBQXVHLG9DQUFvQyxDQUFDO0FBQUUsZ0xBQWdMLHFFQUFxRSxDQUFDLHFEQUFxRCxDQUFDLHlEQUF5RCxDQUFDLHlEQUF5RCxDQUFDO0FBQUUsNkVBQTZFLHVCQUF1QixDQUFDO0FBQUUsdUVBQXVFLGFBQWEsQ0FBQyx3RUFBd0UsQ0FBQyx3REFBd0QsQ0FBQyw0REFBNEQsQ0FBQyw0REFBNEQsQ0FBQyxrRUFBa0UsQ0FBQyw4REFBOEQsQ0FBQztBQUFFLDRFQUE0RSxzQkFBc0IsQ0FBQztBQUFFLGlKQUFpSiwwQkFBMEIsQ0FBQztBQUFFLGtKQUFrSixzQkFBc0IsQ0FBQztBQUFFLDBIQUEwSCx3QkFBd0IsQ0FBQyx1QkFBdUIsQ0FBQztBQUFFLGdHQUFnRyx5QkFBeUIsQ0FBQyx3QkFBd0IsQ0FBQztBQUFDLDBJQUEwSSxpQkFBaUIsQ0FBQztBQUFDLGdKQUFnSixrQkFBa0IsQ0FBQztBQUFFLDhDQUE4Qyw0QkFBNEIsQ0FBQztBQUFFLHdFQUF3RSxhQUFhLENBQUM7QUFBRSx3R0FBd0csb0NBQW9DLENBQUM7QUFBRSxrTEFBa0wscUVBQXFFLENBQUMscURBQXFELENBQUMseURBQXlELENBQUMseURBQXlELENBQUM7QUFBRSw2RUFBNkUsdUJBQXVCLENBQUM7QUFBRSx1RUFBdUUsYUFBYSxDQUFDLHdFQUF3RSxDQUFDLHdEQUF3RCxDQUFDLDREQUE0RCxDQUFDLDREQUE0RCxDQUFDLGtFQUFrRSxDQUFDLDhEQUE4RCxDQUFDO0FBQUUsNEVBQTRFLHNCQUFzQixDQUFDO0FBQUUsd0VBQXdFLGdCQUFnQixDQUFDLGlCQUFpQixDQUFDO0FBQUUsNEVBQTRFLCtCQUErQixDQUFDLGlCQUFpQixDQUFDO0FBQUUsaUhBQWlILCtCQUErQixDQUFDLGlCQUFpQixDQUFDO0FBQUUsb0hBQW9ILCtCQUErQixDQUFDLGlCQUFpQixDQUFDO0FBQUUsdUhBQXVILCtCQUErQixDQUFDLGlCQUFpQixDQUFDO0FBQUUsc0pBQXNKLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQztBQUFFLGdJQUFnSSxxRUFBcUUsQ0FBQyxxREFBcUQsQ0FBQyx5REFBeUQsQ0FBQyx5REFBeUQsQ0FBQztBQUFFLHFKQUFxSixhQUFhLENBQUM7QUFBRTs7Ozt5RkFJLzNaLG9DQUFvQyxDQUFDO0FBQUU7eUdBQ3ZCLGFBQWEsQ0FBQztBQUFFOzs7OzsyR0FLZCx1Q0FBdUMsQ0FBQztBQUMxSTs7Ozt5R0FJZ0cscUVBQXFFLENBQUMscURBQXFELENBQUMseURBQXlELENBQUMseURBQXlELENBQUM7QUFBRTs7Ozt3R0FJblAsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUM7QUFBRTs7OEdBRWpDLHlCQUF5QixDQUFDLDJCQUEyQixDQUFDLHVCQUF1QixDQUFDO0FBQUUsMkVBQTJFLHVCQUF1QixDQUFDO0FBQUUscUVBQXFFLHNCQUFzQixDQUFDO0FBQUUsOENBQThDLGtCQUFrQixDQUFDLDRCQUE0QixDQUFDLHNCQUFzQixDQUFDLDRFQUE0RSxDQUFDLHlCQUF5QixDQUFDO0FBQUUsOEVBQThFLDREQUE0RCxDQUFDO0FBQUMsZ0hBQWdILGtCQUFrQixDQUFDO0FBQUUsaUtBQWlLLG9CQUFvQixDQUFDLGtCQUFrQixDQUFDO0FBQUUsNEVBQTRFLHVCQUF1QixDQUFDO0FBQUUsOENBQThDLGFBQWEsQ0FBQyxnQ0FBZ0MsQ0FBQyxlQUFlLENBQUM7QUFBRSw0RUFBNEUsd0JBQXdCLENBQUM7QUFBQyxpSEFBaUgsa0JBQWtCLENBQUM7QUFBRSxrS0FBa0ssb0JBQW9CLENBQUMsa0JBQWtCLENBQUM7QUFBRSx3RkFBd0Ysd0JBQXdCLENBQUM7QUFBRSw2RUFBNkUsdUJBQXVCLENBQUM7QUFBRSwrREFBK0QscUVBQXFFLENBQUMscURBQXFELENBQUMseURBQXlELENBQUMseURBQXlELENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQywwQkFBMEIsQ0FBQyxrQkFBa0IsQ0FBQyw0QkFBNEIsQ0FBQyw2QkFBNkIsQ0FBQyx1QkFBdUIsQ0FBQztBQUFFLDRJQUE0SSxvQ0FBb0MsQ0FBQztBQUFFLG9KQUFvSixtQ0FBbUMsQ0FBQztBQUFFLDJFQUEyRSx1QkFBdUIsQ0FBQztBQUFFLDZDQUE2QyxVQUFVLENBQUMsY0FBYyxDQUFDO0FBQUUsZ0VBQWdFLHFFQUFxRSxDQUFDLHFEQUFxRCxDQUFDLHlEQUF5RCxDQUFDLHlEQUF5RCxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsMEJBQTBCLENBQUMsa0JBQWtCLENBQUMsNEJBQTRCLENBQUMsNkJBQTZCLENBQUMsdUJBQXVCLENBQUM7QUFBRSw4SUFBOEksb0NBQW9DLENBQUM7QUFBRSxzSkFBc0osbUNBQW1DLENBQUM7QUFBRSw4Q0FBOEMsVUFBVSxDQUFDLGNBQWMsQ0FBQztBQUFDLE1BQU0seUJBQXlCLENBQUM7QUFBQywwQkFBMEIsc0VBQXNFLHdEQUF3RCxDQUFDLDREQUE0RCxDQUFDLEVBQUUsOEVBQThFLGlHQUFpRyxDQUFDLEVBQUUsOENBQThDLDBCQUEwQixDQUFDLEVBQUUsOENBQThDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFLDZFQUE2RSxzQkFBc0IsQ0FBQywyQkFBMkIsQ0FBQywwQkFBMEIsQ0FBQyx3QkFBd0IsQ0FBQyxFQUFFLDhDQUE4QyxpQkFBaUIsQ0FBQyxFQUFFLGtEQUFrRCxTQUFTLENBQUMsRUFBRSw0RUFBNEUsc0JBQXNCLENBQUMsRUFBRSwyRUFBMkUsc0JBQXNCLENBQUMsdUJBQXVCLENBQUMsRUFBRSx1RUFBdUUsd0RBQXdELENBQUMsNERBQTRELENBQUMsa0VBQWtFLENBQUMsOERBQThELENBQUMsRUFBRSxnTEFBZ0wscURBQXFELENBQUMseURBQXlELENBQUMsRUFBRSw2RUFBNkUsc0JBQXNCLENBQUMsMkJBQTJCLENBQUMsMEJBQTBCLENBQUMsdUJBQXVCLENBQUMsRUFBRSx1RUFBdUUsd0RBQXdELENBQUMsNERBQTRELENBQUMsa0VBQWtFLENBQUMsOERBQThELENBQUMsRUFBRSxrTEFBa0wscURBQXFELENBQUMseURBQXlELENBQUMsRUFBRSx1RUFBdUUsd0RBQXdELENBQUMsNERBQTRELENBQUMsa0VBQWtFLENBQUMsOERBQThELENBQUMsRUFBRSxzSkFBc0osZUFBZSxDQUFDLGtCQUFrQixDQUFDLEVBQUUsZ0lBQWdJLHFEQUFxRCxDQUFDLHlEQUF5RCxDQUFDO1NBQ2o5Tjs7Ozt5R0FJZ0cscURBQXFELENBQUMseURBQXlELENBQUMsRUFBRSwyRUFBMkUseUJBQXlCLENBQUMsRUFBRSw4Q0FBOEMseUJBQXlCLENBQUMsRUFBRSwrREFBK0QscURBQXFELENBQUMseURBQXlELENBQUMsRUFBRSxnRUFBZ0UscURBQXFELENBQUMseURBQXlELENBQUMsQ0FBQztBQUFDLHlCQUF5Qiw4Q0FBOEMsYUFBYSxDQUFDLEVBQUUsOENBQThDLGFBQWEsQ0FBQyxFQUFFLDhDQUE4QyxhQUFhLENBQUMsRUFBRSw4Q0FBOEMsYUFBYSxDQUFDLEVBQUUsOENBQThDLGFBQWEsQ0FBQyxFQUFFLDhDQUE4QyxhQUFhLENBQUMsRUFBRSw4Q0FBOEMsYUFBYSxDQUFDLEVBQUUsNkNBQTZDLFlBQVksQ0FBQyxFQUFFLDhDQUE4QyxZQUFZLENBQUMsQ0FBQztBQUFDLGdEQUFnRCw4Q0FBOEMsUUFBUSxDQUFDLEVBQUUsOENBQThDLFNBQVMsQ0FBQyxFQUFFLDhDQUE4QyxTQUFTLENBQUMsRUFBRSw4Q0FBOEMsVUFBVSxDQUFDLEVBQUUsOENBQThDLFNBQVMsQ0FBQyxFQUFFLDhDQUE4QyxTQUFTLENBQUMsRUFBRSw4Q0FBOEMsU0FBUyxDQUFDLENBQUM7QUFBQyx5QkFBeUIsOENBQThDLHlCQUF5QixDQUFDLEVBQUUsc0VBQXNFLHdEQUF3RCxDQUFDLDREQUE0RCxDQUFDLEVBQUUsMkVBQTJFLHNCQUFzQixDQUFDLEVBQUUsNkVBQTZFLHlCQUF5QixDQUFDLEVBQUUsOEVBQThFLG1HQUFtRyxDQUFDLEVBQUUsOENBQThDLDBCQUEwQixDQUFDLEVBQUUsOENBQThDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFLDZFQUE2RSx1QkFBdUIsQ0FBQywyQkFBMkIsQ0FBQywwQkFBMEIsQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLDhDQUE4QyxpQkFBaUIsQ0FBQyxFQUFFLGtEQUFrRCxTQUFTLENBQUMsRUFBRSw0RUFBNEUsc0JBQXNCLENBQUMsRUFBRSw2RUFBNkUsdUJBQXVCLENBQUMsMkJBQTJCLENBQUMsMEJBQTBCLENBQUMsRUFBRSx1RUFBdUUsd0RBQXdELENBQUMsNERBQTRELENBQUMsa0VBQWtFLENBQUMsOERBQThELENBQUMsRUFBRSw0RUFBNEUsc0JBQXNCLENBQUMsRUFBRSxnTEFBZ0wscURBQXFELENBQUMseURBQXlELENBQUMsRUFBRSw2RUFBNkUsc0JBQXNCLENBQUMsMkJBQTJCLENBQUMsMEJBQTBCLENBQUMsdUJBQXVCLENBQUMsRUFBRSx1RUFBdUUsd0RBQXdELENBQUMsNERBQTRELENBQUMsa0VBQWtFLENBQUMsOERBQThELENBQUMsRUFBRSw0RUFBNEUsc0JBQXNCLENBQUMsRUFBRSxrTEFBa0wscURBQXFELENBQUMseURBQXlELENBQUMsRUFBRSw2RUFBNkUsdUJBQXVCLENBQUMsMkJBQTJCLENBQUMsMEJBQTBCLENBQUMsRUFBRSx1RUFBdUUsd0RBQXdELENBQUMsNERBQTRELENBQUMsa0VBQWtFLENBQUMsOERBQThELENBQUMsRUFBRSw0RUFBNEUsc0JBQXNCLENBQUMsRUFBRSxnSUFBZ0kscURBQXFELENBQUMseURBQXlELENBQUM7U0FDbitLOzs7O3lHQUlnRyxxREFBcUQsQ0FBQyx5REFBeUQsQ0FBQyxFQUFFLHVEQUF1RCw2QkFBNkIsQ0FBQyxFQUFFLGdGQUFnRiwwQkFBMEIsQ0FBQywyQkFBMkIsQ0FBQyxlQUFlLENBQUMsRUFBRSwrRUFBK0UsOEJBQThCLENBQUMsNkJBQTZCLENBQUMsZUFBZSxDQUFDLEVBQUUsOERBQThELDZCQUE2QixDQUFDLEVBQUUsdUZBQXVGLDBCQUEwQixDQUFDLDJCQUEyQixDQUFDLGVBQWUsQ0FBQyxFQUFFLHNGQUFzRiw4QkFBOEIsQ0FBQyw2QkFBNkIsQ0FBQyxlQUFlLENBQUMsRUFBRSx5RUFBeUUsNkJBQTZCLENBQUMsRUFBRSxrR0FBa0csMEJBQTBCLENBQUMsMkJBQTJCLENBQUMsZUFBZSxDQUFDLEVBQUUsaUdBQWlHLDhCQUE4QixDQUFDLDZCQUE2QixDQUFDLGVBQWUsQ0FBQyxFQUFFLDRFQUE0RSx1QkFBdUIsQ0FBQyxFQUFFLDhDQUE4QyxpQkFBaUIsQ0FBQyxFQUFFLHdGQUF3RixzQkFBc0IsQ0FBQyxFQUFFLDZFQUE2RSx3QkFBd0IsQ0FBQyxFQUFFLCtEQUErRCxxREFBcUQsQ0FBQyx5REFBeUQsQ0FBQyxFQUFFLGdFQUFnRSxxREFBcUQsQ0FBQyx5REFBeUQsQ0FBQyxDQUFDO0FBRXpxRSw4Q0FBOEMsY0FBYyxDQUFDLGlCQUFpQixDQUFDO0FBQUMsaUhBQWlILGtCQUFrQixDQUFDO0FBQUUsa0tBQWtLLG9CQUFvQixDQUFDLGtCQUFrQixDQUFDO0FBQUUsNkVBQTZFLHlCQUF5QixDQUFDO0FBQUUsdUVBQXVFLGFBQWEsQ0FBQztBQUFFLDhDQUE4QyxhQUFhLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLHVCQUF1QixDQUFDLFNBQVMsQ0FBQztBQUFFLCtFQUErRSxVQUFVLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLHlCQUF5QixDQUFDLHdCQUF3QixDQUFDLGtDQUFrQyxDQUFDO0FBQUUsbU5BQW1OLHNFQUFzRSxDQUFDO0FBQUMsaUhBQWlILGtCQUFrQixDQUFDO0FBQUUsa0tBQWtLLG9CQUFvQixDQUFDLGtCQUFrQixDQUFDO0FBQUUsNkVBQTZFLHdCQUF3QixDQUFDO0FBQUUsNkNBQTZDLGVBQWUsQ0FBQztBQUFFLGlEQUFpRCxVQUFVLENBQUM7QUFBRSwyRUFBMkUsd0JBQXdCLENBQUMsa0JBQWtCLENBQUMsNEJBQTRCLENBQUMsc0JBQXNCLENBQUM7QUFBQyxpSEFBaUgsd0JBQXdCLENBQUM7QUFBRSxrS0FBa0ssMEJBQTBCLENBQUMsd0JBQXdCLENBQUM7QUFBRSx3RkFBd0Ysd0JBQXdCLENBQUM7QUFBRSw2RUFBNkUsdUJBQXVCLENBQUM7QUFBRSx5RUFBeUUsV0FBVyxDQUFDLDZCQUE2QixDQUFDO0FBQUUsOEVBQThFLGNBQWMsQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLHdCQUF3QixDQUFDO0FBQUUsdVZBQXVWLHVDQUF1QyxDQUFDO0FBQUUsb0ZBQW9GLGFBQWEsQ0FBQztBQUFFLG9GQUFvRixhQUFhLENBQUM7QUFBRSxxRkFBcUYsYUFBYSxDQUFDO0FBQUUsb0ZBQW9GLGFBQWEsQ0FBQztBQUFFLG1IQUFtSCxhQUFhLENBQUM7QUFBRSxtSEFBbUgsYUFBYSxDQUFDO0FBQUUsbUhBQW1ILGFBQWEsQ0FBQztBQUFFLG9IQUFvSCxhQUFhLENBQUM7QUFBRSxnR0FBZ0csYUFBYSxDQUFDO0FBQUUsb0dBQW9HLGFBQWEsQ0FBQztBQUFFLG1JQUFtSSxhQUFhLENBQUM7QUFBRSw2R0FBNkcsYUFBYSxDQUFDO0FBQUUseUdBQXlHLGNBQWMsQ0FBQyxlQUFlLENBQUMseUJBQXlCLENBQUMsYUFBYSxDQUFDLHdCQUF3QixDQUFDO0FBQUUsK0dBQStHLGFBQWEsQ0FBQztBQUFFLCtHQUErRyxhQUFhLENBQUM7QUFBRSxnSEFBZ0gsYUFBYSxDQUFDO0FBQUUsK0dBQStHLGFBQWEsQ0FBQztBQUFFLDJIQUEySCx3QkFBd0IsQ0FBQztBQUFFLHdFQUF3RSx5QkFBeUIsQ0FBQztBQUFFLGdHQUFnRyxrQkFBa0IsQ0FBQyw0QkFBNEIsQ0FBQyw2QkFBNkIsQ0FBQyx3QkFBd0IsQ0FBQyw2QkFBNkIsQ0FBQyxlQUFlLENBQUM7QUFBRSx5RUFBeUUsV0FBVyxDQUFDLGlDQUFpQyxDQUFDO0FBQUUsMEdBQTBHLHdCQUF3QixDQUFDO0FBQUUsMkZBQTJGLGFBQWEsQ0FBQztBQUFFLGdIQUFnSCx3QkFBd0IsQ0FBQztBQUFFLGlHQUFpRyxhQUFhLENBQUM7QUFBRSxxRUFBcUUsaUNBQWlDLENBQUMsa0JBQWtCLENBQUMseUJBQXlCLENBQUM7QUFBRSwyRUFBMkUsd0JBQXdCLENBQUM7QUFBRSwyRUFBMkUsdUJBQXVCLENBQUM7QUFBRSw2Q0FBNkMsVUFBVSxDQUFDLGNBQWMsQ0FBQztBQUFFLGdFQUFnRSxjQUFjLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsd0JBQXdCLENBQUMsaUNBQWlDLENBQUMsMkJBQTJCLENBQUM7QUFBRSw4SUFBOEksYUFBYSxDQUFDLHdCQUF3QixDQUFDO0FBQUUsc0pBQXNKLFlBQVksQ0FBQztBQUFFLDhDQUE4QyxVQUFVLENBQUMsY0FBYyxDQUFDO0FBQUUsK0RBQStELHFDQUFxQyxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsbUJBQW1CLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQywwQkFBMEIsQ0FBQyxrQkFBa0IsQ0FBQyw0QkFBNEIsQ0FBQyxvQkFBb0IsQ0FBQyx5QkFBeUIsQ0FBQztBQUFFLDRJQUE0SSxhQUFhLENBQUMsb0JBQW9CLENBQUM7QUFBRSxvSkFBb0osWUFBWSxDQUFDO0FBQUUsMkVBQTJFLHNCQUFzQixDQUFDO0FBQUUsNkNBQTZDLFVBQVUsQ0FBQyxjQUFjLENBQUM7QUFBQyxNQUFNLHlCQUF5QixDQUFDO0FBQUMsMEJBQTBCLGlEQUFpRCxVQUFVLENBQUMsRUFBRSwyRUFBMkUsd0JBQXdCLENBQUMsNEJBQTRCLENBQUMsQ0FBQyxpSEFBaUgsa0JBQWtCLENBQUMsRUFBRSxrS0FBa0ssb0JBQW9CLENBQUMsa0JBQWtCLENBQUMsRUFBRSw2RUFBNkUseUJBQXlCLENBQUMsRUFBRSx5RUFBeUUsZUFBZSxDQUFDLDZCQUE2QixDQUFDLEVBQUUsOEVBQThFLHdCQUF3QixDQUFDLGFBQWEsQ0FBQyx5QkFBeUIsQ0FBQyxFQUFFLHVWQUF1Vix3QkFBd0IsQ0FBQyxFQUFFLG9GQUFvRixhQUFhLENBQUMsRUFBRSxvRkFBb0YsYUFBYSxDQUFDLEVBQUUscUZBQXFGLGFBQWEsQ0FBQyxFQUFFLG9GQUFvRixhQUFhLENBQUMsRUFBRSxtSEFBbUgsYUFBYSxDQUFDLEVBQUUsbUhBQW1ILGFBQWEsQ0FBQyxFQUFFLG1IQUFtSCxhQUFhLENBQUMsRUFBRSxvSEFBb0gsYUFBYSxDQUFDLEVBQUUsZ0dBQWdHLGFBQWEsQ0FBQyxFQUFFLG9HQUFvRyxhQUFhLENBQUMsRUFBRSxtSUFBbUksYUFBYSxDQUFDLEVBQUUseUdBQXlHLHVCQUF1QixDQUFDLEVBQUUsK0dBQStHLGFBQWEsQ0FBQyxFQUFFLCtHQUErRyxhQUFhLENBQUMsRUFBRSxnSEFBZ0gsYUFBYSxDQUFDLEVBQUUsK0dBQStHLGFBQWEsQ0FBQyxFQUFFLGdHQUFnRyw2QkFBNkIsQ0FBQyw2QkFBNkIsQ0FBQyxFQUFFLHlFQUF5RSx1QkFBdUIsQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUMsRUFBRSxxRUFBcUUsdUJBQXVCLENBQUMsMEJBQTBCLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLDRCQUE0QixDQUFDLEVBQUUseUVBQXlFLGNBQWMsQ0FBQyxlQUFlLENBQUMsRUFBRSxtRUFBbUUseUJBQXlCLENBQUMsdUJBQXVCLENBQUMsRUFBRSwyRUFBMkUsdUJBQXVCLENBQUMsQ0FBQztBQUFDLHlCQUF5Qiw4Q0FBOEMsYUFBYSxDQUFDLEVBQUUsOENBQThDLGFBQWEsQ0FBQyxDQUFDO0FBQUMseUJBQXlCLDhDQUE4QyxTQUFTLENBQUMsRUFBRSxpREFBaUQsVUFBVSxDQUFDLEVBQUUsMkVBQTJFLHdCQUF3QixDQUFDLDRCQUE0QixDQUFDLEVBQUUsOENBQThDLFNBQVMsQ0FBQyxFQUFFLHVWQUF1Vix3QkFBd0IsQ0FBQyxFQUFFLHFNQUFxTSx3QkFBd0IsQ0FBQyxFQUFFLHlFQUF5RSxjQUFjLENBQUMsZUFBZSxDQUFDLEVBQUUsMkVBQTJFLHNCQUFzQixDQUFDLEVBQUUsZ0VBQWdFLHlCQUF5QixDQUFDLEVBQUUsK0RBQStELHVCQUF1QixDQUFDLENBQUMiLCJmaWxlIjoic3JjL2FwcC9ob21lL2hvbWUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5mb290ZXJ7XHJcbiAgYmFja2dyb3VuZDogIzExMUQ1RSAhaW1wb3J0YW50O1xyXG4gIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xyXG5cclxufVxyXG5mb290ZXIgYXtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbi5kcm9we1xyXG4gIG1pbi13aWR0aDogLW1vei1hdmFpbGFibGU7XHJcbiAgbWFyZ2luLWxlZnQ6IC03MHB4O1xyXG59XHJcblxyXG4ubmF2YmFyLW5hdntcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBoZWlnaHQ6IDQwcHhcclxuICB9XHJcbiAgLm5hdi1saW5re1xyXG4gICAgICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcclxuICB9XHJcbiAgLm5hdmxpbmt3aGl0e1xyXG4gICAgICBjb2xvcjogIzExMUQ1RSAhaW1wb3J0YW50O1xyXG4gIH1cclxuLmNvbntcclxuICAgICAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbiAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgICAgYmFja2dyb3VuZDogcmVkO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgICB3aWR0aDogMTIwcHg7XHJcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIH1cclxuICAudG9wbmF2IGxpIGE6aG92ZXIge1xyXG4gICAgICBib3JkZXItYm90dG9tOiAwLjFweCBzb2xpZCByZWQ7XHJcblxyXG4gIH1cclxuICAubmF2X3R7XHJcbiAgICAgIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xyXG4gIH1cclxuICAubmF2X2d7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICMxMTFENUU7XHJcbiAgfVxyXG4gIC5uYXZ3aGl0e1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRUJFQ0YwO1xyXG5cclxuICB9XHJcbiAgLm5hdl9pbWd7XHJcbiAgICAgIHdpZHRoOiA4MHB4O1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gIH1cclxuICAgICAubmF2YmFyLWJyYW5kIHtcclxuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICBwYWRkaW5nLXRvcDogLjMxMjVyZW07XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiAuMzEyNXJlbTtcclxuICAgICAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xyXG4gICAgICBmb250LXNpemU6IDEuMjVyZW07XHJcbiAgICAgIGxpbmUtaGVpZ2h0OiBpbmhlcml0O1xyXG4gICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgICBtYXJnaW4tbGVmdDogNzVweDtcclxuICAgICAgei1pbmRleDogNTtcclxuICB9XHJcblxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiBzaWRlIGJhciAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uZmxvYXRfYWN0aW9ucyB7XHJcbnBvc2l0aW9uOiBmaXhlZDtcclxuYmFja2dyb3VuZDogcmVkO1xyXG5ib3JkZXItcmFkaXVzOiAxMHB4O1xyXG5sZWZ0OiAwO1xyXG50b3A6IDM1JTtcclxucGFkZGluZzogMTBweDtcclxuZGlzcGxheTogZmxleDtcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbndpZHRoOiA2JTtcclxuei1pbmRleDo1OTAwO1xyXG59XHJcblxyXG4uZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IHtcclxucGFkZGluZzogMDtcclxubWFyZ2luOiAwIDAgLTMwcHggMDtcclxuZGlzcGxheTogZmxleDtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG59XHJcbi5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtcyB7XHJcbnBhZGRpbmc6IDVweDtcclxud2lkdGg6IDEyMHB4O1xyXG5oZWlnaHQ6IDEyMHB4O1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuYWxpZ24tY29udGVudDogY2VudGVyO1xyXG5wb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbi5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtcyAuaXRlbV9ocmVmIHtcclxudGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG5kaXNwbGF5OiBibG9jaztcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbmZvbnQtc2l6ZTogMTRweDtcclxufVxyXG4uZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLml0ZW1faHJlZiAuaWhyZWZfbG9nbyB7XHJcbndpZHRoOiAxMDBweDtcclxuaGVpZ2h0OiAxMDBweDtcclxubWFyZ2luLWxlZnQ6IDI1cHg7XHJcbn1cclxuLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zIC5pdGVtX2hyZWYgLmlocmVmX3RleHQge1xyXG50ZXh0LWFsaWduOiBjZW50ZXI7XHJcbm1hcmdpbjogMDtcclxuY29sb3I6IHdoaXRlO1xyXG5tYXJnaW4tdG9wOiA3cHg7XHJcbn1cclxuXHJcblxyXG4gLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXM6OmJlZm9yZXtcclxuIGNvbnRlbnQ6IFwiPlwiO1xyXG4gcG9zaXRpb246IGFic29sdXRlO1xyXG4gcmlnaHQ6IC0xMHB4O1xyXG4gdG9wOiAxNSU7XHJcbiBjb2xvcjogd2hpdGU7XHJcbiBmb250LXNpemU6IDIwcHg7XHJcbiB3aWR0aDogNDAlO1xyXG4gZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiB9XHJcbiAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLnRlc3RtZWdpe1xyXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICByaWdodDogLTE4MHB4O1xyXG4gICB0b3A6IDE1JTtcclxuICAgY29sb3I6IGJsYWNrO1xyXG4gICBmb250LXNpemU6IDE3cHg7XHJcbiAgIHdpZHRoOiA0MCU7XHJcbiAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICB3aWR0aDogMTgwcHg7XHJcbiAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgaGVpZ2h0OiA0MHB4O1xyXG4gICBkaXNwbGF5OiBub25lO1xyXG4gICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuIH1cclxuICAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXM6aG92ZXIgLnRlc3RtZWdpe1xyXG4gZGlzcGxheTogZmxleDtcclxuIH1cclxuIFxyXG4gLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLml0ZW1faHJlZiB7XHJcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gfVxyXG4gLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLml0ZW1faHJlZiAuaWhyZWZfbG9nbyB7XHJcbiAgd2lkdGg6IDEwMHB4O1xyXG4gIGhlaWdodDogMTAwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDI1cHg7XHJcbiB9XHJcbiAucHJpbWFyeV9ib2R5IC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtcyAuaXRlbV9ocmVmIC5paHJlZl90ZXh0IHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBtYXJnaW4tdG9wOiA3cHg7XHJcbiB9XHJcbiBcclxuXHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmhvbWUgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmRpdmlkZXIgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG5cclxuLnByaW1hcnlfYm9keSAuZGl2aWRlciAuZGl2aWRlcl9saWduZSB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiA1cHg7XHJcbiAgYmFja2dyb3VuZDogIzExMWQ1ZTtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiB9XHJcbiAucHJpbWFyeV9ib2R5IC5kaXZpZGVyIC5kaXZpZGVyX2xpZ25lOjpiZWZvcmUge1xyXG4gIGNvbnRlbnQ6IFwiXCI7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHdpZHRoOiAzMCU7XHJcbiAgdG9wOiAwO1xyXG4gIGxlZnQ6IDA7XHJcbiAgaGVpZ2h0OiA1cHg7XHJcbiAgYmFja2dyb3VuZDogcmVkO1xyXG4gfVxyXG4gLnByaW1hcnlfYm9keSAuZGl2aWRlciAuZGl2aWRlcl9saWduZTo6YWZ0ZXIge1xyXG4gIGNvbnRlbnQ6IFwiXCI7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHdpZHRoOiAzMCU7XHJcbiAgdG9wOiAwO1xyXG4gIHJpZ2h0OiAwO1xyXG4gIGhlaWdodDogNXB4O1xyXG4gIGJhY2tncm91bmQ6IHJlZDtcclxuIH1cclxuIFxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrMCAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5jb250ZW50e1xyXG4gIHdpZHRoOiBtYXgtY29udGVudDtcclxuICB9XHJcbiAgLnRyYW5zdGlvbntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBtYXJnaW4tbGVmdDogLTM1MHB4O1xyXG4gICAgd2lkdGg6IG1heC1jb250ZW50O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIH1cclxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyB7XHJcbiAgYmFja2dyb3VuZDojMTExZDVlO1xyXG4gIG1pbi1oZWlnaHQ6IGZpdC1jb250ZW50O1xyXG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMDBweDtcclxuIH1cclxuIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcgLmhlYWRpbmdfd3JhcHBlciAuaW1nX3dyYXBwZXIgLmhlYWRpbmdfaW1nIHtcclxuICBoZWlnaHQ6IDEwMDBweDtcclxuICBtYXgtaGVpZ2h0OiA2NDBweDtcclxuICBtYXJnaW4tdG9wOiAxO1xyXG4gIHdpZHRoOiAxMDUwcHg7XHJcbiB9XHJcbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLnRpdGxlX2hlYWRpbmcge1xyXG4gIGZvbnQtc2l6ZTogNjBweDtcclxuICBmb250LXdlaWdodDogODAwO1xyXG4gIGNvbG9yOiAgI2ZmZmZmZjtcclxuICB3aWR0aDogbWF4LWNvbnRlbnQ7XHJcbiB9XHJcbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmRlc2NfaGVhZGluZyB7XHJcbiAgZm9udC1zaXplOiA2MHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgY29sb3I6ICAjZmZmZmZmO1xyXG4gIHdpZHRoOiBtYXgtY29udGVudDtcclxuIH1cclxuIC5jYXJyZSB7XHJcbiAgd2lkdGg6IDIwMHB4O1xyXG4gIGhlaWdodDogOTBweDtcclxuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICBib3JkZXItcmFkaXVzOiAxOHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiAtMjkwcHg7XHJcbiAgbWFyZ2luLXRvcDogMzg1cHg7XHJcbiAgfVxyXG5cclxuXHJcbiAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgKiwgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgKjpiZWZvcmUsIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jICo6YWZ0ZXIge1xyXG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICAgfVxyXG4gICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyBib2R5IHtcclxuICAgYmFja2dyb3VuZDogI2Y1ZjVmNTtcclxuICAgfVxyXG4gICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyB7XHJcbiAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgbWFyZ2luLWxlZnQ6IHVuc2V0O1xyXG4gICB3aWR0aDogNjUwcHg7XHJcbiAgIG1hcmdpbi1sZWZ0OiAxMDBweDtcclxuICAgfVxyXG4gICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyBtYWluIHtcclxuICAgIGxlZnQ6IDUwJTtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHRvcDogNTAlO1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpIHRyYW5zbGF0ZVkoLTUwJSk7XHJcbiAgICB3aWR0aDogMzAwcHg7XHJcbiAgIH1cclxuICAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaDpiZWZvcmUsIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2g6YWZ0ZXIge1xyXG4gICAgY29udGVudDogXCJcIjtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICB9XHJcbiAgIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2g6YmVmb3JlIHtcclxuICAgIGJvcmRlcjogNXB4IHNvbGlkICNmZmZmZmYgO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2Utb3V0O1xyXG4gICAgdHJhbnNpdGlvbi1kZWxheTogMC4zcztcclxuICAgIHdpZHRoOiA0MHB4O1xyXG4gICB9XHJcbiAgIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2g6YWZ0ZXIge1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZmZmZjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDNweDtcclxuICAgIGhlaWdodDogNXB4O1xyXG4gICAgdHJhbnNmb3JtOiByb3RhdGUoLTQ1ZGVnKTtcclxuICAgIHRyYW5zZm9ybS1vcmlnaW46IDAlIDEwMCU7XHJcbiAgICB0cmFuc2l0aW9uOiBhbGwgMC4zcyBlYXNlLW91dDtcclxuICAgIHdpZHRoOiAxNXB4O1xyXG4gICB9XHJcbiAgIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2hfX2lucHV0IHtcclxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDQwcHg7XHJcbiAgICBvcGFjaXR5OiAwO1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIHBhZGRpbmc6IDAgMTVweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRyYW5zaXRpb246IGFsbCAwLjNzIGVhc2Utb3V0O1xyXG4gICAgdHJhbnNpdGlvbi1kZWxheTogMC42cztcclxuICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgei1pbmRleDogMTtcclxuICAgIGNvbG9yOiByZ2IoMjIzLCAyMjMsIDIyMyk7XHJcbiAgIH1cclxuICAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0taGlkZTpiZWZvcmUge1xyXG4gICAgdHJhbnNpdGlvbi1kZWxheTogMC4zcztcclxuICAgfVxyXG4gICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1oaWRlOmFmdGVyIHtcclxuICAgIHRyYW5zaXRpb24tZGVsYXk6IDAuNnM7XHJcbiAgIH1cclxuICAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0taGlkZSAuc2VhcmNoX19pbnB1dCB7XHJcbiAgICB0cmFuc2l0aW9uLWRlbGF5OiAwcztcclxuICAgfVxyXG4gICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93OmFmdGVyIHtcclxuICAgIHRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKSB0cmFuc2xhdGVYKDE1cHgpIHRyYW5zbGF0ZVkoLTJweCk7XHJcbiAgICB3aWR0aDogMDtcclxuICAgfVxyXG4gICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93OmJlZm9yZSB7XHJcbiAgICBib3JkZXI6IDVweCBzb2xpZCAjZmZmZmZmO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIHdpZHRoOiA1MDBweDtcclxuICAgfVxyXG4gICAucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyAuc2VhcmNoLS1zaG93IC5zZWFyY2hfX2lucHV0IHtcclxuICAgIG9wYWNpdHk6IDE7XHJcbiAgICB3aWR0aDogNTAwcHg7XHJcbiAgIH1cclxuXHJcbiAgIFxyXG5cclxuaW5wdXRbdHlwZT1cInRleHRcIl0ge1xyXG4gIGhlaWdodDogNTBweDtcclxuICBmb250LXNpemU6IDMwcHg7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIFxyXG4gIGZvbnQtd2VpZ2h0OiAxMDA7XHJcbiAgYm9yZGVyOiBub25lO1xyXG4gIG91dGxpbmU6IG5vbmU7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIHBhZGRpbmc6IDNweDtcclxuICBwYWRkaW5nLXJpZ2h0OiA2MHB4O1xyXG4gIHdpZHRoOiAwcHg7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIHRvcDogMDtcclxuICByaWdodDogMDtcclxuICBiYWNrZ3JvdW5kOiBub25lO1xyXG4gIHotaW5kZXg6IDM7XHJcbiAgdHJhbnNpdGlvbjogd2lkdGggMC40cyBjdWJpYy1iZXppZXIoMCwgMC43OTUsIDAsIDEpO1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICB9XHJcbiAgXHJcbiAgaW5wdXRbdHlwZT1cInRleHRcIl06Zm9jdXM6aG92ZXIge1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB3aGl0ZTtcclxuICB9XHJcbiAgXHJcbiAgaW5wdXRbdHlwZT1cInRleHRcIl06Zm9jdXMge1xyXG4gIHdpZHRoOiA3MDBweDtcclxuICB6LWluZGV4OiAxO1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB3aGl0ZTtcclxuICBjdXJzb3I6IHRleHQ7XHJcbiAgfVxyXG4gIGlucHV0W3R5cGU9XCJzdWJtaXRcIl0ge1xyXG4gIGhlaWdodDogNTBweDtcclxuICB3aWR0aDogNTBweDtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIGZsb2F0OiByaWdodDtcclxuICBiYWNrZ3JvdW5kOiB1cmwoZGF0YTppbWFnZS9wbmc7YmFzZTY0LGlWQk9SdzBLR2dvQUFBQU5TVWhFVWdBQUFEQUFBQUF3Q0FNQUFBQmczQW0xQUFBQUdYUkZXSFJUYjJaMGQyRnlaUUJCWkc5aVpTQkpiV0ZuWlZKbFlXUjVjY2xsUEFBQUFETlFURlJGVTFOVDlmWDFsSlNVWGw1ZTFkWFZmbjUrYzNOejZ1cnF2NysvdExTMGlZbUpxYW1wbjUrZnlzckszOS9mYVdscC8vLy9WaTRaeXdBQUFCRjBVazVULy8vLy8vLy8vLy8vLy8vLy8vLy8vd0FsclpsaUFBQUJMa2xFUVZSNDJyU1dXUmJESUFoRkhlT1V0TjMvYWdzMXphQTRjSHJLWjhKRlJId29Ya3dUdndHUDFRbzBiWU9iQVB3aUxtYk5BSEJXRkJabEQ5ajBKeGZsRFZpSU9iTkhHL0RvOFBSSFRKazBUZXpBaHY3cWxvSzBKSkVCaCtGOCtVL2hvcElFTE9XZmlaVUNET1pEMVJBRE9RS0E3NW9xNGN2VmtjVCtPZEhucXFwUUNJVFdBam5XVmdHUVVXejEybEp1R3dHb2FXZ0JLelJWQmNDeXBnVWtPQW9XZ0JYL0wwQ214TjQwdTZ4d2NJSjFjT3pXWURmZnAzYXhzUU95dmRrWGlIOUZLUkZ3UFJIWVpVYVhNZ1BMZWlXN1FoYkRSY2l5TFhKYUtoZUN1TGJpVm9xeDFEVlJ5SDI2eWIwaHN1b09GRVBzb3orQlZFME1SbFpOakdaY1JReUhZa21NcDJoQlRJemRrekNUYy9wTHFPbkJyazcveVpkQU9xL3E1TlBCSDFmN3g3ZkdQNEMzQUFNQVFyaHpYOXpoY0dzQUFBQUFTVVZPUks1Q1lJST0pXHJcbiAgICBjZW50ZXIgY2VudGVyIG5vLXJlcGVhdDtcclxuICB0ZXh0LWluZGVudDogLTEwMDAwcHg7XHJcbiAgYm9yZGVyOiBub25lO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDA7XHJcbiAgcmlnaHQ6IDA7XHJcbiAgei1pbmRleDogMjtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgb3BhY2l0eTogMC40O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICB0cmFuc2l0aW9uOiBvcGFjaXR5IDAuNHMgZWFzZTtcclxuICB9XHJcbiAgXHJcbiAgaW5wdXRbdHlwZT1cInN1Ym1pdFwiXTpob3ZlciB7XHJcbiAgb3BhY2l0eTogMC44O1xyXG4gIH1cclxuICBcclxuXHJcblxyXG4gLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipub3RyZSBzdWNjZXMgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gIFxyXG4uaXRlbV9udW0ge1xyXG4gIG1hcmdpbjogMDtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG4gIGNvbG9yOiAjMTExZDVlO1xyXG59XHJcbi5zdWNjZXNzX2l0ZW0ge1xyXG4gIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuLml0ZW1fZGVzYyB7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIGNvbG9yOiAjMTExZDVlO1xyXG59XHJcbi5zdWNjZXNzX2xpc3Qge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gIG1hcmdpbjogMDtcclxuICBwYWRkaW5nOiAwO1xyXG59XHJcbi5zdWNjZXNzX3R4dCB7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICBmb250LXdlaWdodDogNDAwO1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBjb2xvcjogIzExMWQ1ZTtcclxufVxyXG4uc3VjY2Vzc19kZXNjIHtcclxuICBmb250LXNpemU6IDI4cHg7XHJcbiAgY29sb3I6ICMxMTFkNWU7XHJcbn1cclxuLmNvbnRhaW5lcl9ib3gge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogYXV0bztcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcblxyXG59XHJcblxyXG5cclxuLmt7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgbWFyZ2luOiBpbmhlcml0O1xyXG4gIH1cclxuICBcclxuICAgXHJcbi5jaGlmZnJle1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgICB3aWR0aDogNDAlO1xyXG4gICAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBpbmhlcml0O1xyXG4gIH1cclxuXHJcbiAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrMSAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiBcclxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLm91cl9zdWNjZXNzIC5zdWNjZXNzX3dyYXBwZXIgLnN1Y2Nlc3NfdHh0IHtcclxuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIGNvbG9yOiAjMTExZDVlO1xyXG4gfVxyXG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLm91cl9zdWNjZXNzIC5zdWNjZXNzX3dyYXBwZXIgLnN1Y2Nlc3NfZGVzYyB7XHJcbiAgZm9udC1zaXplOiAyOHB4O1xyXG4gIGNvbG9yOiAjMTExZDVlO1xyXG4gfVxyXG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLm91cl9zdWNjZXNzIC5zdWNjZXNzX3dyYXBwZXIgLnN1Y2Nlc3NfbGlzdCB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgbWFyZ2luOiAwO1xyXG4gIHBhZGRpbmc6IDA7XHJcbiB9XHJcbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAub3VyX3N1Y2Nlc3MgLnN1Y2Nlc3Nfd3JhcHBlciAuc3VjY2Vzc19saXN0IC5zdWNjZXNzX2l0ZW0ge1xyXG4gIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiB9XHJcbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAub3VyX3N1Y2Nlc3MgLnN1Y2Nlc3Nfd3JhcHBlciAuc3VjY2Vzc19saXN0IC5zdWNjZXNzX2l0ZW0gLml0ZW1fbnVtIHtcclxuICBtYXJnaW46IDA7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICBjb2xvcjogIzExMWQ1ZTtcclxuIH1cclxuIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5vdXJfc3VjY2VzcyAuc3VjY2Vzc193cmFwcGVyIC5zdWNjZXNzX2xpc3QgLnN1Y2Nlc3NfaXRlbSAuaXRlbV9kZXNjIHtcclxuICBtYXJnaW46IDA7XHJcbiAgY29sb3I6ICMxMTFkNWU7XHJcbiB9XHJcbiBcclxuIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2syICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5jYXJkLWJsb2cgLmNhcmRfd3JhcHBlciAucm93IHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTExZDVlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiB9XHJcbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuY2FyZC1ibG9nIC5jYXJkX3dyYXBwZXIgLnJvdyAuY2FyZC1ib2R5IHtcclxuICBtYXJnaW4tYm90dG9tOiAxNHB4O1xyXG4gIG1hcmdpbi10b3A6IC0yMnB4O1xyXG4gfVxyXG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLmNhcmQtYmxvZyAuY2FyZF93cmFwcGVyIC5yb3cgLmNhcmQtYm9keSAudGVzdC1idG4xIHtcclxuICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gfVxyXG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLmNhcmQtYmxvZyAuY2FyZF93cmFwcGVyIC5yb3cgLmNhcmQtYm9keSAudGVzdC1idG4yIHtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgYmFja2dyb3VuZDogcmVkO1xyXG4gIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIHBhZGRpbmc6IDhweCAzMHB4O1xyXG4gfVxyXG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLmNhcmQtYmxvZyAuY2FyZF93cmFwcGVyIC5yb3cgLmNhcmQtYm9keSAudGVzdC1idG4zIHtcclxuICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gfVxyXG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLmNhcmQtYmxvZyAuY2FyZF93cmFwcGVyIC5yb3cgLmNhcmQtZ3JvdXAge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuIH1cclxuIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5jYXJkLWJsb2cgLmNhcmRfd3JhcHBlciAucm93IC5jYXJkLWdyb3VwIC5jYXJkIHtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBtaW4td2lkdGg6IDA7XHJcbiAgd29yZC13cmFwOiBicmVhay13b3JkO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMxMTFkNWU7XHJcbiAgYm9yZGVyLXJhZGl1czogMTIuMjVyZW07XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gfVxyXG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLmNhcmQtYmxvZyAuY2FyZF93cmFwcGVyIC5yb3cgLmNhcmQtZ3JvdXAgLmNhcmQgLmNhcmQtaW1nLXRvcCB7XHJcbiAgd2lkdGg6IDUwJTtcclxuICBtYXJnaW4tbGVmdDogOTFweDtcclxuIH1cclxuXHJcbiAuYmxvY2sye1xyXG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICBjb2xvcjogd2hpdGU7XHJcbiAgIHBhZGRpbmc6IDE5cHg7XHJcbiAgfVxyXG4gIC5ibG9jazI6aG92ZXJ7XHJcbiAgIGJvcmRlcjogNXB4IHNvbGlkIHdoaXRlO1xyXG4gIH1cclxuIFxyXG4gIC5ibG9jazIgLnRlc3QtYnRuMSB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gICAgbWFyZ2luLXRvcDogMzBweDtcclxuICB9XHJcbiAgLmJsb2NrMiAudGVzdC1idG4yIHtcclxuICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gIH1cclxuICAuYmxvY2syIC50ZXN0LWJ0bjMge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICB9XHJcbiAgLmJsb2NrMjpob3ZlciAudGVzdC1idG4xIHtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIG1hcmdpbi10b3A6IDMwcHg7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOnJlZDsgXHJcbiAgfVxyXG4gIC5ibG9jazI6aG92ZXIgLnRlc3QtYnRuMiB7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOnJlZDsgXHJcbiAgfVxyXG4gIC5ibG9jazI6aG92ZXIgLnRlc3QtYnRuMyB7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOnJlZDsgXHJcbiAgfVxyXG4gIC5ob21le1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgfVxyXG4gLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazMgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gXHJcbi5ibG9jazMgcHtcclxuICBwYWRkaW5nLXJpZ2h0OiAyNXJlbTtcclxuICB3aWR0aDogODAwcHg7XHJcbn1cclxuLnRleHQtYmxvYyAudGV4dF9ib2R5e1xyXG4gIG1hcmdpbi1sZWZ0OiAxNXB4O1xyXG59XHJcbi50ZXh0LWJsb2MgLnRleHRfYm9keSAudGV4dF9jb250ZW50e1xyXG4gIGZvbnQtc2l6ZTogNzBweDtcclxuICBjb2xvcjojMTExZDVlXHJcbn1cclxuLnRleHQtYmxvYyAudGV4dF9ib2R5ICBoMXtcclxuICBtYXJnaW4tdG9wOiAtNzVweDtcclxuICBtYXJnaW4tbGVmdDogLTE3cHg7XHJcbiAgY29sb3I6IzExMWQ1ZVxyXG59XHJcblxyXG4gLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazQgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gXHJcbi5ibG9jazR7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uL2Fzc2V0cy9jcG5pbWFnZXMvaG9tZS9zaW4ucG5nKTtcclxuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG4gIGJhY2tncm91bmQtcG9zaXRpb246NjU4cHggMjM0cHg7XHJcbiAgbWFyZ2luLXRvcDoxMDBweDtcclxufVxyXG4uYmxvY2s0IGgxe1xyXG4gIGNvbG9yOiMxMTFkNWU7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuXHJcbi5ibG9jazQgLmJsb2NrMSBwe3BhZGRpbmctcmlnaHQ6IDQ4cmVtO31cclxuXHJcblxyXG4ub3V0ZXItZGl2LFxyXG4uaW5uZXItZGl2IHtcclxuICBoZWlnaHQ6IDM3OHB4O1xyXG4gIG1heC13aWR0aDogMzAwcHg7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcblxyXG4ub3V0ZXItZGl2IHtcclxuICBwZXJzcGVjdGl2ZTogOTAwcHg7XHJcbiAgcGVyc3BlY3RpdmUtb3JpZ2luOiA1MCUgY2FsYyg1MCUgLSAxOGVtKTtcclxufVxyXG4gLm9uZXtcclxubWFyZ2luOiAxMXB4IDBweCAwcHggNTU2cHhcclxufVxyXG4gLnR3b3tcclxubWFyZ2luOiAtNTgwcHggMCAwIDEwNjVweFxyXG59XHJcbi50aHJlZXtcclxubWFyZ2luOiAtMTJweCAwIDAgMTA1OXB4XHJcbn1cclxuXHJcbi5pbm5lci1kaXYge1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICBmb250LXdlaWdodDogNDAwO1xyXG4gIGNvbG9yOiBibGFjaztcclxuICBmb250LXNpemU6IDFyZW07XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gXHJcbn1cclxuXHJcblxyXG5cclxuLmZyb250IHtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgaGVpZ2h0OiA4NSU7XHJcbiAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgYmFja2ZhY2UtdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gIGJveC1zaGFkb3c6IDAgMCA0MHB4IHJnYmEoMCwgMCwgMCwgMC4xKSBpbnNldDtcclxuICBib3gtc2hhZG93OiAwcHggMXB4IDE1cHggZ3JleTtcclxuICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB0b3A6IDA7XHJcbiAgbGVmdDogMDtcclxufVxyXG5cclxuXHJcbi5mcm9udF9fZmFjZS1waG90bzEge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB0b3A6IDEwcHg7XHJcbiAgaGVpZ2h0OiAxMjBweDtcclxuICB3aWR0aDogMTIwcHg7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG5cclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAvKiBiYWNrZmFjZS12aXNpYmlsaXR5OiBoaWRkZW47XHJcbiAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC42cyBjdWJpYy1iZXppZXIoMC44LCAtMC40LCAwLjIsIDEuNyk7XHJcbiAgICAgICB6LWluZGV4OiAzOyovXHJcbn1cclxuXHJcblxyXG4uZnJvbnRfX2ZhY2UtcGhvdG8yIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgdG9wOiAxMHB4O1xyXG4gIGhlaWdodDogMTIwcHg7XHJcbiAgd2lkdGg6IDEyMHB4O1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAvKiBiYWNrZmFjZS12aXNpYmlsaXR5OiBoaWRkZW47XHJcbiAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC42cyBjdWJpYy1iZXppZXIoMC44LCAtMC40LCAwLjIsIDEuNyk7XHJcbiAgICAgICB6LWluZGV4OiAzOyovXHJcbn1cclxuXHJcblxyXG4uZnJvbnRfX2ZhY2UtcGhvdG8zIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgdG9wOiAxMHB4O1xyXG4gIGhlaWdodDogMTIwcHg7XHJcbiAgd2lkdGg6IDEyMHB4O1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuXHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgLyogYmFja2ZhY2UtdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gICAgICAgdHJhbnNpdGlvbjogYWxsIDAuNnMgY3ViaWMtYmV6aWVyKDAuOCwgLTAuNCwgMC4yLCAxLjcpO1xyXG4gICAgICAgei1pbmRleDogMzsqL1xyXG59XHJcblxyXG4uZnJvbnRfX3RleHQge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB0b3A6IDM1cHg7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgZm9udC1mYW1pbHk6IFwiTW9udHNlcnJhdFwiO1xyXG4gIGZvbnQtc2l6ZTogMThweDtcclxuICBiYWNrZmFjZS12aXNpYmlsaXR5OiBoaWRkZW47XHJcbn1cclxuXHJcbi5mcm9udF9fdGV4dC1oZWFkZXIge1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgZm9udC1mYW1pbHk6IFwiT3N3YWxkXCI7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICBmb250LXNpemU6IDIwcHg7XHJcbn1cclxuXHJcbi5mcm9udF9fdGV4dC1wYXJhIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgdG9wOiAtNXB4O1xyXG4gIGNvbG9yOiAjMDAwO1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBsZXR0ZXItc3BhY2luZzogMC40cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICBmb250LWZhbWlseTogXCJNb250c2VycmF0XCIsIHNhbnMtc2VyaWY7XHJcbn1cclxuXHJcbi5mcm9udC1pY29ucyB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHRvcDogMDtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgbWFyZ2luLXJpZ2h0OiA2cHg7XHJcbiAgY29sb3I6IGdyYXk7XHJcbn1cclxuXHJcbi5mcm9udF9fdGV4dC1ob3ZlciB7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHRvcDogMTBweDtcclxuICBmb250LXNpemU6IDEwcHg7XHJcbiAgY29sb3I6IHJlZDtcclxuICBiYWNrZmFjZS12aXNpYmlsaXR5OiBoaWRkZW47XHJcblxyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICBsZXR0ZXItc3BhY2luZzogLjRweDtcclxuXHJcbiAgYm9yZGVyOiAycHggc29saWQgcmVkO1xyXG4gIHBhZGRpbmc6IDhweCAxNXB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XHJcblxyXG4gIGJhY2tncm91bmQ6IHJlZDtcclxuICBjb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcblxyXG5cclxuXHJcblxyXG5cclxuIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2s1ICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuIC5ibG9jazUgaW1ne1xyXG4gIG1hcmdpbi1sZWZ0OiAxOTBweDtcclxuICBoZWlnaHQ6ODAlO1xyXG4gIHdpZHRoOjgwJVxyXG59XHJcbiAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrNiAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiBcclxuLmJsb2NrNntcclxuICBwYWRkaW5nLXRvcDogMzBweDsgXHJcbiAgcGFkZGluZy1sZWZ0OiAxMTBweDtcclxuICBtYXJnaW4tdG9wOiAtNTAxcHg7IFxyXG4gIGZsb2F0OnJpZ2h0XHJcbn1cclxuLmJsb2NrNiAudGhpcmQtYmxvYy1ib3JkZXJ7XHJcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgYm9yZGVyOiAycHggc29saWQgI0M1QzVDNTtcclxuICAgIG1hcmdpbi10b3A6IDIwcHg7IFxyXG4gICAgd2lkdGg6IDkwJTsgXHJcbiAgcGFkZGluZzogMjBweDtcclxufVxyXG4uYmxvY2s2IC50aGlyZC1ibG9jLWJvcmRlciBoM3tcclxuICBmb250LXNpemU6IDI2cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxufVxyXG4uYmxvY2s2IC50aGlyZC1ibG9jLWJvcmRlcjpob3ZlcntcclxuYm9yZGVyOiAycHggc29saWQgYmx1ZTtcclxufVxyXG4uYmxvY2s2IC50aGlyZC1ibG9jLWJvcmRlcjpob3ZlciBoM3tcclxuICBjb2xvcjogYmx1ZTsgIFxyXG59XHJcblxyXG5cclxuIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2s3ICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuIC5sYXN0QntcclxuICB3aWR0aDogMTAwJTtcclxuIFxyXG59XHJcbi5ibG9jazd7XHJcbiAgcGFkZGluZy10b3A6IDEwMHB4XHJcbn1cclxuXHJcbi5jYXJkMXtcclxuICBtYXJnaW4tcmlnaHQ6IDUwcHg7aGVpZ2h0OiAzNDVweDt3aWR0aDogMzE1cHg7XHJcbn1cclxuLmNhcmQxIC5ib3gxe1xyXG4gIGJveC1zaGFkb3c6IDBweCAxcHggMTVweCBncmV5O1xyXG4gIGJvcmRlcjogbm9uZTtcclxuICBib3JkZXItcmFkaXVzOiA3MXB4IDE0cHggNzFweCAxNHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgcGFkZGluZzogNDBweDtcclxufVxyXG4uY2FyZDJ7XHJcbiAgbWFyZ2luLXJpZ2h0OiA1MHB4O1xyXG4gIGhlaWdodDogMzQ1cHg7XHJcbiAgd2lkdGg6IDMxNXB4O1xyXG59XHJcbi5jYXJkMiAuYm94MntcclxuICBib3gtc2hhZG93OiAwcHggMXB4IDE1cHggZ3JleTtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgYm9yZGVyLXJhZGl1czogNzFweCAxNHB4IDcxcHggMTRweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gIHBhZGRpbmc6IDQwcHg7XHJcbn1cclxuLmNhcmQze1xyXG4gIG1hcmdpbi1yaWdodDogNTBweDtcclxuICBoZWlnaHQ6IDM0NXB4O1xyXG4gIHdpZHRoOiAzMTVweDtcclxufVxyXG4uY2FyZDMgLmJveDN7XHJcbiAgYm94LXNoYWRvdzogMHB4IDFweCAxNXB4IGdyZXk7XHJcbiAgYm9yZGVyOiBub25lO1xyXG4gIGJvcmRlci1yYWRpdXM6IDcxcHggMTRweCA3MXB4IDE0cHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICBwYWRkaW5nOiA0MHB4O1xyXG59XHJcbi5jYXJkMSAuYm94MSBoNHtcclxuICBwYWRkaW5nOiA2OXB4IDAgMCAyNnB4O1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICBjb2xvcjojMDBCRkZGXHJcbn1cclxuLmNhcmQxIC5ib3gxIGg2e1xyXG4gIHBhZGRpbmc6IDBweCAwIDBweCA1NHB4O1xyXG4gIGZvbnQtc2l6ZTogMTBweDtcclxuICBjb2xvcjojYzdjN2M3XHJcbn1cclxuLmNhcmQxIC5pbWFnZXtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgdG9wOiAtMTMycHg7XHJcbiAgbGVmdDogLTExNnB4O1xyXG4gIGhlaWdodDogMTAwcHg7XHJcbiAgd2lkdGg6IDEwMHB4O1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG4uY2FyZDEgLmltYWdlIGltZ3tcclxuICB3aWR0aDogMTIwJTtcclxuICBoZWlnaHQ6IDEyMCVcclxufVxyXG5cclxuLmNhcmQyIC5ib3gyIGg0e1xyXG4gIHBhZGRpbmc6IDU0cHggMCAwIDI2cHg7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG4gICBjb2xvcjojMDBCRkZGXHJcbn1cclxuLmNhcmQyIC5ib3gyIGg2e1xyXG4gIHBhZGRpbmc6IDBweCAwIDBweCA1NHB4O1xyXG4gIGZvbnQtc2l6ZTogMTBweDtcclxuICBjb2xvcjojYzdjN2M3XHJcbn1cclxuLmNhcmQyIC5pbWFnZXtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgIHRvcDogLTEzMnB4O1xyXG4gIGxlZnQ6IC0xMTZweDtcclxuICBoZWlnaHQ6IDEwMHB4O1xyXG4gIHdpZHRoOiAxMDBweDtcclxuICBtYXJnaW46IDAgYXV0bztcclxuICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG4uY2FyZDIgLmltYWdlIGltZ3tcclxuICB3aWR0aDogMTIwJTtcclxuICBoZWlnaHQ6IDEyMCVcclxufVxyXG5cclxuLmNhcmQzIC5ib3gzIGg0e1xyXG4gIHBhZGRpbmc6IDQ0cHggMCAwIDI2cHg7XHJcbiAgZm9udC1zaXplOiAyMHB4O1xyXG4gICBjb2xvcjojMDBCRkZGXHJcbn1cclxuLmNhcmQzIC5ib3gzIGg2e1xyXG4gIHBhZGRpbmc6IDBweCAwIDBweCA1NHB4O1xyXG4gIGZvbnQtc2l6ZTogMTBweDtcclxuICBjb2xvcjojYzdjN2M3XHJcbn1cclxuLmNhcmQzIC5pbWFnZXtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgdG9wOiAtMTMycHg7XHJcbiAgbGVmdDogLTExNnB4O1xyXG4gIGhlaWdodDogMTAwcHg7XHJcbiAgd2lkdGg6IDEwMHB4O1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG4uY2FyZDMgLmltYWdlIGltZ3tcclxuICB3aWR0aDogMTIwJTtcclxuICBoZWlnaHQ6IDEyMCVcclxufVxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcblxyXG5cclxuaDF7XHJcbmZvbnQtd2VpZ2h0OiBib2xkO1xyXG5jb2xvcjogIzExMWQ1ZTtcclxubWFyZ2luOiA1MHB4IDAgNTBweCAwO1xyXG59XHJcblxyXG5oNXtcclxuY29sb3I6ICMxMTFkNWU7XHJcbmZvbnQtc2l6ZTogMTVweDtcclxufVxyXG5we1xyXG5mb250LXNpemU6IDEycHg7XHJcbmNvbG9yOiAjMTExZDVlO1xyXG5cclxufVxyXG4uZm9vdGVyIHB7XHJcbiAgZm9udC1zaXplOiAxMnB4O1xyXG5jb2xvcjogI2ZmZmZmZjtcclxufVxyXG5cclxuXHJcbi5jb3B5cmlnaHR7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzBjMTMzYTtcclxuICBjb2xvcjojZmZmO1xyXG4gIGZvbnQtc2l6ZToxM3B4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipyZXNwb25zaXZlIGNzcyAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG5cclxuXHJcbiAgLyogQ3VzdG9tLCBpUGhvbmUgUmV0aW5hICAqL1xyXG4gICAgQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoIDogMzIwcHgpIGFuZCAobWF4LXdpZHRoIDogNDgwcHgpICB7XHJcbiAgICAgXHJcbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqbmF2IGJhcioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gICAgLm5hdmJhci1icmFuZCB7XHJcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgICAgIHBhZGRpbmctdG9wOiAuMzEyNXJlbTtcclxuICAgICAgICBwYWRkaW5nLWJvdHRvbTogLjMxMjVyZW07XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMS4yNXJlbTtcclxuICAgICAgICBsaW5lLWhlaWdodDogaW5oZXJpdDtcclxuICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAwO1xyXG4gICAgICAgIHotaW5kZXg6IDU7XHJcbiAgfVxyXG4gIFxyXG4gIC5kcm9we1xyXG4gICAgbWluLXdpZHRoOiAtbW96LWF2YWlsYWJsZTtcclxuICAgIG1hcmdpbi1sZWZ0OiAwcHg7XHJcbiAgfVxyXG4gIFxyXG4ubmF2YmFyLW5hdntcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBoZWlnaHQ6bWF4LWNvbnRlbnQ7XHJcbiAgfVxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzaWRlIGJhcioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi8gIFxyXG4ucHJpbWFyeV9ib2R5ICAuZmxvYXRfYWN0aW9uc3tcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbmJhY2tncm91bmQ6IHJlZDtcclxuYm9yZGVyLXJhZGl1czogMTBweDtcclxubGVmdDogMDtcclxudG9wOiAzNSU7XHJcbnBhZGRpbmc6IDEwcHg7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG53aWR0aDogMyU7XHJcbmhlaWdodDogNSU7XHJcbnotaW5kZXg6NTkwMDtcclxufVxyXG4ucHJpbWFyeV9ib2R5ICAuZmxvYXRfYWN0aW9uczo6YWZ0ZXJ7XHJcbiAgY29udGVudDogXCI+XCI7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuYmFja2dyb3VuZDogcmVkO1xyXG5ib3JkZXItcmFkaXVzOiAxMHB4O1xyXG5sZWZ0OiAwO1xyXG50b3A6IDM1JTtcclxucGFkZGluZzogMHB4O1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxud2lkdGg6IDUlO1xyXG5oZWlnaHQ6IDUlO1xyXG5mb250LXdlaWdodDogYm9sZDtcclxuZm9udC1zaXplOiAyMHB4O1xyXG5cclxufVxyXG4ucHJpbWFyeV9ib2R5ICAuZmxvYXRfYWN0aW9uczpob3ZlcjphZnRlcntcclxuICBjb250ZW50OiBcIj5cIjtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG5iYWNrZ3JvdW5kOiByZWQ7XHJcbmJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbmxlZnQ6IDA7XHJcbnRvcDogMzUlO1xyXG5wYWRkaW5nOiAxMHB4O1xyXG5kaXNwbGF5OiBub25lO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxud2lkdGg6IDMlO1xyXG5oZWlnaHQ6IDUlO1xyXG59XHJcbi5wcmltYXJ5X2JvZHkgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCB7XHJcbiAgcGFkZGluZzogMDtcclxuICBtYXJnaW46IDAgMCAtMzBweCAwO1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbn1cclxuLnByaW1hcnlfYm9keSAgLmZsb2F0X2FjdGlvbnM6aG92ZXJ7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIGJhY2tncm91bmQ6IHJlZDtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIGxlZnQ6IDA7XHJcbiAgdG9wOiAzNSU7XHJcbiAgcGFkZGluZzogMTBweDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgd2lkdGg6IDI1JTtcclxuICBoZWlnaHQ6IGF1dG87XHJcbiAgei1pbmRleDogOTk5OTk7XHJcbn1cclxuXHJcbi5wcmltYXJ5X2JvZHkgLmZsb2F0X2FjdGlvbnM6aG92ZXIgLmFjdGlvbnNfY29udGVudCB7XHJcbnBhZGRpbmc6IDA7XHJcbm1hcmdpbjogMCAwIC0zMHB4IDA7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxufVxyXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2swKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovICBcclxuICAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5ne1xyXG4gICAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6MDtcclxuICB9XHJcbiAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdzpiZWZvcmUge1xyXG4gICAgYm9yZGVyOiA1cHggc29saWQgIzExMWQ1ZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICB3aWR0aDogMzAwcHg7XHJcbiAgfVxyXG4gIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3cgLnNlYXJjaF9faW5wdXQge1xyXG4gICAgb3BhY2l0eTogMTtcclxuICAgIHdpZHRoOiAzMDBweDtcclxuICB9XHJcblxyXG4gIC5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcgLmhlYWRpbmdfd3JhcHBlciAuaW1nX3dyYXBwZXIgLmhlYWRpbmdfaW1nIHtcclxuICAgIGZvbnQtc2l6ZTogNDBweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XHJcbiAgICBjb2xvcjogI2ZmZmZmZjtcclxuICAgIHdpZHRoOiBtYXgtY29udGVudDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbi10b3A6IGF1dG87XHJcbiAgICBoZWlnaHQ6IGF1dG87XHJcbiB9XHJcblxyXG5cclxuIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIHdpZHRoOiAzMDBweDtcclxuXHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBtYXJnaW4tbGVmdDogdW5zZXQ7XHJcbiAgd2lkdGg6IC1tb3otYXZhaWxhYmxlO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi50cmFuc3Rpb257XHJcbiAgZGlzcGxheTogZmxleDtcclxuICB3aWR0aDogLW1vei1hdmFpbGFibGU7XHJcbiAgd2lkdGg6IGZpdC1jb250ZW50O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBtYXJnaW46IDA7XHJcbn1cclxuLmNhcnJlIHtcclxuICB3aWR0aDogMjAwcHg7XHJcbiAgaGVpZ2h0OiA5MHB4O1xyXG4gIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDE4cHg7XHJcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgbWFyZ2luLXRvcDogMjUwcHg7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG4uY29udGVudHtcclxuICB3aWR0aDogZml0LWNvbnRlbnQ7XHJcbn1cclxuXHJcbi5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcgLmhlYWRpbmdfd3JhcHBlciAudGl0bGVfaGVhZGluZyB7XHJcbiAgZm9udC1zaXplOiAzMHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA4MDA7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbiAgd2lkdGg6IC1tb3otYXZhaWxhYmxlO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2syKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuXHJcblxyXG5cclxuLmJsb2NrMiAudGVzdC1idG4xIHtcclxuICBib3JkZXItcmFkaXVzOiAyNXB4O1xyXG4gIG1hcmdpbi10b3A6IDMwcHg7XHJcbiAgYm9yZGVyOiAwO1xyXG59XHJcbi5ibG9jazIgLnRlc3QtYnRuMiB7XHJcbmJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbmJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG5jb2xvcjogYmxhY2s7XHJcbmJvcmRlcjogMDtcclxufVxyXG4uYmxvY2syIC50ZXN0LWJ0bjMge1xyXG4gIGJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbiAgYm9yZGVyOiAwO1xyXG59XHJcbi5ibG9jazI6aG92ZXIgLnRlc3QtYnRuMSB7XHJcbiAgYm9yZGVyOiBub25lO1xyXG4gIG1hcmdpbi10b3A6IDMwcHg7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIGJhY2tncm91bmQtY29sb3I6cmVkOyBcclxufVxyXG4uYmxvY2syOmhvdmVyIC50ZXN0LWJ0bjIge1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjpyZWQ7IFxyXG59XHJcbi5ibG9jazI6aG92ZXIgLnRlc3QtYnRuMyB7XHJcbiAgYm9yZGVyOiBub25lO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOnJlZDsgXHJcbn1cclxuXHJcblxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipub3RyZSBzdWNjZXMqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLmNoaWZmcmV7XHJcbiAgZGlzcGxheTogZmxleDtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbndpZHRoOiAxMDAlO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG4ua3tcclxuICBtYXJnaW46IGluaXRpYWw7XHJcbn1cclxuLnN1Y2Nlc3NfaXRlbXtcclxuICBtYXJnaW4tbGVmdDogMjBweDtcclxuICBsaXN0LXN0eWxlOiBub25lO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcbiAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrIDMqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuIC5ibG9jazMgcHtcclxuICBwYWRkaW5nLXJpZ2h0OiAwO1xyXG4gIHdpZHRoOiAyNTBweDtcclxuICB9XHJcblxyXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrIDQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG5cclxuLmJsb2NrNHtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoLi4vLi4vYXNzZXRzL2NwbmltYWdlcy9ob21lL3Npbi5wbmcpO1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjo2NThweCAyMzRweDtcclxuICBtYXJnaW4tdG9wOjEwMHB4O1xyXG4gIH1cclxuICAuYmxvY2s0IC5ibG9jazF7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gIH1cclxuICAuYmxvY2s0IC5ibG9jazEgcHtcclxuICBwYWRkaW5nLXJpZ2h0OiAwO1xyXG4gIH1cclxuICBcclxuICAgLm9uZXtcclxuICBtYXJnaW46IDQwcHggMCAwIDAgO1xyXG4gIH1cclxuICAgLnR3b3tcclxuICBtYXJnaW46IDA7XHJcbiAgfVxyXG4gICAudGhyZWV7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIH1cclxuICAub3V0ZXItZGl2e1xyXG4gICAgZGlzcGxheTogY29udGVudHM7XHJcbiAgfVxyXG4gIFxyXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazUqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gIC5ibG9jazV7XHJcbiAgbWF4LXdpZHRoOiAxMDAlO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuICAuYmxvY2s1IGltZ3tcclxuICBtYXJnaW4tbGVmdDogMDtcclxuICBoZWlnaHQ6MTAwJTtcclxuICB3aWR0aDoxMDAlXHJcbiAgfVxyXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrNioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gIC5ibG9jazZ7XHJcbiAgcGFkZGluZy10b3A6IDMwcHg7IFxyXG4gIHBhZGRpbmctbGVmdDowO1xyXG4gIG1hcmdpbi10b3A6IDA7IFxyXG4gIG1heC13aWR0aDogMTAwJTtcclxuICBkaXNwbGF5OiBjb250ZW50cztcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgfVxyXG4gIFxyXG4gIC5ibG9jazYgLnRoaXJkLWJsb2MtYm9yZGVye1xyXG4gICAgbWFyZ2luOiAyMHB4IGF1dG8gMCBhdXRvO1xyXG4gIH1cclxuICBcclxuICBcclxuIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2s3ICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuIC5ibG9jazd7XHJcbiAgcGFkZGluZy10b3A6IDEwcHg7XHJcbn1cclxuXHJcbiAgICAuY2FyZDF7XHJcbiAgICAgIG1hcmdpbi1yaWdodDogMHB4O1xyXG4gICAgICBoZWlnaHQ6IDM0NXB4O1xyXG4gICAgICB3aWR0aDogMzE1cHg7XHJcbiAgICAgIH1cclxuICAgICAgLmNhcmQxIC5ib3gxe1xyXG4gICAgICBib3gtc2hhZG93OiAwcHggMXB4IDE1cHggZ3JleTtcclxuICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiA3MXB4IDE0cHggNzFweCAxNHB4O1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgICBwYWRkaW5nOiA0MHB4O1xyXG4gICAgICB9XHJcbiAgICAgIC5jYXJkMntcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IDBweDtcclxuICAgICAgICBoZWlnaHQ6IDM0NXB4O1xyXG4gICAgICAgIHdpZHRoOiAzMTVweDtcclxuICAgICAgfVxyXG4gICAgICAuY2FyZDIgLmJveDJ7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDFweCAxNXB4IGdyZXk7XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDcxcHggMTRweCA3MXB4IDE0cHg7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICAgICAgICBwYWRkaW5nOiA0MHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgLmNhcmQze1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogMHB4O1xyXG4gICAgICAgIGhlaWdodDogMzQ1cHg7XHJcbiAgICAgICAgd2lkdGg6IDMxNXB4O1xyXG4gICAgICB9XHJcbiAgICAgIC5jYXJkMyAuYm94M3tcclxuICAgICAgICBib3gtc2hhZG93OiAwcHggMXB4IDE1cHggZ3JleTtcclxuICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNzFweCAxNHB4IDcxcHggMTRweDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgICAgIHBhZGRpbmc6IDQwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICAubGFzdEJ7XHJcbiAgICAgIHdpZHRoOiA4MCU7XHJcbiAgICAgIG1hcmdpbjogNTBweDtcclxuICAgICAgfVxyXG5cclxuXHJcbiAgICAgIC50ZXh0LWNlbnRlcntcclxuICAgICAgbWFyZ2luLWJvdHRvbTogNTBweDtcclxuICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAvKiBFeHRyYSBTbWFsbCBEZXZpY2VzLCBQaG9uZXMgKi9cclxuICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDQ4MHB4KSBhbmQgKG1heC13aWR0aCA6IDc2OHB4KSAge1xyXG4gICAgICAgICAgICBcclxuICAgIFxyXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKm5hdiBiYXIqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuICAubmF2YmFyLWJyYW5kIHtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHBhZGRpbmctdG9wOiAuMzEyNXJlbTtcclxuICAgIHBhZGRpbmctYm90dG9tOiAuMzEyNXJlbTtcclxuICAgIG1hcmdpbi1yaWdodDogMXJlbTtcclxuICAgIGZvbnQtc2l6ZTogMS4yNXJlbTtcclxuICAgIGxpbmUtaGVpZ2h0OiBpbmhlcml0O1xyXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgIG1hcmdpbi1sZWZ0OiAwO1xyXG4gICAgei1pbmRleDogNTtcclxufVxyXG4ubmF2X2ltZyB7XHJcbiAgd2lkdGg6IDgwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICBtYXJnaW4tbGVmdDogMzhweDtcclxufVxyXG5cclxuLmRyb3B7XHJcbiAgbWluLXdpZHRoOiAtbW96LWF2YWlsYWJsZTtcclxuICBtYXJnaW4tbGVmdDogMHB4O1xyXG59XHJcbi5uYXZiYXItbmF2e1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIGhlaWdodDptYXgtY29udGVudDtcclxuICB9XHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzaWRlIGJhcioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi8gIFxyXG4ucHJpbWFyeV9ib2R5ICAuZmxvYXRfYWN0aW9uc3tcclxucG9zaXRpb246IGZpeGVkO1xyXG5iYWNrZ3JvdW5kOiByZWQ7XHJcbmJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbmxlZnQ6IDA7XHJcbnRvcDogMzUlO1xyXG5wYWRkaW5nOiAxMHB4O1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxud2lkdGg6IDMlO1xyXG5oZWlnaHQ6IDUlO1xyXG56LWluZGV4OjU5MDA7XHJcblxyXG59XHJcbi5wcmltYXJ5X2JvZHkgIC5mbG9hdF9hY3Rpb25zOjphZnRlcntcclxuY29udGVudDogXCI+XCI7XHJcbmNvbG9yOiB3aGl0ZTtcclxucG9zaXRpb246IGZpeGVkO1xyXG5iYWNrZ3JvdW5kOiByZWQ7XHJcbmJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbmxlZnQ6IDA7XHJcbnRvcDogMzUlO1xyXG5wYWRkaW5nOiAwcHg7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG53aWR0aDogNSU7XHJcbmhlaWdodDogNSU7XHJcbmZvbnQtd2VpZ2h0OiBib2xkO1xyXG5mb250LXNpemU6IDIwcHg7XHJcblxyXG59XHJcbi5wcmltYXJ5X2JvZHkgIC5mbG9hdF9hY3Rpb25zOmhvdmVyOmFmdGVye1xyXG5jb250ZW50OiBcIj5cIjtcclxuY29sb3I6IHdoaXRlO1xyXG5wb3NpdGlvbjogZml4ZWQ7XHJcbmJhY2tncm91bmQ6IHJlZDtcclxuYm9yZGVyLXJhZGl1czogMTBweDtcclxubGVmdDogMDtcclxudG9wOiAzNSU7XHJcbnBhZGRpbmc6IDEwcHg7XHJcbmRpc3BsYXk6IG5vbmU7XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG53aWR0aDogMyU7XHJcbmhlaWdodDogNSU7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IHtcclxucGFkZGluZzogMDtcclxubWFyZ2luOiAwIDAgLTMwcHggMDtcclxuZGlzcGxheTogbm9uZTtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG59XHJcbi5wcmltYXJ5X2JvZHkgIC5mbG9hdF9hY3Rpb25zOmhvdmVye1xyXG5wb3NpdGlvbjogZml4ZWQ7XHJcbmJhY2tncm91bmQ6IHJlZDtcclxuYm9yZGVyLXJhZGl1czogMTBweDtcclxubGVmdDogMDtcclxudG9wOiAzNSU7XHJcbnBhZGRpbmc6IDEwcHg7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG53aWR0aDogMTglO1xyXG5oZWlnaHQ6IGF1dG87XHJcbnotaW5kZXg6IDk5OTk5O1xyXG59XHJcblxyXG4ucHJpbWFyeV9ib2R5IC5mbG9hdF9hY3Rpb25zOmhvdmVyIC5hY3Rpb25zX2NvbnRlbnQge1xyXG5wYWRkaW5nOiAwO1xyXG5tYXJnaW46IDAgMCAtMzBweCAwO1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbn1cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi8gIFxyXG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5ne1xyXG5ib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czowO1xyXG59XHJcbi5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3c6YmVmb3JlIHtcclxuYm9yZGVyOiA1cHggc29saWQgIzExMWQ1ZTtcclxuYm9yZGVyLXJhZGl1czogMjBweDtcclxuaGVpZ2h0OiA0MHB4O1xyXG53aWR0aDogMzAwcHg7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdyAuc2VhcmNoX19pbnB1dCB7XHJcbm9wYWNpdHk6IDE7XHJcbndpZHRoOiAzMDBweDtcclxufVxyXG5cclxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC5pbWdfd3JhcHBlciB7XHJcbiAgd2lkdGg6IGF1dG87XHJcbn1cclxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC5pbWdfd3JhcHBlciAuaGVhZGluZ19pbWcge1xyXG4gIGhlaWdodDogMTAwJTtcclxuICBtYXgtaGVpZ2h0OiA1ODVweDtcclxuICBtYXJnaW4tdG9wOiAwO1xyXG4gIHdpZHRoOiA3NTBweDtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyB7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5tYXJnaW4tbGVmdDogdW5zZXQ7XHJcbndpZHRoOiAtbW96LWF2YWlsYWJsZTtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxudGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi50cmFuc3Rpb257XHJcbmRpc3BsYXk6IGZsZXg7XHJcbndpZHRoOiAtbW96LWF2YWlsYWJsZTtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbm1hcmdpbjogMDtcclxufVxyXG4uY2FycmUge1xyXG53aWR0aDogMjAwcHg7XHJcbmhlaWdodDogOTBweDtcclxuYmFja2dyb3VuZDogd2hpdGU7XHJcbmJvcmRlci1yYWRpdXM6IDE4cHg7XHJcbm1hcmdpbi1sZWZ0OiBhdXRvO1xyXG5tYXJnaW4tdG9wOiAyNTBweDtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG4uY29udGVudHtcclxud2lkdGg6IGZpdC1jb250ZW50O1xyXG59XHJcblxyXG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLnRpdGxlX2hlYWRpbmcge1xyXG5mb250LXNpemU6IDMwcHg7XHJcbmZvbnQtd2VpZ2h0OiA4MDA7XHJcbmNvbG9yOiAjZmZmZmZmO1xyXG53aWR0aDogLW1vei1hdmFpbGFibGU7XHJcbnRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazIqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG5cclxuXHJcblxyXG4uYmxvY2syIC50ZXN0LWJ0bjEge1xyXG5ib3JkZXItcmFkaXVzOiAyNXB4O1xyXG5tYXJnaW4tdG9wOiAzMHB4O1xyXG5ib3JkZXI6IDA7XHJcbn1cclxuLmJsb2NrMiAudGVzdC1idG4yIHtcclxuYm9yZGVyLXJhZGl1czogMjVweDtcclxuYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbmNvbG9yOiBibGFjaztcclxuYm9yZGVyOiAwO1xyXG59XHJcbi5ibG9jazIgLnRlc3QtYnRuMyB7XHJcbmJvcmRlci1yYWRpdXM6IDI1cHg7XHJcbmJvcmRlcjogMDtcclxufVxyXG4uYmxvY2syOmhvdmVyIC50ZXN0LWJ0bjEge1xyXG5ib3JkZXI6IG5vbmU7XHJcbm1hcmdpbi10b3A6IDMwcHg7XHJcbmNvbG9yOiB3aGl0ZTtcclxuYmFja2dyb3VuZC1jb2xvcjpyZWQ7IFxyXG59XHJcbi5ibG9jazI6aG92ZXIgLnRlc3QtYnRuMiB7XHJcbmNvbG9yOiB3aGl0ZTtcclxuYm9yZGVyOiBub25lO1xyXG5iYWNrZ3JvdW5kLWNvbG9yOnJlZDsgXHJcbn1cclxuLmJsb2NrMjpob3ZlciAudGVzdC1idG4zIHtcclxuYm9yZGVyOiBub25lO1xyXG5jb2xvcjogd2hpdGU7XHJcbmJhY2tncm91bmQtY29sb3I6cmVkOyBcclxufVxyXG5cclxuXHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKm5vdHJlIHN1Y2NlcyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uY2hpZmZyZXtcclxuZGlzcGxheTogZmxleDtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbndpZHRoOiAxMDAlO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG4ua3tcclxubWFyZ2luOiBpbml0aWFsO1xyXG59XHJcbi5zdWNjZXNzX2l0ZW17XHJcbm1hcmdpbi1sZWZ0OiAyMHB4O1xyXG5saXN0LXN0eWxlOiBub25lO1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrIDMqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLmJsb2NrMyBwe1xyXG5wYWRkaW5nLXJpZ2h0OiAwO1xyXG53aWR0aDogMjUwcHg7XHJcbn1cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrIDQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG5cclxuLmJsb2NrNHtcclxuYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uL2Fzc2V0cy9jcG5pbWFnZXMvaG9tZS9zaW4ucG5nKTtcclxuYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuYmFja2dyb3VuZC1wb3NpdGlvbjowcHggNTc2cHg7XHJcbm1hcmdpbi10b3A6MTAwcHg7XHJcbn1cclxuLmJsb2NrNCAuYmxvY2sxe1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5hbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxufVxyXG4uYmxvY2s0IC5ibG9jazEgcHtcclxucGFkZGluZy1yaWdodDogMDtcclxufVxyXG5cclxuLm9uZXtcclxubWFyZ2luOiA0MHB4IDAgMCAwIDtcclxufVxyXG4udHdve1xyXG5tYXJnaW46IDA7XHJcbn1cclxuLnRocmVle1xyXG5tYXJnaW46IDA7XHJcbn1cclxuLm91dGVyLWRpdntcclxuZGlzcGxheTogY29udGVudHM7XHJcbn1cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazUqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uYmxvY2s1e1xyXG5tYXgtd2lkdGg6IDEwMCU7XHJcbndpZHRoOiAxMDAlO1xyXG59XHJcbi5ibG9jazUgaW1ne1xyXG5tYXJnaW4tbGVmdDogMDtcclxuaGVpZ2h0OjEwMCU7XHJcbndpZHRoOjEwMCVcclxufVxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazYqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLmJsb2NrNntcclxucGFkZGluZy10b3A6IDMwcHg7IFxyXG5wYWRkaW5nLWxlZnQ6MDtcclxubWFyZ2luLXRvcDogMDsgXHJcbm1heC13aWR0aDogMTAwJTtcclxuZGlzcGxheTogY29udGVudHM7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG4uYmxvY2s2IC50aGlyZC1ibG9jLWJvcmRlcntcclxubWFyZ2luOiAyMHB4IGF1dG8gMCBhdXRvO1xyXG59XHJcblxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazcgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uYmxvY2s3e1xyXG5wYWRkaW5nLXRvcDogMTBweDtcclxufVxyXG5cclxuLmNhcmQxe1xyXG4gIG1hcmdpbi1yaWdodDogMHB4O1xyXG4gIGhlaWdodDogMzQ1cHg7XHJcbiAgd2lkdGg6IDMxNXB4O1xyXG4gIH1cclxuICAuY2FyZDEgLmJveDF7XHJcbiAgYm94LXNoYWRvdzogMHB4IDFweCAxNXB4IGdyZXk7XHJcbiAgYm9yZGVyOiBub25lO1xyXG4gIGJvcmRlci1yYWRpdXM6IDcxcHggMTRweCA3MXB4IDE0cHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICBwYWRkaW5nOiA0MHB4O1xyXG4gIH1cclxuICAuY2FyZDJ7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDBweDtcclxuICAgIGhlaWdodDogMzQ1cHg7XHJcbiAgICB3aWR0aDogMzE1cHg7XHJcbiAgfVxyXG4gIC5jYXJkMiAuYm94MntcclxuICAgIGJveC1zaGFkb3c6IDBweCAxcHggMTVweCBncmV5O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNzFweCAxNHB4IDcxcHggMTRweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmZmZmY7XHJcbiAgICBwYWRkaW5nOiA0MHB4O1xyXG4gICAgfVxyXG4gIC5jYXJkM3tcclxuICAgIG1hcmdpbi1yaWdodDogMHB4O1xyXG4gICAgaGVpZ2h0OiAzNDVweDtcclxuICAgIHdpZHRoOiAzMTVweDtcclxuICB9XHJcbiAgLmNhcmQzIC5ib3gze1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDFweCAxNXB4IGdyZXk7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA3MXB4IDE0cHggNzFweCAxNHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICAgIHBhZGRpbmc6IDQwcHg7XHJcbiAgICB9XHJcbiAgICBcclxuICAubGFzdEJ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG1hcmdpbjogbm9uZTtcclxuICB9XHJcblxyXG5cclxuICAudGV4dC1jZW50ZXJ7XHJcbiAgbWFyZ2luLWJvdHRvbTogNTBweDtcclxuICB9XHJcblxyXG5cclxuICAgIH1cclxuXHJcblxyXG4gLyogU21hbGwgRGV2aWNlcywgVGFibGV0cyovXHJcbiAgICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiA3NjhweCkgYW5kIChtYXgtd2lkdGggOiA5OTJweCkgIHtcclxuICAgIFxyXG4gICAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqbmF2IGJhcioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gIC5uYXZiYXItYnJhbmQge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgcGFkZGluZy10b3A6IC4zMTI1cmVtO1xyXG4gICAgcGFkZGluZy1ib3R0b206IC4zMTI1cmVtO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xyXG4gICAgZm9udC1zaXplOiAxLjI1cmVtO1xyXG4gICAgbGluZS1oZWlnaHQ6IGluaGVyaXQ7XHJcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDA7XHJcbiAgICB6LWluZGV4OiA1O1xyXG59XHJcbi5uYXZfaW1nIHtcclxuICB3aWR0aDogODBweDtcclxuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiAzOHB4O1xyXG59XHJcbi5uYXZiYXItbmF2e1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIGhlaWdodDptYXgtY29udGVudDtcclxuXHJcbiAgfVxyXG4uZHJvcHtcclxuICBtaW4td2lkdGg6IC1tb3otYXZhaWxhYmxlO1xyXG4gIG1hcmdpbi1sZWZ0OiAwcHg7XHJcbn1cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2lkZSBiYXIqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovICBcclxuLnByaW1hcnlfYm9keSAgLmZsb2F0X2FjdGlvbnN7XHJcbnBvc2l0aW9uOiBmaXhlZDtcclxuYmFja2dyb3VuZDogcmVkO1xyXG5ib3JkZXItcmFkaXVzOiAxMHB4O1xyXG5sZWZ0OiAwO1xyXG50b3A6IDM1JTtcclxucGFkZGluZzogMTBweDtcclxuZGlzcGxheTogZmxleDtcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbndpZHRoOiAzJTtcclxuaGVpZ2h0OiA1JTtcclxuei1pbmRleDo1OTAwO1xyXG59XHJcbi5wcmltYXJ5X2JvZHkgIC5mbG9hdF9hY3Rpb25zOjphZnRlcntcclxuY29udGVudDogXCI+XCI7XHJcbmNvbG9yOiB3aGl0ZTtcclxucG9zaXRpb246IGZpeGVkO1xyXG5iYWNrZ3JvdW5kOiByZWQ7XHJcbmJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbmxlZnQ6IDA7XHJcbnRvcDogMzUlO1xyXG5wYWRkaW5nOiAwcHg7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG53aWR0aDogNSU7XHJcbmhlaWdodDogNSU7XHJcbmZvbnQtd2VpZ2h0OiBib2xkO1xyXG5mb250LXNpemU6IDIwcHg7XHJcblxyXG59XHJcbi5wcmltYXJ5X2JvZHkgIC5mbG9hdF9hY3Rpb25zOmhvdmVyOmFmdGVye1xyXG5jb250ZW50OiBcIj5cIjtcclxuY29sb3I6IHdoaXRlO1xyXG5wb3NpdGlvbjogZml4ZWQ7XHJcbmJhY2tncm91bmQ6IHJlZDtcclxuYm9yZGVyLXJhZGl1czogMTBweDtcclxubGVmdDogMDtcclxudG9wOiAzNSU7XHJcbnBhZGRpbmc6IDEwcHg7XHJcbmRpc3BsYXk6IG5vbmU7XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG53aWR0aDogMyU7XHJcbmhlaWdodDogNSU7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IHtcclxucGFkZGluZzogMDtcclxubWFyZ2luOiAwIDAgLTMwcHggMDtcclxuZGlzcGxheTogbm9uZTtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG59XHJcbi5wcmltYXJ5X2JvZHkgIC5mbG9hdF9hY3Rpb25zOmhvdmVye1xyXG5wb3NpdGlvbjogZml4ZWQ7XHJcbmJhY2tncm91bmQ6IHJlZDtcclxuYm9yZGVyLXJhZGl1czogMTBweDtcclxubGVmdDogMDtcclxudG9wOiAzNSU7XHJcbnBhZGRpbmc6IDEwcHg7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG53aWR0aDogMTIlO1xyXG5oZWlnaHQ6YXV0bztcclxuei1pbmRleDogOTk5OTk7XHJcbn1cclxuXHJcbi5wcmltYXJ5X2JvZHkgLmZsb2F0X2FjdGlvbnM6aG92ZXIgLmFjdGlvbnNfY29udGVudCB7XHJcbnBhZGRpbmc6IDA7XHJcbm1hcmdpbjogMCAwIC0zMHB4IDA7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxufVxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrMCoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqLyAgXHJcbi5ibG9jazB7XHJcbiAgZmxleDogMCAwIGF1dG87XHJcbndpZHRoOiAtbW96LWF2YWlsYWJsZTtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5ne1xyXG5ib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czowO1xyXG59XHJcbi5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3c6YmVmb3JlIHtcclxuYm9yZGVyOiA1cHggc29saWQgIzExMWQ1ZTtcclxuYm9yZGVyLXJhZGl1czogMjBweDtcclxuaGVpZ2h0OiA0MHB4O1xyXG53aWR0aDogMzAwcHg7XHJcbn1cclxuLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2MgLnNlYXJjaC0tc2hvdyAuc2VhcmNoX19pbnB1dCB7XHJcbm9wYWNpdHk6IDE7XHJcbndpZHRoOiAzMDBweDtcclxufVxyXG5cclxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC5pbWdfd3JhcHBlciB7XHJcbiAgd2lkdGg6IGF1dG87XHJcbn1cclxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC5pbWdfd3JhcHBlciAuaGVhZGluZ19pbWcge1xyXG4gIGhlaWdodDogMTAwJTtcclxuICBtYXgtaGVpZ2h0OiA1ODVweDtcclxuICBtYXJnaW4tdG9wOiAwO1xyXG4gIHdpZHRoOiA3NTBweDtcclxufVxyXG4ucHJpbWFyeV9ib2R5IC5zZWFyY2hfYmxvYyB7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5tYXJnaW4tbGVmdDogdW5zZXQ7XHJcbndpZHRoOiAtbW96LWF2YWlsYWJsZTtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxudGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi50cmFuc3Rpb257XHJcbmRpc3BsYXk6IGZsZXg7XHJcbndpZHRoOiAtbW96LWF2YWlsYWJsZTtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbm1hcmdpbjogMDtcclxufVxyXG4uY2FycmUge1xyXG53aWR0aDogMjAwcHg7XHJcbmhlaWdodDogOTBweDtcclxuYmFja2dyb3VuZDogd2hpdGU7XHJcbmJvcmRlci1yYWRpdXM6IDE4cHg7XHJcbm1hcmdpbi1sZWZ0OiBhdXRvO1xyXG5tYXJnaW4tdG9wOiAyNTBweDtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG4uY29udGVudHtcclxud2lkdGg6IGZpdC1jb250ZW50O1xyXG59XHJcblxyXG4ucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLnRpdGxlX2hlYWRpbmcge1xyXG5mb250LXNpemU6IDMwcHg7XHJcbmZvbnQtd2VpZ2h0OiA4MDA7XHJcbmNvbG9yOiAjZmZmZmZmO1xyXG53aWR0aDogLW1vei1hdmFpbGFibGU7XHJcbnRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazIqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG5cclxuLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLmNhcmQtYmxvZyAuY2FyZF93cmFwcGVyIC5yb3cgLmNhcmQtYm9keSB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTRweDtcclxuICBtYXJnaW4tdG9wOiAtMjJweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiB9XHJcblxyXG4uYmxvY2syIC50ZXN0LWJ0bjEge1xyXG5ib3JkZXItcmFkaXVzOiAyNXB4O1xyXG5tYXJnaW4tdG9wOiAwcHg7XHJcbmJvcmRlcjogMDtcclxufVxyXG4uYmxvY2syIC50ZXN0LWJ0bjIge1xyXG5ib3JkZXItcmFkaXVzOiAyNXB4O1xyXG5iYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuY29sb3I6IGJsYWNrO1xyXG5ib3JkZXI6IDA7XHJcbn1cclxuLmJsb2NrMiAudGVzdC1idG4zIHtcclxuYm9yZGVyLXJhZGl1czogMjVweDtcclxuYm9yZGVyOiAwO1xyXG59XHJcbi5ibG9jazI6aG92ZXIgLnRlc3QtYnRuMSB7XHJcbmJvcmRlcjogbm9uZTtcclxubWFyZ2luLXRvcDogMzBweDtcclxuY29sb3I6IHdoaXRlO1xyXG5iYWNrZ3JvdW5kLWNvbG9yOnJlZDsgXHJcbn1cclxuLmJsb2NrMjpob3ZlciAudGVzdC1idG4yIHtcclxuY29sb3I6IHdoaXRlO1xyXG5ib3JkZXI6IG5vbmU7XHJcbmJhY2tncm91bmQtY29sb3I6cmVkOyBcclxufVxyXG4uYmxvY2syOmhvdmVyIC50ZXN0LWJ0bjMge1xyXG5ib3JkZXI6IG5vbmU7XHJcbmNvbG9yOiB3aGl0ZTtcclxuYmFja2dyb3VuZC1jb2xvcjpyZWQ7IFxyXG59XHJcblxyXG5cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqbm90cmUgc3VjY2VzKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5jaGlmZnJle1xyXG5kaXNwbGF5OiBmbGV4O1xyXG53aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmt7XHJcbm1hcmdpbjogaW5pdGlhbDtcclxufVxyXG4uc3VjY2Vzc19pdGVte1xyXG5tYXJnaW4tbGVmdDogMjBweDtcclxubGlzdC1zdHlsZTogbm9uZTtcclxuZGlzcGxheTogZmxleDtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuLm91cl9zdWNjZXNzIC5zdWNjZXNzX3dyYXBwZXIgLnRpdGxle1xyXG4gIHdpZHRoOiAtbW96LWF2YWlsYWJsZTtcclxufVxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrIDMqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLmJsb2NrMyBwe1xyXG5wYWRkaW5nLXJpZ2h0OiAwO1xyXG53aWR0aDogLW1vei1hdmFpbGFibGU7XHJcbn1cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrIDQqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG5cclxuLmJsb2NrNHtcclxuYmFja2dyb3VuZC1pbWFnZTogdXJsKC4uLy4uL2Fzc2V0cy9jcG5pbWFnZXMvaG9tZS9zaW4ucG5nKTtcclxuYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuYmFja2dyb3VuZC1wb3NpdGlvbjo2NThweCAyMzRweDtcclxubWFyZ2luLXRvcDowcHg7XHJcbn1cclxuLmJsb2NrNCAuYmxvY2sxe1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5hbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxufVxyXG4uYmxvY2s0IC5ibG9jazEgcHtcclxucGFkZGluZy1yaWdodDogMHB4O1xyXG59XHJcblxyXG4ub25le1xyXG5tYXJnaW46IDQwcHggMCAwIDAgO1xyXG59XHJcbi50d297XHJcbm1hcmdpbjogMDtcclxufVxyXG4udGhyZWV7XHJcbm1hcmdpbjogMDtcclxufVxyXG4ub3V0ZXItZGl2e1xyXG5kaXNwbGF5OiBjb250ZW50cztcclxufVxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrNSoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5ibG9jazV7XHJcbm1heC13aWR0aDogMTAwJTtcclxud2lkdGg6IDEwMCU7XHJcbn1cclxuLmJsb2NrNSBpbWd7XHJcbm1hcmdpbi1sZWZ0OiAwO1xyXG5oZWlnaHQ6MTAwJTtcclxud2lkdGg6MTAwJVxyXG59XHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrNioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uYmxvY2s2e1xyXG5wYWRkaW5nLXRvcDogMzBweDsgXHJcbnBhZGRpbmctbGVmdDowO1xyXG5tYXJnaW4tdG9wOiAwOyBcclxubWF4LXdpZHRoOiAxMDAlO1xyXG5kaXNwbGF5OiBjb250ZW50cztcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5ibG9jazYgLnRoaXJkLWJsb2MtYm9yZGVye1xyXG5tYXJnaW46IDIwcHggYXV0byAwIGF1dG87XHJcbn1cclxuXHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrNyAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5ibG9jazd7XHJcbnBhZGRpbmctdG9wOiAxMHB4O1xyXG59XHJcblxyXG4uY2FyZDF7XHJcbiAgbWFyZ2luLXJpZ2h0OiAwcHg7XHJcbiAgaGVpZ2h0OiAzNDVweDtcclxuICB3aWR0aDogMzE1cHg7XHJcbiAgfVxyXG4gIC5jYXJkMSAuYm94MXtcclxuICBib3gtc2hhZG93OiAwcHggMXB4IDE1cHggZ3JleTtcclxuICBib3JkZXI6IG5vbmU7XHJcbiAgYm9yZGVyLXJhZGl1czogNzFweCAxNHB4IDcxcHggMTRweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gIHBhZGRpbmc6IDQwcHg7XHJcbiAgfVxyXG4gIC5jYXJkMntcclxuICAgIG1hcmdpbi1yaWdodDogMHB4O1xyXG4gICAgaGVpZ2h0OiAzNDVweDtcclxuICAgIHdpZHRoOiAzMTVweDtcclxuICB9XHJcbiAgLmNhcmQyIC5ib3gye1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDFweCAxNXB4IGdyZXk7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBib3JkZXItcmFkaXVzOiA3MXB4IDE0cHggNzFweCAxNHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcclxuICAgIHBhZGRpbmc6IDQwcHg7XHJcbiAgICB9XHJcbiAgLmNhcmQze1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAwcHg7XHJcbiAgICBoZWlnaHQ6IDM0NXB4O1xyXG4gICAgd2lkdGg6IDMxNXB4O1xyXG4gIH1cclxuICAuY2FyZDMgLmJveDN7XHJcbiAgICBib3gtc2hhZG93OiAwcHggMXB4IDE1cHggZ3JleTtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDcxcHggMTRweCA3MXB4IDE0cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xyXG4gICAgcGFkZGluZzogNDBweDtcclxuICAgIH1cclxuICAgIFxyXG4gIC5sYXN0QntcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWFyZ2luOiBub25lO1xyXG4gIH1cclxuXHJcblxyXG4gIC50ZXh0LWNlbnRlcntcclxuICBtYXJnaW4tYm90dG9tOiA1MHB4O1xyXG4gIH1cclxuXHJcbiAgICB9XHJcblxyXG4gIFxyXG4gLyogTWVkaXVtIERldmljZXMsIERlc2t0b3BzICovXHJcbiAgICBAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGggOiA5OTJweCkgYW5kIChtYXgtd2lkdGggOiAxMjAwcHgpICB7XHJcbiAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqbmF2IGJhciAqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gLmRyb3B7XHJcbiAgbWluLXdpZHRoOiAtbW96LWF2YWlsYWJsZTtcclxuICBtYXJnaW4tbGVmdDogLTcwcHg7XHJcbiB9ICAgICBcclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiBzaWRlIGJhciAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uZmxvYXRfYWN0aW9ucyB7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIGJhY2tncm91bmQ6IHJlZDtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIGxlZnQ6IDA7XHJcbiAgdG9wOiAzNSU7XHJcbiAgcGFkZGluZzogMTBweDtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgd2lkdGg6IDglO1xyXG4gIHotaW5kZXg6NTkwMDtcclxuICB9XHJcbiAgXHJcbiAgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCB7XHJcbiAgcGFkZGluZzogMDtcclxuICBtYXJnaW46IDAgMCAtMzBweCAwO1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgfVxyXG4gIC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtcyB7XHJcbiAgcGFkZGluZzogNXB4O1xyXG4gIHdpZHRoOiAxMjBweDtcclxuICBoZWlnaHQ6IDEyMHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB9XHJcbiAgXHJcbiAgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zIC5pdGVtX2hyZWYge1xyXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICB9XHJcbiAgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zIC5pdGVtX2hyZWYgLmlocmVmX2xvZ28ge1xyXG4gIHdpZHRoOiAxMDBweDtcclxuICBoZWlnaHQ6IDEwMHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiAyNXB4O1xyXG4gIH1cclxuICAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLml0ZW1faHJlZiAuaWhyZWZfdGV4dCB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIG1hcmdpbjogMDtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgbWFyZ2luLXRvcDogN3B4O1xyXG4gIH1cclxuICBcclxuICBcclxuICAgLnByaW1hcnlfYm9keSAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXM6OmJlZm9yZXtcclxuICAgY29udGVudDogXCI+XCI7XHJcbiAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgcmlnaHQ6IC0xMHB4O1xyXG4gICB0b3A6IDE1JTtcclxuICAgY29sb3I6IHdoaXRlO1xyXG4gICBmb250LXNpemU6IDIwcHg7XHJcbiAgIHdpZHRoOiA0MCU7XHJcbiAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICB9XHJcbiAgIFxyXG4gICAuZmxvYXRfYWN0aW9ucyAuYWN0aW9uc19jb250ZW50IC5hY3Rpb25faXRlbXMgLnRlc3RtZWdpe1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICByaWdodDogLTE4MHB4O1xyXG4gICAgIHRvcDogMTUlO1xyXG4gICAgIGNvbG9yOiBibGFjaztcclxuICAgICBmb250LXNpemU6IDE3cHg7XHJcbiAgICAgd2lkdGg6IDQwJTtcclxuICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICB3aWR0aDogMTgwcHg7XHJcbiAgICAgYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgfVxyXG4gICAgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zOmhvdmVyIC50ZXN0bWVnaXtcclxuICAgZGlzcGxheTogZmxleDtcclxuICAgfVxyXG4gICAucHJpbWFyeV9ib2R5IC5mbG9hdF9hY3Rpb25zIC5hY3Rpb25zX2NvbnRlbnQgLmFjdGlvbl9pdGVtcyAuaXRlbV9ocmVmIHtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICB9XHJcbiAgIC5wcmltYXJ5X2JvZHkgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zIC5pdGVtX2hyZWYgLmlocmVmX2xvZ28ge1xyXG4gICAgd2lkdGg6IDEwMHB4O1xyXG4gICAgaGVpZ2h0OiAxMDBweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAyNXB4O1xyXG4gICB9XHJcbiAgIC5wcmltYXJ5X2JvZHkgLmZsb2F0X2FjdGlvbnMgLmFjdGlvbnNfY29udGVudCAuYWN0aW9uX2l0ZW1zIC5pdGVtX2hyZWYgLmlocmVmX3RleHQge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgbWFyZ2luLXRvcDogN3B4O1xyXG4gICB9XHJcbiAgIFxyXG4gICAgICBcclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazAgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uY29udGVudHtcclxuICB3aWR0aDogbWF4LWNvbnRlbnQ7XHJcbiAgfVxyXG4gIC50cmFuc3Rpb257XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbWFyZ2luLWxlZnQ6IC0xNDBweDtcclxuICAgIHdpZHRoOiBtYXgtY29udGVudDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICB9XHJcbi5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcge1xyXG4gIGJhY2tncm91bmQ6IzExMWQ1ZTtcclxuICBtaW4taGVpZ2h0OiBmaXQtY29udGVudDtcclxuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMTAwcHg7XHJcbiB9XHJcbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmltZ193cmFwcGVyIC5oZWFkaW5nX2ltZyB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIG1heC1oZWlnaHQ6IDU4NXB4O1xyXG4gIG1hcmdpbi10b3A6IDE7XHJcbiAgd2lkdGg6IDk5M3B4O1xyXG4gfVxyXG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC50aXRsZV9oZWFkaW5nIHtcclxuICBmb250LXNpemU6IDQwcHg7XHJcbiAgZm9udC13ZWlnaHQ6IDgwMDtcclxuICBjb2xvcjogICNmZmZmZmY7XHJcbiAgd2lkdGg6IG1heC1jb250ZW50O1xyXG4gfVxyXG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC5kZXNjX2hlYWRpbmcge1xyXG4gIGZvbnQtc2l6ZTogNDBweDtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG4gIGNvbG9yOiAgI2ZmZmZmZjtcclxuICB3aWR0aDogbWF4LWNvbnRlbnQ7XHJcbiB9XHJcbiAuY2FycmUge1xyXG4gIHdpZHRoOiAyMDBweDtcclxuICBoZWlnaHQ6IDkwcHg7XHJcbiAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgYm9yZGVyLXJhZGl1czogMThweDtcclxuICBtYXJnaW4tbGVmdDogODBweDtcclxuICBtYXJnaW4tdG9wOiAzMzVweDtcclxuICB9XHJcbiAgXHJcbiAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2Mge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgICBtYXJnaW4tbGVmdDogdW5zZXQ7XHJcbiAgICB3aWR0aDogLW1vei1hdmFpbGFibGU7XHJcbiAgICBtYXJnaW4tbGVmdDogMHB4O1xyXG4gICAgfVxyXG4gICAgXHJcbiAgIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3c6YmVmb3JlIHtcclxuICAgIGJvcmRlcjogNXB4IHNvbGlkICNmZmZmZmY7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgd2lkdGg6IDQwMHB4O1xyXG4gICB9XHJcbiAgIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3cgLnNlYXJjaF9faW5wdXQge1xyXG4gICAgb3BhY2l0eTogMTtcclxuICAgIHdpZHRoOiAtbW96LWF2YWlsYWJsZTtcclxuICAgfVxyXG5cclxuXHJcbiAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJsb2NrMioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgIC5ob21le1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG5cclxuICAgIC5ibG9jazJ7XHJcbiAgICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgcGFkZGluZzogMjJweDtcclxuICAgICAgfVxyXG4gICAgICAuYmxvY2syOmhvdmVye1xyXG4gICAgICAgYm9yZGVyOiA1cHggc29saWQgd2hpdGU7XHJcbiAgICAgICBwYWRkaW5nOiAxOHB4O1xyXG4gICAgICB9XHJcblxyXG4gICAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazQgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gXHJcbiAgICAgICAuYmxvY2s0e1xyXG4gICAgICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCguLi8uLi9hc3NldHMvY3BuaW1hZ2VzL2hvbWUvc2luLnBuZyk7XHJcbiAgICAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOjI1OHB4IDIyMHB4O1xyXG4gICAgICAgIG1hcmdpbi10b3A6MHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuYmxvY2s0IC5ibG9jazF7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuYmxvY2s0IC5ibG9jazEgcHtcclxuICAgICAgICAgIHBhZGRpbmctcmlnaHQ6IDUwJTtcclxuICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIC5vbmV7XHJcbiAgICAgICAgICBtYXJnaW46IDExcHggMHB4IDBweCAyNjNweFxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgIC50d297XHJcbiAgICAgICAgICBtYXJnaW46IC01ODBweCAwIDAgNjUwcHhcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIC50aHJlZXtcclxuICAgICAgICAgIG1hcmdpbjogLTEycHggMCAwIDY1MHB4XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipibG9jazUgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gICAgICAgICAgLmJsb2NrNSBpbWd7XHJcbiAgICAgICAgICAgIG1hcmdpbi1sZWZ0OiA2MHB4O1xyXG4gICAgICAgICAgICBoZWlnaHQ6MTAwJTtcclxuICAgICAgICAgICAgd2lkdGg6MTAwJVxyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2s2ICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuIFxyXG4gICAgICAgICAgLmJsb2NrNntcclxuICAgICAgICAgICAgcGFkZGluZy10b3A6IDMwcHg7IFxyXG4gICAgICAgICAgICBwYWRkaW5nLWxlZnQ6IDExMHB4O1xyXG4gICAgICAgICAgICBtYXJnaW4tdG9wOiAtNDkwcHg7IFxyXG4gICAgICAgICAgICBmbG9hdDpyaWdodFxyXG4gICAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcblxyXG4gICAgIC8qTGFyZ2UgRGV2aWNlcywgV2lkZSBTY3JlZW5zKi9cclxuICAgIEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aCA6IDEyMDBweCkgYW5kIChtYXgtd2lkdGggOiAxNTAwcHgpIHtcclxuICAgICAgXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2swICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLmNvbnRlbnR7XHJcbiAgd2lkdGg6IG1heC1jb250ZW50O1xyXG4gIH1cclxuICAudHJhbnN0aW9ue1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgcGFkZGluZy1sZWZ0OiAxMTVweDtcclxuICAgIHdpZHRoOiBtYXgtY29udGVudDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICB9XHJcbi5wcmltYXJ5X2JvZHkgLmhvbWVfY29udGFpbmVyIC5zZWN0aW9uX2hlYWRpbmcge1xyXG4gIGJhY2tncm91bmQ6IzExMWQ1ZTtcclxuICBtaW4taGVpZ2h0OiBmaXQtY29udGVudDtcclxuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMTAwcHg7XHJcbiB9XHJcbiAucHJpbWFyeV9ib2R5IC5ob21lX2NvbnRhaW5lciAuc2VjdGlvbl9oZWFkaW5nIC5oZWFkaW5nX3dyYXBwZXIgLmltZ193cmFwcGVyIC5oZWFkaW5nX2ltZyB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIG1heC1oZWlnaHQ6IDU4NXB4O1xyXG4gIG1hcmdpbi10b3A6IDE7XHJcbiAgd2lkdGg6IDk5M3B4O1xyXG4gfVxyXG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC50aXRsZV9oZWFkaW5nIHtcclxuICBmb250LXNpemU6IDQwcHg7XHJcbiAgZm9udC13ZWlnaHQ6IDgwMDtcclxuICBjb2xvcjogICNmZmZmZmY7XHJcbiAgd2lkdGg6IG1heC1jb250ZW50O1xyXG4gfVxyXG4gLnByaW1hcnlfYm9keSAuaG9tZV9jb250YWluZXIgLnNlY3Rpb25faGVhZGluZyAuaGVhZGluZ193cmFwcGVyIC5kZXNjX2hlYWRpbmcge1xyXG4gIGZvbnQtc2l6ZTogNDBweDtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG4gIGNvbG9yOiAgI2ZmZmZmZjtcclxuICB3aWR0aDogbWF4LWNvbnRlbnQ7XHJcbiB9XHJcbiAuY2FycmUge1xyXG4gIHdpZHRoOiAyMDBweDtcclxuICBoZWlnaHQ6IDkwcHg7XHJcbiAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgYm9yZGVyLXJhZGl1czogMThweDtcclxuICBtYXJnaW4tbGVmdDogODBweDtcclxuICBtYXJnaW4tdG9wOiAzMzVweDtcclxuICB9XHJcbiAgXHJcbiAgLnByaW1hcnlfYm9keSAuc2VhcmNoX2Jsb2Mge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgICBtYXJnaW4tbGVmdDogdW5zZXQ7XHJcbiAgICB3aWR0aDogLW1vei1hdmFpbGFibGU7XHJcbiAgICBtYXJnaW4tbGVmdDogMHB4O1xyXG4gICAgfVxyXG4gICAgXHJcbiAgIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3c6YmVmb3JlIHtcclxuICAgIGJvcmRlcjogNXB4IHNvbGlkICNmZmZmZmY7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgd2lkdGg6IDQwMHB4O1xyXG4gICB9XHJcbiAgIC5wcmltYXJ5X2JvZHkgLnNlYXJjaF9ibG9jIC5zZWFyY2gtLXNob3cgLnNlYXJjaF9faW5wdXQge1xyXG4gICAgb3BhY2l0eTogMTtcclxuICAgIHdpZHRoOiAtbW96LWF2YWlsYWJsZTtcclxuICAgfVxyXG5cclxuICAgICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqYmxvY2s0ICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuIFxyXG4gICAgICAgIC5ibG9jazQgLmJsb2NrMXtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICAgICAgfVxyXG4gICAgIFxyXG5cclxuICAgICAgICAub25le1xyXG4gICAgICAgICAgbWFyZ2luOiAxMXB4IDBweCAwcHggNDU2cHhcclxuICAgICAgICAgIH1cclxuICAgICAgICAgICAudHdve1xyXG4gICAgICAgICAgbWFyZ2luOiAtNTgwcHggMCAwIDc5MnB4XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICAudGhyZWV7XHJcbiAgICAgICAgICBtYXJnaW46IC0xMnB4IDAgMCA3OTJweFxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgXHJcbiAgICB9IFxyXG5cclxuXHJcblxyXG4gLyogLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy9mb290ZXIgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLyAqL1xyXG5cclxuIFxyXG4gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTRjODlhNzQ4Om5vdCguZWxlbWVudG9yLW1vdGlvbi1lZmZlY3RzLWVsZW1lbnQtdHlwZS1iYWNrZ3JvdW5kKSwgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC00Yzg5YTc0OCA+IC5lbGVtZW50b3ItbW90aW9uLWVmZmVjdHMtY29udGFpbmVyID4gLmVsZW1lbnRvci1tb3Rpb24tZWZmZWN0cy1sYXllcntiYWNrZ3JvdW5kLWNvbG9yOiNGQkZCRkI7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNGM4OWE3NDh7dHJhbnNpdGlvbjpiYWNrZ3JvdW5kIDAuM3MsIGJvcmRlciAwLjNzLCBib3JkZXItcmFkaXVzIDAuM3MsIGJveC1zaGFkb3cgMC4zcztwYWRkaW5nOjk2cHggMHB4IDk2cHggMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTRjODlhNzQ4ID4gLmVsZW1lbnRvci1iYWNrZ3JvdW5kLW92ZXJsYXl7dHJhbnNpdGlvbjpiYWNrZ3JvdW5kIDAuM3MsIGJvcmRlci1yYWRpdXMgMC4zcywgb3BhY2l0eSAwLjNzO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTE0Mzk2NjJkID4gLmVsZW1lbnRvci1lbGVtZW50LXBvcHVsYXRlZHt0cmFuc2l0aW9uOmJhY2tncm91bmQgMC4zcywgYm9yZGVyIDAuM3MsIGJvcmRlci1yYWRpdXMgMC4zcywgYm94LXNoYWRvdyAwLjNzO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTE0Mzk2NjJkID4gLmVsZW1lbnRvci1lbGVtZW50LXBvcHVsYXRlZCA+IC5lbGVtZW50b3ItYmFja2dyb3VuZC1vdmVybGF5e3RyYW5zaXRpb246YmFja2dyb3VuZCAwLjNzLCBib3JkZXItcmFkaXVzIDAuM3MsIG9wYWNpdHkgMC4zczt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC05N2ZlMDJme3RleHQtYWxpZ246Y2VudGVyO3dpZHRoOmF1dG87bWF4LXdpZHRoOmF1dG87YWxpZ24tc2VsZjpjZW50ZXI7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtOTdmZTAyZiBpbWd7d2lkdGg6MTAwJTttYXgtd2lkdGg6MTAwJTtoZWlnaHQ6NDE4cHg7b2JqZWN0LWZpdDpjb250YWluO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTk3ZmUwMmYgPiAuZWxlbWVudG9yLXdpZGdldC1jb250YWluZXJ7bWFyZ2luOi00N3B4IC0xNDdweCAtMTQ3cHggLTE0N3B4O3BhZGRpbmc6MHB4IDBweCAwcHggMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWNhNzY5NDR7dGV4dC1hbGlnbjpsZWZ0O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWNhNzY5NDQgLmVsZW1lbnRvci1oZWFkaW5nLXRpdGxle2NvbG9yOiNDRjFCMzc7Zm9udC1zaXplOjEycHg7Zm9udC13ZWlnaHQ6NjAwO3RleHQtdHJhbnNmb3JtOnVwcGVyY2FzZTtsZXR0ZXItc3BhY2luZzoycHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtY2E3Njk0NCA+IC5lbGVtZW50b3Itd2lkZ2V0LWNvbnRhaW5lcnttYXJnaW46MHB4IDBweCAyNHB4IDBweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iNTFlN2I5e3RleHQtYWxpZ246bGVmdDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iNTFlN2I5IC5lbGVtZW50b3ItaGVhZGluZy10aXRsZXtjb2xvcjojMTMxYjVjO2ZvbnQtZmFtaWx5OnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LXByaW1hcnktZm9udC1mYW1pbHkgKSwgU2Fucy1zZXJpZjtmb250LXNpemU6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktcHJpbWFyeS1mb250LXNpemUgKTtmb250LXdlaWdodDp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS1wcmltYXJ5LWZvbnQtd2VpZ2h0ICk7bGluZS1oZWlnaHQ6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktcHJpbWFyeS1saW5lLWhlaWdodCApO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWI1MWU3YjkgPiAuZWxlbWVudG9yLXdpZGdldC1jb250YWluZXJ7bWFyZ2luOjBweCAwcHggMjRweCAwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNWIzYTllZjAgPiAuZWxlbWVudG9yLXdpZGdldC1jb250YWluZXJ7bWFyZ2luOjBweCAwcHggMTZweCAwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNDg4MzlhMzggLmVsZW1lbnRza2l0X2lucHV0X2xhYmVse2NvbG9yOiMwMDAwMDA7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNDg4MzlhMzggLmVraXRfZm9ybV9jb250cm9se2JvcmRlci1yYWRpdXM6NzVweCA3NXB4IDc1cHggNzVweDtib3JkZXItc3R5bGU6c29saWQ7Ym9yZGVyLXdpZHRoOjFweCAxcHggMXB4IDFweDtib3JkZXItY29sb3I6I0RFREVERTtwYWRkaW5nOjEycHggMTZweCAxMnB4IDMycHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNDg4MzlhMzggLmVsZW1lbnRza2l0X2lucHV0X2NvbnRhaW5lcntmbGV4OjAgMCA2NiU7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNDg4MzlhMzggLmVsZW1lbnRza2l0X2lubGluZV9mb3JtIC5lbGVtZW50c2tpdF9pbnB1dF93cmFwZXI6bm90KDpsYXN0LWNoaWxkKXttYXJnaW4tcmlnaHQ6OHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTQ4ODM5YTM4IC5la2l0X2Zvcm1fY29udHJvbDo6LXdlYmtpdC1pbnB1dC1wbGFjZWhvbGRlcntjb2xvcjojNkI2QjZCO2ZvbnQtc2l6ZToxNHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTQ4ODM5YTM4IC5la2l0X2Zvcm1fY29udHJvbDo6LW1vei1wbGFjZWhvbGRlcntjb2xvcjojNkI2QjZCO2ZvbnQtc2l6ZToxNHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTQ4ODM5YTM4IC5la2l0X2Zvcm1fY29udHJvbDotbXMtaW5wdXQtcGxhY2Vob2xkZXJ7Y29sb3I6IzZCNkI2Qjtmb250LXNpemU6MTRweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC00ODgzOWEzOCAuZWtpdF9mb3JtX2NvbnRyb2w6LW1vei1wbGFjZWhvbGRlcntjb2xvcjojNkI2QjZCO2ZvbnQtc2l6ZToxNHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTQ4ODM5YTM4IC5la2l0LW1haWwtc3VibWl0e2ZvbnQtc2l6ZToxMnB4O2ZvbnQtd2VpZ2h0OjYwMDtsZXR0ZXItc3BhY2luZzoxcHg7Ym9yZGVyLXJhZGl1czo4OHB4IDg2cHggODhweCA4OHB4O3BhZGRpbmc6MTNweCAwcHggMTJweCAwcHg7d2lkdGg6MTQzcHg7bWFyZ2luOjBweCAwcHggMHB4IDBweDtjb2xvcjojZmZmO2JhY2tncm91bmQtY29sb3I6IzEzMWI1Yzt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC00ODgzOWEzOCAuZWtpdC1tYWlsLXN1Ym1pdCBzdmcgcGF0aHtzdHJva2U6I2ZmZjtmaWxsOiNmZmY7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNDg4MzlhMzggLmVraXQtbWFpbC1zdWJtaXQ6aG92ZXJ7Y29sb3I6I2ZmZjt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC00ODgzOWEzOCAuZWtpdC1tYWlsLXN1Ym1pdDpob3ZlciBzdmcgcGF0aHtzdHJva2U6I2ZmZjtmaWxsOiNmZmY7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNDg4MzlhMzggLmVraXQtbWFpbC1zdWJtaXQ6YmVmb3Jle2JhY2tncm91bmQtY29sb3I6I0NGMUIzNzt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC00ODgzOWEzOCAuZWtpdC1tYWlsLXN1Ym1pdCA+IGksICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNDg4MzlhMzggLmVraXQtbWFpbC1zdWJtaXQgPiBzdmd7bWFyZ2luLXJpZ2h0OjExcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNDg4MzlhMzggLmVsZW1lbnRza2l0X2lucHV0X2dyb3VwX3RleHR7YmFja2dyb3VuZC1jb2xvcjojRkZGRkZGO2ZvbnQtc2l6ZToxMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTQ4ODM5YTM4IC5lbGVtZW50c2tpdF9pbnB1dF9ncm91cF90ZXh0IGl7Y29sb3I6IzEzMWI1Yzt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC00ODgzOWEzOCAuZWxlbWVudHNraXRfaW5wdXRfZ3JvdXBfdGV4dCBzdmcgcGF0aHtzdHJva2U6IzEzMWI1YztmaWxsOiMxMzFiNWM7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNDg4MzlhMzggLmVsZW1lbnRza2l0X2lucHV0X2dyb3VwX3RleHQgc3Zne21heC13aWR0aDoxMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTQ4ODM5YTM4ID4gLmVsZW1lbnRvci13aWRnZXQtY29udGFpbmVye21hcmdpbjowcHggMHB4IDE2cHggMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LThkMmQ0NzF7Y29sb3I6I0FFQUVBRTtmb250LXNpemU6MTRweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC00ODk1NzZmMjpub3QoLmVsZW1lbnRvci1tb3Rpb24tZWZmZWN0cy1lbGVtZW50LXR5cGUtYmFja2dyb3VuZCksICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNDg5NTc2ZjIgPiAuZWxlbWVudG9yLW1vdGlvbi1lZmZlY3RzLWNvbnRhaW5lciA+IC5lbGVtZW50b3ItbW90aW9uLWVmZmVjdHMtbGF5ZXJ7YmFja2dyb3VuZC1jb2xvcjojMTExRTVEO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTQ4OTU3NmYyID4gLmVsZW1lbnRvci1iYWNrZ3JvdW5kLW92ZXJsYXl7b3BhY2l0eTowLjI7dHJhbnNpdGlvbjpiYWNrZ3JvdW5kIDAuM3MsIGJvcmRlci1yYWRpdXMgMC4zcywgb3BhY2l0eSAwLjNzO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTQ4OTU3NmYye3RyYW5zaXRpb246YmFja2dyb3VuZCAwLjNzLCBib3JkZXIgMC4zcywgYm9yZGVyLXJhZGl1cyAwLjNzLCBib3gtc2hhZG93IDAuM3M7cGFkZGluZzowcHggMHB4IDBweCAwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMjVkYmRmOWEgPiAuZWxlbWVudG9yLWVsZW1lbnQtcG9wdWxhdGVke3BhZGRpbmc6MHB4IDBweCAwcHggMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTc1NzE2OTM2ID4gLmVsZW1lbnRvci1jb250YWluZXJ7bWF4LXdpZHRoOjEyNTFweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC03NTcxNjkzNnttYXJnaW4tdG9wOjMycHg7bWFyZ2luLWJvdHRvbTo5MHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTdiYWI5MWI2ID4gLmVsZW1lbnRvci1lbGVtZW50LXBvcHVsYXRlZHttYXJnaW46MHB4IDUwcHggMHB4IDBweDstLWUtY29sdW1uLW1hcmdpbi1yaWdodDo1MHB4Oy0tZS1jb2x1bW4tbWFyZ2luLWxlZnQ6MHB4O3BhZGRpbmc6MHB4IDBweCAwcHggMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTc0MzU4OWJje3RleHQtYWxpZ246bGVmdDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC03NDM1ODliYyBpbWd7d2lkdGg6MzUlO2JvcmRlci1yYWRpdXM6MHB4IDBweCAwcHggMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWEzZWI0Y2J7LS1lLWljb24tbGlzdC1pY29uLXNpemU6MTRweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1hM2ViNGNiIC5lbGVtZW50b3ItaWNvbi1saXN0LXRleHR7Y29sb3I6I0ZGRkZGRjt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1hM2ViNGNiIC5lbGVtZW50b3ItaWNvbi1saXN0LWl0ZW06aG92ZXIgLmVsZW1lbnRvci1pY29uLWxpc3QtdGV4dHtjb2xvcjojY2MxNDE0O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWEzZWI0Y2IgLmVsZW1lbnRvci1pY29uLWxpc3QtaXRlbSA+IC5lbGVtZW50b3ItaWNvbi1saXN0LXRleHQsICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYTNlYjRjYiAuZWxlbWVudG9yLWljb24tbGlzdC1pdGVtID4gYXtmb250LXNpemU6MTJweDtmb250LXdlaWdodDo2MDA7bGV0dGVyLXNwYWNpbmc6MnB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTMwNWE5NzZiID4gLmVsZW1lbnRvci1lbGVtZW50LXBvcHVsYXRlZHtwYWRkaW5nOjBweCAwcHggMHB4IDBweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC01NzgxNGMwYSAuZWxlbWVudG9yLWhlYWRpbmctdGl0bGV7Y29sb3I6I0ZGRkZGRjtmb250LWZhbWlseTp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS00NTZkM2Q0LWZvbnQtZmFtaWx5ICksIFNhbnMtc2VyaWY7Zm9udC1zaXplOnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LTQ1NmQzZDQtZm9udC1zaXplICk7Zm9udC13ZWlnaHQ6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktNDU2ZDNkNC1mb250LXdlaWdodCApO2xpbmUtaGVpZ2h0OnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LTQ1NmQzZDQtbGluZS1oZWlnaHQgKTtsZXR0ZXItc3BhY2luZzp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS00NTZkM2Q0LWxldHRlci1zcGFjaW5nICk7d29yZC1zcGFjaW5nOnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LTQ1NmQzZDQtd29yZC1zcGFjaW5nICk7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNTc4MTRjMGEgPiAuZWxlbWVudG9yLXdpZGdldC1jb250YWluZXJ7bWFyZ2luOjBweCAwcHggMHB4IDBweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC03MTMwNTNkIC5lbGVtZW50b3ItaWNvbi1saXN0LWl0ZW1zOm5vdCguZWxlbWVudG9yLWlubGluZS1pdGVtcykgLmVsZW1lbnRvci1pY29uLWxpc3QtaXRlbTpub3QoOmxhc3QtY2hpbGQpe3BhZGRpbmctYm90dG9tOmNhbGMoNnB4LzIpO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTcxMzA1M2QgLmVsZW1lbnRvci1pY29uLWxpc3QtaXRlbXM6bm90KC5lbGVtZW50b3ItaW5saW5lLWl0ZW1zKSAuZWxlbWVudG9yLWljb24tbGlzdC1pdGVtOm5vdCg6Zmlyc3QtY2hpbGQpe21hcmdpbi10b3A6Y2FsYyg2cHgvMik7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNzEzMDUzZCAuZWxlbWVudG9yLWljb24tbGlzdC1pdGVtcy5lbGVtZW50b3ItaW5saW5lLWl0ZW1zIC5lbGVtZW50b3ItaWNvbi1saXN0LWl0ZW17bWFyZ2luLXJpZ2h0OmNhbGMoNnB4LzIpO21hcmdpbi1sZWZ0OmNhbGMoNnB4LzIpO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTcxMzA1M2QgLmVsZW1lbnRvci1pY29uLWxpc3QtaXRlbXMuZWxlbWVudG9yLWlubGluZS1pdGVtc3ttYXJnaW4tcmlnaHQ6Y2FsYygtNnB4LzIpO21hcmdpbi1sZWZ0OmNhbGMoLTZweC8yKTt9Ym9keS5ydGwgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC03MTMwNTNkIC5lbGVtZW50b3ItaWNvbi1saXN0LWl0ZW1zLmVsZW1lbnRvci1pbmxpbmUtaXRlbXMgLmVsZW1lbnRvci1pY29uLWxpc3QtaXRlbTphZnRlcntsZWZ0OmNhbGMoLTZweC8yKTt9Ym9keTpub3QoLnJ0bCkgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC03MTMwNTNkIC5lbGVtZW50b3ItaWNvbi1saXN0LWl0ZW1zLmVsZW1lbnRvci1pbmxpbmUtaXRlbXMgLmVsZW1lbnRvci1pY29uLWxpc3QtaXRlbTphZnRlcntyaWdodDpjYWxjKC02cHgvMik7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNzEzMDUzZCAuZWxlbWVudG9yLWljb24tbGlzdC1pY29uIGl7Y29sb3I6I0ZGRkZGRjt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC03MTMwNTNkIC5lbGVtZW50b3ItaWNvbi1saXN0LWljb24gc3Zne2ZpbGw6I0ZGRkZGRjt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC03MTMwNTNkey0tZS1pY29uLWxpc3QtaWNvbi1zaXplOjE3cHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNzEzMDUzZCAuZWxlbWVudG9yLWljb24tbGlzdC10ZXh0e2NvbG9yOiNGRkZGRkY7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNzEzMDUzZCAuZWxlbWVudG9yLWljb24tbGlzdC1pdGVtOmhvdmVyIC5lbGVtZW50b3ItaWNvbi1saXN0LXRleHR7Y29sb3I6dmFyKCAtLWUtZ2xvYmFsLWNvbG9yLWFjY2VudCApO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTcxMzA1M2QgLmVsZW1lbnRvci1pY29uLWxpc3QtaXRlbSA+IC5lbGVtZW50b3ItaWNvbi1saXN0LXRleHQsICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNzEzMDUzZCAuZWxlbWVudG9yLWljb24tbGlzdC1pdGVtID4gYXtmb250LWZhbWlseTp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS10ZXh0LWZvbnQtZmFtaWx5ICksIFNhbnMtc2VyaWY7Zm9udC1zaXplOnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LXRleHQtZm9udC1zaXplICk7Zm9udC13ZWlnaHQ6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktdGV4dC1mb250LXdlaWdodCApO2xpbmUtaGVpZ2h0OnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LXRleHQtbGluZS1oZWlnaHQgKTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC03MGYwNDVkMiA+IC5lbGVtZW50b3ItZWxlbWVudC1wb3B1bGF0ZWR7cGFkZGluZzowcHggMHB4IDBweCAwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMzk5N2VlZTkgLmVsZW1lbnRvci1oZWFkaW5nLXRpdGxle2NvbG9yOiNGRkZGRkY7Zm9udC1mYW1pbHk6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktNDU2ZDNkNC1mb250LWZhbWlseSApLCBTYW5zLXNlcmlmO2ZvbnQtc2l6ZTp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS00NTZkM2Q0LWZvbnQtc2l6ZSApO2ZvbnQtd2VpZ2h0OnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LTQ1NmQzZDQtZm9udC13ZWlnaHQgKTtsaW5lLWhlaWdodDp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS00NTZkM2Q0LWxpbmUtaGVpZ2h0ICk7bGV0dGVyLXNwYWNpbmc6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktNDU2ZDNkNC1sZXR0ZXItc3BhY2luZyApO3dvcmQtc3BhY2luZzp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS00NTZkM2Q0LXdvcmQtc3BhY2luZyApO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTM5OTdlZWU5ID4gLmVsZW1lbnRvci13aWRnZXQtY29udGFpbmVye21hcmdpbjowcHggMHB4IDBweCAwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMmE1Y2NlYjQgLmVsZW1lbnRvci1pY29uLWxpc3QtaXRlbXM6bm90KC5lbGVtZW50b3ItaW5saW5lLWl0ZW1zKSAuZWxlbWVudG9yLWljb24tbGlzdC1pdGVtOm5vdCg6bGFzdC1jaGlsZCl7cGFkZGluZy1ib3R0b206Y2FsYyg2cHgvMik7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMmE1Y2NlYjQgLmVsZW1lbnRvci1pY29uLWxpc3QtaXRlbXM6bm90KC5lbGVtZW50b3ItaW5saW5lLWl0ZW1zKSAuZWxlbWVudG9yLWljb24tbGlzdC1pdGVtOm5vdCg6Zmlyc3QtY2hpbGQpe21hcmdpbi10b3A6Y2FsYyg2cHgvMik7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMmE1Y2NlYjQgLmVsZW1lbnRvci1pY29uLWxpc3QtaXRlbXMuZWxlbWVudG9yLWlubGluZS1pdGVtcyAuZWxlbWVudG9yLWljb24tbGlzdC1pdGVte21hcmdpbi1yaWdodDpjYWxjKDZweC8yKTttYXJnaW4tbGVmdDpjYWxjKDZweC8yKTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC0yYTVjY2ViNCAuZWxlbWVudG9yLWljb24tbGlzdC1pdGVtcy5lbGVtZW50b3ItaW5saW5lLWl0ZW1ze21hcmdpbi1yaWdodDpjYWxjKC02cHgvMik7bWFyZ2luLWxlZnQ6Y2FsYygtNnB4LzIpO31ib2R5LnJ0bCAgLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTJhNWNjZWI0IC5lbGVtZW50b3ItaWNvbi1saXN0LWl0ZW1zLmVsZW1lbnRvci1pbmxpbmUtaXRlbXMgLmVsZW1lbnRvci1pY29uLWxpc3QtaXRlbTphZnRlcntsZWZ0OmNhbGMoLTZweC8yKTt9Ym9keTpub3QoLnJ0bCkgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC0yYTVjY2ViNCAuZWxlbWVudG9yLWljb24tbGlzdC1pdGVtcy5lbGVtZW50b3ItaW5saW5lLWl0ZW1zIC5lbGVtZW50b3ItaWNvbi1saXN0LWl0ZW06YWZ0ZXJ7cmlnaHQ6Y2FsYygtNnB4LzIpO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTJhNWNjZWI0ey0tZS1pY29uLWxpc3QtaWNvbi1zaXplOjE0cHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMmE1Y2NlYjQgLmVsZW1lbnRvci1pY29uLWxpc3QtdGV4dHtjb2xvcjojRkZGRkZGO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTJhNWNjZWI0IC5lbGVtZW50b3ItaWNvbi1saXN0LWl0ZW06aG92ZXIgLmVsZW1lbnRvci1pY29uLWxpc3QtdGV4dHtjb2xvcjp2YXIoIC0tZS1nbG9iYWwtY29sb3ItYWNjZW50ICk7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMmE1Y2NlYjQgLmVsZW1lbnRvci1pY29uLWxpc3QtaXRlbSA+IC5lbGVtZW50b3ItaWNvbi1saXN0LXRleHQsICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMmE1Y2NlYjQgLmVsZW1lbnRvci1pY29uLWxpc3QtaXRlbSA+IGF7Zm9udC1mYW1pbHk6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktdGV4dC1mb250LWZhbWlseSApLCBTYW5zLXNlcmlmO2ZvbnQtc2l6ZTp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS10ZXh0LWZvbnQtc2l6ZSApO2ZvbnQtd2VpZ2h0OnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LXRleHQtZm9udC13ZWlnaHQgKTtsaW5lLWhlaWdodDp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS10ZXh0LWxpbmUtaGVpZ2h0ICk7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNzdhODFkZDYgPiAuZWxlbWVudG9yLWVsZW1lbnQtcG9wdWxhdGVke3BhZGRpbmc6MHB4IDBweCAwcHggMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTFiOWJiYWNjIC5lbGVtZW50b3ItaGVhZGluZy10aXRsZXtjb2xvcjojRkZGRkZGO2ZvbnQtZmFtaWx5OnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LTQ1NmQzZDQtZm9udC1mYW1pbHkgKSwgU2Fucy1zZXJpZjtmb250LXNpemU6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktNDU2ZDNkNC1mb250LXNpemUgKTtmb250LXdlaWdodDp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS00NTZkM2Q0LWZvbnQtd2VpZ2h0ICk7bGluZS1oZWlnaHQ6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktNDU2ZDNkNC1saW5lLWhlaWdodCApO2xldHRlci1zcGFjaW5nOnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LTQ1NmQzZDQtbGV0dGVyLXNwYWNpbmcgKTt3b3JkLXNwYWNpbmc6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktNDU2ZDNkNC13b3JkLXNwYWNpbmcgKTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC0xYjliYmFjYyA+IC5lbGVtZW50b3Itd2lkZ2V0LWNvbnRhaW5lcnttYXJnaW46MHB4IDBweCAwcHggMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWJlMmNkMWYgLm1lbnUtaXRlbSBhLmhmZS1tZW51LWl0ZW17cGFkZGluZy1sZWZ0OjBweDtwYWRkaW5nLXJpZ2h0OjBweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIC5tZW51LWl0ZW0gYS5oZmUtc3ViLW1lbnUtaXRlbXtwYWRkaW5nLWxlZnQ6Y2FsYyggMHB4ICsgMjBweCApO3BhZGRpbmctcmlnaHQ6MHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWJlMmNkMWYgLmhmZS1uYXYtbWVudV9fbGF5b3V0LXZlcnRpY2FsIC5tZW51LWl0ZW0gdWwgdWwgYS5oZmUtc3ViLW1lbnUtaXRlbXtwYWRkaW5nLWxlZnQ6Y2FsYyggMHB4ICsgNDBweCApO3BhZGRpbmctcmlnaHQ6MHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWJlMmNkMWYgLmhmZS1uYXYtbWVudV9fbGF5b3V0LXZlcnRpY2FsIC5tZW51LWl0ZW0gdWwgdWwgdWwgYS5oZmUtc3ViLW1lbnUtaXRlbXtwYWRkaW5nLWxlZnQ6Y2FsYyggMHB4ICsgNjBweCApO3BhZGRpbmctcmlnaHQ6MHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWJlMmNkMWYgLmhmZS1uYXYtbWVudV9fbGF5b3V0LXZlcnRpY2FsIC5tZW51LWl0ZW0gdWwgdWwgdWwgdWwgYS5oZmUtc3ViLW1lbnUtaXRlbXtwYWRkaW5nLWxlZnQ6Y2FsYyggMHB4ICsgODBweCApO3BhZGRpbmctcmlnaHQ6MHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWJlMmNkMWYgLm1lbnUtaXRlbSBhLmhmZS1tZW51LWl0ZW0sICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiAubWVudS1pdGVtIGEuaGZlLXN1Yi1tZW51LWl0ZW17cGFkZGluZy10b3A6M3B4O3BhZGRpbmctYm90dG9tOjNweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIGEuaGZlLW1lbnUtaXRlbSwgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIGEuaGZlLXN1Yi1tZW51LWl0ZW17Zm9udC1mYW1pbHk6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktdGV4dC1mb250LWZhbWlseSApLCBTYW5zLXNlcmlmO2ZvbnQtc2l6ZTp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS10ZXh0LWZvbnQtc2l6ZSApO2ZvbnQtd2VpZ2h0OnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LXRleHQtZm9udC13ZWlnaHQgKTtsaW5lLWhlaWdodDp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS10ZXh0LWxpbmUtaGVpZ2h0ICk7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiAubWVudS1pdGVtIGEuaGZlLW1lbnUtaXRlbSwgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIC5zdWItbWVudSBhLmhmZS1zdWItbWVudS1pdGVte2NvbG9yOiNGRkZGRkY7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiAubWVudS1pdGVtIGEuaGZlLW1lbnUtaXRlbTpob3ZlcixcclxuICAgICAgICAgICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiAuc3ViLW1lbnUgYS5oZmUtc3ViLW1lbnUtaXRlbTpob3ZlcixcclxuICAgICAgICAgICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiAubWVudS1pdGVtLmN1cnJlbnQtbWVudS1pdGVtIGEuaGZlLW1lbnUtaXRlbSxcclxuICAgICAgICAgICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiAubWVudS1pdGVtIGEuaGZlLW1lbnUtaXRlbS5oaWdobGlnaHRlZCxcclxuICAgICAgICAgICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiAubWVudS1pdGVtIGEuaGZlLW1lbnUtaXRlbTpmb2N1c3tjb2xvcjp2YXIoIC0tZS1nbG9iYWwtY29sb3ItYWNjZW50ICk7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiAubWVudS1pdGVtLmN1cnJlbnQtbWVudS1pdGVtIGEuaGZlLW1lbnUtaXRlbSxcclxuICAgICAgICAgICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiAubWVudS1pdGVtLmN1cnJlbnQtbWVudS1hbmNlc3RvciBhLmhmZS1tZW51LWl0ZW17Y29sb3I6I0JDQkNCQzt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIC5zdWItbWVudSBhLmhmZS1zdWItbWVudS1pdGVtLCBcclxuICAgICAgICAgICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiAuZWxlbWVudG9yLW1lbnUtdG9nZ2xlLFxyXG4gICAgICAgICAgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIG5hdi5oZmUtZHJvcGRvd24gbGkgYS5oZmUtbWVudS1pdGVtLFxyXG4gICAgICAgICAgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIG5hdi5oZmUtZHJvcGRvd24gbGkgYS5oZmUtc3ViLW1lbnUtaXRlbSxcclxuICAgICAgICAgICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiBuYXYuaGZlLWRyb3Bkb3duLWV4cGFuZGlibGUgbGkgYS5oZmUtbWVudS1pdGVtLFxyXG4gICAgICAgICAgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIG5hdi5oZmUtZHJvcGRvd24tZXhwYW5kaWJsZSBsaSBhLmhmZS1zdWItbWVudS1pdGVte2NvbG9yOnZhciggLS1lLWdsb2JhbC1jb2xvci1zZWNvbmRhcnkgKTt9XHJcbiAgICAgICAgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIC5zdWItbWVudSBsaSBhLmhmZS1zdWItbWVudS1pdGVtLFxyXG4gICAgICAgICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiBuYXYuaGZlLWRyb3Bkb3duIGxpIGEuaGZlLXN1Yi1tZW51LWl0ZW0sXHJcbiAgICAgICAgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIG5hdi5oZmUtZHJvcGRvd24gbGkgYS5oZmUtbWVudS1pdGVtLFxyXG4gICAgICAgICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiBuYXYuaGZlLWRyb3Bkb3duLWV4cGFuZGlibGUgbGkgYS5oZmUtbWVudS1pdGVtLFxyXG4gICAgICAgICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiBuYXYuaGZlLWRyb3Bkb3duLWV4cGFuZGlibGUgbGkgYS5oZmUtc3ViLW1lbnUtaXRlbXtmb250LWZhbWlseTp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS10ZXh0LWZvbnQtZmFtaWx5ICksIFNhbnMtc2VyaWY7Zm9udC1zaXplOnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LXRleHQtZm9udC1zaXplICk7Zm9udC13ZWlnaHQ6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktdGV4dC1mb250LXdlaWdodCApO2xpbmUtaGVpZ2h0OnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LXRleHQtbGluZS1oZWlnaHQgKTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIC5zdWItbWVudSBhLmhmZS1zdWItbWVudS1pdGVtLFxyXG4gICAgICAgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIG5hdi5oZmUtZHJvcGRvd24gbGkgYS5oZmUtbWVudS1pdGVtLFxyXG4gICAgICAgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIG5hdi5oZmUtZHJvcGRvd24gbGkgYS5oZmUtc3ViLW1lbnUtaXRlbSxcclxuICAgICAgICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiBuYXYuaGZlLWRyb3Bkb3duLWV4cGFuZGlibGUgbGkgYS5oZmUtbWVudS1pdGVtLFxyXG4gICAgICAgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIG5hdi5oZmUtZHJvcGRvd24tZXhwYW5kaWJsZSBsaSBhLmhmZS1zdWItbWVudS1pdGVte3BhZGRpbmctdG9wOjE1cHg7cGFkZGluZy1ib3R0b206MTVweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIC5zdWItbWVudSBsaS5tZW51LWl0ZW06bm90KDpsYXN0LWNoaWxkKSwgXHJcbiAgICAgICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiBuYXYuaGZlLWRyb3Bkb3duIGxpLm1lbnUtaXRlbTpub3QoOmxhc3QtY2hpbGQpLFxyXG4gICAgICAgLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWJlMmNkMWYgbmF2LmhmZS1kcm9wZG93bi1leHBhbmRpYmxlIGxpLm1lbnUtaXRlbTpub3QoOmxhc3QtY2hpbGQpe2JvcmRlci1ib3R0b20tc3R5bGU6c29saWQ7Ym9yZGVyLWJvdHRvbS1jb2xvcjojYzRjNGM0O2JvcmRlci1ib3R0b20td2lkdGg6MXB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWJlMmNkMWYgPiAuZWxlbWVudG9yLXdpZGdldC1jb250YWluZXJ7bWFyZ2luOi01cHggMHB4IDBweCAwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMzAxZGY0ZGUgPiAuZWxlbWVudG9yLWNvbnRhaW5lcnsvKiBtYXgtd2lkdGg6MTIwMHB4OyAqL30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTMwMWRmNGRle2JvcmRlci1zdHlsZTpzb2xpZDtib3JkZXItd2lkdGg6MXB4IDBweCAwcHggMHB4O2JvcmRlci1jb2xvcjojRkZGRkZGMTc7dHJhbnNpdGlvbjpiYWNrZ3JvdW5kIDAuM3MsIGJvcmRlciAwLjNzLCBib3JkZXItcmFkaXVzIDAuM3MsIGJveC1zaGFkb3cgMC4zcztwYWRkaW5nOjIwcHggMHB4IDIwcHggMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTMwMWRmNGRlID4gLmVsZW1lbnRvci1iYWNrZ3JvdW5kLW92ZXJsYXl7dHJhbnNpdGlvbjpiYWNrZ3JvdW5kIDAuM3MsIGJvcmRlci1yYWRpdXMgMC4zcywgb3BhY2l0eSAwLjNzO30uZWxlbWVudG9yLWJjLWZsZXgtd2lkZ2V0ICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtZTZkMjU0NC5lbGVtZW50b3ItY29sdW1uIC5lbGVtZW50b3Itd2lkZ2V0LXdyYXB7YWxpZ24taXRlbXM6Y2VudGVyO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWU2ZDI1NDQuZWxlbWVudG9yLWNvbHVtbi5lbGVtZW50b3ItZWxlbWVudFtkYXRhLWVsZW1lbnRfdHlwZT1cImNvbHVtblwiXSA+IC5lbGVtZW50b3Itd2lkZ2V0LXdyYXAuZWxlbWVudG9yLWVsZW1lbnQtcG9wdWxhdGVke2FsaWduLWNvbnRlbnQ6Y2VudGVyO2FsaWduLWl0ZW1zOmNlbnRlcjt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1lNmQyNTQ0ID4gLmVsZW1lbnRvci1lbGVtZW50LXBvcHVsYXRlZHtwYWRkaW5nOjBweCAwcHggMHB4IDBweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC0xYTA2NzM0NHtjb2xvcjojRkZGRkZGO2ZvbnQtZmFtaWx5OlwiUm9ib3RvXCIsIFNhbnMtc2VyaWY7Zm9udC13ZWlnaHQ6NDAwO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTFhMDY3MzQ0ID4gLmVsZW1lbnRvci13aWRnZXQtY29udGFpbmVye21hcmdpbjowcHggMHB4IC0xNXB4IDBweDt9LmVsZW1lbnRvci1iYy1mbGV4LXdpZGdldCAgLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTJjYTc1ZGQwLmVsZW1lbnRvci1jb2x1bW4gLmVsZW1lbnRvci13aWRnZXQtd3JhcHthbGlnbi1pdGVtczpjZW50ZXI7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMmNhNzVkZDAuZWxlbWVudG9yLWNvbHVtbi5lbGVtZW50b3ItZWxlbWVudFtkYXRhLWVsZW1lbnRfdHlwZT1cImNvbHVtblwiXSA+IC5lbGVtZW50b3Itd2lkZ2V0LXdyYXAuZWxlbWVudG9yLWVsZW1lbnQtcG9wdWxhdGVke2FsaWduLWNvbnRlbnQ6Y2VudGVyO2FsaWduLWl0ZW1zOmNlbnRlcjt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC0yY2E3NWRkMC5lbGVtZW50b3ItY29sdW1uID4gLmVsZW1lbnRvci13aWRnZXQtd3JhcHtqdXN0aWZ5LWNvbnRlbnQ6ZmxleC1lbmQ7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMmNhNzVkZDAgPiAuZWxlbWVudG9yLWVsZW1lbnQtcG9wdWxhdGVke3BhZGRpbmc6MHB4IDBweCAwcHggMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTJjYzlhZmMgLmVsZW1lbnRvci1idXR0b257Zm9udC1mYW1pbHk6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktdGV4dC1mb250LWZhbWlseSApLCBTYW5zLXNlcmlmO2ZvbnQtc2l6ZTp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS10ZXh0LWZvbnQtc2l6ZSApO2ZvbnQtd2VpZ2h0OnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LXRleHQtZm9udC13ZWlnaHQgKTtsaW5lLWhlaWdodDp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS10ZXh0LWxpbmUtaGVpZ2h0ICk7ZmlsbDojRkZGRkZGO2NvbG9yOiNGRkZGRkY7YmFja2dyb3VuZC1jb2xvcjojMEE2Q0ZGMDA7Ym9yZGVyLXN0eWxlOnNvbGlkO2JvcmRlci13aWR0aDowcHggMHB4IDBweCAwcHg7Ym9yZGVyLXJhZGl1czowcHggMHB4IDBweCAwcHg7cGFkZGluZzowcHggMHB4IDBweCAwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMmNjOWFmYyAuZWxlbWVudG9yLWJ1dHRvbjpob3ZlciwgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC0yY2M5YWZjIC5lbGVtZW50b3ItYnV0dG9uOmZvY3Vze2NvbG9yOnZhciggLS1lLWdsb2JhbC1jb2xvci1hY2NlbnQgKTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC0yY2M5YWZjIC5lbGVtZW50b3ItYnV0dG9uOmhvdmVyIHN2ZywgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC0yY2M5YWZjIC5lbGVtZW50b3ItYnV0dG9uOmZvY3VzIHN2Z3tmaWxsOnZhciggLS1lLWdsb2JhbC1jb2xvci1hY2NlbnQgKTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC0yY2M5YWZjID4gLmVsZW1lbnRvci13aWRnZXQtY29udGFpbmVye21hcmdpbjowcHggMjBweCAwcHggMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTJjYzlhZmN7d2lkdGg6YXV0bzttYXgtd2lkdGg6YXV0bzt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC03ZTBhZjhhZiAuZWxlbWVudG9yLWJ1dHRvbntmb250LWZhbWlseTp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS10ZXh0LWZvbnQtZmFtaWx5ICksIFNhbnMtc2VyaWY7Zm9udC1zaXplOnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LXRleHQtZm9udC1zaXplICk7Zm9udC13ZWlnaHQ6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktdGV4dC1mb250LXdlaWdodCApO2xpbmUtaGVpZ2h0OnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LXRleHQtbGluZS1oZWlnaHQgKTtmaWxsOiNGRkZGRkY7Y29sb3I6I0ZGRkZGRjtiYWNrZ3JvdW5kLWNvbG9yOiMwQTZDRkYwMDtib3JkZXItc3R5bGU6c29saWQ7Ym9yZGVyLXdpZHRoOjBweCAwcHggMHB4IDBweDtib3JkZXItcmFkaXVzOjBweCAwcHggMHB4IDBweDtwYWRkaW5nOjBweCAwcHggMHB4IDBweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC03ZTBhZjhhZiAuZWxlbWVudG9yLWJ1dHRvbjpob3ZlciwgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC03ZTBhZjhhZiAuZWxlbWVudG9yLWJ1dHRvbjpmb2N1c3tjb2xvcjp2YXIoIC0tZS1nbG9iYWwtY29sb3ItYWNjZW50ICk7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtN2UwYWY4YWYgLmVsZW1lbnRvci1idXR0b246aG92ZXIgc3ZnLCAgLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTdlMGFmOGFmIC5lbGVtZW50b3ItYnV0dG9uOmZvY3VzIHN2Z3tmaWxsOnZhciggLS1lLWdsb2JhbC1jb2xvci1hY2NlbnQgKTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC03ZTBhZjhhZnt3aWR0aDphdXRvO21heC13aWR0aDphdXRvO306cm9vdHstLXBhZ2UtdGl0bGUtZGlzcGxheTpub25lO31AbWVkaWEobWF4LXdpZHRoOjEwMjRweCl7IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iNTFlN2I5IC5lbGVtZW50b3ItaGVhZGluZy10aXRsZXtmb250LXNpemU6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktcHJpbWFyeS1mb250LXNpemUgKTtsaW5lLWhlaWdodDp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS1wcmltYXJ5LWxpbmUtaGVpZ2h0ICk7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNDg5NTc2ZjIgPiAuZWxlbWVudG9yLWJhY2tncm91bmQtb3ZlcmxheXtiYWNrZ3JvdW5kLWltYWdlOnVybChcImh0dHA6Ly9hc2twcm9qZWN0Lm5ldC93cnl0ZXIvd3AtY29udGVudC91cGxvYWRzL3NpdGVzLzM2LzIwMjEvMTAvYmdfOC5wbmdcIik7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNDg5NTc2ZjJ7cGFkZGluZzo3MHB4IDMwcHggMHB4IDMwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNzU3MTY5MzZ7bWFyZ2luLXRvcDowcHg7bWFyZ2luLWJvdHRvbTo3MHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTdiYWI5MWI2ID4gLmVsZW1lbnRvci1lbGVtZW50LXBvcHVsYXRlZHttYXJnaW46MHB4IDBweCAwcHggMHB4Oy0tZS1jb2x1bW4tbWFyZ2luLXJpZ2h0OjBweDstLWUtY29sdW1uLW1hcmdpbi1sZWZ0OjBweDtwYWRkaW5nOjBweCAwcHggNTBweCAwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNzQzNTg5YmN7dGV4dC1hbGlnbjpjZW50ZXI7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNzQzNTg5YmMgaW1ne3dpZHRoOjI3JTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC03NDM1ODliYyA+IC5lbGVtZW50b3Itd2lkZ2V0LWNvbnRhaW5lcnttYXJnaW46MHB4IDBweCAwcHggMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWEzZWI0Y2IgPiAuZWxlbWVudG9yLXdpZGdldC1jb250YWluZXJ7bWFyZ2luOjBweCAwcHggMHB4IDBweDtwYWRkaW5nOjBweCAwcHggMHB4IDBweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC01NzgxNGMwYSAuZWxlbWVudG9yLWhlYWRpbmctdGl0bGV7Zm9udC1zaXplOnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LTQ1NmQzZDQtZm9udC1zaXplICk7bGluZS1oZWlnaHQ6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktNDU2ZDNkNC1saW5lLWhlaWdodCApO2xldHRlci1zcGFjaW5nOnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LTQ1NmQzZDQtbGV0dGVyLXNwYWNpbmcgKTt3b3JkLXNwYWNpbmc6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktNDU2ZDNkNC13b3JkLXNwYWNpbmcgKTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC03MTMwNTNkIC5lbGVtZW50b3ItaWNvbi1saXN0LWl0ZW0gPiAuZWxlbWVudG9yLWljb24tbGlzdC10ZXh0LCAgLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTcxMzA1M2QgLmVsZW1lbnRvci1pY29uLWxpc3QtaXRlbSA+IGF7Zm9udC1zaXplOnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LXRleHQtZm9udC1zaXplICk7bGluZS1oZWlnaHQ6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktdGV4dC1saW5lLWhlaWdodCApO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTcwZjA0NWQyID4gLmVsZW1lbnRvci1lbGVtZW50LXBvcHVsYXRlZHttYXJnaW46MHB4IDBweCAwcHggMHB4Oy0tZS1jb2x1bW4tbWFyZ2luLXJpZ2h0OjBweDstLWUtY29sdW1uLW1hcmdpbi1sZWZ0OjBweDtwYWRkaW5nOjBweCAwcHggMHB4IDBweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC0zOTk3ZWVlOSAuZWxlbWVudG9yLWhlYWRpbmctdGl0bGV7Zm9udC1zaXplOnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LTQ1NmQzZDQtZm9udC1zaXplICk7bGluZS1oZWlnaHQ6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktNDU2ZDNkNC1saW5lLWhlaWdodCApO2xldHRlci1zcGFjaW5nOnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LTQ1NmQzZDQtbGV0dGVyLXNwYWNpbmcgKTt3b3JkLXNwYWNpbmc6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktNDU2ZDNkNC13b3JkLXNwYWNpbmcgKTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC0yYTVjY2ViNCAuZWxlbWVudG9yLWljb24tbGlzdC1pdGVtID4gLmVsZW1lbnRvci1pY29uLWxpc3QtdGV4dCwgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC0yYTVjY2ViNCAuZWxlbWVudG9yLWljb24tbGlzdC1pdGVtID4gYXtmb250LXNpemU6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktdGV4dC1mb250LXNpemUgKTtsaW5lLWhlaWdodDp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS10ZXh0LWxpbmUtaGVpZ2h0ICk7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMWI5YmJhY2MgLmVsZW1lbnRvci1oZWFkaW5nLXRpdGxle2ZvbnQtc2l6ZTp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS00NTZkM2Q0LWZvbnQtc2l6ZSApO2xpbmUtaGVpZ2h0OnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LTQ1NmQzZDQtbGluZS1oZWlnaHQgKTtsZXR0ZXItc3BhY2luZzp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS00NTZkM2Q0LWxldHRlci1zcGFjaW5nICk7d29yZC1zcGFjaW5nOnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LTQ1NmQzZDQtd29yZC1zcGFjaW5nICk7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiAubWVudS1pdGVtIGEuaGZlLW1lbnUtaXRlbSwgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIC5tZW51LWl0ZW0gYS5oZmUtc3ViLW1lbnUtaXRlbXtwYWRkaW5nLXRvcDozcHg7cGFkZGluZy1ib3R0b206M3B4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWJlMmNkMWYgYS5oZmUtbWVudS1pdGVtLCAgLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWJlMmNkMWYgYS5oZmUtc3ViLW1lbnUtaXRlbXtmb250LXNpemU6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktdGV4dC1mb250LXNpemUgKTtsaW5lLWhlaWdodDp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS10ZXh0LWxpbmUtaGVpZ2h0ICk7fVxyXG4gICAgICAgICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiAuc3ViLW1lbnUgbGkgYS5oZmUtc3ViLW1lbnUtaXRlbSxcclxuICAgICAgICAgLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWJlMmNkMWYgbmF2LmhmZS1kcm9wZG93biBsaSBhLmhmZS1zdWItbWVudS1pdGVtLFxyXG4gICAgICAgICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiBuYXYuaGZlLWRyb3Bkb3duIGxpIGEuaGZlLW1lbnUtaXRlbSxcclxuICAgICAgICAgLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWJlMmNkMWYgbmF2LmhmZS1kcm9wZG93bi1leHBhbmRpYmxlIGxpIGEuaGZlLW1lbnUtaXRlbSxcclxuICAgICAgICAgLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWJlMmNkMWYgbmF2LmhmZS1kcm9wZG93bi1leHBhbmRpYmxlIGxpIGEuaGZlLXN1Yi1tZW51LWl0ZW17Zm9udC1zaXplOnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LXRleHQtZm9udC1zaXplICk7bGluZS1oZWlnaHQ6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktdGV4dC1saW5lLWhlaWdodCApO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWJlMmNkMWYgPiAuZWxlbWVudG9yLXdpZGdldC1jb250YWluZXJ7bWFyZ2luOi01cHggMHB4IC0xMHB4IDBweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC0zMDFkZjRkZXtwYWRkaW5nOjIwcHggMHB4IDIwcHggMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTJjYzlhZmMgLmVsZW1lbnRvci1idXR0b257Zm9udC1zaXplOnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LXRleHQtZm9udC1zaXplICk7bGluZS1oZWlnaHQ6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktdGV4dC1saW5lLWhlaWdodCApO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTdlMGFmOGFmIC5lbGVtZW50b3ItYnV0dG9ue2ZvbnQtc2l6ZTp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS10ZXh0LWZvbnQtc2l6ZSApO2xpbmUtaGVpZ2h0OnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LXRleHQtbGluZS1oZWlnaHQgKTt9fUBtZWRpYShtaW4td2lkdGg6NzY4cHgpeyAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMTQzOTY2MmR7d2lkdGg6MTQuMTA0JTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC02NWI5NTk5MXt3aWR0aDozOS44OTQlO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTQyNTE4YWE2e3dpZHRoOjQ1LjMzNCU7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtN2JhYjkxYjZ7d2lkdGg6NDAuODUxJTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC0zMDVhOTc2Ynt3aWR0aDoyMy44NjglO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTcwZjA0NWQye3dpZHRoOjE5LjI3NiU7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNzdhODFkZDZ7d2lkdGg6MTYuMDA1JTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1lNmQyNTQ0e3dpZHRoOjU1LjYxJTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC0yY2E3NWRkMHt3aWR0aDo0NC4zOSU7fX1AbWVkaWEobWF4LXdpZHRoOjEwMjRweCkgYW5kIChtaW4td2lkdGg6NzY4cHgpeyAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMTQzOTY2MmR7d2lkdGg6MiU7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNjViOTU5OTF7d2lkdGg6NDIlO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTQyNTE4YWE2e3dpZHRoOjU0JTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC03YmFiOTFiNnt3aWR0aDoxMDAlO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTMwNWE5NzZie3dpZHRoOjQ0JTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC03MGYwNDVkMnt3aWR0aDoxNCU7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNzdhODFkZDZ7d2lkdGg6MzklO319QG1lZGlhKG1heC13aWR0aDo3NjdweCl7IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC00Yzg5YTc0OHtwYWRkaW5nOjQ4cHggMHB4IDQ4cHggMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWI1MWU3YjkgLmVsZW1lbnRvci1oZWFkaW5nLXRpdGxle2ZvbnQtc2l6ZTp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS1wcmltYXJ5LWZvbnQtc2l6ZSApO2xpbmUtaGVpZ2h0OnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LXByaW1hcnktbGluZS1oZWlnaHQgKTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iNTFlN2I5ID4gLmVsZW1lbnRvci13aWRnZXQtY29udGFpbmVye21hcmdpbjowcHggMHB4IDBweCAwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNDI1MThhYTYgPiAuZWxlbWVudG9yLWVsZW1lbnQtcG9wdWxhdGVke3BhZGRpbmc6MHB4IDMwcHggMHB4IDMwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNDg5NTc2ZjIgPiAuZWxlbWVudG9yLWJhY2tncm91bmQtb3ZlcmxheXtiYWNrZ3JvdW5kLWltYWdlOnVybChcImh0dHA6Ly9hc2twcm9qZWN0Lm5ldC9mbG9yaXN0eS93cC1jb250ZW50L3VwbG9hZHMvc2l0ZXMvMzUvMjAyMS8xMC9iZ183LnBuZ1wiKTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC00ODk1NzZmMntwYWRkaW5nOjcwcHggMjBweCAwcHggMjBweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC03NTcxNjkzNnttYXJnaW4tdG9wOjBweDttYXJnaW4tYm90dG9tOjUwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtN2JhYjkxYjYgPiAuZWxlbWVudG9yLWVsZW1lbnQtcG9wdWxhdGVke21hcmdpbjowcHggMHB4IDUwcHggMHB4Oy0tZS1jb2x1bW4tbWFyZ2luLXJpZ2h0OjBweDstLWUtY29sdW1uLW1hcmdpbi1sZWZ0OjBweDtwYWRkaW5nOjBweCAwcHggMHB4IDBweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC03NDM1ODliY3t0ZXh0LWFsaWduOmNlbnRlcjt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC03NDM1ODliYyBpbWd7d2lkdGg6NTAlO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTc0MzU4OWJjID4gLmVsZW1lbnRvci13aWRnZXQtY29udGFpbmVye21hcmdpbjowcHggMHB4IDBweCAwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMzA1YTk3NmIgPiAuZWxlbWVudG9yLWVsZW1lbnQtcG9wdWxhdGVke21hcmdpbjowcHggMHB4IDQwcHggMHB4Oy0tZS1jb2x1bW4tbWFyZ2luLXJpZ2h0OjBweDstLWUtY29sdW1uLW1hcmdpbi1sZWZ0OjBweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC01NzgxNGMwYSAuZWxlbWVudG9yLWhlYWRpbmctdGl0bGV7Zm9udC1zaXplOnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LTQ1NmQzZDQtZm9udC1zaXplICk7bGluZS1oZWlnaHQ6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktNDU2ZDNkNC1saW5lLWhlaWdodCApO2xldHRlci1zcGFjaW5nOnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LTQ1NmQzZDQtbGV0dGVyLXNwYWNpbmcgKTt3b3JkLXNwYWNpbmc6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktNDU2ZDNkNC13b3JkLXNwYWNpbmcgKTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC01NzgxNGMwYSA+IC5lbGVtZW50b3Itd2lkZ2V0LWNvbnRhaW5lcnttYXJnaW46MHB4IDBweCAwcHggMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTcxMzA1M2QgLmVsZW1lbnRvci1pY29uLWxpc3QtaXRlbSA+IC5lbGVtZW50b3ItaWNvbi1saXN0LXRleHQsICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNzEzMDUzZCAuZWxlbWVudG9yLWljb24tbGlzdC1pdGVtID4gYXtmb250LXNpemU6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktdGV4dC1mb250LXNpemUgKTtsaW5lLWhlaWdodDp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS10ZXh0LWxpbmUtaGVpZ2h0ICk7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNzBmMDQ1ZDIgPiAuZWxlbWVudG9yLWVsZW1lbnQtcG9wdWxhdGVke21hcmdpbjowcHggMHB4IDBweCAwcHg7LS1lLWNvbHVtbi1tYXJnaW4tcmlnaHQ6MHB4Oy0tZS1jb2x1bW4tbWFyZ2luLWxlZnQ6MHB4O3BhZGRpbmc6MHB4IDBweCAwcHggMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTM5OTdlZWU5IC5lbGVtZW50b3ItaGVhZGluZy10aXRsZXtmb250LXNpemU6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktNDU2ZDNkNC1mb250LXNpemUgKTtsaW5lLWhlaWdodDp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS00NTZkM2Q0LWxpbmUtaGVpZ2h0ICk7bGV0dGVyLXNwYWNpbmc6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktNDU2ZDNkNC1sZXR0ZXItc3BhY2luZyApO3dvcmQtc3BhY2luZzp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS00NTZkM2Q0LXdvcmQtc3BhY2luZyApO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTM5OTdlZWU5ID4gLmVsZW1lbnRvci13aWRnZXQtY29udGFpbmVye21hcmdpbjowcHggMHB4IDBweCAwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMmE1Y2NlYjQgLmVsZW1lbnRvci1pY29uLWxpc3QtaXRlbSA+IC5lbGVtZW50b3ItaWNvbi1saXN0LXRleHQsICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMmE1Y2NlYjQgLmVsZW1lbnRvci1pY29uLWxpc3QtaXRlbSA+IGF7Zm9udC1zaXplOnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LXRleHQtZm9udC1zaXplICk7bGluZS1oZWlnaHQ6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktdGV4dC1saW5lLWhlaWdodCApO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTc3YTgxZGQ2ID4gLmVsZW1lbnRvci1lbGVtZW50LXBvcHVsYXRlZHttYXJnaW46MHB4IDBweCA1MHB4IDBweDstLWUtY29sdW1uLW1hcmdpbi1yaWdodDowcHg7LS1lLWNvbHVtbi1tYXJnaW4tbGVmdDowcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMWI5YmJhY2MgLmVsZW1lbnRvci1oZWFkaW5nLXRpdGxle2ZvbnQtc2l6ZTp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS00NTZkM2Q0LWZvbnQtc2l6ZSApO2xpbmUtaGVpZ2h0OnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LTQ1NmQzZDQtbGluZS1oZWlnaHQgKTtsZXR0ZXItc3BhY2luZzp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS00NTZkM2Q0LWxldHRlci1zcGFjaW5nICk7d29yZC1zcGFjaW5nOnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LTQ1NmQzZDQtd29yZC1zcGFjaW5nICk7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMWI5YmJhY2MgPiAuZWxlbWVudG9yLXdpZGdldC1jb250YWluZXJ7bWFyZ2luOjBweCAwcHggMHB4IDBweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIGEuaGZlLW1lbnUtaXRlbSwgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIGEuaGZlLXN1Yi1tZW51LWl0ZW17Zm9udC1zaXplOnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LXRleHQtZm9udC1zaXplICk7bGluZS1oZWlnaHQ6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktdGV4dC1saW5lLWhlaWdodCApO31cclxuICAgICAgICAgLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWJlMmNkMWYgLnN1Yi1tZW51IGxpIGEuaGZlLXN1Yi1tZW51LWl0ZW0sXHJcbiAgICAgICAgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIG5hdi5oZmUtZHJvcGRvd24gbGkgYS5oZmUtc3ViLW1lbnUtaXRlbSxcclxuICAgICAgICAgLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWJlMmNkMWYgbmF2LmhmZS1kcm9wZG93biBsaSBhLmhmZS1tZW51LWl0ZW0sXHJcbiAgICAgICAgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIG5hdi5oZmUtZHJvcGRvd24tZXhwYW5kaWJsZSBsaSBhLmhmZS1tZW51LWl0ZW0sXHJcbiAgICAgICAgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIG5hdi5oZmUtZHJvcGRvd24tZXhwYW5kaWJsZSBsaSBhLmhmZS1zdWItbWVudS1pdGVte2ZvbnQtc2l6ZTp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS10ZXh0LWZvbnQtc2l6ZSApO2xpbmUtaGVpZ2h0OnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LXRleHQtbGluZS1oZWlnaHQgKTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIC5zdWItbWVudXtib3JkZXItcmFkaXVzOjBweCAwcHggMHB4IDBweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIC5zdWItbWVudSBsaS5tZW51LWl0ZW06Zmlyc3QtY2hpbGR7Ym9yZGVyLXRvcC1sZWZ0LXJhZGl1czowcHg7Ym9yZGVyLXRvcC1yaWdodC1yYWRpdXM6MHB4O292ZXJmbG93OmhpZGRlbjt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIC5zdWItbWVudSBsaS5tZW51LWl0ZW06bGFzdC1jaGlsZHtib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czowcHg7Ym9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czowcHg7b3ZlcmZsb3c6aGlkZGVuO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LWJlMmNkMWYgbmF2LmhmZS1kcm9wZG93bntib3JkZXItcmFkaXVzOjBweCAwcHggMHB4IDBweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1iZTJjZDFmIG5hdi5oZmUtZHJvcGRvd24gbGkubWVudS1pdGVtOmZpcnN0LWNoaWxke2JvcmRlci10b3AtbGVmdC1yYWRpdXM6MHB4O2JvcmRlci10b3AtcmlnaHQtcmFkaXVzOjBweDtvdmVyZmxvdzpoaWRkZW47fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiBuYXYuaGZlLWRyb3Bkb3duIGxpLm1lbnUtaXRlbTpsYXN0LWNoaWxke2JvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOjBweDtib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOjBweDtvdmVyZmxvdzpoaWRkZW47fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiBuYXYuaGZlLWRyb3Bkb3duLWV4cGFuZGlibGV7Ym9yZGVyLXJhZGl1czowcHggMHB4IDBweCAwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiBuYXYuaGZlLWRyb3Bkb3duLWV4cGFuZGlibGUgbGkubWVudS1pdGVtOmZpcnN0LWNoaWxke2JvcmRlci10b3AtbGVmdC1yYWRpdXM6MHB4O2JvcmRlci10b3AtcmlnaHQtcmFkaXVzOjBweDtvdmVyZmxvdzpoaWRkZW47fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtYmUyY2QxZiBuYXYuaGZlLWRyb3Bkb3duLWV4cGFuZGlibGUgbGkubWVudS1pdGVtOmxhc3QtY2hpbGR7Ym9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6MHB4O2JvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6MHB4O292ZXJmbG93OmhpZGRlbjt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC1lNmQyNTQ0ID4gLmVsZW1lbnRvci1lbGVtZW50LXBvcHVsYXRlZHtwYWRkaW5nOjBweCAwcHggMHB4IDBweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC0xYTA2NzM0NHt0ZXh0LWFsaWduOmNlbnRlcjt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC0yY2E3NWRkMC5lbGVtZW50b3ItY29sdW1uID4gLmVsZW1lbnRvci13aWRnZXQtd3JhcHtqdXN0aWZ5LWNvbnRlbnQ6Y2VudGVyO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTJjYTc1ZGQwID4gLmVsZW1lbnRvci1lbGVtZW50LXBvcHVsYXRlZHtwYWRkaW5nOjBweCAwcHggMTVweCAwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMmNjOWFmYyAuZWxlbWVudG9yLWJ1dHRvbntmb250LXNpemU6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktdGV4dC1mb250LXNpemUgKTtsaW5lLWhlaWdodDp2YXIoIC0tZS1nbG9iYWwtdHlwb2dyYXBoeS10ZXh0LWxpbmUtaGVpZ2h0ICk7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtN2UwYWY4YWYgLmVsZW1lbnRvci1idXR0b257Zm9udC1zaXplOnZhciggLS1lLWdsb2JhbC10eXBvZ3JhcGh5LXRleHQtZm9udC1zaXplICk7bGluZS1oZWlnaHQ6dmFyKCAtLWUtZ2xvYmFsLXR5cG9ncmFwaHktdGV4dC1saW5lLWhlaWdodCApO319XHJcblxyXG4gICAgICAgICAgLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTFkYzViZDRke21hcmdpbi10b3A6MHB4O21hcmdpbi1ib3R0b206MHB4O30uZWxlbWVudG9yLWJjLWZsZXgtd2lkZ2V0ICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMzMwY2EzNzEuZWxlbWVudG9yLWNvbHVtbiAuZWxlbWVudG9yLXdpZGdldC13cmFwe2FsaWduLWl0ZW1zOmNlbnRlcjt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC0zMzBjYTM3MS5lbGVtZW50b3ItY29sdW1uLmVsZW1lbnRvci1lbGVtZW50W2RhdGEtZWxlbWVudF90eXBlPVwiY29sdW1uXCJdID4gLmVsZW1lbnRvci13aWRnZXQtd3JhcC5lbGVtZW50b3ItZWxlbWVudC1wb3B1bGF0ZWR7YWxpZ24tY29udGVudDpjZW50ZXI7YWxpZ24taXRlbXM6Y2VudGVyO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTMzMGNhMzcxID4gLmVsZW1lbnRvci1lbGVtZW50LXBvcHVsYXRlZHtwYWRkaW5nOjMycHggMHB4IDMycHggMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTU4ZDEyMjFkIC5lbGVtZW50b3ItaGVhZGluZy10aXRsZXtjb2xvcjojMTMxYjVjO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTU4ZDEyMjFke2NvbG9yOiMxMzFiNWM7bWFyZ2luLXRvcDowcHg7bWFyZ2luLWJvdHRvbTowcHg7cGFkZGluZzowcHggMHB4IDBweCAwcHg7ei1pbmRleDoxO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTU4ZDEyMjFkLmpldC1zdGlja3ktc2VjdGlvbi1zdGlja3ktLXN0dWNre3otaW5kZXg6OTk7bWFyZ2luLXRvcDowcHg7bWFyZ2luLWJvdHRvbTowcHg7cGFkZGluZzoxMnB4IDBweCAxMnB4IDBweDtiYWNrZ3JvdW5kLWNvbG9yOiNGRkZGRkY7Ym94LXNoYWRvdzowcHggMHB4IDBweCAxcHggI0RFREVERTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC01OGQxMjIxZC5qZXQtc3RpY2t5LXNlY3Rpb24tc3RpY2t5LS1zdHVjay5qZXQtc3RpY2t5LXRyYW5zaXRpb24taW4sICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNThkMTIyMWQuamV0LXN0aWNreS1zZWN0aW9uLXN0aWNreS0tc3R1Y2suamV0LXN0aWNreS10cmFuc2l0aW9uLW91dHt0cmFuc2l0aW9uOm1hcmdpbiAwLjVzLCBwYWRkaW5nIDAuNXMsIGJhY2tncm91bmQgMC41cywgYm94LXNoYWRvdyAwLjVzO30uZWxlbWVudG9yLWJjLWZsZXgtd2lkZ2V0ICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNDM1Mjg5ZTYuZWxlbWVudG9yLWNvbHVtbiAuZWxlbWVudG9yLXdpZGdldC13cmFwe2FsaWduLWl0ZW1zOmNlbnRlcjt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC00MzUyODllNi5lbGVtZW50b3ItY29sdW1uLmVsZW1lbnRvci1lbGVtZW50W2RhdGEtZWxlbWVudF90eXBlPVwiY29sdW1uXCJdID4gLmVsZW1lbnRvci13aWRnZXQtd3JhcC5lbGVtZW50b3ItZWxlbWVudC1wb3B1bGF0ZWR7YWxpZ24tY29udGVudDpjZW50ZXI7YWxpZ24taXRlbXM6Y2VudGVyO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTQzNTI4OWU2ID4gLmVsZW1lbnRvci1lbGVtZW50LXBvcHVsYXRlZHtwYWRkaW5nOjBweCAwcHggMHB4IDMwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMjY5ZTYxZHt0ZXh0LWFsaWduOmxlZnQ7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMjY5ZTYxZCBpbWd7d2lkdGg6MTAwJTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC0yNjllNjFkID4gLmVsZW1lbnRvci13aWRnZXQtY29udGFpbmVye21hcmdpbjowcHggLTIwcHggMHB4IDBweDtib3JkZXItc3R5bGU6c29saWQ7Ym9yZGVyLXdpZHRoOjBweCAwcHggMHB4IDBweDtib3JkZXItY29sb3I6I0ZGRkZGRjYzO30uZWxlbWVudG9yLWJjLWZsZXgtd2lkZ2V0ICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNGNiMmY1MDIuZWxlbWVudG9yLWNvbHVtbiAuZWxlbWVudG9yLXdpZGdldC13cmFwe2FsaWduLWl0ZW1zOnNwYWNlLWFyb3VuZDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC00Y2IyZjUwMi5lbGVtZW50b3ItY29sdW1uLmVsZW1lbnRvci1lbGVtZW50W2RhdGEtZWxlbWVudF90eXBlPVwiY29sdW1uXCJdID4gLmVsZW1lbnRvci13aWRnZXQtd3JhcC5lbGVtZW50b3ItZWxlbWVudC1wb3B1bGF0ZWR7YWxpZ24tY29udGVudDpzcGFjZS1hcm91bmQ7YWxpZ24taXRlbXM6c3BhY2UtYXJvdW5kO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTRjYjJmNTAyLmVsZW1lbnRvci1jb2x1bW4gPiAuZWxlbWVudG9yLXdpZGdldC13cmFwe2p1c3RpZnktY29udGVudDpmbGV4LWVuZDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC00Y2IyZjUwMiA+IC5lbGVtZW50b3ItZWxlbWVudC1wb3B1bGF0ZWR7cGFkZGluZzowcHggNnB4IDBweCAwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbWVudS1jb250YWluZXJ7aGVpZ2h0OjQwcHg7Ym9yZGVyLXJhZGl1czowcHggMHB4IDBweCAwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbmF2YmFyLW5hdiA+IGxpID4gYXtmb250LXNpemU6MTZweDtmb250LXdlaWdodDo3MDA7Y29sb3I6IzEzMWI1YztwYWRkaW5nOjBweCAzMHB4IDBweCAwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbmF2YmFyLW5hdiA+IGxpID4gYTpob3ZlciwgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC04OTU2YmI3IC5lbGVtZW50c2tpdC1uYXZiYXItbmF2ID4gbGkgPiBhOmZvY3VzLCAgLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTg5NTZiYjcgLmVsZW1lbnRza2l0LW5hdmJhci1uYXYgPiBsaSA+IGE6YWN0aXZlLCAgLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTg5NTZiYjcgLmVsZW1lbnRza2l0LW5hdmJhci1uYXYgPiBsaTpob3ZlciA+IGF7YmFja2dyb3VuZC1jb2xvcjpyZ2JhKDI1NSwgMjU1LCAyNTUsIDApO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTg5NTZiYjcgLmVsZW1lbnRza2l0LW5hdmJhci1uYXYgPiBsaSA+IGE6aG92ZXJ7Y29sb3I6IzEzMWI1Yzt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC04OTU2YmI3IC5lbGVtZW50c2tpdC1uYXZiYXItbmF2ID4gbGkgPiBhOmZvY3Vze2NvbG9yOiMxMzFiNWM7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbmF2YmFyLW5hdiA+IGxpID4gYTphY3RpdmV7Y29sb3I6IzEzMWI1Yzt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC04OTU2YmI3IC5lbGVtZW50c2tpdC1uYXZiYXItbmF2ID4gbGk6aG92ZXIgPiBhe2NvbG9yOiMxMzFiNWM7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbmF2YmFyLW5hdiA+IGxpOmhvdmVyID4gYSAuZWxlbWVudHNraXQtc3VibWVudS1pbmRpY2F0b3J7Y29sb3I6IzEzMWI1Yzt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC04OTU2YmI3IC5lbGVtZW50c2tpdC1uYXZiYXItbmF2ID4gbGkgPiBhOmhvdmVyIC5lbGVtZW50c2tpdC1zdWJtZW51LWluZGljYXRvcntjb2xvcjojMTMxYjVjO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTg5NTZiYjcgLmVsZW1lbnRza2l0LW5hdmJhci1uYXYgPiBsaSA+IGE6Zm9jdXMgLmVsZW1lbnRza2l0LXN1Ym1lbnUtaW5kaWNhdG9ye2NvbG9yOiMxMzFiNWM7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbmF2YmFyLW5hdiA+IGxpID4gYTphY3RpdmUgLmVsZW1lbnRza2l0LXN1Ym1lbnUtaW5kaWNhdG9ye2NvbG9yOiMxMzFiNWM7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbmF2YmFyLW5hdiA+IGxpLmN1cnJlbnQtbWVudS1pdGVtID4gYXtjb2xvcjojMTMxYjVjO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTg5NTZiYjcgLmVsZW1lbnRza2l0LW5hdmJhci1uYXYgPiBsaS5jdXJyZW50LW1lbnUtYW5jZXN0b3IgPiBhe2NvbG9yOiMxMzFiNWM7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbmF2YmFyLW5hdiA+IGxpLmN1cnJlbnQtbWVudS1hbmNlc3RvciA+IGEgLmVsZW1lbnRza2l0LXN1Ym1lbnUtaW5kaWNhdG9ye2NvbG9yOiMxMzFiNWM7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbmF2YmFyLW5hdiA+IGxpID4gYSAuZWxlbWVudHNraXQtc3VibWVudS1pbmRpY2F0b3J7Y29sb3I6IzZCNkI2Qjt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC04OTU2YmI3IC5lbGVtZW50c2tpdC1uYXZiYXItbmF2IC5lbGVtZW50c2tpdC1zdWJtZW51LXBhbmVsID4gbGkgPiBhe2ZvbnQtc2l6ZToxNXB4O2ZvbnQtd2VpZ2h0OjQwMDtwYWRkaW5nOjhweCAxNXB4IDhweCAyNHB4O2NvbG9yOiMxMzFiNWM7YmFja2dyb3VuZC1jb2xvcjojRkZGRkZGO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTg5NTZiYjcgLmVsZW1lbnRza2l0LW5hdmJhci1uYXYgLmVsZW1lbnRza2l0LXN1Ym1lbnUtcGFuZWwgPiBsaSA+IGE6aG92ZXJ7Y29sb3I6I0M4NDA0MDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC04OTU2YmI3IC5lbGVtZW50c2tpdC1uYXZiYXItbmF2IC5lbGVtZW50c2tpdC1zdWJtZW51LXBhbmVsID4gbGkgPiBhOmZvY3Vze2NvbG9yOiNDODQwNDA7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbmF2YmFyLW5hdiAuZWxlbWVudHNraXQtc3VibWVudS1wYW5lbCA+IGxpID4gYTphY3RpdmV7Y29sb3I6I0M4NDA0MDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC04OTU2YmI3IC5lbGVtZW50c2tpdC1uYXZiYXItbmF2IC5lbGVtZW50c2tpdC1zdWJtZW51LXBhbmVsID4gbGk6aG92ZXIgPiBhe2NvbG9yOiNDODQwNDA7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbmF2YmFyLW5hdiAuZWxlbWVudHNraXQtc3VibWVudS1wYW5lbCA+IGxpLmN1cnJlbnQtbWVudS1pdGVtID4gYXtjb2xvcjojMDBBRTlEICFpbXBvcnRhbnQ7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtc3VibWVudS1wYW5lbHtwYWRkaW5nOjE1cHggMHB4IDE1cHggMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTg5NTZiYjcgLmVsZW1lbnRza2l0LW5hdmJhci1uYXYgLmVsZW1lbnRza2l0LXN1Ym1lbnUtcGFuZWx7Ym9yZGVyLXN0eWxlOnNvbGlkO2JvcmRlci13aWR0aDowcHggMHB4IDBweCAwcHg7Ym9yZGVyLWNvbG9yOnJnYmEoMiwgMSwgMSwgMCk7YmFja2dyb3VuZC1jb2xvcjojRkZGRkZGO2JvcmRlci1yYWRpdXM6MnB4IDJweCAycHggMnB4O21pbi13aWR0aDoyMjBweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC04OTU2YmI3IC5lbGVtZW50c2tpdC1tZW51LWhhbWJ1cmdlcntmbG9hdDpyaWdodDtiYWNrZ3JvdW5kLWNvbG9yOnJnYmEoMiwgMSwgMSwgMCk7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbWVudS1oYW1idXJnZXIgLmVsZW1lbnRza2l0LW1lbnUtaGFtYnVyZ2VyLWljb257YmFja2dyb3VuZC1jb2xvcjojMzMzMzMzO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTg5NTZiYjcgLmVsZW1lbnRza2l0LW1lbnUtaGFtYnVyZ2VyID4gLmVraXQtbWVudS1pY29ue2NvbG9yOiMzMzMzMzM7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbWVudS1oYW1idXJnZXI6aG92ZXIgLmVsZW1lbnRza2l0LW1lbnUtaGFtYnVyZ2VyLWljb257YmFja2dyb3VuZC1jb2xvcjojMzMzMzMzO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTg5NTZiYjcgLmVsZW1lbnRza2l0LW1lbnUtaGFtYnVyZ2VyOmhvdmVyID4gLmVraXQtbWVudS1pY29ue2NvbG9yOiMzMzMzMzM7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbWVudS1jbG9zZXtiYWNrZ3JvdW5kLWNvbG9yOnJnYmEoMiwgMSwgMSwgMCk7Ym9yZGVyLXN0eWxlOnNvbGlkO2NvbG9yOnJnYmEoNTEsIDUxLCA1MSwgMSk7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbWVudS1jbG9zZTpob3Zlcntjb2xvcjpyZ2JhKDAsIDAsIDAsIDAuNSk7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyA+IC5lbGVtZW50b3Itd2lkZ2V0LWNvbnRhaW5lcnttYXJnaW46MHB4IDE2cHggMHB4IDBweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC04OTU2YmI3e3dpZHRoOmF1dG87bWF4LXdpZHRoOmF1dG87fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMTFhZDM5YjQgLmVsZW1lbnRvci1idXR0b257Zm9udC1zaXplOjE1cHg7Zm9udC13ZWlnaHQ6NTAwO2ZpbGw6I0ZGRkZGRjtjb2xvcjojRkZGRkZGO2JhY2tncm91bmQtY29sb3I6I0M4NDA0MDtib3JkZXItcmFkaXVzOjI1cHggMjVweCAyNXB4IDI1cHg7cGFkZGluZzoxMHB4IDI1cHggMTBweCAyNXB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTExYWQzOWI0IC5lbGVtZW50b3ItYnV0dG9uOmhvdmVyLCAgLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTExYWQzOWI0IC5lbGVtZW50b3ItYnV0dG9uOmZvY3Vze2NvbG9yOiNGRkZGRkY7YmFja2dyb3VuZC1jb2xvcjojOUIwMzAzO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTExYWQzOWI0IC5lbGVtZW50b3ItYnV0dG9uOmhvdmVyIHN2ZywgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC0xMWFkMzliNCAuZWxlbWVudG9yLWJ1dHRvbjpmb2N1cyBzdmd7ZmlsbDojRkZGRkZGO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTExYWQzOWI0e3dpZHRoOmF1dG87bWF4LXdpZHRoOmF1dG87fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtOWY0NjYxMSAuZWxlbWVudG9yLWJ1dHRvbntmb250LWZhbWlseTpcIk51bml0byBTYW5zXCIsIFNhbnMtc2VyaWY7Zm9udC1zaXplOjE1cHg7Zm9udC13ZWlnaHQ6NjAwO3RleHQtdHJhbnNmb3JtOm5vbmU7bGV0dGVyLXNwYWNpbmc6MHB4O2ZpbGw6IzEzMWI1Yztjb2xvcjojMTMxYjVjO2JhY2tncm91bmQtY29sb3I6IzEzMUI1QzAwO2JvcmRlci1zdHlsZTpzb2xpZDtib3JkZXItd2lkdGg6MnB4IDJweCAycHggMnB4O2JvcmRlci1jb2xvcjojMTMxYjVjO3BhZGRpbmc6OHB4IDE3cHggOHB4IDE1cHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtOWY0NjYxMSAuZWxlbWVudG9yLWJ1dHRvbjpob3ZlciwgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC05ZjQ2NjExIC5lbGVtZW50b3ItYnV0dG9uOmZvY3Vze2NvbG9yOiM3RTdEN0Q7Ym9yZGVyLWNvbG9yOiNDMEMwQzA7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtOWY0NjYxMSAuZWxlbWVudG9yLWJ1dHRvbjpob3ZlciBzdmcsICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtOWY0NjYxMSAuZWxlbWVudG9yLWJ1dHRvbjpmb2N1cyBzdmd7ZmlsbDojN0U3RDdEO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTlmNDY2MTEgPiAuZWxlbWVudG9yLXdpZGdldC1jb250YWluZXJ7bWFyZ2luOjBweCAwcHggMHB4IDdweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC05ZjQ2NjExe3dpZHRoOmF1dG87bWF4LXdpZHRoOmF1dG87fTpyb290ey0tcGFnZS10aXRsZS1kaXNwbGF5Om5vbmU7fUBtZWRpYShtYXgtd2lkdGg6MTAyNHB4KXsgLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTI2OWU2MWQgaW1ne3dpZHRoOjEwMCU7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMjY5ZTYxZCA+IC5lbGVtZW50b3Itd2lkZ2V0LWNvbnRhaW5lcnttYXJnaW46MHB4IC0yMHB4IDBweCAwcHg7Ym9yZGVyLXdpZHRoOjBweCAwcHggMHB4IDBweDt9LmVsZW1lbnRvci1iYy1mbGV4LXdpZGdldCAgLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTRjYjJmNTAyLmVsZW1lbnRvci1jb2x1bW4gLmVsZW1lbnRvci13aWRnZXQtd3JhcHthbGlnbi1pdGVtczpjZW50ZXI7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNGNiMmY1MDIuZWxlbWVudG9yLWNvbHVtbi5lbGVtZW50b3ItZWxlbWVudFtkYXRhLWVsZW1lbnRfdHlwZT1cImNvbHVtblwiXSA+IC5lbGVtZW50b3Itd2lkZ2V0LXdyYXAuZWxlbWVudG9yLWVsZW1lbnQtcG9wdWxhdGVke2FsaWduLWNvbnRlbnQ6Y2VudGVyO2FsaWduLWl0ZW1zOmNlbnRlcjt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC04OTU2YmI3IC5lbGVtZW50c2tpdC1uYXYtaWRlbnRpdHktcGFuZWx7cGFkZGluZzoxMHB4IDBweCAxMHB4IDBweDt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC04OTU2YmI3IC5lbGVtZW50c2tpdC1tZW51LWNvbnRhaW5lcnttYXgtd2lkdGg6MzUwcHg7Ym9yZGVyLXJhZGl1czowcHggMHB4IDBweCAwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbmF2YmFyLW5hdiA+IGxpID4gYXtiYWNrZ3JvdW5kLXNpemU6MHB4IGF1dG87Y29sb3I6IzMzMzMzMztwYWRkaW5nOjhweCAzMnB4IDhweCAzMnB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTg5NTZiYjcgLmVsZW1lbnRza2l0LW5hdmJhci1uYXYgPiBsaSA+IGE6aG92ZXIsICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbmF2YmFyLW5hdiA+IGxpID4gYTpmb2N1cywgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC04OTU2YmI3IC5lbGVtZW50c2tpdC1uYXZiYXItbmF2ID4gbGkgPiBhOmFjdGl2ZSwgIC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC04OTU2YmI3IC5lbGVtZW50c2tpdC1uYXZiYXItbmF2ID4gbGk6aG92ZXIgPiBhe2JhY2tncm91bmQtc2l6ZTowcHggYXV0bzt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC04OTU2YmI3IC5lbGVtZW50c2tpdC1uYXZiYXItbmF2ID4gbGkgPiBhOmhvdmVye2NvbG9yOiMwMEJEQUE7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbmF2YmFyLW5hdiA+IGxpID4gYTpmb2N1c3tjb2xvcjojMDBCREFBO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTg5NTZiYjcgLmVsZW1lbnRza2l0LW5hdmJhci1uYXYgPiBsaSA+IGE6YWN0aXZle2NvbG9yOiMwMEJEQUE7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbmF2YmFyLW5hdiA+IGxpOmhvdmVyID4gYXtjb2xvcjojMDBCREFBO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTg5NTZiYjcgLmVsZW1lbnRza2l0LW5hdmJhci1uYXYgPiBsaTpob3ZlciA+IGEgLmVsZW1lbnRza2l0LXN1Ym1lbnUtaW5kaWNhdG9ye2NvbG9yOiMwMEJEQUE7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbmF2YmFyLW5hdiA+IGxpID4gYTpob3ZlciAuZWxlbWVudHNraXQtc3VibWVudS1pbmRpY2F0b3J7Y29sb3I6IzAwQkRBQTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC04OTU2YmI3IC5lbGVtZW50c2tpdC1uYXZiYXItbmF2ID4gbGkgPiBhOmZvY3VzIC5lbGVtZW50c2tpdC1zdWJtZW51LWluZGljYXRvcntjb2xvcjojMDBCREFBO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTg5NTZiYjcgLmVsZW1lbnRza2l0LW5hdmJhci1uYXYgPiBsaSA+IGE6YWN0aXZlIC5lbGVtZW50c2tpdC1zdWJtZW51LWluZGljYXRvcntjb2xvcjojMDBCREFBO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTg5NTZiYjcgLmVsZW1lbnRza2l0LW5hdmJhci1uYXYgPiBsaS5jdXJyZW50LW1lbnUtaXRlbSA+IGF7Y29sb3I6IzAwQkRBQTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC04OTU2YmI3IC5lbGVtZW50c2tpdC1uYXZiYXItbmF2ID4gbGkuY3VycmVudC1tZW51LWFuY2VzdG9yID4gYXtjb2xvcjojMDBCREFBO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTg5NTZiYjcgLmVsZW1lbnRza2l0LW5hdmJhci1uYXYgPiBsaS5jdXJyZW50LW1lbnUtYW5jZXN0b3IgPiBhIC5lbGVtZW50c2tpdC1zdWJtZW51LWluZGljYXRvcntjb2xvcjojMDBCREFBO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTg5NTZiYjcgLmVsZW1lbnRza2l0LW5hdmJhci1uYXYgLmVsZW1lbnRza2l0LXN1Ym1lbnUtcGFuZWwgPiBsaSA+IGF7cGFkZGluZzo4cHggOHB4IDhweCA4cHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbmF2YmFyLW5hdiAuZWxlbWVudHNraXQtc3VibWVudS1wYW5lbCA+IGxpID4gYTpob3Zlcntjb2xvcjojNUE2Q0ZGO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTg5NTZiYjcgLmVsZW1lbnRza2l0LW5hdmJhci1uYXYgLmVsZW1lbnRza2l0LXN1Ym1lbnUtcGFuZWwgPiBsaSA+IGE6Zm9jdXN7Y29sb3I6IzVBNkNGRjt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC04OTU2YmI3IC5lbGVtZW50c2tpdC1uYXZiYXItbmF2IC5lbGVtZW50c2tpdC1zdWJtZW51LXBhbmVsID4gbGkgPiBhOmFjdGl2ZXtjb2xvcjojNUE2Q0ZGO30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTg5NTZiYjcgLmVsZW1lbnRza2l0LW5hdmJhci1uYXYgLmVsZW1lbnRza2l0LXN1Ym1lbnUtcGFuZWwgPiBsaTpob3ZlciA+IGF7Y29sb3I6IzVBNkNGRjt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC04OTU2YmI3IC5lbGVtZW50c2tpdC1uYXZiYXItbmF2IC5lbGVtZW50c2tpdC1zdWJtZW51LXBhbmVse2JvcmRlci13aWR0aDowcHggMHB4IDBweCA0MHB4O2JvcmRlci1yYWRpdXM6MHB4IDBweCAwcHggMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTg5NTZiYjcgLmVsZW1lbnRza2l0LW1lbnUtaGFtYnVyZ2Vye3BhZGRpbmc6OHB4IDhweCA4cHggOHB4O3dpZHRoOjQ1cHg7Ym9yZGVyLXJhZGl1czozcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbWVudS1jbG9zZXtwYWRkaW5nOjBweCA4cHggOHB4IDhweDttYXJnaW46MTJweCAxMnB4IDEycHggMTJweDt3aWR0aDo0NXB4O2JvcmRlci1yYWRpdXM6M3B4O2JvcmRlci13aWR0aDowcHggMHB4IDBweCAwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbmF2LWxvZ28gPiBpbWd7bWF4LXdpZHRoOjcwcHg7bWF4LWhlaWdodDo2MHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTg5NTZiYjcgLmVsZW1lbnRza2l0LW5hdi1sb2dve21hcmdpbjoyNHB4IDI0cHggMHB4IDI0cHg7cGFkZGluZzo1cHggNXB4IDVweCA1cHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyA+IC5lbGVtZW50b3Itd2lkZ2V0LWNvbnRhaW5lcnttYXJnaW46MHB4IDMycHggMHB4IDBweDt9fUBtZWRpYShtaW4td2lkdGg6NzY4cHgpeyAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtNDM1Mjg5ZTZ7d2lkdGg6MjMuMjg5JTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC00Y2IyZjUwMnt3aWR0aDo3Ni43MTElO319QG1lZGlhKG1heC13aWR0aDo3NjdweCl7IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC00MzUyODllNnt3aWR0aDozMCU7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtMjY5ZTYxZCBpbWd7d2lkdGg6MTAwJTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC0yNjllNjFkID4gLmVsZW1lbnRvci13aWRnZXQtY29udGFpbmVye21hcmdpbjowcHggLTMwcHggMHB4IDBweDtib3JkZXItd2lkdGg6MHB4IDBweCAwcHggMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTRjYjJmNTAye3dpZHRoOjcwJTt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC04OTU2YmI3IC5lbGVtZW50c2tpdC1uYXZiYXItbmF2ID4gbGkgPiBhOmhvdmVyLCAgLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTg5NTZiYjcgLmVsZW1lbnRza2l0LW5hdmJhci1uYXYgPiBsaSA+IGE6Zm9jdXMsICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbmF2YmFyLW5hdiA+IGxpID4gYTphY3RpdmUsICAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbmF2YmFyLW5hdiA+IGxpOmhvdmVyID4gYXtiYWNrZ3JvdW5kLXNpemU6MHB4IGF1dG87fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyAuZWxlbWVudHNraXQtbmF2YmFyLW5hdiA+IGxpLmN1cnJlbnQtbWVudS1pdGVtID4gYSwgLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTg5NTZiYjcgLmVsZW1lbnRza2l0LW5hdmJhci1uYXYgPiBsaS5jdXJyZW50LW1lbnUtYW5jZXN0b3IgPiBhe2JhY2tncm91bmQtc2l6ZTowcHggYXV0bzt9IC5lbGVtZW50b3ItZWxlbWVudC5lbGVtZW50b3ItZWxlbWVudC04OTU2YmI3IC5lbGVtZW50c2tpdC1uYXYtbG9nbyA+IGltZ3ttYXgtd2lkdGg6NzBweDttYXgtaGVpZ2h0OjUwcHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtODk1NmJiNyA+IC5lbGVtZW50b3Itd2lkZ2V0LWNvbnRhaW5lcnttYXJnaW46MHB4IDhweCAwcHggMHB4O30gLmVsZW1lbnRvci1lbGVtZW50LmVsZW1lbnRvci1lbGVtZW50LTExYWQzOWI0IC5lbGVtZW50b3ItYnV0dG9ue3BhZGRpbmc6OHB4IDEycHggOHB4IDEycHg7fSAuZWxlbWVudG9yLWVsZW1lbnQuZWxlbWVudG9yLWVsZW1lbnQtOWY0NjYxMSAuZWxlbWVudG9yLWJ1dHRvbntwYWRkaW5nOjBweCAwcHggMHB4IDhweDt9fVxyXG4iXX0= */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](HomeComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-home',
                templateUrl: './home.component.html',
                styleUrls: ['./home.component.css']
            }]
    }], function () { return [{ type: src_app_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"] }, { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }, { type: _services_cpn_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/map-french/map-french.component.ts":
/*!****************************************************!*\
  !*** ./src/app/map-french/map-french.component.ts ***!
  \****************************************************/
/*! exports provided: MapFrenchComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapFrenchComponent", function() { return MapFrenchComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");



const _c0 = function (a0) { return { choix: a0 }; };
class MapFrenchComponent {
    constructor() {
        this.myOutput = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.message = " Info about the action";
    }
    /*************************cahnge input *************/
    ngOnChanges(changes) {
        this.dept = changes.myinputDep.currentValue;
        this.selectDept(changes.myinputDep.currentValue);
    }
    ngOnInit() {
        $("path, circle").hover(function (e) {
            $('#info-box').css('display', 'block');
            $('#info-box').html($(this).data('department') + "  " + $(this).data('name'));
            // console.log('hover', $( '#info-box').html($(this).data('info')))
        });
        $("path, circle").mouseleave(function (e) {
            $('#info-box').css('display', 'none');
        });
        $(document).mousemove(function (e) {
            $('#info-box').css('top', e.pageY - $('#info-box').height() - 30);
            $('#info-box').css('left', e.pageX - ($('#info-box').width()) / 2);
        }).mouseover();
    }
    selectDept(val) {
        let mapCityName = document.getElementById("placeName");
        let map = document.getElementById("svgContent");
        let region = map.querySelectorAll(".region");
        region.forEach((regionItem) => {
            let departement = regionItem.querySelectorAll('.departement');
            departement.forEach((departementItem) => {
                var _a, _b, _c, _d, _e;
                if (((_a = departementItem.attributes[2]) === null || _a === void 0 ? void 0 : _a.nodeValue) == val || ((_b = departementItem.attributes[2]) === null || _b === void 0 ? void 0 : _b.nodeValue) == val) {
                    // mapCityName.innerHTML ="<b>"+regionItem.dataset.name+"</b> : "+departementItem.dataset.name+" <sup>("+departementItem.attributes[3]?.nodeValue+")</sup>";
                    this.map = {
                        region: (_c = regionItem === null || regionItem === void 0 ? void 0 : regionItem.attributes[1]) === null || _c === void 0 ? void 0 : _c.nodeValue,
                        departement: (_d = departementItem === null || departementItem === void 0 ? void 0 : departementItem.attributes[1]) === null || _d === void 0 ? void 0 : _d.nodeValue,
                        zipCode: (_e = departementItem === null || departementItem === void 0 ? void 0 : departementItem.attributes[2]) === null || _e === void 0 ? void 0 : _e.nodeValue,
                    };
                    this.myOutput.emit(this.map);
                }
            });
        });
    }
}
MapFrenchComponent.ɵfac = function MapFrenchComponent_Factory(t) { return new (t || MapFrenchComponent)(); };
MapFrenchComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: MapFrenchComponent, selectors: [["app-map-french"]], inputs: { myinputDep: "myinputDep" }, outputs: { myOutput: "myOutput" }, features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵNgOnChangesFeature"]], decls: 123, vars: 303, consts: [["id", "info-box", 1, "round", "right-in"], [1, "mapfrench_container"], [1, "mapfrench_wrapper"], ["id", "svgContent", "version", "1.1", "xmlns", "http://www.w3.org/2000/svg", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", "x", "0px", "y", "0px", "viewBox", "0 0 700 590", 0, "xml", "space", "preserve", 1, "map_content"], ["data-name", "Guadeloupe", "data-department", "971", "data-code_insee", "01", 1, "region"], ["data-name", "Guadeloupe", "data-department", "971", "d", "M35.87,487.13l0.7,7.2l-4.5-1.1l-2,1.7l-5.8-0.6l-1.7-1.2l4.9,0.5l3.2-4.4L35.87,487.13z M104.87,553.63 l-4.4-1.8l-1.9,0.8l0.2,2.1l-1.9,0.3l-2.2,4.9l0.7,2.4l1.7,2.9l3.4,1.2l3.4-0.5l5.3-5l-0.4-2.5L104.87,553.63z M110.27,525.53 l-6.7-2.2l-2.4-4.2l-11.1-2.5l-2.7-5.7l-0.7-7.7l-6.2-4.7l-5.9,5.5l-0.8,2.9l1.2,4.5l3.1,1.2l-1,3.4l-2.6,1.2l-2.5,5.1l-1.9-0.2 l-1,1.9l-4.3-0.7l1.8-0.7l-3.5-3.7l-10.4-4.1l-3.4,1.6l-2.4,4.8l-0.5,3.5l3.1,9.7l0.6,12l6.3,9l0.6,2.7c3-1.2,6-2.5,9.1-3.7l5.9-6.9 l-0.4-8.7l-2.8-5.3l0.2-5.5l3.6,0.2l0.9-1.7l1.4,3.1l6.8,2l13.8-4.9L110.27,525.53z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Martinique", "data-department", "972", "data-code_insee", "02", 1, "region"], ["data-name", "Martinique", "data-department", "972", "d", "m44.23,433.5l1.4-4.1l-6.2-7.5l0.3-5.8l4.8-4 l4.9-0.9l17,9.9l7,8.8l9.4-5.2l1.8,2.2l-2.8,0.8l0.7,2.6l-2.9,1l-2.2-2.4l-1.9,1.7l0.6,2.5l5.1,1.6l-5.3,4.9l1.6,2.3l4.5-1.5 l-0.8,5.6l3.7,0.2l7.6,19l-1.8,5.5l-4.1,5.1h-2.6l-2-3l3.7-5.7l-4.3,1.7l-2.5-2.5l-2.4,1.2l-6-2.8l-5.5,0.1l-5.4,3.5l-2.4-2.1 l0.2-2.7l-2-2l2.5-4.9l3.4-2.5l4.9,3.4l3.2-1.9l-4.4-4.7l0.2-2.4l-1.8,1.2l-7.2-1.1l-7.6-7L44.23,433.5z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Guyane", "data-department", "973", "data-code_insee", "03", 1, "region"], ["data-name", "Guyane", "data-department", "973", "d", "m95.2,348.97l-11.7,16.4l0.3,2.4l-7.3,14.9 l-4.4,3.9l-2.6,1.3l-2.3-1.7l-4.4,0.8l0.7-1.8l-10.6-0.3l-4.3,0.8l-4.1,4.1l-9.1-4.4l6.6-11.8l0.3-6l4.2-10.8l-8.3-9.6l-2.7-8 l-0.6-11.4l3.8-7.5l5.9-5.4l1-4l4.2,0.5l-2.3-2l24.7,8.6l9.2,8.8l3.1,0.3l-0.7,1.2l6.1,4l1.4,4.1l-2.4,3.1l2.6-1.6l0.1-5.5l4,3.5 l2.4,7L95.2,348.97z", 1, "departement", 3, "ngClass", "click"], ["data-name", "La R\u00E9union", "data-department", "974", "data-code_insee", "04", 1, "region"], ["data-name", "La R\u00E9union", "data-department", "974", "d", "m41.33,265.3l-6.7-8.5l1.3-6l4.1-2.4l0.7-7.9 l3.3,0.4l7.6-6.1l5.7-0.8l21,4l5,5.3v4.1l7.3,10.1l6.7,4.5l1,3.6l-3.3,7.9l0.9,9.6l-3.4,3.5l-17.3,2.9l-19.6-6.5l-3.8-3.6l-4.7-1.2 l-0.9-2.5l-3.6-2.3L41.33,265.3z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Mayotte", "data-department", "976", "data-code_insee", "06", 1, "region"], ["data-name", "Mayotte", "data-department", "976", "d", "m57.79,157.13l11.32,5.82l-3.24,7.46l-5.66,7.52l5.66,8.37l-4.04,5.7l-5.66,8.01l5.66,4.37l-7.28,4.37l-8.09-2.73l-4.04-5.04v-4.85l-3.24-6.55l7.28,3.88l4.04,1.13v-7.14l-4.85-8.43v-14.8l-8.09-2.61l-3.24-2.67v-5.76l8.9-6.79l7.28,10.19L57.79,157.13z M78.07,164.38l-5.56,3.42l4.81,5.59l3.93-4.79L78.07,164.38z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Ile-de-France", "data-code_insee", "11", 1, "region"], ["data-name", "Paris", "data-department", "75", "d", "M641.8,78.3l-0.2,3.8l-1,2.6l-8.3-1.7l-6-0.6l-5.2,3h-4l-2.5-0.3l-0.4-0.1l-13.5-5l-3-3.8l-4.3-1.9l-0.5-0.2 l0.4-1.9l1.3-3.1l2.7-2.1l2.9-1.1l3.9,0.5h0.1l0.9-2.2l7.1-4.6l14-0.1l1.8,3.6l1.8,2.4l0.6,0.9l0.1,0.4L631,68l0.4,5.4l0.4,1.8v0.1 l-0.3,0.8l0.1,3.6l0.6-0.5l1.6-1.6l2-0.5l2-0.5L641.8,78.3z M396.8,154.7l-3.2-0.5l-2.5,1.7l3,3.5l5.3-0.1l-1.8-1.9L396.8,154.7z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Seine-et-Marne", "data-department", "77", "d", "m441.1,176.1l-2.9,0.8l0.4,8.5l-15.4,3 l-0.2,5.8l-3.9,5.4l-11.2,2.7l-9.2-0.7l2.6-1.5l0.6-2.7l-4.2-4.3L397,190l3.4-4.8l4-17.2l-0.5-1l1.1-4.1l-0.3-2.9v-0.1l-1.3-4.7 l1.3-2.5l-1.7-5.1l0.1-0.1l1.7-2.3l-0.2-2l6.9,1l2-2.2l2.5,1.6l8.1-2.9l2.6,0.7l1.8,2.5l-0.7,2.8l3.9,4.2l9.3,6l-0.4,2l-2.6,2.2 l3.5,8.3l2.6,1.7L441.1,176.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Yvelines", "data-department", "78", "d", "m364.1,158.1l-3.6-6.6l-1.8-5.8l2.3-2.6 l3.8,0.1l9.5,0.8l9,3.6l5.5,6.1l-2,3.1l3.2,5.2l-7.1,5.4l-1.6,2.6l0.7,2.9l-4.6,8.6l-3.1,0.7L372,180l-1.2-5.6l-6.2-5.4L364.1,158.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Essonne", "data-department", "91", "d", "m401.6,164.8l2.3,2.2l0.5,1l-4,17.2L397,190 l-3.7-0.6l-2.8,1.8l-1.5-2.7l-1.9,2.9l-6.9,0.7l-2.8-10.6l4.6-8.6l-0.7-2.9l1.6-2.6l7.1-5.4v-0.1l3.7,1.6l5.1,2.1L401.6,164.8z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Hauts-de-Seine", "data-department", "92", "d", "M391.1,155.9l3,3.5l-0.4,4.1l-3.7-1.6v0.1l-3.2-5.2l2-3.1l3.6-2.6l1.3,2l-0.1,1.1L391.1,155.9z M612.6,54.1 l1.6-0.7l0.7-1.9l0.5-1.8l-0.1-1.1l-0.2-1.4l-4.6-1.9l-4.6-0.9l-4,1.3l-7.6,5.6l-6.1,5.8l-5.3,3l-1,1l-3.75,7.4l1.79,7.17 l-0.06,0.07l0.01,0.06l-2.74,3.23l0.68,2.44l2.5,4.8l3.3-0.5l1,5.2l3.9-0.3l1.4,3.5l3.4,1.6l0.5,2.1l5.3,4.2l4.3,1.3l-0.1,4.9 l5.7,3.5l3.15-5.91l-0.7-5.46l0.72-1.2l0.4-1.3l0.7-2.1l-1.4-1.9l0.3-1.2l0.8-2.8l-1-2.6l0.5-0.3l0.5-0.3l0.9-0.5l0.7-1.1l-0.4-0.1 l-13.5-5l-3-3.8l-4.3-1.9l-0.5-0.2l0.3-1.9l1.4-3.1l2.7-2.1l2.8-1.1h0.1l3.9,0.5l0.9-2.2l7.2-4.6l-0.7-2l-0.6-2l1.4-0.7L612.6,54.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Seine-Saint-Denis", "data-department", "93", "d", "M404.7,152.7l-1.3,2.5l1.3,4.7v0.1l-7.1-2.6l-0.8-2.7l-3.2-0.5l0.1-1.1l-1.3-2l3.3-1.3l2.6,1.1 c1.6-1.1,3.2-2.2,4.7-3.3L404.7,152.7z M663.2,73.89l0.06-0.08l-0.02-0.04l2.61-3.38l-3.95-0.3l-1.6-5.9l0.06-0.06l-0.02-0.06 l6.36-6.56l0.1-5.42l1.1-4l-1.2-3.4l-5.1-8l0.07-0.08l-0.03-0.04l2.65-3.33l-0.89-4.04l-4.5-2.9l-4.1,1.7l-6.4,8.8l-8.2,6.2 l-0.7-0.2l-7.8-1.1l-1.9,1l-5.1-4.6l-1.3-0.2l-1.9-0.7l-5.1,3l-1.6,2.7l-1-1.2l-5.9-2.1l-1.96,2.25v0.2l0.66,2.45l3.9,0.8l4.7,1.9 l0.1,1.4l0.1,1.1l-0.2,0.9l-0.3,0.9l-0.7,1.9l-1.6,0.7l-0.3,0.8l-1.4,0.7l0.6,2l0.7,2l13.9-0.2l0.1,0.1l1.8,3.6l1.8,2.4l0.6,0.8 l0.1,0.5L631,68l0.4,5.4l0.4,1.8l5.9-0.5l0.5-0.3c0.1,0,0.1,0,0.2,0l6.3-2.8l2.9,0.4l0.7,1.3l3,1.5l4,2.9c0,0.1,0.1,0.2,0.2,0.2 l0.7,0.5l6,6.2l0.8,0.6c0.1,0,0.2,0.1,0.3,0.1l3.6,2.6l0.04-0.13l0.43-1.3l0.23-0.68l-1.8-6L663.2,73.89z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Val-de-Marne", "data-department", "94", "d", "M404.7,160l0.3,2.9l-1.1,4.1l-2.3-2.2l-2.8,0.8l-5.1-2.1l0.4-4.1l5.3-0.1l-1.8-1.9L404.7,160z M668.09,102.2 h0.06l-0.02-0.12l3.31-0.19l-1.55-3.58l-3.69-2.41l0.8-8h-0.1l-3.6-2.6c-0.1,0-0.2-0.1-0.3-0.1l-0.8-0.6l-6-6.2l-0.7-0.5 c-0.1,0-0.2-0.1-0.2-0.2l-4-2.9l-3-1.5l-0.7-1.3l-2.9-0.4l-6.3,2.8c-0.1,0-0.1,0-0.2,0l-0.5,0.3l-5.9,0.5v0.1l-0.3,0.8l0.1,3.6 l0.6-0.5l1.6-1.7l2-0.4l2-0.5l4,1.7l-0.2,3.8l-1,2.6l-8.3-1.7l-6-0.6l-5.2,3h-4l-2.5-0.3l-0.6,1.1h-0.1l-0.9,0.5l-0.5,0.3l-0.5,0.3 l1,2.5v0.1l-0.8,2.8l-0.3,1.2l1.4,1.9l-0.7,2.1l-0.4,1.3l-0.7,1.2l0.78,5.38h0.06l2.1,0.2l4.7,2.8l3.1-2.2l0.1,5.5l3.3,2.4l4.9-1.8 l0.7,2.5l5.2-2.3l0.5,1.3l1.7,1.7l4.6-3.6l2.1-0.5l5.2-1.8l1.9,6.8l1.7,2.5l3.3,1.8l5.44,1.88l-0.68-5.05l0.05-0.08l-0.01-0.04 l2.5-4.2l2.73-2.74l-1.38-3.64l0.07-0.06l-0.03-0.07l2.35-1.96L668.09,102.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Val-d\u2019Oise", "data-department", "95", "d", "m374.3,144l-9.5-0.8l4-9.5l1.6,3.2l5.6,1.1 l6.3-1.8l9.2,2.2l2.2-1.6l10.9,6.4l0.2,2l-1.7,2.3l-0.1,0.1c-1.5,1.1-3.1,2.2-4.7,3.3l-2.6-1.1l-3.3,1.3l-3.6,2.6l-5.5-6.1 L374.3,144z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Centre-Val de Loire", "data-code_insee", "24", 1, "region"], ["data-name", "Cher", "data-department", "18", "d", "m385.3,235.4l5-2.4l13.5,3.1l3.9,4.8l9-1.7l2,6.5l-1.7,5.8l2.7,2.1 l3.1,7.6l0.3,5.9l2.2,2l-0.2,5.8l-1.3,8.9h-0.1h-4l-4.8,3.7l-8.4,2.9l-2.3,1.9l1.7,5.3l-1.7,2.4l-8.7,1l-3.5,5.9v0.1l-4.9-0.2 l1.5-3.5l-0.9-8.9l-4.7-7.9l1.4-2.7l-2.3-2.2l2.5-5.1l-2.3-11.7l-11.6-1.6l2.8-5.5l2.8,0.1l0.6-2.8l9.7-2l-2.1-5.9l5.9-4.1 L385.3,235.4z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Eure-et-Loir", "data-department", "28", "d", "m333.1,200.9l-2.1-3.8l-1.1-7.5l7.5-5.1 l-0.5-4.6l0.2-4.5l-4.8-4.4l-0.1-3.2l2.4-2.6l6-1.1l5.3-3.2l2.8,1.6l6-1.3l-0.2-2.8l6-6.9l3.6,6.6l0.5,10.9l6.2,5.4l1.2,5.6l2.3,2.2 l3.1-0.7l2.8,10.6l-0.5,1.5l-4.8,10.8l-8.5,0.6l-6,2.8l0.2,2.8l-3.3-1.9l-5.5,3.5L339,201.4l-6.3,1.3L333.1,200.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Indre", "data-department", "36", "d", "m357.8,308.5l-2.8,2.9l-1.7-2.5l-5.8,1.1 l-2.6-1.1l1.5-2.8l-2.5-1.3l-2.6-5.4h-2.9l-4.6-4.4l0.8-5.8l-2.1-3l5.6-0.5l-1-2.7l3.3-11.9l5.1-2.7l2.3,1.7l2.6-3.5l2.5-2.1l-1-4.9 l6-3.2l2.5,1.3l1.5-2.6l6.4-0.9l5.2,3.5l-2.8,5.5l11.6,1.6l2.3,11.7l-2.5,5.1l2.3,2.2l-1.4,2.7l4.7,7.9l0.9,8.9l-1.5,3.5l-2.7,0.8 l-13.2-2.7l-1.9,2.5L357.8,308.5z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Indre-et-Loire", "data-department", "37", "d", "m303.9,263l-5.5-3.2v-0.1l5.8-15.3l1.7-9.3 l0.7-2.4l6.1,2.6l-0.5-3.3l2.8,0.3l7.7-4.5l10.5,0.5l-0.2,5.5l2.2-1.8l6,3.4l-0.7,2.7l3.4,5.1l-1.2,9.1l2.4,1.9l2.6-1.3l4.2,6.7 l1,4.9l-2.5,2.1l-2.6,3.5l-2.3-1.7l-5.1,2.7l-3.3,11.9l1,2.7l-5.6,0.5l-7.1-10l-0.3-3.1l-5.3-3l1.4,2.9l-10,0.4l-2.8-1.4l-1.3-6.1 l-2.9,0.3L303.9,263z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Loir-et-Cher", "data-department", "41", "d", "m357.9,256.4l-6,3.2l-4.2-6.7l-2.6,1.3 l-2.4-1.9l1.2-9.1l-3.4-5.1l0.7-2.7l-6-3.4l-2.2,1.8l0.2-5.5l-10.5-0.5l0.6-3.5l3.2-1.1l6.3-10.6l-0.4-5.5l-1.7-2.2l2-2.1v-0.1 l6.3-1.3l12.8,10.8l5.5-3.5l3.3,1.9l2.5,7.1l-1.8,3.2l1.7,5.6l3-1.3l2.4,1.5l1.1,3.8l2.9,0.6l1.9-2.3l15.2,1.6l0.8,2.6l-5,2.4 l5.1,7.6l-5.9,4.1l2.1,5.9l-9.7,2l-0.6,2.8l-2.8-0.1l-5.2-3.5l-6.4,0.9l-1.5,2.6L357.9,256.4z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Loiret", "data-department", "45", "d", "m393.3,189.4l3.7,0.6l0.7,3.1l4.2,4.3l-0.6,2.7 l-2.6,1.5l9.2,0.7l11.2-2.7l6.7,7.5l0.4,5.8l-4.6,4.9l1.1,2.9l-1.6,2.4l-5.3,3.3l3,2.8l2.2,6.9l-2.8,0.7l-1.5,2.4l-9,1.7l-3.9-4.8 l-13.5-3.1l-0.8-2.6l-15.2-1.6l-1.9,2.3l-2.9-0.6l-1.1-3.8l-2.4-1.5l-3,1.3l-1.7-5.6l1.8-3.2l-2.5-7.1l-0.2-2.8l6-2.8l8.5-0.6 l4.8-10.8l0.5-1.5l6.9-0.7l1.9-2.9l1.5,2.7L393.3,189.4z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Bourgogne-Franche-Comt\u00E9", "data-code_insee", "27", 1, "region"], ["data-name", "Cote-d\u2019Or", "data-department", "21", "d", "m523.6,241.7l3.9,8.2l-1.2,1.3l-1.8,8.2 l-6.2,6.8l-1.1,4.1v-0.1l-15,1.5l-8.8,4.2l-5.6-6.3l-5.5-1.9l-1.3-2.6l-5.7-1.7l-2.4-2.6V260l0.4-3.2l-3.7-1.2l-1.3-6h0.1l-1.3-2.7 l1.3-8.1l6.7-10.4l-1.7-2.3l2.8-2.1l0.3-3.7l-3.1-3.9l1.9-3.1l2.2-2l6.1-0.9l4.7-3.9l3.9,0.5l3.5,0.7l0.5,2.7l2.6,1l-0.3,2.9 l2.9,0.3l1.8,2.2l1,3.1l-2.8,2.4l2.3,4.8l9.2,2l3,1.6v2.8l4.8-1.9h0.1l2.7-1.6l2,3l0.1,3.2l-4.6,4.1L523.6,241.7z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Doubs", "data-department", "25", "d", "m590.1,245.2l-2.4,2.2l0.4,3l-4.8,6.2l-4.8,4 l-0.4,2.9l-2.5,2.7l-5.7,1.7l-0.3,0.3l-1.7,2.3l0.9,2.7l-0.7,4.5l0.5,2.5l-9.5,8.8l-2.9,5.2l-0.22,0.69l-3.68-3.49l3.6-7.4l2.1-2.3 l-4.2-4.1l-2.9-0.5l-5.8-10.1l-3,0.8l-1.5-2.5l-2,2.1l-1.2-2.5l3-5.1l-5.2-7.8l22.3-10.2l3-4.7l5.6-1.9l2.8,0.9l1.8-2.2l3.2-0.4 l0.5-2.8l5.9,0.8l0.2-0.1h0.1l5.9,2.7l-1.4,2.5l1.4,2.4l0.41-0.46l-0.11,0.16l-2.2,4.9l7-0.7L590.1,245.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Jura", "data-department", "39", "d", "m552.3,291.4l3.68,3.49L553.4,303l-5.3,7.2 l-5.5,3.2l-3.8,0.2l-0.4-2.8l-3.4-1.6l-4,4.4l-2.9,0.1l-0.1-3h-2.9l-4.3-7.7l2.8-1.1l-0.8-5.3l2.8-5l-2.2-8.7l-2.5-1.6l5-3.7 l-8.3-4.4l-0.4-2.9l1.1-4.1l6.2-6.8l1.8-8.2l1.2-1.3l2.3,2l5.4,0.1l5.2,7.8l-3,5.1l1.2,2.5l2-2.1l1.5,2.5l3-0.8l5.8,10.1l2.9,0.5 l4.2,4.1l-2.1,2.3L552.3,291.4z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Ni\u00E8vre", "data-department", "58", "d", "m462.8,250l5.5-0.4l1.3,6l3.7,1.2l-0.4,3.2v0.8 l-1.1,0.3l-2.7,0.4v1.3l-2.8,1l0.3,5.9l-2.1,1.7l4,7l-1.9,2.1l0.7,2.9l-11.3,5.7l-7-2.8l-5.9,6l-4.4-3.7l-2.8,1.7l-6.4-0.2l-5.7-6.3 l1.3-8.9l0.2-5.8l-2.2-2l-0.3-5.9l-3.1-7.6l-2.7-2.1l1.7-5.8l-2-6.5l1.5-2.4l2.8-0.7v0.1h3.4l7.4,4.8h6l4.6-4.3l3.9,5.6l5.5,3 l5.8-0.9l0.9,3.7l2.8-0.9L462.8,250z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Haute-Saone", "data-department", "70", "d", "m579.1,225.9l1.4,5.5l-0.2,0.1l-5.9-0.8 l-0.5,2.8l-3.2,0.4l-1.8,2.2l-2.8-0.9l-5.6,1.9l-3,4.7L535.2,252l-5.4-0.1l-2.3-2l-3.9-8.2l-2.6-1.4l4.6-4.1l-0.1-3.2l-2-3l-2.7,1.6 h-0.1l1.2-2.5l6.6-3.9l2.1,1.8l3.2-1l0.3-8.3l2-2.4l2.9,0.3l2.3-3.2l-0.2-1.4l8-5.8l7,4.3l5.8-1.6l4.9,3.6l5.1-2.2l8.4,6.6l-2.3,5.7 L579.1,225.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Saone-et-Loire", "data-department", "71", "d", "m517.2,270.2v0.1l0.4,2.9l8.3,4.4l-5,3.7 l2.5,1.6l2.2,8.7l-2.8,5l0.8,5.3l-2.8,1.1l-4.8-3.3l-5.4,1.3l-5.9-1.5l-5.9,20.9l-5.7-7.7l-1.6,2.3l-2.5-1.5l-2.2,1.6l-2.2-1.7 l-2.3,1.9l-0.29,2.91L482,318.2v0.1l-5.7,3.8l-2.1-2.1l-8,1.5l-5.2-3.3v-3l3.7-4.6l0.5-5.5l-1.6-2.4l-7.9-2.9l-6.7-13.5l7,2.8 l11.3-5.7l-0.7-2.9l1.9-2.1l-4-7l2.1-1.7l-0.3-5.9l2.8-1l2.7-1.7l1.1-0.3l2.4,2.6l5.7,1.7l1.3,2.6l5.5,1.9l5.6,6.3l8.8-4.2 L517.2,270.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Yonne", "data-department", "89", "d", "m425.8,207.1l-6.7-7.5l3.9-5.4l0.2-5.8l15.4-3 l3.6,1.5l4.5,5.5l2.5,8.3l2-2.2l3.6,4.1l5,10.9l12.6-1.6l2.9,1.4l-1.9,3.1l3.1,3.9l-0.3,3.7l-2.8,2.1l1.7,2.3l-6.7,10.4l-1.3,8.1 l1.3,2.7h-0.1l-5.5,0.4l-1.5-2.8l-2.8,0.9l-0.9-3.7l-5.8,0.9l-5.5-3l-3.9-5.6l-4.6,4.3h-6l-7.4-4.8H421v-0.1l-2.2-6.9l-3-2.8 l5.3-3.3l1.6-2.4l-1.1-2.9l4.6-4.9L425.8,207.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Territoire de Belfort", "data-department", "90", "d", "m580.3,215.9l0.9-0.6l7.6,5l0.5,9l2.8-0.2l2,5 l-0.1,0.1l-2.79,0.39l-1.11-0.39l-3.19,4.34L586.5,239l-1.4-2.4l1.4-2.5l-5.9-2.7h-0.1l-1.4-5.5l-1.1-4.3L580.3,215.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Normandie", "data-code_insee", "28", 1, "region"], ["data-name", "Calvados", "data-department", "14", "d", "m316.9,148l-0.7,2.2l-5.6-1l-7,1.7l-7.2,5.4 l-2.9,0.3l-5.7-1.1l-2.6,1.7l-4.9-3l-6.4,2.3l-2.7-1.3l-0.9,2.7l-5.4,2.9l-9.7-2.1l-1.8-2.4l4.5-5.3l-1.6-2.3l8.1-4.9l-2.2-8.2 l2-2.6l-8.4-3.1l-0.5-6.6v-0.1l0.1-0.7l1.8,0.8l1.9-2.1l3.4-0.3l9.4,3.3l13.9,1.5l6.9,3.4l5.7-0.7l4.7-2.5l4.1-3.7l5.1-1.1l0.3,8.3 h2.9l-2.3,2.1l2.8,9.4l-1.4,3L316.9,148z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Eure", "data-department", "27", "d", "m316.4,153.4l-0.2-3.2l0.7-2.2l-2.3-4.1l1.4-3l-2.8-9.4l2.3-2.1h-2.9 l-0.3-8.3l1.7-0.4l0.28-0.1h1.52l-0.9-0.2l0.8-0.3l-1.29-0.3l5.89-2.4l7.6,5l3.4-0.7l4.9,3l-1.9,2.4l2.1,2.1l5.4,2.4l1.4-2.7 l8.2-2.5l4.8-7l13.1,3.3l3.5,8.4l-4,2.6l-4,9.5l-3.8-0.1l-2.3,2.6l1.8,5.8l-6,6.9l0.2,2.8l-6,1.3l-2.8-1.6l-5.3,3.2l-6,1.1l-2.4,2.6 l-3.4-2.1l1.7-2.3l-7.8-9.5L316.4,153.4z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Manche", "data-department", "50", "d", "m255.2,158.7l9.7,2.1l4.1,4.2l-1.8,6.7 l-3.6,4.5h-0.1l-8.6-0.8l-5.4-2.3l-7.1,4.8l-2.7-1l-4.7-9.6l1.9-0.2l4.8,0.4l2.5-1.1l0.5-2.2l-2.4,1.3l-5.1-5.6l-0.3-5.3l2-6.1 l-0.3-4.9l-1.8-3.6l0.4-7.4l1.5-2l-2.5,0.3l-2-5l0.3-2.2l-2.4-1.2l-2.9-4.1l-0.7-5.9l-1.4-1.9l1.8-1.8l0.1-2.8l-0.5-2.3l-2.2-1.1 l-1-2.5l2.1-0.2l11.9,4.2h2.4l4-2.6l5.1,0.6l1.8,1.7l0.9,2.7l-3.2,5.2l4,6.5l1.1,4.3l-0.1,0.7v0.1l0.5,6.6l8.4,3.1l-2,2.6l2.2,8.2 l-8.1,4.9l1.6,2.3l-4.5,5.3L255.2,158.7z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Orne", "data-department", "61", "d", "m266.9,179.9l-3.3-3.7l3.6-4.5l1.8-6.7 l-4.1-4.2l5.4-2.9l0.9-2.7l2.7,1.3l6.4-2.3l4.9,3l2.6-1.7l5.7,1.1l2.9-0.3l7.2-5.4l7-1.7l5.6,1l0.2,3.2l6.3,0.5l7.8,9.5l-1.7,2.3 l3.4,2.1l0.1,3.2l4.8,4.4l-0.2,4.5l0.5,4.6l-7.5,5.1l1.1,7.5l-3.2-0.7l-3.1-3.5l-2.9,1l-7.2-5l-1.6-8.4l-2.8-1.5l-11,5.9l-3-0.1 v-0.1v-2.9l-3.3-1.6l-1.9-6l-2.7-0.2l-0.7,2.7h-9.1l-6.7,3.3l-2.5-1.7L266.9,179.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Seine-Maritime", "data-department", "76", "d", "m314.41,119.8l-7.61-1.8l-1.2-2l-0.1-2.3 l4.4-9.7l13.8-7.4L326,95l10.3-2.1l4.8-1.8l2.4,0.3L352,87l5.11-4.09l11.79,9.99l3.4,8.4l-3.1,4.7l1.4,8.7l-1.3,8l-13.1-3.3l-4.8,7 l-8.2,2.5l-1.4,2.7l-5.4-2.4l-2.1-2.1l1.9-2.4l-4.9-3l-3.4,0.7l-7.6-5L314.41,119.8z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Hauts-de-France", "data-code_insee", "32", 1, "region"], ["data-name", "Aisne", "data-department", "02", "d", "m450.3,82.6l16.7,4.6l2.91,0.94L470.6,94l-1.3,3.5l1.3,3.1l-5,7.2 l-2.7,0.3l0.3,14.3l-1,2.8l-5.3-1.8l-8,4l-1.2,2.6l3.2,8l-5.5,2.3l1.6,2.4l-0.8,2.7l2.5,1.3l-7.7,10.2l-9.3-6l-3.9-4.2l0.7-2.8 l-1.8-2.5l-2.6-0.7l2.1-1.7l-0.5-2.8l-2.9-1.1l-2.4,1.5l-0.7-2.9l3,0.2l-2.9-4.5l2.6-1.7l2.4-5.7l2.6-1.1l-2.2-1.8l0.8-4.5 l-0.4-10.2l-2.3-7l3.9-8.1l0.4-3.8l12.6-0.6l2.6-2.2l2.3,1.7L450.3,82.6z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Nord", "data-department", "59", "d", "m384.33,25.06l0.87-0.26l2,0.8l1.1-2.1l7.9-2.1 l2.9,0.3l4.4-1.9v-0.1l1.2,4.8l2.3,3.7l-1.6,1.9l0.6,0.8l1.2,5.8h3.4l2.7,5.1l3.1,1.5h2.1l0.6-2.4l8.1-3l3.8,7.5l0.1,1l1.3,5.2 l2,3.5h0.1l2.8,0.6l2.1-1.4l2.4-0.2l-0.5,2.2l2.2-0.7l2.8,1l1.8,4.4l-0.6,2.3l0.7,2.3l1.4,1.9l1.1-2.6l4.6-0.3l2.4,1.1L462,64l5.5,6 l2.3,0.2l-2.1,2.4l-1.4,4.7l2.6,0.2l1.4,3.3l-3.5,3.9l0.2,2.5l-16.7-4.6l-5.2,1.8l-2.3-1.7l-2.6,2.2l-12.6,0.6l-3.3-2.6l3.5-10.6 l-1.8-2.4l-3-0.4l0.7-2.7l-3.9-5.2l3.1-1.6l-3.8-5.3l-5.9-1l1-6.1l-1.3-2.5l-1.7,2.2l-11.6-0.5l-4.1-4.2l0.6-2.8l-5.5-2.6 L384.33,25.06z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Oise", "data-department", "60", "d", "m372.8,131.1l-3.5-8.4l1.3-8l-1.4-8.7l3.1-4.7 l4.1,3.7l3.1-1.2l14.4,2.2l12.8,6.7l8.6-6.8l10.3-1.5l0.4,10.2l-0.8,4.5l2.2,1.8l-2.6,1.1l-2.4,5.7l-2.6,1.7l2.9,4.5l-3-0.2l0.7,2.9 l2.4-1.5l2.9,1.1l0.5,2.8l-2.1,1.7l-8.1,2.9l-2.5-1.6l-2,2.2l-6.9-1l-10.9-6.4l-2.2,1.6l-9.2-2.2L376,138l-5.6-1.1l-1.6-3.2 L372.8,131.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Pas-de-Calais", "data-department", "62", "d", "m379.8,68.9l7.1,5.8l12-2.5l-2.6,5.7L398,81 l2.5-3.1l8.4,3.5l0.8-2.8l2.8,4.6l2.4-1.7l0.8,3.2l8.6-1.8l3.5-10.6l-1.8-2.4l-3-0.4l0.7-2.7l-3.9-5.2l3.1-1.6l-3.8-5.3l-5.9-1 l1-6.1l-1.3-2.5l-1.7,2.2l-11.6-0.5l-4.1-4.2l0.6-2.8l-5.5-2.6l-6.27-12.14L372.6,28.5l-6.4,5.4l0.9,5.6l-1.7,4.6l0.6,6.7l2,4.2 l-1.7-1.4l-0.3,9.7l2.27,1.58l10.53,1.02L379.8,68.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Somme", "data-department", "80", "d", "m424.3,82.9l3.3,2.6l-0.4,3.8l-3.9,8.1l2.3,7 l-10.3,1.5l-8.6,6.8l-12.8-6.7l-14.4-2.2l-3.1,1.2l-4.1-3.7l-3.4-8.4l-11.79-9.99L359.5,81l3.4-6.6l1.9-1.1l0.1-0.1l1.4,1.8l3.5,0.3 l-5.6-6l1.2-5.1l2.9,0.7l-0.03-0.02l10.53,1.02l1,3l7.1,5.8l12-2.5l-2.6,5.7L398,81l2.5-3.1l8.4,3.5l0.8-2.8l2.8,4.6l2.4-1.7 l0.8,3.2L424.3,82.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Grand Est", "data-code_insee", "44", 1, "region"], ["data-name", "Ardennes", "data-department", "08", "d", "m469.91,88.14l0.79,0.26l9.8,0.4l7.3-3.2l1.1-6 l4-3.8l2.8-0.2v3.8L494,81l-0.6,5.2l3.3,4.5l-1,2.4l0.6,3.1l1.4,1.9l3.3-0.9l4.3,2.4l2.8,3.8l4.9,0.6l2,1.7l-0.9,2.4l2.1-0.13 l-1.6,1.13l-2,2.7l-5.7-2.1l-1.9,2l0.8,8.8l-3.2,5.1l1.4,2.5l-4.2,3.6v0.1l-20.1-1.9l-9.8-6.6l-6.7-0.9l-0.3-14.3l2.7-0.3l5-7.2 l-1.3-3.1l1.3-3.5L469.91,88.14z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Aube", "data-department", "10", "d", "m442.2,186.9l-3.6-1.5l-0.4-8.5l2.9-0.8l3-5 l3.2,4.5l9,1.2v-3.3l9.5-7.6l6.5-0.9l3.1,0.5l0.4,6.1l2.6,2c1.9,0.8,3.8,1.5,5.6,2.3l2.5-1.5l3.3,1.1l-0.6,3.4l2.4,5.2l5.6,3 l0.5,9.9l-0.1,2.7l-5.6,2.5l0.2,4.8l-3.9-0.5l-4.7,3.9l-6.1,0.9l-2.2,2l-2.9-1.4l-12.6,1.6l-5-10.9l-3.6-4.1l-2,2.2l-2.5-8.3 L442.2,186.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Marne", "data-department", "51", "d", "m440.6,158.9l0.4-2l7.7-10.2l-2.5-1.3l0.8-2.7 l-1.6-2.4l5.5-2.3l-3.2-8l1.2-2.6l8-4l5.3,1.8l1-2.8l6.7,0.9l9.8,6.6l20.1,1.9l2.2,9l-1,4.1l2.6,1.3l-0.6,3.9l-3.1,1.1l-1.1,5.8 l3.2,4.6l0.5,4.1l-8.6,2.2l2.2,2.5l-2.3,2.2l0.7,2.9h-4.7l-3.3-1.1l-2.5,1.5c-1.8-0.8-3.7-1.5-5.6-2.3l-2.6-2l-0.4-6.1l-3.1-0.5 l-6.5,0.9l-9.5,7.6v3.3l-9-1.2l-3.2-4.5l-2.6-1.7l-3.5-8.3L440.6,158.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Haute-Marne", "data-department", "52", "d", "m493.9,167.9l8.6-2.2l3.4,5.2l16.9,10.4 l-2.4,2.3l12.7,9.5l-1.7,8.6l5.5,4.7l0.2,3.1l2.7-1.1l1.3,2.5v0.1l0.2,1.4l-2.3,3.2l-2.9-0.3l-2,2.4l-0.3,8.3l-3.2,1l-2.1-1.8 l-6.6,3.9l-1.2,2.5l-4.8,1.9v-2.8l-3-1.6l-9.2-2l-2.3-4.8l2.8-2.4l-1-3.1l-1.8-2.2l-2.9-0.3l0.3-2.9l-2.6-1l-0.5-2.7l-3.5-0.7 l-0.2-4.8l5.6-2.5l0.1-2.7l-0.5-9.9l-5.6-3l-2.4-5.2l0.6-3.4h4.7l-0.7-2.9l2.3-2.2L493.9,167.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Meurthe-et-Moselle", "data-department", "54", "d", "m588.2,170.9l1.9,1.3l-1.5,0.4l-10.6,7.6l-6.1-1.6l-1.6-2.7l-5.3,3.8 l-6,1l-2.4-1.8l-5.4,2l-1.1,2.8l-5.7,0.7l-4.1-4.8l0.1-2.9l-5.8-0.6l0.2-2.9l-2.5-2l1.7-2.8l-1.3-8.6l2.2-13.8l0.9-2.7l-4.9-11.5 l1.5-5.9l-1.2-2.7l-4.4-4.8l-5.3,2l-0.7-5.3l4.8-1.7l2-1.9h6.8l2.54,2.31L539.6,124l2.5,1.6l1.2,3.6l-1.7,3.1l1,5.6l-2.8,0.1 l4.3,7.5l11.5,4l-0.3,2.9l2.7,5.1l8.5,1.5l5.3,3.9l14.4,5.3L588.2,170.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Meuse", "data-department", "55", "d", "m516.2,107.97l1.2-0.07l1.5,1.6l1.9,5.6 l0.7,5.3l5.3-2l4.4,4.8l1.2,2.7l-1.5,5.9l4.9,11.5l-0.9,2.7l-2.2,13.8l1.3,8.6l-1.7,2.8l2.5,2l-0.2,2.9l-1.9,2.3l-3-0.5l-6.9,3.4 l-16.9-10.4l-3.4-5.2l-0.5-4.1l-3.2-4.6l1.1-5.8l3.1-1.1l0.6-3.9l-2.6-1.3l1-4.1l-2.2-9v-0.1l4.2-3.6l-1.4-2.5l3.2-5.1l-0.8-8.8 l1.9-2l5.7,2.1l2-2.7L516.2,107.97z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Moselle", "data-department", "57", "d", "m539.6,124l-2.65-10.19l0.65,0.59h2.4l1.5,2.1 l2.3,0.7l2.3-0.5l1-2.3l2-1.2l2.2-0.2l4.5,2.3l4.9-0.1l3.1,3.8l2.3,1.9l-0.5,2l3.7,3.2l2.8,4.5v2.3l4.2,0.7l1.2-1.9l-0.3-2.4 l2.6-0.2l3.8,1.8l1.4,3.5l2.1-1.5l2.5,1.9l5.8-0.4l5.3-4.2l2.2,1.4l0.5,2.1l2.4,2.4l3.2,1.5h0.03l-1.73,4.4l-1.4,2.6l-8.9,0.3 l-9.1-4.6l-0.8-2.8l-5,10.8l5.5,2.4l-1.6,2.5l2.3,1.7l1.3-2.5l3,0.3l4.3,3.4l-3,13.3l-2.3,1.8l-3.4-0.3l-2-2.7l-14.4-5.3l-5.3-3.9 l-8.5-1.5l-2.7-5.1l0.3-2.9l-11.5-4l-4.3-7.5l2.8-0.1l-1-5.6l1.7-3.1l-1.2-3.6L539.6,124z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Bas-Rhin", "data-department", "67", "d", "m631.8,140.7l-2.8,9.4l-7.8,10.5l-2,1.5l-1.4,3.3l0.3,4.9l-2.4,7.2 l0.7,3.6l-1.5,2l-1.2,5.5l-3.16,6.23L605.9,193l-0.3-2.8l-8.5-5.6l-3.1-0.2l-5.2-2.2l1.3-10l-1.9-1.3l3.4,0.3l2.3-1.8l3-13.3 l-4.3-3.4l-3-0.3l-1.3,2.5l-2.3-1.7l1.6-2.5l-5.5-2.4l5-10.8l0.8,2.8l9.1,4.6l8.9-0.3l1.4-2.6l1.73-4.4l8.87,0.6l2.4-0.6 L631.8,140.7z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Haut-Rhin", "data-department", "68", "d", "m605.9,193l4.64,1.83l-0.04,0.07v5.3l1.6,1.9 l0.2,3.4l-2.2,11.1l0.1,6.7l1.8,1.5l0.6,3.5l-2.2,2l-0.2,2.3l-3.1,0.9l0.5,2.2l-1.5,1.6h-2.7l-3.8,1.4l-3-1.1l0.3-2.5l-2.4-1.1 l-0.4,0.1l-2-5l-2.8,0.2l-0.5-9l-7.6-5l2.8-2.4v-6.2l4.8-7.8l4.1-13.5l1.1-1l3.1,0.2l8.5,5.6L605.9,193z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Vosges", "data-department", "88", "d", "m520.4,183.6l2.4-2.3l6.9-3.4l3,0.5l1.9-2.3 l5.8,0.6l-0.1,2.9l4.1,4.8l5.7-0.7l1.1-2.8l5.4-2l2.4,1.8l6-1l5.3-3.8l1.6,2.7l6.1,1.6l10.6-7.6l1.5-0.4l-1.3,10l5.2,2.2l-1.1,1 l-4.1,13.5l-4.8,7.8v6.2l-2.8,2.4l-0.9,0.6l-8.4-6.6l-5.1,2.2l-4.9-3.6l-5.8,1.6l-7-4.3l-8,5.8v-0.1l-1.3-2.5l-2.7,1.1l-0.2-3.1 l-5.5-4.7l1.7-8.6L520.4,183.6z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Pays de la Loire", "data-code_insee", "52", 1, "region"], ["data-name", "Loire-Atlantique", "data-department", "44", "d", "m213.1,265.2l1.8-1l-2.8-4.1l-7.8-3l3-1.3 l0.6-2.2l-0.5-2.5l1.4-2.1l5.8-1.1l-5.5-0.7l-6.6,3.7l-4.1-3.2l-2.2,1l-2.2-1.2l-0.5-4.9l0.9-2.5l3-0.5l-0.9-2.2l-0.18-0.31 l13.18-3.89l0.4-6l5.2-3.4l13.2-0.4l1.6-2.9l9-3.9l6.8,3.6l7.2,13.3l-2.7-0.4l-1.9,2.4l8.5,3.3l0.3,5.9l-14.3,2.1l-2.9,2.2l3,0.8 l3.6,4.7l0.8,2.8l-2.8,4.5l2.8,1.4l0.4,3l-4.8-3.5l-1.5,2.4l-3.2,0.7l0.5,3l-2.4,2.1l-2.3-1.7v-3.1l-3.4,0.2l-0.2,9.5l-11.7-5 L213.1,265.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Maine-et-Loire", "data-department", "49", "d", "m270.6,269.2l-12.3,0.8l-10.6-3.8l-0.4-3 l-2.8-1.4l2.8-4.5l-0.8-2.8l-3.6-4.7l-3-0.8l2.9-2.2l14.3-2.1l-0.3-5.9l-8.5-3.3l1.9-2.4l2.7,0.4l-7.2-13.3l0.4-2.2l10.5,3.5 l2.1-1.9l8.7,3.6l3,0.4l5.9-2.7l5.1,1.7l0.6,2.7l6.7-0.2l0.2,3.5l2,2l3.1-1.3l5.2,3.3l7.4,0.1l-0.7,2.4l-1.7,9.3l-5.8,15.3v0.1 l-6.6,5.9l-2.3-2.3l-9.6,0.2l-5.6,0.8L270.6,269.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Mayenne", "data-department", "53", "d", "m256.6,221.5l-10.5-3.5l3.6-8.6l5.5-2.2 l-1.9-17.3l1.5-2.4l0.1-12.1l8.6,0.8h0.1l3.3,3.7l2.4-1.6l2.5,1.7l6.7-3.3h9.1l0.7-2.7l2.7,0.2l1.9,6l3.3,1.6v2.9v0.1l-4.3,2.7 l0.3,6.9l-4.4,4l1.2,2.9l-5,4.6l1.4,3.4l-5.5,7.7l1.5,5.6l-5.1-1.7l-5.9,2.7l-3-0.4l-8.7-3.6L256.6,221.5z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Sarthe", "data-department", "72", "d", "m312.7,235.3l-6.1-2.6l-7.4-0.1l-5.2-3.3 l-3.1,1.3l-2-2l-0.2-3.5l-6.7,0.2l-0.6-2.7l-1.5-5.6l5.5-7.7l-1.4-3.4l5-4.6l-1.2-2.9l4.4-4l-0.3-6.9l4.3-2.7l3,0.1l11-5.9l2.8,1.5 l1.6,8.4l7.2,5l2.9-1l3.1,3.5l3.2,0.7l2.1,3.8l-0.4,1.8v0.1l-2,2.1l1.7,2.2l0.4,5.5l-6.3,10.6l-3.2,1.1l-0.6,3.5l-7.7,4.5l-2.8-0.3 L312.7,235.3z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Vend\u00E9e", "data-department", "85", "d", "m269.3,305.1l0.2-7.4l-4.7-17.9l-4.2-4.1l-2.3-5.7l-10.6-3.8l-4.8-3.5l-1.5,2.4l-3.2,0.7 l0.5,3l-2.4,2.1l-2.3-1.7v-3.1l-3.4,0.2l-0.2,9.5l-11.7-5l-5.6-5.6l-0.3,0.1l-0.8,2.6l-3.4,4.3l-1.2,2.3l0.2,2.4l8.7,9.5l2.7,5.6 l1.2,5.3l8,5.4l3.4,0.5l3.9,4.3l2.9-0.1l2,1.2l1.8,2.5l-0.9-2.1l3.9,3.3l0.5-2.7l2.4,0.3l7.1-2.7l-1.4,2.9l6.5-0.3l2.4,1.8l9.1-4.5 L269.3,305.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Bretagne", "data-code_insee", "53", 1, "region", "region-53"], ["data-name", "Cotes-d\u2019Armor", "data-department", "22", "d", "m208.7,188.9l-4.9,7.1l-2.9,1.1l-1.5-2.7 l-3.5-0.9l-6.2,7.5l-1.8-6l-3,0.9l-12.9-6.5l-7.9,3l-12.46-3.29l2.06-4.11l-2.5-9.3l2.5-8.3l-3.6-4.7l1.1-4.3l1.2,1.4l3.2-0.4 l1.1-7.7l1.5-1.6l2.2-0.6l1.9,1.4h2.5l2.1-1l2.2,0.3l1.5-1.8l0.9,2L170,153l3-3.6l2.9-0.8l-0.1,2.3l-1.2,4.4l1.7-3.1l2.6-0.5l-1.1,2 l7.2,7.8l2.2,5.4l3,2l0.8,3.7l0.7-2.2l3-1l2.4-2.7l8.1-3.3l2.7-0.2l-2,2.5l2.9-1.1l1.8,4.4l1.3-1.9l2.5,0.2v-0.09l1.6,3.99h-0.3h0.3 l2.5,0.3l0.7,0.2l0.4,1.7l-1.9,13L208.7,188.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Finist\u00E8re", "data-department", "29", "d", "m151.6,210.1l2,3.4l-0.8,1.4l-5.5-1.2l-1.2-1.9 l2.2-0.7l-3,0.8l-0.3-2.7v2.7l-2.5,0.7l-2.2-1l-4.2-6.1l-0.8,2.5l-2.3,0.2l-3.5-3.1l1.6-4.6l-2.4,4.3l1.3,1.9l-2.2,1l-1,2.8 l-5.9-0.2l-2.1-1.6l1.5-1.6l-1.5-5.5l-2.4-3.1l-2.8-1.8l1.6-1.7l-2.1,1.4l-7.5-2.2l2.2-1.3l12.5-1.8l1.8,1.8l2-1.3l0.7-2.5l-1.6-3.6 l-6.8-2.5l-1.5,2.6l-2.6-4.2l1.3-1.8l-0.3-2.2l1.7,2.3l4.9,1l4.6-0.8l2.1,3.1l5.4,1l-3.7-0.9l-2.8-2l2.2-0.5l-4.2-2l2-1.5l-2.6-0.2 l-2.7,0.8l-0.8-2.2l7.1-4.5l-4.4,2.2l-2.3,0.1l-7.5,2.9l-2.7-1.2l-2.7,1.2l-1.5-1.8l0.6-5.3l2.5-1.6l-2.2-0.9l0.8-2.6l1.8-1.6 l2.1-0.8l5.1,1.5l-1.9-1.1l2.5-1.2l1.6,1.4l-1.9-1.7l1.2-1.9l2.9-0.1l3.8-2l2.3,2.6l6.7-3.1l3,1.6l1-2.2l2.9-0.5l0.4,5l2.2-1.5 l1.3,2.5l1.2-4.5l4.7,0.3l1.2,1.7l-1.1,4.3l3.6,4.7l-2.5,8.3l2.5,9.3l-2.06,4.11l-0.04-0.01v0.1l-6.8,3.2l0.5,3.5l3.4,5.5l8.1,1.3 l0.1,5.4l-2.5,2.8L151.6,210.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Ille-et-Vilaine", "data-department", "35", "d", "m255.2,207.2l-5.5,2.2l-3.6,8.6l-0.4,2.2 l-6.8-3.6l-9,3.9l-1.6,2.9l-13.2,0.4l-5.2,3.4l-1-5.8l3-0.7l-2.8-1.5l2.4-2.2l1-3.2l-2.4-1.7l1.6-2.6l-1.2-2.5l-5.1-2.8l-0.5-2.8 l3.5-0.9l-3.6-0.1l-1-4.4l4.9-7.1l9-2.5l1.9-13l-0.4-1.7l-0.7-0.2l-2.5-0.3l-1.6-3.99l0.05-0.86l0.05-0.85l0.7-0.1h2.1v0.1l1.7,4.4 l1.3,2l-0.5,2.1l1.4-2.1l-2.3-5.1l0.7-2.5l2.2-1.5l2.3-0.6l2.2,1l-1.5,2.3l2.9,2.4l7.3-0.6l4.7,9.6l2.7,1l7.1-4.8l5.4,2.3l-0.1,12.1 l-1.5,2.4L255.2,207.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Morbihan", "data-department", "56", "d", "M167.7,242.6l2.9,1.2l-1.1,2.1l-5.1-1.2l-1.3-2.7l0.4-3l2.1,1.4L167.7,242.6z M209.1,219.2l2.4-2.2l1-3.2 l-2.4-1.7l1.6-2.6l-1.2-2.5l-5.1-2.8l-0.5-2.8l3.5-0.9l-3.6-0.1l-1-4.4l-2.9,1.1l-1.5-2.7l-3.5-0.9l-6.2,7.5l-1.8-6l-3,0.9 l-12.9-6.5l-7.9,3l-12.46-3.29l-0.04,0.09l-6.8,3.2l0.5,3.5l3.4,5.5l8.1,1.3l0.1,5.4l-2.5,2.8l-2.8-0.8l2,3.4l0.1,1.5l2.9,4.4 l2.3-0.2l1.5-1.7l-0.8-5.1l0.6,2.4l1.7,1.7l1.9-1.7l-2.5,4.2l2.2,1.4l-2.3-0.6l3.2,1.9l0.1,0.1l1.6,1l1.7-2.5l-1.6,3.1l2.1,2.6 l0.6,3.5l-0.9,2.8l2.1,1.1l-1.2-3l0.5-3.8l2.2,1.6l5.1,0.1l-0.7-5l1.4,2l2.1,1.5l4.8-0.5l2.1,2.4l-1,2.2l-2.1-0.6l-4.8,0.4l3.8,3.3 l12.9-0.9l3.1,1.5l-3.4,0.1l1.42,2.39l13.18-3.89l0.4-6l-1-5.8l3-0.7L209.1,219.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Nouvelle-Aquitaine", "data-code_insee", "75", 1, "region"], ["data-name", "Charente", "data-department", "16", "d", "m294.8,379.2l-2,2v-0.1l-6.3-6.3l-6-1.2l1.7-3l-2.3-2l2.4-1.7l-1.5-2.6 l1.7-2.6l-2.4-1.7l-0.3-3l-5-3.1l2.2-2.1l-3.2-5.6l8.1-3.3l2.3,2l2.7-0.1l2.7-11.6l2.7-1.6l0.3-3l5.8-2.5l3.5,0.4l0.8-0.8h0.1l9.1,3 l2.9-0.8l-1.4-2.4l2.2-1.8l4.1,3.9l3.8-1.4l1.3-2.5l4.8,0.6l-0.2,5.1l4.7,3.6l-0.6,3.2l-2.6,1.1l-4,8l-2.8,0.6l-3.4,3.8h0.1 l-5.7,6.1l-2.1,5.3l-7.9,5.9l-0.7,5.7l-4.1,5.8L294.8,379.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Charente-Maritime", "data-department", "17", "d", "M242.8,341.1l-1.4-5l-3.5-3l-1.3-2.3l1.5-3.6l1.7,1.8l2.9,0.5l1.4,8.4L242.8,341.1z M241.9,318.9l-5.8-4.5 l-4.4-1.5l-0.6,2.9l2.7,0.1l4.8,3.3L241.9,318.9z M286.5,374.8l-6-1.2l1.7-3l-2.3-2l2.4-1.7l-1.5-2.6l1.7-2.6l-2.4-1.7l-0.3-3 l-5-3.1l2.2-2.1l-3.2-5.6l8.1-3.3l2.3,2l2.7-0.1l2.7-11.6l-3.6-4.7l-17.4-6.7l-5.9-6.5v-3.7l-2.4-1.8l-6.5,0.3l1.4-2.9l-7.1,2.7 l0.5,0.1l-0.6,3.4l-4.5,5.9l2.4,0.3l2.2,1.7l3,7.2l-1.5,1.9l-0.2,5.1l-3.3,3.1l-0.1,2.6l-2.2,0.4l-1.5,1.7l1.1,4.3l9,6.5l1.5,2.6 l4.3,2.7l3.7,4.8l1.81,7.3l3.79-0.5l0.7,2.8l6.4,1.7l0.6,5.8l6.1,4.3l9.4,1l2-5l0.1-0.4v-0.1L286.5,374.8z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Corr\u00E8ze", "data-department", "19", "d", "m363.6,392.3l-8.1,0.8l-3.5-7l-3.2-0.7l-0.2-3 l-2.3-1.5l2-1.8l-1.7-3l3.6-4.6l-2.9-4.7l1.6-2.7l2.5,1.2l4.7-4l5.7-1.3l4.9-4.6l8.7-4l7-3.4l11.2,5.2l2.3-2.6l2.7,0.8l2.4-2.4 l1.2,5.6l-1.7,2.4l1.2,7.9l0.7,6l-6.2-2l-0.6,3.5l-7.6,9.5l1.8,2.2l-2.3,1.9l-0.3,3.5l-3.1,1.1l1.5,3.4l-3.2,1.9h-0.1l-6.7-0.2 l-5.3,2.7L363.6,392.3z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Creuse", "data-department", "23", "d", "m396.6,343.5l4.4,5.5l-2.4,2.4l-2.7-0.8 l-2.3,2.6l-11.2-5.2l-7,3.4l-0.6-5.9l-4.7-3l-6.4-0.5l-0.1-2.8l-2.9-1.5l0.9-3.4l-1.8-5.2l-6.6-9.8l3-5.3l-1.2-2.6l2.8-2.9l11.5-1.1 l1.9-2.5l13.2,2.7l2.7-0.8l4.9,0.2l1.1,3.9c2.5,1.6,4.9,3.2,7.4,4.8l3.6,8.4l-0.5,4.1l2.3,6.7L396.6,343.5z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Dordogne", "data-department", "24", "d", "m307.7,414.3l-2.8-6.4l-1-1.3l0.9-2.9l-2.4-2.6l-2,3.2l-9.8-2.3l2-2 l0.2-5.7l2.8-5.5l-1.2-2.8l-3.7,0.6l2-5l0.1-0.4l2-2l5.5-0.7l4.1-5.8l0.7-5.7l7.9-5.9l2.1-5.3l5.7-6.1l6.2,3l-0.1,4.7l9.5-1.1 l7.2,5.6l-2,2.7l5.7,2.2l2.9,4.7l-3.6,4.6l1.7,3l-2,1.8l2.3,1.5l0.2,3l3.2,0.7l3.5,7l-0.7,5l-1.4,5.3l-4.5,3.2l0.6,3.6l-6,3.4 l-4.7,6.5l-4.2-4.2l-5.4,2.7l-1.5-6l-6.1,1l-2.2-1.8l-2.8,2L307.7,414.3z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Gironde", "data-department", "33", "d", "m243.9,420.1l-5.8,2.6v-4.6l2.2-3.2l0.5-2.3 l1.9-1.7l1.8,1.4l3.1-0.2l-1.1-4.6l-3.5-3.4l-2.8,4l-1.2,3.8l6.2-50l0.9-2.8l3.3-3.4l1.4,4.7l9,9l2.8,7.6l1.7-3.1l-0.59-2.4 l3.79-0.5l0.7,2.8l6.4,1.7l0.6,5.8l6.1,4.3l9.4,1l3.7-0.6l1.2,2.8l-2.8,5.5l-0.2,5.7l-2,2l9.8,2.3l2-3.2l2.4,2.6l-0.9,2.9l1,1.3 l-3.1-0.1l-1.2,2.5l-2.7-0.9l-1.1,3.3l2.9,1.4l-8.5,8.6l-0.6,8.9l-3,2.3l1.5,2.5l-4.5,4l-2.1-2.7l-1.6,3.6h-6.4l-0.6-4.7l-11-7.7 l0.4-2.8l-17.2,0.7l1.5-5.4L243.9,420.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Landes", "data-department", "40", "d", "m222.32,481.21l1.08-1.51l3.9-7.1l8.8-37.8 l2-11.7v-0.4l5.8-2.6l3.7,1.3l-1.5,5.4l17.2-0.7l-0.4,2.8l11,7.7l0.6,4.7h6.4l1.6-3.6l2.1,2.7l0.4,4.6l11.7,2.9l-3.6,5.2l0.7,2.6 l-0.4,2.9l-2.5,1.3l-0.6-3l-9.4,2.7l0.5,6.4l-4.2,11.1l1.6,2.7l-8.6,1.5l-3.3-1.1l-4.8,1.9l-2.2-2l-2.3,1.5l-2.5-2.3l-9.8,2 l-1.6,2.2l-2.5-1.4l-2.7,1.3l-1.2-2.8l-11,2.5L222.32,481.21z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Lot-et-Garonne", "data-department", "47", "d", "m293.8,455.6v0.1l-0.7-2.6l3.6-5.2L285,445 l-0.4-4.6l4.5-4l-1.5-2.5l3-2.3l0.6-8.9l8.5-8.6l-2.9-1.4l1.1-3.3l2.7,0.9l1.2-2.5l3.1,0.1l2.8,6.4l8.9-0.5l2.8-2l2.2,1.8l6.1-1 l1.5,6l5.4-2.7l4.2,4.2l-3.4,3.1l2.7,9.1l-7.5,2v2.9l2.4,1.4l-4.4,5.5l1.3,2.7l-2.8-0.2l-3.6,4.7l-2.7,1.3l-8.6-1l-5,2.9l-8.3-0.7 l-1.4,2.5L293.8,455.6z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Pyr\u00E9n\u00E9es-Atlantiques", "data-department", "64", "d", "m276.9,513.4l3.4-0.8l-0.4-2.9l8-9.3l-0.8-3.1 l2.7-1.4l-0.5-7.2h-2.9l1.5-2.8l-2.5-5.8l-6.6-0.3l-8.6,1.5l-3.3-1.1l-4.8,1.9l-2.2-2l-2.3,1.5l-2.5-2.3l-9.8,2l-1.6,2.2l-2.5-1.4 l-2.7,1.3l-1.2-2.8l-11,2.5l-3.98-1.89l-3.52,4.89l-2.7,1.9l-4.5,0.9l1.9,4.5l4.5-0.2l0.2,2.2l2.4,1l2.2-2.1l2.4,1.3l2.5,0.1 l1.4,2.8l-2.5,6.7l-2.1,2.2l1.3,2.2l4.3-0.1l0.7-3.4l2.3-0.1l-1.3,2.4l5.9,2.3l1.5,1.8h2.5l6.1,3.8l5.8,0.4l2.3-1l1.4,2.1l0.3,2.8 l2.7,1.3l3.9,4l2.1,0.9l1.1-2.1l2.7,2.1l3.6-1.1l0.19-0.16l1.41-9.34L276.9,513.4z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Deux-S\u00E8vres", "data-department", "79", "d", "m292.3,331.6l-2.7,1.6l-3.6-4.7l-17.4-6.7 l-5.9-6.5v-3.7l9.1-4.5l-2.5-2l0.2-7.4l-4.7-17.9l-4.2-4.1l-2.3-5.7l12.3-0.8l3.7-4.8l5.6-0.8l9.6-0.2l2.3,2.3l3.4,9l-0.8,3l2.7,1.2 l-4.5,14.1l2.7-0.9l1.5,3l-3.4,5.5l0.5,5.8l2.1,2l-0.1,2.8l6.4,0.2l-3.2,8.5l4.5,3l-0.8,2.8h-0.1l-0.8,0.8l-3.5-0.4l-5.8,2.5 L292.3,331.6z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Vienne", "data-department", "86", "d", "m329.6,320.8v3.5l-4.8-0.6l-1.3,2.5l-3.8,1.4 l-4.1-3.9l-2.2,1.8l1.4,2.4l-2.9,0.8l-9.1-3l0.8-2.8l-4.5-3l3.2-8.5l-6.4-0.2l0.1-2.8l-2.1-2l-0.5-5.8l3.4-5.5l-1.5-3l-2.7,0.9 l4.5-14.1l-2.7-1.2l0.8-3l-3.4-9l6.6-5.9l5.5,3.2l0.3,3.2l2.9-0.3l1.3,6.1l2.8,1.4l10-0.4l-1.4-2.9l5.3,3l0.3,3.1l7.1,10l2.1,3 l-0.8,5.8l4.6,4.4h2.9l2.6,5.4l2.5,1.3l-1.5,2.8l-0.8-0.3l-1.3,2.4l-3.3-0.9l-1.3,3l-5.6,2.7L329.6,320.8z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Haute-Vienne", "data-department", "87", "d", "m348.9,364.1l-1.6,2.7l-5.7-2.2l2-2.7l-7.2-5.6 l-9.5,1.1l0.1-4.7l-6.2-3h-0.1l3.4-3.8l2.8-0.6l4-8l2.6-1.1l0.6-3.2l-4.7-3.6l0.2-5.1v-3.5l3-5l5.6-2.7l1.3-3l3.3,0.9l1.3-2.4 l0.8,0.3l2.6,1.1l5.8-1.1l1.7,2.5l1.2,2.6l-3,5.3l6.6,9.8l1.8,5.2l-0.9,3.4l2.9,1.5l0.1,2.8l6.4,0.5l4.7,3l0.6,5.9l-8.7,4l-4.9,4.6 l-5.7,1.3l-4.7,4L348.9,364.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Occitanie", "data-code_insee", "76", 1, "region"], ["data-name", "Ari\u00E8ge", "data-department", "09", "d", "m369.82,543.59l0.78-0.89l-2.6-1.1l-2-2.1 l-3.7-0.1l-1.7-1.7l-2.8,0.4l-1.3,2.1l-2.4-0.8l-2.8-5.9l-10-0.6l-1.3-2.8l-13.2-3.9l-0.5-1.4l3.8-5.2l2.8-1v-5.9l3.9-4l2.8-1.1 l6.2,4.1l-0.4-5.6l5.4-1.6l-3-4.8l2.8-1.1l3.4,5.5l2.8-0.5l0.6-2.8l5.7,2.2l2-2.3l2.2,5.5l8.7,3.9l2.2,5.2l0.2,3.1l-2.2,2.3l2.4,2.5 l-1.2,3l-3.2,0.6l0.8,5.7l3.4,1.5l3.3-1.2l4.8,5.6l-7.4,0.2l-1.3,2.6L369.82,543.59z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Aude", "data-department", "11", "d", "m435.07,504.37l-1.47,1.53l-5.2,9.3l-0.9,3.5 l0.15,9.57l-9.45-5.57l-8.2,5.4l-13.6-1l-2.7,1.4l1.4,6l-8.6,3.9l-4.8-5.6l-3.3,1.2l-3.4-1.5l-0.8-5.7l3.2-0.6l1.2-3l-2.4-2.5 l2.2-2.3l-0.2-3.1l-2.2-5.2l-8.7-3.9l-2.2-5.5l8.4-10l1.4,2.7l5.2-1.8l0.5-0.8l1.8,2.3l6.3,0.9l1.1-3.3l2.8-0.5l12,1.4l-0.5,2.8 l3.5,5l2.5-1.6l1.4,2.9l3.1-0.8l3.8-5.3l1,2.9l13.8,4.7l1.7,2L435.07,504.37z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Aveyron", "data-department", "12", "d", "m430.8,440.7l9.4,4.5l-2,3.9l-2.8,1.1l8.4,4.1 l-4.3,5.3l0.3,1.5l-3.7,1l-3,5.3l-6.3-1.3l-0.1,8.7l-5.7-0.1l-1.3-2.8l-11.1-1.3l-4.2-5l-4.3-11.5l-4.8-4.3L385,444l-6.1,2.8 l-4.3-3.6l2.3-2.4l-3.1-2.7l0.4-3l-0.8-9.1l7.6-5l5.9-1.4l1.7-1.5h0.1l5.1-3.2l6.4,1.5l3.8-4.8l3-9.1l4.7-4.2l5.2,4l1.3,4.2l2.4,1.6 l-0.5,3l2.6,5.1v0.1l4.2,4.5l2.9,8.8l-0.5,8.7L430.8,440.7z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Gard", "data-department", "30", "d", "m480,487.2l-2.8-0.6l-1.9-1.6l-1.1-3.4h-0.1 l3.3-4.4l-1.5-3l-6.1-6.7l-3-0.2l-0.2-3l-6.8-1.4l0.9-2.7l-1.9-2.6l-3.9,0.6l-4.2,3.9l-0.1,2.8l-5.3-2.5l-2.2,1.7l-0.4-2.9l-2.9-0.1 l-0.3-1.5l4.3-5.3l-8.4-4.1l2.8-1.1l2-3.9l7.8,3.4l3.9-0.5l0.1-3.3l8.7,2.2l6.3-1.8l-1.4-3l1.2-2.9l-3.9-7.7l3.6-2.5l1.1-2.1 l2.7,5.9l7.8,5l7.1-4.3l0.1,3.1l2.5-2.3h2.8l6,3.5l2.6,4.4l0.2,5.5l6.3,6.4l-4.5,5l-3.9,4.1l-1.9,10.6l-3.3-0.9l-4.2,4.8l1,2.7 l-5.8,1.8L480,487.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Haute-Garonne", "data-department", "31", "d", "m326.8,526.2l-5.5-1.5l-1.2,2.4l0.2,7.6 l-8.8-0.7l-1.7,0.3l-0.6-7l5.5-3.2l2.6-5.3l-0.8-2.7l-3.1,0.3l0.6-3.5l-4.6-4l7.1-11.2l3.1-1.1l3.5-5.3l11.4,2.5l0.7-5.8l6.5-6.1 l-9.1-13.3l9.9-0.9l1.7,2.3l5.8-2.5l-2.2-2.3l11.7-4.3l1.4,6.3l2.6,1.2l0.2,2.8l2.3,2.1l-0.7,5.4l14.3,9.3l1,2.8l-0.5,0.8l-5.2,1.8 l-1.4-2.7l-8.4,10l-2,2.3l-5.7-2.2l-0.6,2.8l-2.8,0.5l-3.4-5.5l-2.8,1.1l3,4.8l-5.4,1.6l0.4,5.6l-6.2-4.1l-2.8,1.1l-3.9,4v5.9 l-2.8,1l-3.8,5.2L326.8,526.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Gers", "data-department", "32", "d", "m330.6,461.7l2,6.9l9.1,13.3l-6.5,6.1l-0.7,5.8 l-11.4-2.5l-3.5,5.3l-3.1,1.1l-12.4-2.2l-1.4-3l-5.5,0.6l-2.6-8.7l-3.3-1.3l-2-3.5l-3.9,0.5l-6.6-0.3l-1.6-2.7l4.2-11.1l-0.5-6.4 l9.4-2.7l0.6,3l2.5-1.3l0.4-2.9v-0.1l3.7,0.7l1.4-2.5l8.3,0.7l5-2.9l8.6,1l2.7-1.3l5.3,1.7l-3.3,4.6L330.6,461.7z", 1, "departement", 3, "ngClass", "click"], ["data-name", "H\u00E9rault", "data-department", "34", "d", "m474.1,481.6l-2.4-0.1l-5.9,2.6l-3.6,3.2 l-7.2,4.6l-4.3,4.2l2.1-3.5l-4.3,6.6h-6.8l-5.5,4l-1.13,1.17l-0.17-0.17l-1.7-2l-13.8-4.7l-1-2.9l-3.8,5.3l-3.1,0.8l-1.4-2.9 l-2.5,1.6l-3.5-5l0.5-2.8l3.4-2l0.8-3l-0.7-9.7l6.1,2.2c2.3-1.5,4.6-2.9,6.8-4.4l5.7,0.1l0.1-8.7l6.3,1.3l3-5.3l3.7-1l2.9,0.1 l0.4,2.9l2.2-1.7l5.3,2.5l0.1-2.8l4.2-3.9l3.9-0.6l1.9,2.6l-0.9,2.7l6.8,1.4l0.2,3l3,0.2l6.1,6.7l1.5,3L474.1,481.6z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Lot", "data-department", "46", "d", "m385.4,413.1l3.3,5h-0.1l-1.7,1.5L381,421 l-7.6,5l0.8,9.1l-6.2,0.8l-7.5,5.5l-2.6-2.3l-8.7,2.5l-0.5-4l-2.4,1.5l-2.7-1l-4.5-4l2.1-2.3l-3.1,0.5l-2.7-9.1l3.4-3.1l4.7-6.5 l6-3.4l-0.6-3.6l4.5-3.2l1.4-5.3l0.7-5l8.1-0.8l6.7,6.1l5.3-2.7l6.7,0.2l1,5.4l3.8,6L385.4,413.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Loz\u00E8re", "data-department", "48", "d", "m463.4,418.7l4.2,8.3l-1.1,2.1l-3.6,2.5 l3.9,7.7l-1.2,2.9l1.4,3l-6.3,1.8l-8.7-2.2l-0.1,3.3l-3.9,0.5l-7.8-3.4l-9.4-4.5l-1.5-2.4l0.5-8.7l-2.9-8.8l-4.2-4.5v-0.1l6.9-15.9 l1.7,2.3l6.8-5.7l1-1l2.3,1.7l1.5,5.7l6.4,1.2l0.1-2.8l2.9,0.2l9,7.7L463.4,418.7z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Hautes-Pyr\u00E9n\u00E9es", "data-department", "65", "d", "m314.7,524.1l-5.5,3.2l0.6,7l-0.7,0.2l-2.3-1.6 l-2.4,1.8l-2.5-0.5l-1.9-1.7l-3.9-0.3l-6.9,2.1l-2.2-0.9l-2.1-1.7l-1.1-2.5l-7.8-5.5l-2.11,1.84l1.41-9.34l1.6-2.8l3.4-0.8l-0.4-2.9 l8-9.3l-0.8-3.1l2.7-1.4l-0.5-7.2h-2.9l1.5-2.8l-2.5-5.8l3.9-0.5l2,3.5l3.3,1.3l2.6,8.7l5.5-0.6l1.4,3l12.4,2.2l-7.1,11.2l4.6,4 l-0.6,3.5l3.1-0.3l0.8,2.7L314.7,524.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Pyr\u00E9n\u00E9es-Orientales", "data-department", "66", "d", "m427.65,528.27l0.25,15.63l3.9,3.3l1.9,3.8 h-2.3l-8.1-2.7l-6.9,3.9l-3-0.2l-2.4,1.1l-0.6,2.4l-2.1,1.2l-2.4-0.7l-2.9,1l-4-3.1l-7-2.9l-2.5,1.4h-3l-1,2.1l-4.6,2l-1.9-1.7 l-1.7-4.8l-7.5-2l-2-2.1l2.02-2.31l7.98-2.39l1.3-2.6l7.4-0.2l8.6-3.9l-1.4-6l2.7-1.4l13.6,1l8.2-5.4L427.65,528.27z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Tarn", "data-department", "81", "d", "m419.7,471.9l1.3,2.8c-2.2,1.5-4.5,2.9-6.8,4.4 l-6.1-2.2l0.7,9.7l-0.8,3l-3.4,2l-12-1.4l-2.8,0.5l-1.1,3.3l-6.3-0.9l-1.8-2.3l-1-2.8l-14.3-9.3l0.7-5.4l-2.3-2.1l-0.2-2.8l-2.6-1.2 l-1.4-6.3l0.5-2.8l4.8-3.2l1-2.7L364,450l3-1.1l2.7,1.1l9.2-3.2l6.1-2.8l10.3,5.8l4.8,4.3l4.3,11.5l4.2,5L419.7,471.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Tarn-et-Garonne", "data-department", "82", "d", "m360,458.1l-0.5,2.8l-11.7,4.3l2.2,2.3 l-5.8,2.5l-1.7-2.3l-9.9,0.9l-2-6.9l-5.1-4.1l3.3-4.6l-5.3-1.7l3.6-4.7l2.8,0.2l-1.3-2.7l4.4-5.5l-2.4-1.4v-2.9l7.5-2l3.1-0.5 l-2.1,2.3l4.5,4l2.7,1l2.4-1.5l0.5,4l8.7-2.5l2.6,2.3l7.5-5.5l6.2-0.8l-0.4,3l3.1,2.7l-2.3,2.4l4.3,3.6l-9.2,3.2l-2.7-1.1l-3,1.1 l1.8,2.2l-1,2.7L360,458.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Auvergne-Rhone-Alpes", "data-code_insee", "84", 1, "region"], ["data-name", "Ain", "data-department", "01", "d", "m542,347l-5.7,6.7l-11.2-15.2l-2.8,0.7l-3,5.1 l-6-2l-6.4,0.5l-3.7-5.7l-2.8,0.5l-3.1-9.2l1.5-8l5.9-20.9l5.9,1.5l5.4-1.3l4.8,3.3l4.3,7.7h2.9l0.1,3l2.9-0.1l4-4.4l3.4,1.6 l0.4,2.8l3.8-0.2l5.5-3.2l5.3-7.2l4.5,2.7l-1.8,4.7l0.3,2.5l-4.4,1.5l-1.9,2l0.2,2.8l0.46,0.19l-4.36,4.71h-2.9l0.8,9.3L542,347z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Allier", "data-department", "03", "d", "m443.1,292.3l5.9-6l6.7,13.5l7.9,2.9l1.6,2.4l-0.5,5.5l-3.7,4.6 l-3.9,1.3l-0.5,3l1.5,12.4l-5.5,4.8l-3.5-4.3l-6.4-0.4l-1.4-3.2l-13.1-0.5l-1.6-2.5l-3.3,0.5l-4.4-4.5l1.2-2.8l-2.3-1.7l-11.2,8 l-2.5-1.2l-3.6-8.4c-2.5-1.6-4.9-3.2-7.4-4.8L392,307v-0.1l3.5-5.9l8.7-1l1.7-2.4l-1.7-5.3l2.3-1.9l8.4-2.9l4.8-3.7h4h0.1l5.7,6.3 l6.4,0.2l2.8-1.7L443.1,292.3z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Ard\u00E8che", "data-department", "07", "d", "m496.5,434.2l0.1,3.7l-6-3.5h-2.8l-2.5,2.3 l-0.1-3.1l-7.1,4.3l-7.8-5l-2.7-5.9l-4.2-8.3l-2.1-9.1l6.7-6.4l5.9-1.9l3.4-5.9l3.4-0.4l-0.7-2.8l2.6-2.3l1.5-5.2l2.6,1.2v-3.1 l0.9-4.1l3.5-0.8l3.2-4.9l5-2.7l2,4.2l0.5,10.3l3.8,11.3l-1.5,6.2l-3.5,4.5l1,7.1l-3,5.9L496.5,434.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Cantal", "data-department", "15", "d", "m435.6,387.9l3.5,8l-1,1l-6.8,5.7l-1.7-2.3 l-6.9,15.9l-2.6-5.1l0.5-3l-2.4-1.6l-1.3-4.2l-5.2-4l-4.7,4.2l-3,9.1l-3.8,4.8l-6.4-1.5l-5.1,3.2l-3.3-5l1.7-5.8l-3.8-6l-1-5.4h0.1 l3.2-1.9l-1.5-3.4l3.1-1.1l0.3-3.5l2.3-1.9l-1.8-2.2l7.6-9.5l0.6-3.5l6.2,2l-0.7-6l7.5,3.5l1.5,2.5l6.7,0.3l6.5,5.4l3.7-4.1v3.9 l5.5,1.5l3.3,8.7l2.6,1.1L435.6,387.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Drome", "data-department", "26", "d", "m535.1,404.4l-3,0.5l-0.8-17.5l-3,1.7l-8.2-1.9 l-2.7,1l1.1-6.3l-3.3-7.8l-4.9-2.7l-9,3.1l0.5,10.3l3.8,11.3l-1.5,6.2l-3.5,4.5l1,7.1l-3,5.9l-2.1,14.4l5.9,0.7l3.5,4.2l8.7-3.9 l2.4,1.4l2.5-2.2l0.5,5.8l9.3,0.9l0.1,2.8l5.2,2.3l4.3-4.8l2.3-0.1l1-0.2l0.2-4.7l-10-5.7l-1.5-2.6l3.2-5.1l4.2,1.4l2.5-2.5l-3-2.3 l2.5-6.7l5.8-0.3l0.3-3.4l-5.9-0.8L535.1,404.4z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Is\u00E8re", "data-department", "38", "d", "m513.6,349.4l-0.3-7.1l6,2l3-5.1l2.8-0.7 l11.2,15.2l6.5,10.5l6.2,0.2l0.3-2.8l9.4,2.1l2.7,6.3l-2.3,5.5l1,5.4l5.2,1.5l-1.6,3.8l1.8,4.2l4.4,3.1l-0.4,5.8l-3.1-1.1l-12.6,3.9 l-0.9,2.8l-5.5,1.2l-1,3.1l-5.9-0.8l-5.4-4l-3,0.5l-0.8-17.5l-3,1.7l-8.2-1.9l-2.7,1l1.1-6.3l-3.3-7.8l-4.9-2.7l-9,3.1l-2-4.2v-4.4 l-0.2-1.1h0.1l4.4-3.9l-1.9-2.5l2.5-2.5l6.9-1.5L513.6,349.4z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Loire", "data-department", "42", "d", "m499.3,365.9v4.4l-5,2.7l-3.2,4.9l-3.5,0.8 l-2.2-2.4l-2.6,1l-0.7-5.5l-6-2.2l-6.2,3l-2.8,0.4l-2.3-2l-2.8,0.8l3-7.1l-2.7-7.5l-4.6-3.8l-4.7-7.7l2.1-6.3l-2.5-2.7l5.5-4.8 l-1.5-12.4l0.5-3l3.9-1.3v3l5.2,3.3l8-1.5l2.1,2.1l5.7-3.8l0.01-0.09l2.09,2.99l-4.9,3.5l-1.6,8.6l5.2,6.7l-1.7,5.9l2.3,1.6 l-1.3,2.5l1.1,3l4.6,4.1l5.9,2.1l0.9,3l4.6,2.6h-0.1L499.3,365.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Haute-Loire", "data-department", "43", "d", "m485.4,376.3l2.2,2.4l-0.9,4.1v3.1l-2.6-1.2 l-1.5,5.2l-2.6,2.3l0.7,2.8l-3.4,0.4l-3.4,5.9l-5.9,1.9l-6.7,6.4l-9-7.7l-2.9-0.2l-0.1,2.8l-6.4-1.2l-1.5-5.7l-2.3-1.7l-3.5-8 l3.4-0.2l-2.6-1.1l-3.3-8.7l-5.5-1.5v-3.9v-0.1l9.6-3.2l8.5,0.1l5.2,3.2l11.1-0.7l2.8-0.8l2.3,2l2.8-0.4l6.2-3l6,2.2l0.7,5.5 L485.4,376.3z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Puy-de-Dome", "data-department", "63", "d", "m449.1,332.4l3.5,4.3l2.5,2.7l-2.1,6.3l4.7,7.7 l4.6,3.8l2.7,7.5l-3,7.1l-11.1,0.7l-5.2-3.2l-8.5-0.1l-9.6,3.2v0.1l-3.7,4.1l-6.5-5.4l-6.7-0.3l-1.5-2.5l-7.5-3.5l-1.2-7.9l1.7-2.4 L401,349l-4.4-5.5l9.3-8.6l-2.3-6.7l0.5-4.1l2.5,1.2l11.2-8l2.3,1.7l-1.2,2.8l4.4,4.5l3.3-0.5l1.6,2.5l13.1,0.5l1.4,3.2L449.1,332.4z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Rhone", "data-department", "69", "d", "m493.1,312.7l5.7,7.7l-1.5,8l3.1,9.2l2.8-0.5 l3.7,5.7l6.4-0.5l0.3,7.1l-2.5,5l-6.9,1.5l-2.5,2.5l1.9,2.5l-4.4,3.9l-4.6-2.6l-0.9-3l-5.9-2.1l-4.6-4.1l-1.1-3l1.3-2.5l-2.3-1.6 l1.7-5.9l-5.2-6.7l1.6-8.6l4.9-3.5l-2.09-2.99l0.29-2.91l2.3-1.9l2.2,1.7l2.2-1.6l2.5,1.5L493.1,312.7z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Savoie", "data-department", "73", "d", "m603.7,362l-1,10.3l-3.1,1.4l-2.2,0.7l-4.5,3.4 l-1.5,2.4l-2.5-1.4l-5.1,1.3l-2,1.8v0.1l-6.8,1.9l-2,2l-7.7-3.5l-5.2-1.5l-1-5.4l2.3-5.5l-2.7-6.3l-9.4-2.1l-0.3,2.8l-6.2-0.2 l-6.5-10.5l5.7-6.7l2.3-13.6l2.7,6.7l2.7,0.9l1.3,2.5l3,1.7l2.6-1.6l3.2,0.8l4.6,3.6l9.4-13.9l2.4,1.6l-0.6,3l2.3,1.8l6.2,2.3 l2.2-1.5l0.62-0.76l1.88,4.66l2.7,1.1l1.5,1.9l2.8,0.4l-0.7,3l1.3,5.2l5.1,4L603.7,362z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Haute-Savoie", "data-department", "74", "d", "m547,340.1l-2.7-6.7l-0.8-9.3h2.9l4.36-4.71 l2.24,0.91l2.3-1l2.3,0.1l3.4-3.5l2.1-1l1-2.3l-2.8-1.3l1.8-5.1l2.4-0.8l2.3,1l3.6-2.9l9.5-1.3l3.2,0.6l-0.5,2.7l4.2,4.1l-2.1,6.4 l-0.6,1.5l4.6,1.7l-0.1,4.8l2-1.4l4.6,6.6l-1.3,5l-2.5,1.7l-4.9,0.9l-0.6,3.7l0.02,0.04l-0.62,0.76l-2.2,1.5l-6.2-2.3l-2.3-1.8 l0.6-3l-2.4-1.6l-9.4,13.9l-4.6-3.6l-3.2-0.8l-2.6,1.6l-3-1.7l-1.3-2.5L547,340.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Provence-Alpes-Cote d'Azur", "data-code_insee", "93", 1, "region"], ["data-name", "Alpes-de-Haute-Provence", "data-department", "04", "d", "m596.5,409.9l0.57-0.5l-0.37,4.5l-2.2,1.5 l-0.6,2.9l3.5,4l-1.8,4.8l0.19,0.21L589,435.1l-2,5.3l4.3,8.5l7,7.7l-5.2-0.6l-5.2,3.8l1.2,2.6l-3,1.4l-9.8,0.4l-1.2,3.5l-5.9-3.6 l-10.1,8.5l-4-4.8l-2.7,1.8l-5.3-0.2l-6.1-6l-3.4-1.1l1.7-2.5l-3.7-5.2l1.2-3l-2.2-5.4l4.3-4.8l2.3-0.1l1-0.2l5.9-1.4l3.8,1 l-3.4-4.9l3.9,1.1l1.4-8.6l5.3-4l3.3-0.7l3.5,4.5l0.7-3.8l3.8-4.2l11.1,3.3l9-10.2L596.5,409.9z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Hautes-Alpes", "data-department", "05", "d", "m597.1,409l-0.03,0.4l-0.57,0.5l-6,3.3l-9,10.2 l-11.1-3.3l-3.8,4.2l-0.7,3.8l-3.5-4.5l-3.3,0.7l-5.3,4l-1.4,8.6l-3.9-1.1l3.4,4.9l-3.8-1l-5.9,1.4l0.2-4.7l-10-5.7l-1.5-2.6 l3.2-5.1l4.2,1.4l2.5-2.5l-3-2.3l2.5-6.7l5.8-0.3l0.3-3.4l1-3.1l5.5-1.2l0.9-2.8l12.6-3.9l3.1,1.1l0.4-5.8l-4.4-3.1l-1.8-4.2 l1.6-3.8l7.7,3.5l2-2l6.8-1.9l1.8,4.5l2.4,0.6l1.1,2l0.4,3l1.2,2.2l3,2.3l5.7,0.5l2.2,1.3l-0.7,2.1l3.2,4.7l-3,1.5L597.1,409z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Alpes-Maritimes", "data-department", "06", "d", "m605.3,477.1l-3.2-0.1l-1.3,1.8l-0.1,2.2 l-0.42,0.77l-2.18-3.97l0.8-2.9l-5.6-2.6l-1.7-5.6l-5.5-2.9l3-1.4l-1.2-2.6l5.2-3.8l5.2,0.6l-7-7.7l-4.3-8.5l2-5.3l6.79-7.79 l6.91,7.79l6.9,1.6l4.2,2.8l2.5-0.4l1.8,1.4l10.3-2.4l2.7-1.8l-0.3,2.6l1.5,2.2l0.3,3.2l-1.6,1.9l-0.2,2.3l-2.7,1.6l-3.3,5l-0.5,1.6 l1.1,2.7l-1.1,2.7l-3.5,2.9l-2.3,0.5l-0.9,2.4l-3-0.9l-1.5,2.1l-2.3,0.5L609,472l0.1,2.8l-2.4,0.6L605.3,477.1z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Bouches-du-Rhone", "data-department", "13", "d", "m545,500.2l2.5-2l-2.2-6.3l1.1-2.6l2.7-0.5 l-5.5-9.1l2-5.3l3.3-0.8l-1.9-3.8l-0.1-0.1l-6.6,4.3l-3.2,0.2l-12-4.8l-3.5,0.7l-4.5-2.3l-5.5-5.7l-10.4-2.9l-3.9,4.1l-1.9,10.6 l-3.3-0.9l-4.2,4.8l1,2.7l-5.8,1.8l-3.1,4.9l0.2,0.1h13.2l2.2,0.9l1,2.2l-1.6,1.5l2.2,1.4l7.4,0.1l3.2,1.3l1.8-1.7l-1.5-2.8l0.4-2.4 l4.9,1l3,5.3l10-0.8l2.6-1.1l1.8,2l-0.2,2.5l1,2l-1.2,2.2h9.2l1.3,2l2.2-0.8l1.7,0.2L545,500.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Var", "data-department", "83", "d", "m600.28,481.77l-1.38,2.53l-6.8,1.7l-0.7,2.5 l-5.5,5.7l5,0.7l-2,4.8l-4,0.2l-4.8,2.5l-3.5,1.1l0.1,2.7l-4.9-1.5l-2.7,0.5l-1.6,1.6l-0.4,2.3l-2.2,1.6l1.4-1.8l-2.4-1.7l-2.2,0.7 l-1.6-1.6l-3.1,0.1l0.9,2.2l-2.3-0.4l-1.5,1.7l-3-1.1l0.6-2.3l-6.4-4.1l-0.5-0.1l0.2-2.1l2.5-2l-2.2-6.3l1.1-2.6l2.7-0.5l-5.5-9.1 l2-5.3l3.3-0.8l-1.9-3.8l0.1-0.4l5.3,0.2l2.7-1.8l4,4.8l10.1-8.5l5.9,3.6l1.2-3.5l9.8-0.4l5.5,2.9l1.7,5.6l5.6,2.6l-0.8,2.9 L600.28,481.77z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Vaucluse", "data-department", "84", "d", "m541,463.4l6.1,6l-0.1,0.4l-0.1-0.1l-6.6,4.3 l-3.2,0.2l-12-4.8l-3.5,0.7l-4.5-2.3l-5.5-5.7l-10.4-2.9l4.5-5l-6.3-6.4l-0.2-5.5l-2.6-4.4l-0.1-3.7l5.9,0.7l3.5,4.2l8.7-3.9 l2.4,1.4l2.5-2.2l0.5,5.8l9.3,0.9l0.1,2.8l5.2,2.3l2.2,5.4l-1.2,3l3.7,5.2l-1.7,2.5L541,463.4z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Corse", "data-department", "20", "data-code_insee", "94", 1, "region"], ["data-name", "Corse-du-Sud", "data-department", "2A", "d", "m640.5,554.2l3.2-1.7l0.7,8.4l-0.15,0.54 l-1.85,4.86l-2.7,1.9l3.3,0.4l-5.8,14.7l-3.1-1.2l-1.2-2.8l-11.2-3.4l-4.8-4.4l0.2-3l4.9-3.3l-9.5-1.9l2.7-7l-0.9-5.8l-7.3,2.6 l3-8.4l2.6-1.6l-7.9-4.4l-1.1-5.5l5.3-3.8l-3.8-4.2l-2.6,1l0.5-2.7l13.6,2.1l1.2,3.5l6,3.4l6,5.9l0.5,3.2l2.7,1.1l3.7,11 L640.5,554.2z", 1, "departement", 3, "ngClass", "click"], ["data-name", "Haute-Corse", "data-department", "2B", "d", "m643.7,551.5v1l-3.2,1.7l-3.8-0.5l-3.7-11 l-2.7-1.1l-0.5-3.2l-6-5.9l-6-3.4l-1.2-3.5l-13.6-2.1v-0.2l3.9-5l-0.3-3.4l2.2-2.8l2.8-0.3l0.9-2.9l10.7-4.2l3.5-4.9l8.6,1.3 l-0.5-17.4l2.4-2l2.9,1.1l0.18,0.89l1.52,8.21l-0.5,10.6l4,5.6l3.8,26l-5.4,11.9V551.5L643.7,551.5z", 1, "departement", 3, "ngClass", "click"]], template: function MapFrenchComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "svg", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "g", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "path", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_5_listener() { return ctx.selectDept(971); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "g", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "path", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_7_listener() { return ctx.selectDept(972); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "g", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "path", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_9_listener() { return ctx.selectDept(973); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "g", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "path", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_11_listener() { return ctx.selectDept(974); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "g", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "path", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_13_listener() { return ctx.selectDept(976); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "g", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "path", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_15_listener() { return ctx.selectDept(75); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "path", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_16_listener() { return ctx.selectDept(77); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "path", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_17_listener() { return ctx.selectDept(78); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "path", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_18_listener() { return ctx.selectDept(91); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "path", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_19_listener() { return ctx.selectDept(92); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "path", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_20_listener() { return ctx.selectDept(93); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "path", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_21_listener() { return ctx.selectDept(94); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "path", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_22_listener() { return ctx.selectDept(95); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "g", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "path", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_24_listener() { return ctx.selectDept(18); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "path", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_25_listener() { return ctx.selectDept(28); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "path", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_26_listener() { return ctx.selectDept(36); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "path", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_27_listener() { return ctx.selectDept(37); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "path", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_28_listener() { return ctx.selectDept(41); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "path", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_29_listener() { return ctx.selectDept(45); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "g", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "path", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_31_listener() { return ctx.selectDept(21); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "path", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_32_listener() { return ctx.selectDept(25); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "path", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_33_listener() { return ctx.selectDept(39); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "path", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_34_listener() { return ctx.selectDept(58); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "path", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_35_listener() { return ctx.selectDept(70); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "path", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_36_listener() { return ctx.selectDept(71); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "path", 37);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_37_listener() { return ctx.selectDept(89); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "path", 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_38_listener() { return ctx.selectDept(90); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "g", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "path", 40);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_40_listener() { return ctx.selectDept(14); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "path", 41);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_41_listener() { return ctx.selectDept(27); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "path", 42);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_42_listener() { return ctx.selectDept(50); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "path", 43);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_43_listener() { return ctx.selectDept(61); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "path", 44);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_44_listener() { return ctx.selectDept(76); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "g", 45);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "path", 46);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_46_listener() { return ctx.selectDept(2); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "path", 47);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_47_listener() { return ctx.selectDept(59); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "path", 48);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_48_listener() { return ctx.selectDept(60); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "path", 49);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_49_listener() { return ctx.selectDept(62); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "path", 50);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_50_listener() { return ctx.selectDept(80); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "g", 51);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "path", 52);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_52_listener() { return ctx.selectDept(8); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "path", 53);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_53_listener() { return ctx.selectDept(10); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "path", 54);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_54_listener() { return ctx.selectDept(51); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "path", 55);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_55_listener() { return ctx.selectDept(52); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "path", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_56_listener() { return ctx.selectDept(54); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "path", 57);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_57_listener() { return ctx.selectDept(55); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "path", 58);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_58_listener() { return ctx.selectDept(57); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "path", 59);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_59_listener() { return ctx.selectDept(67); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "path", 60);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_60_listener() { return ctx.selectDept(68); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "path", 61);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_61_listener() { return ctx.selectDept(88); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "g", 62);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "path", 63);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_63_listener() { return ctx.selectDept(44); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "path", 64);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_64_listener() { return ctx.selectDept(49); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "path", 65);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_65_listener() { return ctx.selectDept(53); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "path", 66);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_66_listener() { return ctx.selectDept(72); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "path", 67);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_67_listener() { return ctx.selectDept(85); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "g", 68);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "path", 69);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_69_listener() { return ctx.selectDept(22); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "path", 70);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_70_listener() { return ctx.selectDept(29); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "path", 71);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_71_listener() { return ctx.selectDept(35); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "path", 72);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_72_listener() { return ctx.selectDept(56); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "g", 73);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "path", 74);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_74_listener() { return ctx.selectDept(16); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "path", 75);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_75_listener() { return ctx.selectDept(17); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "path", 76);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_76_listener() { return ctx.selectDept(19); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "path", 77);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_77_listener() { return ctx.selectDept(23); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "path", 78);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_78_listener() { return ctx.selectDept(24); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "path", 79);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_79_listener() { return ctx.selectDept(33); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "path", 80);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_80_listener() { return ctx.selectDept(40); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "path", 81);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_81_listener() { return ctx.selectDept(47); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "path", 82);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_82_listener() { return ctx.selectDept(64); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "path", 83);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_83_listener() { return ctx.selectDept(79); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "path", 84);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_84_listener() { return ctx.selectDept(86); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "path", 85);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_85_listener() { return ctx.selectDept(87); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "g", 86);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "path", 87);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_87_listener() { return ctx.selectDept(9); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "path", 88);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_88_listener() { return ctx.selectDept(11); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "path", 89);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_89_listener() { return ctx.selectDept(12); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "path", 90);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_90_listener() { return ctx.selectDept(30); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "path", 91);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_91_listener() { return ctx.selectDept(31); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "path", 92);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_92_listener() { return ctx.selectDept(32); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "path", 93);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_93_listener() { return ctx.selectDept(34); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "path", 94);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_94_listener() { return ctx.selectDept(46); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "path", 95);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_95_listener() { return ctx.selectDept(48); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "path", 96);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_96_listener() { return ctx.selectDept(65); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "path", 97);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_97_listener() { return ctx.selectDept(66); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "path", 98);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_98_listener() { return ctx.selectDept(81); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "path", 99);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_99_listener() { return ctx.selectDept(82); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "g", 100);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "path", 101);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_101_listener() { return ctx.selectDept(1); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "path", 102);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_102_listener() { return ctx.selectDept(3); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "path", 103);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_103_listener() { return ctx.selectDept(7); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "path", 104);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_104_listener() { return ctx.selectDept(15); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "path", 105);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_105_listener() { return ctx.selectDept(26); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "path", 106);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_106_listener() { return ctx.selectDept(38); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "path", 107);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_107_listener() { return ctx.selectDept(42); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "path", 108);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_108_listener() { return ctx.selectDept(34); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](109, "path", 109);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_109_listener() { return ctx.selectDept(63); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "path", 110);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_110_listener() { return ctx.selectDept(69); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](111, "path", 111);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_111_listener() { return ctx.selectDept(73); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "path", 112);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_112_listener() { return ctx.selectDept(74); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](113, "g", 113);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "path", 114);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_114_listener() { return ctx.selectDept(4); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](115, "path", 115);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_115_listener() { return ctx.selectDept(5); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "path", 116);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_116_listener() { return ctx.selectDept(6); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "path", 117);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_117_listener() { return ctx.selectDept(13); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](118, "path", 118);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_118_listener() { return ctx.selectDept(83); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](119, "path", 119);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_119_listener() { return ctx.selectDept(84); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](120, "g", 120);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "path", 121);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_121_listener() { return ctx.selectDept("2A"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "path", 122);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MapFrenchComponent_Template__svg_path_click_122_listener() { return ctx.selectDept("2B"); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](101, _c0, "971" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](103, _c0, "972" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](105, _c0, "973" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](107, _c0, "974" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](109, _c0, "976" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](111, _c0, "75" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](113, _c0, "77" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](115, _c0, "78" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](117, _c0, "91" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](119, _c0, "92" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](121, _c0, "93" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](123, _c0, "94" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](125, _c0, "95" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](127, _c0, "18" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](129, _c0, "28" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](131, _c0, "36" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](133, _c0, "37" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](135, _c0, "41" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](137, _c0, "45" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](139, _c0, "21" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](141, _c0, "25" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](143, _c0, "39" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](145, _c0, "58" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](147, _c0, "70" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](149, _c0, "71" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](151, _c0, "89" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](153, _c0, "90" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](155, _c0, "14" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](157, _c0, "27" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](159, _c0, "50" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](161, _c0, "61" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](163, _c0, "76" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](165, _c0, "02" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](167, _c0, "59" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](169, _c0, "60" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](171, _c0, "62" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](173, _c0, "80" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](175, _c0, "08" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](177, _c0, "10" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](179, _c0, "51" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](181, _c0, "52" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](183, _c0, "54" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](185, _c0, "55" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](187, _c0, "57" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](189, _c0, "67" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](191, _c0, "68" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](193, _c0, "88" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](195, _c0, "44" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](197, _c0, "49" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](199, _c0, "53" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](201, _c0, "72" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](203, _c0, "85" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](205, _c0, "22" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](207, _c0, "29" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](209, _c0, "35" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](211, _c0, "56" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](213, _c0, "16" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](215, _c0, "17" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](217, _c0, "19" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](219, _c0, "23" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](221, _c0, "24" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](223, _c0, "33" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](225, _c0, "40" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](227, _c0, "47" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](229, _c0, "64" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](231, _c0, "79" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](233, _c0, "86" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](235, _c0, "87" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](237, _c0, "09" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](239, _c0, "11" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](241, _c0, "12" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](243, _c0, "30" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](245, _c0, "31" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](247, _c0, "32" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](249, _c0, "34" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](251, _c0, "46" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](253, _c0, "48" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](255, _c0, "65" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](257, _c0, "66" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](259, _c0, "81" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](261, _c0, "82" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](263, _c0, "01" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](265, _c0, "03" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](267, _c0, "07" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](269, _c0, "15" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](271, _c0, "26" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](273, _c0, "38" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](275, _c0, "42" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](277, _c0, "43" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](279, _c0, "63" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](281, _c0, "69" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](283, _c0, "73" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](285, _c0, "74" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](287, _c0, "04" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](289, _c0, "05" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](291, _c0, "06" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](293, _c0, "13" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](295, _c0, "83" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](297, _c0, "84" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](299, _c0, "2A" === ctx.dept));
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](301, _c0, "2B" === ctx.dept));
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["NgClass"]], styles: [".mapfrench_container[_ngcontent-%COMP%] {\r\n  height: 100%;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%] {\r\n  display: flex;\r\n  flex-direction: column;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_title[_ngcontent-%COMP%] {\r\n  text-align: center;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%] {\r\n  height: 100%;\r\n  max-height: 550px;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]   path[_ngcontent-%COMP%] {\r\n  fill: #7f8076;\r\n  stroke: white;\r\n  stroke-width: 1px;\r\n  cursor: pointer;\r\n  transition: 0.3s;\r\n  position: relative;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]   path.selected[_ngcontent-%COMP%] {\r\n  fill: red;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]:hover   path[_ngcontent-%COMP%] {\r\n  fill: blue;\r\n\r\n}\r\npolygon[_ngcontent-%COMP%]{\r\n  -webkit-clip-path: polygon(0% 0%, 100% 0%, 100% 75%, 75% 75%, 64% 100%, 50% 75%, 0% 75%);\r\n          clip-path: polygon(0% 0%, 100% 0%, 100% 75%, 75% 75%, 64% 100%, 50% 75%, 0% 75%);\r\n\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   .popup[_ngcontent-%COMP%]{\r\n  outline: solid 1px rgb(182, 182, 182);\r\n\r\n  fill: #0000ff;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   .popup[_ngcontent-%COMP%]   text[_ngcontent-%COMP%]{\r\n\r\n  fill: #0000ff;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]   path[_ngcontent-%COMP%]:hover {\r\n  fill: red;\r\n  stroke: white;\r\n  stroke-width: 1px;\r\n  cursor: pointer;\r\n}\r\n.mapfrench_container[_ngcontent-%COMP%]   .mapfrench_wrapper[_ngcontent-%COMP%]   .map_content[_ngcontent-%COMP%]   g[_ngcontent-%COMP%]   .choix[_ngcontent-%COMP%] {\r\n  fill: red;\r\n  stroke: white;\r\n  stroke-width: 1px;\r\n  cursor: pointer;\r\n}\r\n\r\n#info-box[_ngcontent-%COMP%] {\r\n  display: none ;\r\n  position: absolute ;\r\n  top: 0px ;\r\n  left: 0px  ;\r\n  z-index: 1;\r\n  background-color: #2c2c2c ;\r\n  border: 2px solid #ffffff ;\r\n  color: #ffffff;\r\n  border-radius: 5px ;\r\n  padding: 5px ;\r\n  font-family: arial ;\r\n  width: 200px;\r\n  height: 30px;\r\n  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;\r\n  text-align: center;\r\n    }\r\n.round[_ngcontent-%COMP%]{\r\n      border-radius: 30px;\r\n      -webkit-border-radius: 30px;\r\n      -moz-border-radius: 30px;\r\n    }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFwLWZyZW5jaC9tYXAtZnJlbmNoLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFZO0FBQ2Q7QUFDQTtFQUNFLGFBQWE7RUFDYixzQkFBc0I7QUFDeEI7QUFDQTtFQUNFLGtCQUFrQjtBQUNwQjtBQUNBO0VBQ0UsWUFBWTtFQUNaLGlCQUFpQjtBQUNuQjtBQUVBO0VBQ0UsYUFBYTtFQUNiLGFBQWE7RUFDYixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLFNBQVM7QUFDWDtBQUNBO0VBQ0UsVUFBVTs7QUFFWjtBQUNBO0VBQ0Usd0ZBQWdGO1VBQWhGLGdGQUFnRjs7QUFFbEY7QUFDQTtFQUNFLHFDQUFxQzs7RUFFckMsYUFBYTtBQUNmO0FBQ0E7O0VBRUUsYUFBYTtBQUNmO0FBRUE7RUFDRSxTQUFTO0VBQ1QsYUFBYTtFQUNiLGlCQUFpQjtFQUNqQixlQUFlO0FBQ2pCO0FBRUE7RUFDRSxTQUFTO0VBQ1QsYUFBYTtFQUNiLGlCQUFpQjtFQUNqQixlQUFlO0FBQ2pCO0FBRUEsK0NBQStDO0FBQy9DO0VBQ0UsY0FBYztFQUNkLG1CQUFtQjtFQUNuQixTQUFTO0VBQ1QsV0FBVztFQUNYLFVBQVU7RUFDViwwQkFBMEI7RUFDMUIsMEJBQTBCO0VBQzFCLGNBQWM7RUFDZCxtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osWUFBWTtFQUNaLG1EQUFtRDtFQUNuRCxrQkFBa0I7SUFDaEI7QUFDQTtNQUNFLG1CQUFtQjtNQUNuQiwyQkFBMkI7TUFDM0Isd0JBQXdCO0lBQzFCIiwiZmlsZSI6InNyYy9hcHAvbWFwLWZyZW5jaC9tYXAtZnJlbmNoLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFwZnJlbmNoX2NvbnRhaW5lciB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5tYXBmcmVuY2hfY29udGFpbmVyIC5tYXBmcmVuY2hfd3JhcHBlciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG59XHJcbi5tYXBmcmVuY2hfY29udGFpbmVyIC5tYXBmcmVuY2hfd3JhcHBlciAubWFwX3RpdGxlIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLm1hcGZyZW5jaF9jb250YWluZXIgLm1hcGZyZW5jaF93cmFwcGVyIC5tYXBfY29udGVudCB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG4gIG1heC1oZWlnaHQ6IDU1MHB4O1xyXG59XHJcblxyXG4ubWFwZnJlbmNoX2NvbnRhaW5lciAubWFwZnJlbmNoX3dyYXBwZXIgLm1hcF9jb250ZW50IGcgcGF0aCB7XHJcbiAgZmlsbDogIzdmODA3NjtcclxuICBzdHJva2U6IHdoaXRlO1xyXG4gIHN0cm9rZS13aWR0aDogMXB4O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICB0cmFuc2l0aW9uOiAwLjNzO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG4ubWFwZnJlbmNoX2NvbnRhaW5lciAubWFwZnJlbmNoX3dyYXBwZXIgLm1hcF9jb250ZW50IGcgcGF0aC5zZWxlY3RlZCB7XHJcbiAgZmlsbDogcmVkO1xyXG59XHJcbi5tYXBmcmVuY2hfY29udGFpbmVyIC5tYXBmcmVuY2hfd3JhcHBlciAubWFwX2NvbnRlbnQgZzpob3ZlciBwYXRoIHtcclxuICBmaWxsOiBibHVlO1xyXG5cclxufVxyXG5wb2x5Z29ue1xyXG4gIGNsaXAtcGF0aDogcG9seWdvbigwJSAwJSwgMTAwJSAwJSwgMTAwJSA3NSUsIDc1JSA3NSUsIDY0JSAxMDAlLCA1MCUgNzUlLCAwJSA3NSUpO1xyXG5cclxufVxyXG4ubWFwZnJlbmNoX2NvbnRhaW5lciAubWFwZnJlbmNoX3dyYXBwZXIgLm1hcF9jb250ZW50IC5wb3B1cHtcclxuICBvdXRsaW5lOiBzb2xpZCAxcHggcmdiKDE4MiwgMTgyLCAxODIpO1xyXG5cclxuICBmaWxsOiAjMDAwMGZmO1xyXG59XHJcbi5tYXBmcmVuY2hfY29udGFpbmVyIC5tYXBmcmVuY2hfd3JhcHBlciAubWFwX2NvbnRlbnQgLnBvcHVwIHRleHR7XHJcblxyXG4gIGZpbGw6ICMwMDAwZmY7XHJcbn1cclxuXHJcbi5tYXBmcmVuY2hfY29udGFpbmVyIC5tYXBmcmVuY2hfd3JhcHBlciAubWFwX2NvbnRlbnQgZyBwYXRoOmhvdmVyIHtcclxuICBmaWxsOiByZWQ7XHJcbiAgc3Ryb2tlOiB3aGl0ZTtcclxuICBzdHJva2Utd2lkdGg6IDFweDtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbi5tYXBmcmVuY2hfY29udGFpbmVyIC5tYXBmcmVuY2hfd3JhcHBlciAubWFwX2NvbnRlbnQgZyAgLmNob2l4IHtcclxuICBmaWxsOiByZWQ7XHJcbiAgc3Ryb2tlOiB3aGl0ZTtcclxuICBzdHJva2Utd2lkdGg6IDFweDtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqcG9wdXAqKioqKioqKioqKioqKioqL1xyXG4jaW5mby1ib3gge1xyXG4gIGRpc3BsYXk6IG5vbmUgO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZSA7XHJcbiAgdG9wOiAwcHggO1xyXG4gIGxlZnQ6IDBweCAgO1xyXG4gIHotaW5kZXg6IDE7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzJjMmMyYyA7XHJcbiAgYm9yZGVyOiAycHggc29saWQgI2ZmZmZmZiA7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbiAgYm9yZGVyLXJhZGl1czogNXB4IDtcclxuICBwYWRkaW5nOiA1cHggO1xyXG4gIGZvbnQtZmFtaWx5OiBhcmlhbCA7XHJcbiAgd2lkdGg6IDIwMHB4O1xyXG4gIGhlaWdodDogMzBweDtcclxuICBib3gtc2hhZG93OiByZ2JhKDAsIDAsIDAsIDAuMTUpIDEuOTVweCAxLjk1cHggMi42cHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG4gICAgLnJvdW5ke1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gICAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiAgICAgIC1tb3otYm9yZGVyLXJhZGl1czogMzBweDtcclxuICAgIH1cclxuXHJcbiAgICBcclxuXHJcblxyXG4gICAgIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MapFrenchComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-map-french',
                templateUrl: './map-french.component.html',
                styleUrls: ['./map-french.component.css']
            }]
    }], function () { return []; }, { myinputDep: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }], myOutput: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"]
        }] }); })();


/***/ }),

/***/ "./src/app/material-module.ts":
/*!************************************!*\
  !*** ./src/app/material-module.ts ***!
  \************************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/cdk/a11y */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/a11y.js");
/* harmony import */ var _angular_cdk_clipboard__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/cdk/clipboard */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/clipboard.js");
/* harmony import */ var _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/drag-drop */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/drag-drop.js");
/* harmony import */ var _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/cdk/portal */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/portal.js");
/* harmony import */ var _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/scrolling */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/scrolling.js");
/* harmony import */ var _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/cdk/stepper */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/stepper.js");
/* harmony import */ var _angular_cdk_table__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/cdk/table */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/table.js");
/* harmony import */ var _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/cdk/tree */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/tree.js");
/* harmony import */ var _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/autocomplete */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/autocomplete.js");
/* harmony import */ var _angular_material_badge__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/badge */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/badge.js");
/* harmony import */ var _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/bottom-sheet */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/bottom-sheet.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
/* harmony import */ var _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/button-toggle */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button-toggle.js");
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/card */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/card.js");
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/checkbox */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/checkbox.js");
/* harmony import */ var _angular_material_chips__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/chips */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/chips.js");
/* harmony import */ var _angular_material_stepper__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/stepper */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/stepper.js");
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/datepicker */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/datepicker.js");
/* harmony import */ var _angular_material_dialog__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/dialog */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/dialog.js");
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/divider */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/divider.js");
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/material/expansion */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/expansion.js");
/* harmony import */ var _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/material/grid-list */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/grid-list.js");
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/material/icon */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/icon.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/input.js");
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/material/list */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/list.js");
/* harmony import */ var _angular_material_menu__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/material/menu */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/menu.js");
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @angular/material/core */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @angular/material/paginator */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/paginator.js");
/* harmony import */ var _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! @angular/material/progress-bar */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/progress-bar.js");
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! @angular/material/progress-spinner */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/progress-spinner.js");
/* harmony import */ var _angular_material_radio__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @angular/material/radio */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/radio.js");
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! @angular/material/select */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/select.js");
/* harmony import */ var _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! @angular/material/sidenav */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/sidenav.js");
/* harmony import */ var _angular_material_slider__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! @angular/material/slider */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/slider.js");
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! @angular/material/slide-toggle */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/slide-toggle.js");
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! @angular/material/snack-bar */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/snack-bar.js");
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_37__ = __webpack_require__(/*! @angular/material/sort */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/sort.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_38__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/table.js");
/* harmony import */ var _angular_material_tabs__WEBPACK_IMPORTED_MODULE_39__ = __webpack_require__(/*! @angular/material/tabs */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/tabs.js");
/* harmony import */ var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_40__ = __webpack_require__(/*! @angular/material/toolbar */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/toolbar.js");
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_41__ = __webpack_require__(/*! @angular/material/tooltip */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/tooltip.js");
/* harmony import */ var _angular_material_tree__WEBPACK_IMPORTED_MODULE_42__ = __webpack_require__(/*! @angular/material/tree */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/tree.js");
/* harmony import */ var _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_43__ = __webpack_require__(/*! @angular/cdk/overlay */ "./node_modules/@angular/cdk/__ivy_ngcc__/fesm2015/overlay.js");













































class MaterialModule {
}
MaterialModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: MaterialModule });
MaterialModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ factory: function MaterialModule_Factory(t) { return new (t || MaterialModule)(); }, imports: [_angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_1__["A11yModule"],
        _angular_cdk_clipboard__WEBPACK_IMPORTED_MODULE_2__["ClipboardModule"],
        _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_6__["CdkStepperModule"],
        _angular_cdk_table__WEBPACK_IMPORTED_MODULE_7__["CdkTableModule"],
        _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_8__["CdkTreeModule"],
        _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["DragDropModule"],
        _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_9__["MatAutocompleteModule"],
        _angular_material_badge__WEBPACK_IMPORTED_MODULE_10__["MatBadgeModule"],
        _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_11__["MatBottomSheetModule"],
        _angular_material_button__WEBPACK_IMPORTED_MODULE_12__["MatButtonModule"],
        _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__["MatButtonToggleModule"],
        _angular_material_card__WEBPACK_IMPORTED_MODULE_14__["MatCardModule"],
        _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_15__["MatCheckboxModule"],
        _angular_material_chips__WEBPACK_IMPORTED_MODULE_16__["MatChipsModule"],
        _angular_material_stepper__WEBPACK_IMPORTED_MODULE_17__["MatStepperModule"],
        _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__["MatDatepickerModule"],
        _angular_material_dialog__WEBPACK_IMPORTED_MODULE_19__["MatDialogModule"],
        _angular_material_divider__WEBPACK_IMPORTED_MODULE_20__["MatDividerModule"],
        _angular_material_expansion__WEBPACK_IMPORTED_MODULE_21__["MatExpansionModule"],
        _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_22__["MatGridListModule"],
        _angular_material_icon__WEBPACK_IMPORTED_MODULE_23__["MatIconModule"],
        _angular_material_input__WEBPACK_IMPORTED_MODULE_24__["MatInputModule"],
        _angular_material_list__WEBPACK_IMPORTED_MODULE_25__["MatListModule"],
        _angular_material_menu__WEBPACK_IMPORTED_MODULE_26__["MatMenuModule"],
        _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatNativeDateModule"],
        _angular_material_paginator__WEBPACK_IMPORTED_MODULE_28__["MatPaginatorModule"],
        _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_29__["MatProgressBarModule"],
        _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_30__["MatProgressSpinnerModule"],
        _angular_material_radio__WEBPACK_IMPORTED_MODULE_31__["MatRadioModule"],
        _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatRippleModule"],
        _angular_material_select__WEBPACK_IMPORTED_MODULE_32__["MatSelectModule"],
        _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_33__["MatSidenavModule"],
        _angular_material_slider__WEBPACK_IMPORTED_MODULE_34__["MatSliderModule"],
        _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_35__["MatSlideToggleModule"],
        _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_36__["MatSnackBarModule"],
        _angular_material_sort__WEBPACK_IMPORTED_MODULE_37__["MatSortModule"],
        _angular_material_table__WEBPACK_IMPORTED_MODULE_38__["MatTableModule"],
        _angular_material_tabs__WEBPACK_IMPORTED_MODULE_39__["MatTabsModule"],
        _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_40__["MatToolbarModule"],
        _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_41__["MatTooltipModule"],
        _angular_material_tree__WEBPACK_IMPORTED_MODULE_42__["MatTreeModule"],
        _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_43__["OverlayModule"],
        _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_4__["PortalModule"],
        _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_5__["ScrollingModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](MaterialModule, { exports: [_angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_1__["A11yModule"],
        _angular_cdk_clipboard__WEBPACK_IMPORTED_MODULE_2__["ClipboardModule"],
        _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_6__["CdkStepperModule"],
        _angular_cdk_table__WEBPACK_IMPORTED_MODULE_7__["CdkTableModule"],
        _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_8__["CdkTreeModule"],
        _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["DragDropModule"],
        _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_9__["MatAutocompleteModule"],
        _angular_material_badge__WEBPACK_IMPORTED_MODULE_10__["MatBadgeModule"],
        _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_11__["MatBottomSheetModule"],
        _angular_material_button__WEBPACK_IMPORTED_MODULE_12__["MatButtonModule"],
        _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__["MatButtonToggleModule"],
        _angular_material_card__WEBPACK_IMPORTED_MODULE_14__["MatCardModule"],
        _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_15__["MatCheckboxModule"],
        _angular_material_chips__WEBPACK_IMPORTED_MODULE_16__["MatChipsModule"],
        _angular_material_stepper__WEBPACK_IMPORTED_MODULE_17__["MatStepperModule"],
        _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__["MatDatepickerModule"],
        _angular_material_dialog__WEBPACK_IMPORTED_MODULE_19__["MatDialogModule"],
        _angular_material_divider__WEBPACK_IMPORTED_MODULE_20__["MatDividerModule"],
        _angular_material_expansion__WEBPACK_IMPORTED_MODULE_21__["MatExpansionModule"],
        _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_22__["MatGridListModule"],
        _angular_material_icon__WEBPACK_IMPORTED_MODULE_23__["MatIconModule"],
        _angular_material_input__WEBPACK_IMPORTED_MODULE_24__["MatInputModule"],
        _angular_material_list__WEBPACK_IMPORTED_MODULE_25__["MatListModule"],
        _angular_material_menu__WEBPACK_IMPORTED_MODULE_26__["MatMenuModule"],
        _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatNativeDateModule"],
        _angular_material_paginator__WEBPACK_IMPORTED_MODULE_28__["MatPaginatorModule"],
        _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_29__["MatProgressBarModule"],
        _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_30__["MatProgressSpinnerModule"],
        _angular_material_radio__WEBPACK_IMPORTED_MODULE_31__["MatRadioModule"],
        _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatRippleModule"],
        _angular_material_select__WEBPACK_IMPORTED_MODULE_32__["MatSelectModule"],
        _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_33__["MatSidenavModule"],
        _angular_material_slider__WEBPACK_IMPORTED_MODULE_34__["MatSliderModule"],
        _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_35__["MatSlideToggleModule"],
        _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_36__["MatSnackBarModule"],
        _angular_material_sort__WEBPACK_IMPORTED_MODULE_37__["MatSortModule"],
        _angular_material_table__WEBPACK_IMPORTED_MODULE_38__["MatTableModule"],
        _angular_material_tabs__WEBPACK_IMPORTED_MODULE_39__["MatTabsModule"],
        _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_40__["MatToolbarModule"],
        _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_41__["MatTooltipModule"],
        _angular_material_tree__WEBPACK_IMPORTED_MODULE_42__["MatTreeModule"],
        _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_43__["OverlayModule"],
        _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_4__["PortalModule"],
        _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_5__["ScrollingModule"]] }); })();
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MaterialModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                exports: [
                    _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_1__["A11yModule"],
                    _angular_cdk_clipboard__WEBPACK_IMPORTED_MODULE_2__["ClipboardModule"],
                    _angular_cdk_stepper__WEBPACK_IMPORTED_MODULE_6__["CdkStepperModule"],
                    _angular_cdk_table__WEBPACK_IMPORTED_MODULE_7__["CdkTableModule"],
                    _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_8__["CdkTreeModule"],
                    _angular_cdk_drag_drop__WEBPACK_IMPORTED_MODULE_3__["DragDropModule"],
                    _angular_material_autocomplete__WEBPACK_IMPORTED_MODULE_9__["MatAutocompleteModule"],
                    _angular_material_badge__WEBPACK_IMPORTED_MODULE_10__["MatBadgeModule"],
                    _angular_material_bottom_sheet__WEBPACK_IMPORTED_MODULE_11__["MatBottomSheetModule"],
                    _angular_material_button__WEBPACK_IMPORTED_MODULE_12__["MatButtonModule"],
                    _angular_material_button_toggle__WEBPACK_IMPORTED_MODULE_13__["MatButtonToggleModule"],
                    _angular_material_card__WEBPACK_IMPORTED_MODULE_14__["MatCardModule"],
                    _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_15__["MatCheckboxModule"],
                    _angular_material_chips__WEBPACK_IMPORTED_MODULE_16__["MatChipsModule"],
                    _angular_material_stepper__WEBPACK_IMPORTED_MODULE_17__["MatStepperModule"],
                    _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_18__["MatDatepickerModule"],
                    _angular_material_dialog__WEBPACK_IMPORTED_MODULE_19__["MatDialogModule"],
                    _angular_material_divider__WEBPACK_IMPORTED_MODULE_20__["MatDividerModule"],
                    _angular_material_expansion__WEBPACK_IMPORTED_MODULE_21__["MatExpansionModule"],
                    _angular_material_grid_list__WEBPACK_IMPORTED_MODULE_22__["MatGridListModule"],
                    _angular_material_icon__WEBPACK_IMPORTED_MODULE_23__["MatIconModule"],
                    _angular_material_input__WEBPACK_IMPORTED_MODULE_24__["MatInputModule"],
                    _angular_material_list__WEBPACK_IMPORTED_MODULE_25__["MatListModule"],
                    _angular_material_menu__WEBPACK_IMPORTED_MODULE_26__["MatMenuModule"],
                    _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatNativeDateModule"],
                    _angular_material_paginator__WEBPACK_IMPORTED_MODULE_28__["MatPaginatorModule"],
                    _angular_material_progress_bar__WEBPACK_IMPORTED_MODULE_29__["MatProgressBarModule"],
                    _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_30__["MatProgressSpinnerModule"],
                    _angular_material_radio__WEBPACK_IMPORTED_MODULE_31__["MatRadioModule"],
                    _angular_material_core__WEBPACK_IMPORTED_MODULE_27__["MatRippleModule"],
                    _angular_material_select__WEBPACK_IMPORTED_MODULE_32__["MatSelectModule"],
                    _angular_material_sidenav__WEBPACK_IMPORTED_MODULE_33__["MatSidenavModule"],
                    _angular_material_slider__WEBPACK_IMPORTED_MODULE_34__["MatSliderModule"],
                    _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_35__["MatSlideToggleModule"],
                    _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_36__["MatSnackBarModule"],
                    _angular_material_sort__WEBPACK_IMPORTED_MODULE_37__["MatSortModule"],
                    _angular_material_table__WEBPACK_IMPORTED_MODULE_38__["MatTableModule"],
                    _angular_material_tabs__WEBPACK_IMPORTED_MODULE_39__["MatTabsModule"],
                    _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_40__["MatToolbarModule"],
                    _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_41__["MatTooltipModule"],
                    _angular_material_tree__WEBPACK_IMPORTED_MODULE_42__["MatTreeModule"],
                    _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_43__["OverlayModule"],
                    _angular_cdk_portal__WEBPACK_IMPORTED_MODULE_4__["PortalModule"],
                    _angular_cdk_scrolling__WEBPACK_IMPORTED_MODULE_5__["ScrollingModule"],
                ]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/not-found/not-found.component.ts":
/*!**************************************************!*\
  !*** ./src/app/not-found/not-found.component.ts ***!
  \**************************************************/
/*! exports provided: NotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotFoundComponent", function() { return NotFoundComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class NotFoundComponent {
    constructor() { }
    ngOnInit() {
    }
}
NotFoundComponent.ɵfac = function NotFoundComponent_Factory(t) { return new (t || NotFoundComponent)(); };
NotFoundComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: NotFoundComponent, selectors: [["app-not-found"]], decls: 11, vars: 0, consts: [[1, "container"], [1, "boo-wrapper"], [1, "boo"], [1, "face"], [1, "shadow"]], template: function NotFoundComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Whoops!");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, " We couldn't find the page you ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "br");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, " were looking for. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, styles: ["@keyframes floating {\r\n    0% {\r\n        transform: translate3d(0, 0, 0);\r\n   }\r\n    45% {\r\n        transform: translate3d(0, -10%, 0);\r\n   }\r\n    55% {\r\n        transform: translate3d(0, -10%, 0);\r\n   }\r\n    100% {\r\n        transform: translate3d(0, 0, 0);\r\n   }\r\n}\r\n@keyframes floatingShadow {\r\n    0% {\r\n        transform: scale(1);\r\n   }\r\n    45% {\r\n        transform: scale(0.85);\r\n   }\r\n    55% {\r\n        transform: scale(0.85);\r\n   }\r\n    100% {\r\n        transform: scale(1);\r\n   }\r\n}\r\nbody[_ngcontent-%COMP%] {\r\n    background-color: #f7f7f7;\r\n}\r\n.container[_ngcontent-%COMP%] {\r\n    font-family: 'Varela Round', sans-serif;\r\n    color: #9b9b9b;\r\n    position: relative;\r\n    height: 100vh;\r\n    text-align: center;\r\n    font-size: 16px;\r\n}\r\n.container[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%] {\r\n    font-size: 32px;\r\n    margin-top: 32px;\r\n}\r\n.boo-wrapper[_ngcontent-%COMP%] {\r\n    width: 100%;\r\n    position: absolute;\r\n    top: 50%;\r\n    left: 50%;\r\n    transform: translate(-50%, -50%);\r\n    paddig-top: 64px;\r\n    paddig-bottom: 64px;\r\n}\r\n.boo[_ngcontent-%COMP%] {\r\n    width: 160px;\r\n    height: 184px;\r\n    background-color: #f7f7f7;\r\n    margin-left: auto;\r\n    margin-right: auto;\r\n    border: 3.3939393939px solid #9b9b9b;\r\n    border-bottom: 0;\r\n    overflow: hidden;\r\n    border-radius: 80px 80px 0 0;\r\n    box-shadow: -16px 0 0 2px rgba(234, 234, 234, .5) inset;\r\n    position: relative;\r\n    padding-bottom: 32px;\r\n    animation: floating 3s ease-in-out infinite;\r\n}\r\n.boo[_ngcontent-%COMP%]::after {\r\n    content: '';\r\n    display: block;\r\n    position: absolute;\r\n    left: -18.8235294118px;\r\n    bottom: -8.3116883117px;\r\n    width: calc(100% + 32px);\r\n    height: 32px;\r\n    background-repeat: repeat-x;\r\n    background-size: 32px 32px;\r\n    background-position: left bottom;\r\n    background-image: linear-gradient(-45deg, #f7f7f7 16px, transparent 0), linear-gradient(45deg, #f7f7f7 16px, transparent 0), linear-gradient(-45deg, #9b9b9b 18.8235294118px, transparent 0), linear-gradient(45deg, #9b9b9b 18.8235294118px, transparent 0);\r\n}\r\n.boo[_ngcontent-%COMP%]   .face[_ngcontent-%COMP%] {\r\n    width: 24px;\r\n    height: 3.2px;\r\n    border-radius: 5px;\r\n    background-color: #9b9b9b;\r\n    position: absolute;\r\n    left: 50%;\r\n    bottom: 56px;\r\n    transform: translateX(-50%);\r\n}\r\n.boo[_ngcontent-%COMP%]   .face[_ngcontent-%COMP%]::before, .boo[_ngcontent-%COMP%]   .face[_ngcontent-%COMP%]::after {\r\n    content: '';\r\n    display: block;\r\n    width: 6px;\r\n    height: 6px;\r\n    background-color: #9b9b9b;\r\n    border-radius: 50%;\r\n    position: absolute;\r\n    bottom: 40px;\r\n}\r\n.boo[_ngcontent-%COMP%]   .face[_ngcontent-%COMP%]::before {\r\n    left: -24px;\r\n}\r\n.boo[_ngcontent-%COMP%]   .face[_ngcontent-%COMP%]::after {\r\n    right: -24px;\r\n}\r\n.shadow[_ngcontent-%COMP%] {\r\n    width: 128px;\r\n    height: 16px;\r\n    background-color: rgba(234, 234, 234, .75);\r\n    margin-top: 40px;\r\n    margin-right: auto;\r\n    margin-left: auto;\r\n    border-radius: 50%;\r\n    animation: floatingShadow 3s ease-in-out infinite;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbm90LWZvdW5kL25vdC1mb3VuZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0k7UUFDSSwrQkFBK0I7R0FDcEM7SUFDQztRQUNJLGtDQUFrQztHQUN2QztJQUNDO1FBQ0ksa0NBQWtDO0dBQ3ZDO0lBQ0M7UUFDSSwrQkFBK0I7R0FDcEM7QUFDSDtBQUNBO0lBQ0k7UUFDSSxtQkFBbUI7R0FDeEI7SUFDQztRQUNJLHNCQUFzQjtHQUMzQjtJQUNDO1FBQ0ksc0JBQXNCO0dBQzNCO0lBQ0M7UUFDSSxtQkFBbUI7R0FDeEI7QUFDSDtBQUNBO0lBQ0kseUJBQXlCO0FBQzdCO0FBQ0E7SUFDSSx1Q0FBdUM7SUFDdkMsY0FBYztJQUNkLGtCQUFrQjtJQUNsQixhQUFhO0lBQ2Isa0JBQWtCO0lBQ2xCLGVBQWU7QUFDbkI7QUFDQTtJQUNJLGVBQWU7SUFDZixnQkFBZ0I7QUFDcEI7QUFDQTtJQUNJLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsUUFBUTtJQUNSLFNBQVM7SUFDVCxnQ0FBZ0M7SUFDaEMsZ0JBQWdCO0lBQ2hCLG1CQUFtQjtBQUN2QjtBQUNBO0lBQ0ksWUFBWTtJQUNaLGFBQWE7SUFDYix5QkFBeUI7SUFDekIsaUJBQWlCO0lBQ2pCLGtCQUFrQjtJQUNsQixvQ0FBb0M7SUFDcEMsZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQiw0QkFBNEI7SUFDNUIsdURBQXVEO0lBQ3ZELGtCQUFrQjtJQUNsQixvQkFBb0I7SUFDcEIsMkNBQTJDO0FBQy9DO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsY0FBYztJQUNkLGtCQUFrQjtJQUNsQixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLHdCQUF3QjtJQUN4QixZQUFZO0lBQ1osMkJBQTJCO0lBQzNCLDBCQUEwQjtJQUMxQixnQ0FBZ0M7SUFDaEMsNFBBQTRQO0FBQ2hRO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsYUFBYTtJQUNiLGtCQUFrQjtJQUNsQix5QkFBeUI7SUFDekIsa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxZQUFZO0lBQ1osMkJBQTJCO0FBQy9CO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsY0FBYztJQUNkLFVBQVU7SUFDVixXQUFXO0lBQ1gseUJBQXlCO0lBQ3pCLGtCQUFrQjtJQUNsQixrQkFBa0I7SUFDbEIsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksV0FBVztBQUNmO0FBQ0E7SUFDSSxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxZQUFZO0lBQ1osWUFBWTtJQUNaLDBDQUEwQztJQUMxQyxnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIsaURBQWlEO0FBQ3JEIiwiZmlsZSI6InNyYy9hcHAvbm90LWZvdW5kL25vdC1mb3VuZC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGtleWZyYW1lcyBmbG9hdGluZyB7XHJcbiAgICAwJSB7XHJcbiAgICAgICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUzZCgwLCAwLCAwKTtcclxuICAgfVxyXG4gICAgNDUlIHtcclxuICAgICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZTNkKDAsIC0xMCUsIDApO1xyXG4gICB9XHJcbiAgICA1NSUge1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwgLTEwJSwgMCk7XHJcbiAgIH1cclxuICAgIDEwMCUge1xyXG4gICAgICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlM2QoMCwgMCwgMCk7XHJcbiAgIH1cclxufVxyXG5Aa2V5ZnJhbWVzIGZsb2F0aW5nU2hhZG93IHtcclxuICAgIDAlIHtcclxuICAgICAgICB0cmFuc2Zvcm06IHNjYWxlKDEpO1xyXG4gICB9XHJcbiAgICA0NSUge1xyXG4gICAgICAgIHRyYW5zZm9ybTogc2NhbGUoMC44NSk7XHJcbiAgIH1cclxuICAgIDU1JSB7XHJcbiAgICAgICAgdHJhbnNmb3JtOiBzY2FsZSgwLjg1KTtcclxuICAgfVxyXG4gICAgMTAwJSB7XHJcbiAgICAgICAgdHJhbnNmb3JtOiBzY2FsZSgxKTtcclxuICAgfVxyXG59XHJcbmJvZHkge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2Y3ZjdmNztcclxufVxyXG4uY29udGFpbmVyIHtcclxuICAgIGZvbnQtZmFtaWx5OiAnVmFyZWxhIFJvdW5kJywgc2Fucy1zZXJpZjtcclxuICAgIGNvbG9yOiAjOWI5YjliO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgaGVpZ2h0OiAxMDB2aDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxufVxyXG4uY29udGFpbmVyIGgxIHtcclxuICAgIGZvbnQtc2l6ZTogMzJweDtcclxuICAgIG1hcmdpbi10b3A6IDMycHg7XHJcbn1cclxuLmJvby13cmFwcGVyIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiA1MCU7XHJcbiAgICBsZWZ0OiA1MCU7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgIHBhZGRpZy10b3A6IDY0cHg7XHJcbiAgICBwYWRkaWctYm90dG9tOiA2NHB4O1xyXG59XHJcbi5ib28ge1xyXG4gICAgd2lkdGg6IDE2MHB4O1xyXG4gICAgaGVpZ2h0OiAxODRweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNmN2Y3Zjc7XHJcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgIGJvcmRlcjogMy4zOTM5MzkzOTM5cHggc29saWQgIzliOWI5YjtcclxuICAgIGJvcmRlci1ib3R0b206IDA7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgYm9yZGVyLXJhZGl1czogODBweCA4MHB4IDAgMDtcclxuICAgIGJveC1zaGFkb3c6IC0xNnB4IDAgMCAycHggcmdiYSgyMzQsIDIzNCwgMjM0LCAuNSkgaW5zZXQ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMzJweDtcclxuICAgIGFuaW1hdGlvbjogZmxvYXRpbmcgM3MgZWFzZS1pbi1vdXQgaW5maW5pdGU7XHJcbn1cclxuLmJvbzo6YWZ0ZXIge1xyXG4gICAgY29udGVudDogJyc7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGxlZnQ6IC0xOC44MjM1Mjk0MTE4cHg7XHJcbiAgICBib3R0b206IC04LjMxMTY4ODMxMTdweDtcclxuICAgIHdpZHRoOiBjYWxjKDEwMCUgKyAzMnB4KTtcclxuICAgIGhlaWdodDogMzJweDtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiByZXBlYXQteDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogMzJweCAzMnB4O1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogbGVmdCBib3R0b207XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoLTQ1ZGVnLCAjZjdmN2Y3IDE2cHgsIHRyYW5zcGFyZW50IDApLCBsaW5lYXItZ3JhZGllbnQoNDVkZWcsICNmN2Y3ZjcgMTZweCwgdHJhbnNwYXJlbnQgMCksIGxpbmVhci1ncmFkaWVudCgtNDVkZWcsICM5YjliOWIgMTguODIzNTI5NDExOHB4LCB0cmFuc3BhcmVudCAwKSwgbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjOWI5YjliIDE4LjgyMzUyOTQxMThweCwgdHJhbnNwYXJlbnQgMCk7XHJcbn1cclxuLmJvbyAuZmFjZSB7XHJcbiAgICB3aWR0aDogMjRweDtcclxuICAgIGhlaWdodDogMy4ycHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjOWI5YjliO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbGVmdDogNTAlO1xyXG4gICAgYm90dG9tOiA1NnB4O1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpO1xyXG59XHJcbi5ib28gLmZhY2U6OmJlZm9yZSwgLmJvbyAuZmFjZTo6YWZ0ZXIge1xyXG4gICAgY29udGVudDogJyc7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHdpZHRoOiA2cHg7XHJcbiAgICBoZWlnaHQ6IDZweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICM5YjliOWI7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MCU7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBib3R0b206IDQwcHg7XHJcbn1cclxuLmJvbyAuZmFjZTo6YmVmb3JlIHtcclxuICAgIGxlZnQ6IC0yNHB4O1xyXG59XHJcbi5ib28gLmZhY2U6OmFmdGVyIHtcclxuICAgIHJpZ2h0OiAtMjRweDtcclxufVxyXG4uc2hhZG93IHtcclxuICAgIHdpZHRoOiAxMjhweDtcclxuICAgIGhlaWdodDogMTZweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjM0LCAyMzQsIDIzNCwgLjc1KTtcclxuICAgIG1hcmdpbi10b3A6IDQwcHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIGFuaW1hdGlvbjogZmxvYXRpbmdTaGFkb3cgM3MgZWFzZS1pbi1vdXQgaW5maW5pdGU7XHJcbn1cclxuIl19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NotFoundComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-not-found',
                templateUrl: './not-found.component.html',
                styleUrls: ['./not-found.component.css']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/security/token-interceptor.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/security/token-interceptor.service.ts ***!
  \*******************************************************/
/*! exports provided: TokenInterceptorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenInterceptorService", function() { return TokenInterceptorService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/token-storage.service */ "./src/app/services/token-storage.service.ts");



;
class TokenInterceptorService {
    constructor(token) {
        this.token = token;
    }
    intercept(request, next) {
        request = request.clone({
            setHeaders: {
                Authorization: `Bearer ${this.token.getToken()}`
            }
        });
        return next.handle(request);
    }
}
TokenInterceptorService.ɵfac = function TokenInterceptorService_Factory(t) { return new (t || TokenInterceptorService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"])); };
TokenInterceptorService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: TokenInterceptorService, factory: TokenInterceptorService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TokenInterceptorService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _services_token_storage_service__WEBPACK_IMPORTED_MODULE_1__["TokenStorageService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/services/cpn/auth.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/cpn/auth.service.ts ***!
  \**********************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _baseUrl__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../baseUrl */ "./src/app/baseUrl.ts");






const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]();
headers.append('Content-Type', 'multipart/form-data');
headers.append('Accept', 'application/json');
class AuthService {
    constructor(http) {
        this.http = http;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]().set('Content-Type', 'application/json');
        this.previousUrl = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](null);
        this.previousUrl$ = this.previousUrl.asObservable();
    }
    setPreviousUrl(previousUrl) {
        this.previousUrl.next(previousUrl);
    }
    register(form) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_3__["baseUrl"] + '/api/inscription', form, { headers, withCredentials: false });
    }
    login(form) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_3__["baseUrl"] + '/api/login', form, { headers, withCredentials: false });
    }
    sendMail(mail) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_3__["baseUrl"] + '/api/forgot-password', mail, { headers, withCredentials: false });
    }
    resetPass(form) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_3__["baseUrl"] + '/api/reset-password', form, { headers, withCredentials: false });
    }
    getUser() {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_3__["baseUrl"] + '/api/profile', { headers, withCredentials: false });
    }
    updatUser(form) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_3__["baseUrl"] + '/api/update-profile', form, { headers, withCredentials: false });
    }
    getFellower() {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_3__["baseUrl"] + '/api/linkd', { headers, withCredentials: false });
    }
}
AuthService.ɵfac = function AuthService_Factory(t) { return new (t || AuthService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"])); };
AuthService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: AuthService, factory: AuthService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "./src/app/services/cpn/avis.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/cpn/avis.service.ts ***!
  \**********************************************/
/*! exports provided: AvisService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AvisService", function() { return AvisService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");




const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]();
headers.append('Content-Type', 'multipart/form-data');
headers.append('Accept', 'application/json');
class AvisService {
    constructor(http) {
        this.http = http;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]().set('Content-Type', 'application/json');
        this.url = "http://crm.cpn-aide-aux-entreprises.com";
    }
    /*********************** avis/add ****************************/
    addAvis(form) {
        return this.http.post(this.url + "/api/avis/save", form, { headers, withCredentials: false });
    }
}
AvisService.ɵfac = function AvisService_Factory(t) { return new (t || AvisService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"])); };
AvisService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: AvisService, factory: AvisService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](AvisService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "./src/app/services/cpn/test-egibilite.service.ts":
/*!********************************************************!*\
  !*** ./src/app/services/cpn/test-egibilite.service.ts ***!
  \********************************************************/
/*! exports provided: TestEgibiliteService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestEgibiliteService", function() { return TestEgibiliteService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _baseUrl__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../baseUrl */ "./src/app/baseUrl.ts");





const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]();
headers.append('Content-Type', 'multipart/form-data');
headers.append('Accept', 'application/json');
class TestEgibiliteService {
    constructor(http) {
        this.http = http;
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpHeaders"]().set('Content-Type', 'application/json');
    }
    /*********************** test/activities/get ****************************/
    getActivites() {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/activities/get");
    }
    /*********************** test/transitions/get ****************************/
    getTransitions() {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/transitions/get");
    }
    /*********************** test/grants/region/get ****************************/
    regionalGrant(region, budget, naf) {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/grants/region/" + region + "/" + budget + "/" + naf);
    }
    /*********************** test/grants/cpn/get ****************************/
    cpnGrant(service, budget) {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/grants/cpn/" + service + "/" + budget);
    }
    /*********************** test/events/get ****************************/
    getEvents() {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/events/get");
    }
    /*********************** test/events/add ****************************/
    addEvents(form) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/events/add", form);
    }
    /*********************** test/service/turnover ****************************/
    getServiceTurnover(range) {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/service/turnover/" + range[0] + "/" + range[1], { withCredentials: false });
    }
    /*********************** test/company/siren ****************************/
    getCompanySiren(siret) {
        return this.http.get(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/company/siren/" + siret, { withCredentials: false });
    }
    /*********************** test/contact/save ****************************/
    addContact(form) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/contact/save", form, { withCredentials: false });
    }
    /*********************** test/contact/confirm ****************************/
    contactConfirm(form) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/contact/confirm", form, { withCredentials: false });
    }
    /*********************** test/zoom/generate ****************************/
    addZoom(form) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/zoom/generate", form, { withCredentials: false });
    }
    /*********************** test/timer/save ****************************/
    addTimer(form) {
        return this.http.post(_baseUrl__WEBPACK_IMPORTED_MODULE_2__["baseUrl"] + "/api/test/timer/save", form, { withCredentials: false });
    }
}
TestEgibiliteService.ɵfac = function TestEgibiliteService_Factory(t) { return new (t || TestEgibiliteService)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"])); };
TestEgibiliteService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({ token: TestEgibiliteService, factory: TestEgibiliteService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](TestEgibiliteService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return [{ type: _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"] }]; }, null); })();


/***/ }),

/***/ "./src/app/services/token-storage.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/token-storage.service.ts ***!
  \***************************************************/
/*! exports provided: TokenStorageService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenStorageService", function() { return TokenStorageService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';
const projet = 'projet';
class TokenStorageService {
    constructor() { }
    signOut() {
        window.sessionStorage.clear();
    }
    saveToken(token) {
        window.sessionStorage.removeItem(TOKEN_KEY);
        window.sessionStorage.setItem(TOKEN_KEY, token);
    }
    getToken() {
        return window.sessionStorage.getItem(TOKEN_KEY);
    }
    getUSERKEY() {
        return window.sessionStorage.getItem(USER_KEY);
    }
    saveUser(user) {
        window.sessionStorage.removeItem(USER_KEY);
        window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
    }
    saveProjectId(id) {
        window.sessionStorage.setItem(projet, id);
    }
    getProjectId() {
        window.sessionStorage.getItem(projet);
    }
    getUser() {
        if (window.sessionStorage.getItem(USER_KEY)) {
            const user = window.sessionStorage.getItem(USER_KEY);
            if (user) {
                return user;
            }
        }
        return false;
    }
}
TokenStorageService.ɵfac = function TokenStorageService_Factory(t) { return new (t || TokenStorageService)(); };
TokenStorageService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: TokenStorageService, factory: TokenStorageService.ɵfac, providedIn: 'root' });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TokenStorageService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/test/test.component.ts":
/*!****************************************!*\
  !*** ./src/app/test/test.component.ts ***!
  \****************************************/
/*! exports provided: TestComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TestComponent", function() { return TestComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/table.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _services_cpn_test_egibilite_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/cpn/test-egibilite.service */ "./src/app/services/cpn/test-egibilite.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/button */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/button.js");
/* harmony import */ var _map_french_map_french_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../map-french/map-french.component */ "./src/app/map-french/map-french.component.ts");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/__ivy_ngcc__/fesm2015/ng-select-ng-select.js");
/* harmony import */ var _angular_slider_ngx_slider__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular-slider/ngx-slider */ "./node_modules/@angular-slider/ngx-slider/__ivy_ngcc__/fesm2015/angular-slider-ngx-slider.js");
/* harmony import */ var _angular_material_radio__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/radio */ "./node_modules/@angular/material/__ivy_ngcc__/fesm2015/radio.js");
/* harmony import */ var _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @fullcalendar/angular */ "./node_modules/@fullcalendar/angular/__ivy_ngcc__/fesm2015/fullcalendar-angular.js");















function TestComponent_div_3_Template(rf, ctx) { if (rf & 1) {
    const _r23 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 40);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 42);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Testez votre \u00E9ligibilit\u00E9");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_3_Template_button_click_6_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r23); const ctx_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r22.checkForm(ctx_r22.test.active.step); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Commencer");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_4_Template(rf, ctx) { if (rf & 1) {
    const _r25 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 45);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "app-map-french", 46);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("myOutput", function TestComponent_div_4_Template_app_map_french_myOutput_2_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r25); const ctx_r24 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r24.GetChildData($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Choisissez votre Code postal");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 47);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("myinputDep", ctx_r1.testEgibFormGroup.get("codeP").value);
} }
function TestComponent_div_5_Template(rf, ctx) { if (rf & 1) {
    const _r27 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 48);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 49);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Quel est votre secteur d'activit\u00E9 ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "fieldset", 35);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "ng-select", 50);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function TestComponent_div_5_Template_ng_select_change_7_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r27); const ctx_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r26.onChange($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("items", ctx_r2.activities == null ? null : ctx_r2.activities.data);
} }
const _c0 = function (a0) { return { butttonREd: a0 }; };
function TestComponent_div_6_Template(rf, ctx) { if (rf & 1) {
    const _r29 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 51);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 52);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Status juridique");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "button", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_6_Template_button_click_8_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r29); const ctx_r28 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r28.getstatus("SARL"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "SARL");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "button", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_6_Template_button_click_10_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r29); const ctx_r30 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r30.getstatus("SAS"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "SAS");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "button", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_6_Template_button_click_13_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r29); const ctx_r31 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r31.getstatus("SASU"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "SASU");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "button", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_6_Template_button_click_15_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r29); const ctx_r32 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r32.getstatus("EURL"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "EURL");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "button", 55);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_6_Template_button_click_17_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r29); const ctx_r33 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r33.getstatus("MICRO-ENT"); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "MICRO-ENT");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](5, _c0, ctx_r3.testEgibFormGroup.get("status").value === "SARL"));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](7, _c0, ctx_r3.testEgibFormGroup.get("status").value === "SAS"));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](9, _c0, ctx_r3.testEgibFormGroup.get("status").value === "SASU"));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](11, _c0, ctx_r3.testEgibFormGroup.get("status").value === "EURL"));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](13, _c0, ctx_r3.testEgibFormGroup.get("status").value === "MICRO-ENT"));
} }
function TestComponent_div_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 56);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 57);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Nom de votre entreprise");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 58);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c1 = function (a0) { return { baissD: a0 }; };
const _c2 = function (a0) { return { augmD: a0 }; };
function TestComponent_div_8_Template(rf, ctx) { if (rf & 1) {
    const _r37 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 59);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 60);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Avez vous perdu du chiffre d'affaires pendant la crise sanitaire ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 53);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 61);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_8_Template_div_click_8_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r37); const ctx_r36 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r36.changeEtatB(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Baisse");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 62);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](12, "input", 63, 64);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_8_Template_button_click_15_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r37); const ctx_r38 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r38.incTurn(10, 0 - 100, 0); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "+");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_8_Template_button_click_17_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r37); const ctx_r39 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r39.decTurn(10, 0 - 100, 0); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "-");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 61);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_8_Template_div_click_20_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r37); const ctx_r40 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r40.changeEtatA(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Augmentation");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 66);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](24, "input", 63, 64);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_8_Template_button_click_27_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r37); const ctx_r41 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r41.incTurn(10, 0, 100); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, "+");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_8_Template_button_click_29_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r37); const ctx_r42 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r42.decTurn(10, 0, 100); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "-");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "input", 67);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "label", 68);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Etat Stable");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](4, _c1, ctx_r5.showB === true));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("readonly", true);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](6, _c2, ctx_r5.showA === true));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("readonly", true);
} }
function TestComponent_div_9_option_7_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option", 73);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r44 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r44);
} }
function TestComponent_div_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 69);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Avez vous d\u00E9ja obtenu des aides de l'\u00E9tat");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 70);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "select", 71);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, TestComponent_div_9_option_7_Template, 2, 1, "option", 72);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r6.test.data[ctx_r6.test.active.step].options);
} }
function TestComponent_div_10_Template(rf, ctx) { if (rf & 1) {
    const _r46 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 74);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 75);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 76);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Dernier chiffre d'affaires r\u00E9alis\u00E9 ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 77);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 78);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "ngx-slider", 79);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("valueChange", function TestComponent_div_10_Template_ngx_slider_valueChange_9_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r46); const ctx_r45 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r45.minValue = $event; })("highValueChange", function TestComponent_div_10_Template_ngx_slider_highValueChange_9_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r46); const ctx_r47 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r47.maxValue = $event; })("userChangeStart", function TestComponent_div_10_Template_ngx_slider_userChangeStart_9_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r46); const ctx_r48 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r48.onUserChangeStart($event); })("userChange", function TestComponent_div_10_Template_ngx_slider_userChange_9_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r46); const ctx_r49 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r49.onUserChange($event); })("userChangeEnd", function TestComponent_div_10_Template_ngx_slider_userChangeEnd_9_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r46); const ctx_r50 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r50.onUserChangeEnd($event); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx_r7.minValue)("highValue", ctx_r7.maxValue)("options", ctx_r7.options);
} }
function TestComponent_div_11_option_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r52 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r52);
} }
function TestComponent_div_11_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 80);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 81);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Nombre de salari\u00E9s ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "select", 82);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "select les nombres salaries");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, TestComponent_div_11_option_9_Template, 2, 1, "option", 83);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r8.test.data[ctx_r8.test.active.step].options);
} }
function TestComponent_div_12_div_1_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 89);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 90);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Avez vous un site internet pour votre entreprise ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-radio-group", 91);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-radio-button", 92);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Oui");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-radio-button", 93);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Non");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_1_div_2_Template(rf, ctx) { if (rf & 1) {
    const _r64 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 94);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 95);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Quel est votre type de site internet ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-radio-group", 96);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-radio-button", 97);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "E-commerce");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-radio-button", 98);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Vitrine");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "mat-radio-button", 99);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Market-place");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 100);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Nombre de Vente");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 101);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "input", 102, 103);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_12_div_1_div_2_Template_button_click_21_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r64); const ctx_r63 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r63.incVente(10, 0, 100); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "+");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_12_div_1_div_2_Template_button_click_23_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r64); const ctx_r65 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r65.decVente(10, 0, 100); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "-");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "Nombre de Visite");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 101);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "input", 104, 105);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_12_div_1_div_2_Template_button_click_32_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r64); const ctx_r66 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r66.incVisite(10, 0, 100); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "+");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_12_div_1_div_2_Template_button_click_34_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r64); const ctx_r67 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r67.decVisite(10, 0, 100); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "-");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 54);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "Nombre d'utilisateurs");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 101);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](40, "input", 106, 107);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 65);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_12_div_1_div_2_Template_button_click_43_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r64); const ctx_r68 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r68.incUser(10, 0, 100); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "+");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "button", 44);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_12_div_1_div_2_Template_button_click_45_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r64); const ctx_r69 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3); return ctx_r69.decUser(10, 0, 100); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "-");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("readonly", true);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("readonly", true);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("readonly", true);
} }
function TestComponent_div_12_div_1_div_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 108);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 109);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Lien du site ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 110);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_1_div_4_option_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r71 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r71);
} }
function TestComponent_div_12_div_1_div_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 111);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "D\u00E2te de d\u00E9veloppement ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 112);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "select", 113);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "choisir un date ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, TestComponent_div_12_div_1_div_4_option_9_Template, 2, 1, "option", 83);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r58 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r58.test.data[ctx_r58.test.active.step].website[ctx_r58.test.active.subStep].options);
} }
function TestComponent_div_12_div_1_div_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 114);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 109);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "L'agence qui a d\u00E9velopp\u00E9 votre site ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 115);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-radio-button", 116);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Internet/Freelance");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TestComponent_div_12_div_1_div_1_Template, 11, 0, "div", 84);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TestComponent_div_12_div_1_div_2_Template, 47, 3, "div", 85);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, TestComponent_div_12_div_1_div_3_Template, 7, 0, "div", 86);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, TestComponent_div_12_div_1_div_4_Template, 10, 1, "div", 87);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, TestComponent_div_12_div_1_div_5_Template, 9, 0, "div", 88);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r53 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r53.test.active.subStep == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r53.test.active.subStep == 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r53.test.active.subStep == 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r53.test.active.subStep == 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r53.test.active.subStep == 5);
} }
function TestComponent_div_12_div_2_div_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 121);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 122);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Utilisez-vous des Plate-formes en ligne ou des logiciels pour faciliter vos ventes ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-radio-group", 123);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-radio-button", 92);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Oui");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-radio-button", 93);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Non");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_2_div_2_option_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option", 127);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 128);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r78 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r78);
} }
function TestComponent_div_12_div_2_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 124);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Quels types d'outils utilisez-vous pour vos ventes ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "(Exemple:un CRM)");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "select", 125);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "choisir un crm");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, TestComponent_div_12_div_2_div_2_option_10_Template, 3, 1, "option", 126);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r73 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r73.test.data[ctx_r73.test.active.step].crm[ctx_r73.test.active.subStep].options);
} }
function TestComponent_div_12_div_2_div_3_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 129);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 130);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Utilisez-vous des Plate-formes en ligne ou des logiciels pour faciliter votre logistique interne ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "mat-radio-group", 131);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "mat-radio-button", 92);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Oui");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-radio-button", 93);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Non");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_12_div_2_div_4_option_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option", 127);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 128);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r80 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r80);
} }
function TestComponent_div_12_div_2_div_4_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 132);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Quels types d'outils utilisez-vous pour votre logistique ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "(Exemple:un ERP)");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "select", 133);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "choisir un erp");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, TestComponent_div_12_div_2_div_4_option_10_Template, 3, 1, "option", 126);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r75 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r75.test.data[ctx_r75.test.active.step].crm[ctx_r75.test.active.subStep].options);
} }
function TestComponent_div_12_div_2_div_5_option_9_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const item_r82 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r82);
} }
function TestComponent_div_12_div_2_div_5_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 111);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "D\u00E2te de d\u00E9veloppement ?");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 112);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "select", 134);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "choisir un date ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, TestComponent_div_12_div_2_div_5_option_9_Template, 2, 1, "option", 83);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r76 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r76.test.data[ctx_r76.test.active.step].crm[ctx_r76.test.active.subStep].options);
} }
function TestComponent_div_12_div_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TestComponent_div_12_div_2_div_1_Template, 11, 0, "div", 117);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TestComponent_div_12_div_2_div_2_Template, 11, 1, "div", 118);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, TestComponent_div_12_div_2_div_3_Template, 11, 0, "div", 119);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, TestComponent_div_12_div_2_div_4_Template, 11, 1, "div", 120);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, TestComponent_div_12_div_2_div_5_Template, 10, 1, "div", 87);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r54 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r54.test.active.subStep == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r54.test.active.subStep == 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r54.test.active.subStep == 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r54.test.active.subStep == 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r54.test.active.subStep == 5);
} }
function TestComponent_div_12_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, TestComponent_div_12_div_1_Template, 6, 5, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, TestComponent_div_12_div_2_Template, 6, 5, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r9.test.active.subStepCat == 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r9.test.active.subStepCat == 2);
} }
function TestComponent_div_13_div_9_Template(rf, ctx) { if (rf & 1) {
    const _r86 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 142);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 143);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 144);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "i", 145);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 146);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 147);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "input", 148);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_13_div_9_Template_input_click_8_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r86); const items_r84 = ctx.$implicit; const ctx_r85 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2); return ctx_r85.selectService(items_r84 == null ? null : items_r84.id); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const items_r84 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](items_r84 == null ? null : items_r84.name);
} }
function TestComponent_div_13_Template(rf, ctx) { if (rf & 1) {
    const _r88 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 135);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 136);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "label", 137);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Service");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "input", 138);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup", function TestComponent_div_13_Template_input_keyup_6_listener($event) { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r88); const ctx_r87 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r87.valuechange($event.target.value); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "i", 139);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 140);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, TestComponent_div_13_div_9_Template, 9, 1, "div", 141);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r10.dataSource.filteredData);
} }
function TestComponent_div_14_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 149);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Budget d'investissement");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 70);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "select", 150);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "option");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "choisir un budget");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "option", 151);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "300$");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "option", 152);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "400$");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "option", 153);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "500$");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "option", 154);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "600$");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_15_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 155);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 156);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Num\u00E9ro de Siret");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 157);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_16_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 158);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 159);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Adresse");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 144);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "label", 137);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Adresse");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "input", 160);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 146);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "label", 137);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "ville");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "input", 161);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "label", 137);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "code postal");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "input", 162);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("readonly", true);
} }
function TestComponent_div_17_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 163);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Fiche de rensignement");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "img", 164);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 144);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "label", 137);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Nom");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "input", 165);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 144);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "label", 137);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Pr\u00E9nom");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "input", 166);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 144);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "label", 137);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "mail");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "input", 167);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 144);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "label", 137);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "Num\u00E9ro de portable");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "input", 168);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 144);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "label", 137);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "Num\u00E9ro d'entreprise");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "input", 169);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 146);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "mat-radio-group", 170);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "mat-radio-button", 171);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "G\u00E9rant");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "mat-radio-button", 172);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Associ\u00E9");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "mat-radio-button", 173);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, "Dir\u00E9cteur");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "mat-radio-button", 174);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "autre");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_18_p_13_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "de ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "strong", 181);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r89 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", ctx_r89.test.result.cpn.amount, " \u20AC");
} }
function TestComponent_div_18_Template(rf, ctx) { if (rf & 1) {
    const _r91 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 175);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 176);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "F\u00E9licitaion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "vous \u00EAtes \u00E9ligible");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "img", 177);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Vous b\u00E9neficiez d'un ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "strong");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Ch\u00E8que Num\u00E9rique");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, TestComponent_div_18_p_13_Template, 4, 1, "p", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "offert par le ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "strong");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "CPN");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "img", 178);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 179);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_18_Template_div_click_19_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r91); const ctx_r90 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r90.showResult(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](20, "img", 180);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r15.test.result.cpn.amount);
} }
function TestComponent_div_19_Template(rf, ctx) { if (rf & 1) {
    const _r93 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 175);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 176);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "F\u00E9licitaion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "vous \u00EAtes \u00E9ligible");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "img", 177);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Vous b\u00E9neficiez d'un ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "strong");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Ch\u00E8que commerce connect\u00E9");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "de ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "strong", 181);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "offert par le ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "strong");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20, "CPN");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "img", 178);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 179);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_19_Template_div_click_22_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r93); const ctx_r92 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r92.elgiblTest(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "img", 180);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r16 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", ctx_r16.test.result.regional.amount, " $");
} }
function TestComponent_div_20_Template(rf, ctx) { if (rf & 1) {
    const _r95 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 175);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h1", 182);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Malheureusement");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h1", 182);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "vous n'\u00EAtes pas \u00E9ligible");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "img", 183);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "\u00E0 l'aide de votre r\u00E9gion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 179);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_20_Template_div_click_10_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r95); const ctx_r94 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r94.elgiblTest(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "img", 180);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
function TestComponent_div_21_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 184);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "img", 185);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h1");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Vos disponibilit\u00E9s");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 186);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "full-calendar", 187, 188);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "mat-radio-group", 189);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "mat-radio-button", 190);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Entretien vid\u00E9o direct");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "mat-radio-button", 191);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Visite de courtoisie");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("options", ctx_r18.calendarOption);
} }
function TestComponent_div_22_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 192);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 41);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 43);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "img", 193);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "F\u00E9licitaion");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Un conseiller entrera en contact avec");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, " vous dans 30min");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "br");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 194);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "p");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Suivez-nous");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 195);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](16, "img", 196);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "img", 197);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "img", 198);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "img", 199);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} }
const _c3 = function (a0) { return { checkIcon: a0 }; };
function TestComponent_div_27_Template(rf, ctx) { if (rf & 1) {
    const _r98 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 200);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "a", 201);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_27_Template_a_click_1_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r98); const ctx_r97 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r97.prevStep(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "i", 202);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 203);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](13, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](17, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](19, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](23, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "a");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "i", 204);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "a", 201);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function TestComponent_div_27_Template_a_click_36_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r98); const ctx_r99 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r99.checkForm(ctx_r99.test.active.step); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "i", 205);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](16, _c3, ctx_r20.test.active.step == 1));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](18, _c3, ctx_r20.test.active.step == 2));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](20, _c3, ctx_r20.test.active.step == 3));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](22, _c3, ctx_r20.test.active.step == 4));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](24, _c3, ctx_r20.test.active.step == 5));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](26, _c3, ctx_r20.test.active.step == 6));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](28, _c3, ctx_r20.test.active.step == 7));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](30, _c3, ctx_r20.test.active.step == 8));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](32, _c3, ctx_r20.test.active.step == 9));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](34, _c3, ctx_r20.test.active.step == 10));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](36, _c3, ctx_r20.test.active.step == 11));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](38, _c3, ctx_r20.test.active.step == 12));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](40, _c3, ctx_r20.test.active.step == 13));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](42, _c3, ctx_r20.test.active.step == 14));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](44, _c3, ctx_r20.test.active.step == 15));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](46, _c3, ctx_r20.test.active.step == 16));
} }
class TestComponent {
    /***********************************life cycle *******************/
    constructor(_formBuilder, testService) {
        this._formBuilder = _formBuilder;
        this.testService = testService;
        /***************************all variable **************************/
        this.myInputDepartment = "03";
        this.i = 9;
        this.transition = [];
        this.onChange = ($event) => {
            console.log('activitie', this.testEgibFormGroup.value.activite);
            console.log(`SELECTION CHANGED INTO ${$event.name || ''}`);
        };
        /***********************select transition ******************/
        this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"]();
        /**********************************************turnover ******************************************/
        this.turn = 0;
        this.showA = false;
        this.showB = false;
        /**********************************************nombre visite ******************************************/
        this.visite = 0;
        /********************************************** nombre vente ******************************************/
        this.vente = 0;
        /**********************************************nombre user ******************************************/
        this.users = 0;
        /****************************************************** resultat de test **************************************/
        this.eleg = null;
        /*****************************************test step **************************************/
        this.test = {
            active: {
                step: 0,
                subStep: 1,
                subStepCat: 0,
                stepType: "form",
                popup: false,
                confirmed: false,
            },
            result: {
                isOpen: false,
                isLoading: false,
                isCpn: true,
                regional: {
                    id: null,
                    region: null,
                    eligible: false,
                    voucher: null,
                    amount: null,
                },
                cpn: {
                    id: null,
                    amount: null,
                    originalPrice: null,
                    sellPrice: null,
                },
            },
            zoom: {
                generating: false,
                generated: false,
            },
            orientations: [],
            data: [
                {
                    step: 0,
                    title: "Bienvenue"
                },
                {
                    step: 1,
                    title: "Renseigner le code postal",
                },
                {
                    step: 2,
                    title: "Nom de l'entreprise"
                },
                {
                    step: 3,
                    title: "Statut juridique"
                },
                {
                    step: 4,
                    title: "Secteur d'activité",
                    options: [],
                },
                {
                    step: 5,
                    title: "Avez vous perdu du chiffre d'affaires pendant la crise sanitaire",
                    labels: [
                        "Baisse",
                        "",
                        "",
                        "",
                        "",
                        "-50%",
                        "",
                        "",
                        "",
                        "",
                        "Stable",
                        "",
                        "",
                        "",
                        "",
                        "50%",
                        "",
                        "",
                        "",
                        "",
                        "Hausse"
                    ],
                },
                {
                    step: 6,
                    title: "Avez vous déja obtenu des aides de l'état",
                    options: [
                        "Chéque numérique et aide numérique de votre région",
                        "Crédit d'impôt",
                        "Fond de solidarité",
                        "Chaumage partiel",
                        "Aucune aide",
                    ],
                },
                {
                    step: 7,
                    title: "Dernier chiffre d'affaires réalisé",
                    labels: [
                        "5k €",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "700k €",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "3.5m €",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "7.5m €",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "30m €",
                    ],
                    selectedRange: [0, 1],
                    range: [
                        { value: 5000, legend: "5k €" },
                        { value: 50000 },
                        { value: 100000 },
                        { value: 200000 },
                        { value: 300000 },
                        { value: 400000 },
                        { value: 500000 },
                        { value: 600000 },
                        { value: 700000, legend: "700k €" },
                        { value: 800000 },
                        { value: 900000 },
                        { value: 1000000 },
                        { value: 1500000 },
                        { value: 2000000 },
                        { value: 2500000 },
                        { value: 3000000 },
                        { value: 3500000, legend: "3.5m €" },
                        { value: 4000000 },
                        { value: 4500000 },
                        { value: 5000000 },
                        { value: 5500000 },
                        { value: 6000000 },
                        { value: 6500000 },
                        { value: 7000000 },
                        { value: 7500000, legend: "7.5m €" },
                        { value: 8000000 },
                        { value: 8500000 },
                        { value: 9000000 },
                        { value: 9500000 },
                        { value: 10000000 },
                        { value: 15000000 },
                        { value: 20000000 },
                        { value: 25000000 },
                        { value: 30000000, legend: "30m €" },
                    ]
                },
                {
                    step: 8,
                    title: "Nombre de salariés",
                    options: [
                        "de 0 à 5 Personnes",
                        "de 5 à 10 Personnes",
                        "de 10 à 20 Personnes",
                        "de 20 à 30 Personnes",
                        "de 30 à 40 Personnes",
                        "de 40 à 50 Personnes",
                        "plus de 50 Personnes",
                    ],
                },
                {
                    step: 9,
                    title: "Type de site",
                    website: [
                        {
                            subStep: 0,
                            title: ""
                        },
                        {
                            subStep: 1,
                            title: "Avez vous un site internet pour votre entreprise"
                        },
                        {
                            subStep: 2,
                            title: "Type de site"
                        },
                        {
                            subStep: 3,
                            title: "Lien de site"
                        },
                        {
                            subStep: 4,
                            title: "Date de développement",
                            options: [
                                "Avant 2000",
                                "Année 2000-2003",
                                "Année 2003-2006",
                                "Année 2006-2009",
                                "Année 2009-2012",
                                "Année 2012-2015",
                                "Année 2015-2018",
                                "Année 2018-2021",
                            ]
                        },
                        {
                            subStep: 5,
                            title: "L'agence qui a développé votre site"
                        }
                    ],
                    crm: [
                        {
                            subStep: 0,
                            title: ""
                        },
                        {
                            subStep: 1,
                            title: "Avez vous un crm pour votre entreprise"
                        },
                        {
                            subStep: 2,
                            title: "Quel type de CRM vous utilisez",
                            options: [
                                "Zoho",
                                "SAP",
                                "Sage",
                                "Oracle",
                                "NetSuite",
                                "Cegid",
                                "Microsoft Dynamics",
                                "Divalto",
                                "WaveSoft",
                                "Odoo",
                                "Archipelia",
                                "Axonaut",
                            ]
                        },
                        {
                            subStep: 3,
                            title: "Le crm a été développé"
                        },
                        {
                            subStep: 4,
                            title: "Quel type de ERP vous utilisez",
                            options: [
                                "Zoho",
                                "SAP",
                                "Sage",
                                "Oracle",
                                "NetSuite",
                                "Cegid",
                                "Microsoft Dynamics",
                                "Divalto",
                                "WaveSoft",
                                "Odoo",
                                "Archipelia",
                                "Axonaut",
                            ]
                        },
                        {
                            subStep: 5,
                            title: "Date de développement",
                            options: [
                                "Avant 2000",
                                "Année 2000-2003",
                                "Année 2003-2006",
                                "Année 2006-2009",
                                "Année 2009-2012",
                                "Année 2012-2015",
                                "Année 2015-2018",
                                "Année 2018-2021",
                            ]
                        }
                    ]
                },
                {
                    step: 10,
                    title: "Quel projet est à subventionner pour votre transition numérique",
                    tabServices: null,
                    loading: false,
                    services: ["Services éligible", "Services suplémentaire"],
                    tabCategories: null,
                    categories: ["Tous", "Graphique", "Développement", "Montage", "Marketing"],
                    options: [],
                },
                {
                    step: 11,
                    title: "Budget d'investissement",
                    budget: 5,
                    min: 400,
                    target: 500,
                    max: 100000,
                },
                {
                    step: 12,
                    title: "Numéros d'identification",
                    loading: false,
                },
                {
                    step: 13,
                    title: "Adresse",
                },
                {
                    step: 14,
                    title: "Fiche de renseignement",
                    options: [
                        "Gérant",
                        "Directeur",
                        "Associé",
                        "Autre"
                    ],
                },
                {
                    step: 15,
                    title: "Vos disponibilités",
                },
                {
                    step: 16,
                    title: "Type de client",
                    items: ['☹️', '🙁', '😐', '🙂', '😊', '😍'],
                    labels: [
                        "agressif",
                        "indécis",
                        "anxieux",
                        "économe",
                        "compréhensif",
                        "roi",
                    ]
                },
                {
                    step: 17,
                    title: "Merci pour votre temps",
                },
            ],
        };
        this.min = 0;
        this.max = 0;
        this.maxValue = 80;
        this.minValue = this.min;
        this.options = {
            showTicks: true,
            draggableRangeOnly: false,
            stepsArray: this.test.data[7].range,
            translate: (value, label) => {
                console.log('stepprec', value);
                return (value / 1000 > 900) ? ((value / 1000) / 1000).toFixed(1) + "m €" : (value / 1000).toFixed(0) + "k €";
            }
        };
        this.logText = '';
        this.range = [];
        /*************************form  data contact************************/
        this.testEgibFormGroup = this._formBuilder.group({
            codeP: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("[0-9 ]{5}")]],
            nomSoc: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            activite: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            status: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            help: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            Nvente: ['0%', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            Nvisite: ['0%', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            Nuser: ['0%', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            personneSal: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            turnover: [0, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            lastTurnover: [0, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            haveSite: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            haveCrm: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            haveErp: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            liensite: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            datesite: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            siteVal: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            dateCrm: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            nomCrm: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            nomErp: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            typeCRM: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            typeERP: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            typeSite: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            crmDev: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            agence: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            budget: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            service: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            siret: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].pattern("[0-9 ]{14}")]],
            siren: [''],
            naf: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            adresse: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            region: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            city: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            country: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            prenom: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            nom: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            phone: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            departement: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            phoneEntrep: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            post: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            contactID: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            meetingType: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            search: [''],
        });
        /*************************form data event *******************************/
        this.addEventForm = this._formBuilder.group({
            title: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            dateDebut: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            cid: this.testEgibFormGroup.get("contactID").value
        });
        /*************************************calandreir ***************************/
        this.calendarOption = {
            customButtons: {
                myCustomButton: {
                    text: 'custom!',
                    click: function () {
                        alert('clicked the custom button!');
                    }
                }
            },
            locale: "fr",
            initialView: 'dayGridMonth',
            //initialEvents: INITIAL_EVENTS, // alternatively, use the events setting to fetch from a feed
            weekends: true,
            editable: true,
            selectable: true,
            selectMirror: true,
            droppable: false,
            displayEventTime: true,
            disableDragging: false,
            timeZone: 'UTC',
            refetchResourcesOnNavigate: true,
            headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                right: 'dayGridMonth,dayGridWeek,dayGridDay'
            },
            dayMaxEvents: true,
            events: [],
            dateClick: this.handleDateClick,
        };
    }
    get f() { return this.addEventForm.controls; }
    ngOnInit() {
        this.getTransition();
        this.getActivite();
    }
    /**********************get departement *******************/
    GetChildData(data) {
        console.log("region", data);
        this.testEgibFormGroup.get('codeP').setValue(data === null || data === void 0 ? void 0 : data.zipCode);
        this.testEgibFormGroup.get('region').setValue(data === null || data === void 0 ? void 0 : data.region);
        this.testEgibFormGroup.get('departement').setValue(data === null || data === void 0 ? void 0 : data.departement);
    }
    /***************************************select date rendez vous *************************/
    /*Show Modal with Forn on dayClick Event*/
    handleDateClick() {
        console.log("dateselect");
    }
    sendRendvous() {
        console.log('event', this.addEventForm.value);
        this.testService.addEvents({ title: this.addEventForm.value.title, dateDebut: this.addEventForm.value.dateDebut, cid: this.cid }).subscribe(res => {
            console.log('event', res);
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                position: 'top-end',
                icon: 'success',
                title: 'ajout reussie',
                showConfirmButton: false,
                timer: 1500
            });
        }, error => {
            console.log(error);
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'quelque chose est incorrect !',
            });
        });
    }
    /***********************************generate lien zoom ****************/
    generateZoomLink() {
        this.test.zoom.generating = true;
        this.test.zoom.generated = true;
        this.testService.addZoom({ cid: this.cid, type: this.testEgibFormGroup.value.meetingType })
            .subscribe(response => {
            console.log('zoom', response);
            if (!response.error) {
                this.test.zoom.generating = false;
                this.test.zoom.generated = true;
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                    icon: 'success',
                    title: 'genrate lien zoom reussie',
                    showConfirmButton: false,
                    timer: 1500
                });
                this.nextStep();
            }
            else {
                this.test.zoom.generating = false;
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: response.message + ' !',
                });
            }
        }, error => {
            console.log(error);
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'error 500 !',
            });
        });
    }
    /***********************get all  activites ******************/
    getActivite() {
        this.testService.getActivites().subscribe(res => {
            this.activities = res;
            console.log("activi", this.activities);
        });
    }
    /*********************** selection turnover ******************/
    selectedRange(val) {
        this.testEgibFormGroup.value.turnover = val;
    }
    valuechange(val) {
        this.dataSource.filter = val.trim().toLowerCase();
        this.transition = this.dataSource.filteredData;
    }
    getTransition() {
        this.testService.getTransitions().subscribe(res => {
            this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_2__["MatTableDataSource"](res === null || res === void 0 ? void 0 : res.data);
            console.log("transition", this.transition);
            //  this.elegible=res?.data.filter(data=>data.category==='Services')
            //  this.graphic=res?.data.filter(data=>data.category==='Graphique')
            // this.montage=res?.data.filter(data=>data.category==='Montage')
            // this.marketing=res?.data.filter(data=>data.category==='Marketing')
            // this.development=res?.data.filter(data=>data.category==='Développement')
            // console.log("elegi",this.elegible)
            // console.log("suplimet",this.development)
        });
    }
    /*************************************services ***************************************/
    selectService(val) {
        console.log('service', val);
        this.testEgibFormGroup.get('service').setValue(val);
    }
    incTurn(val, min, max) {
        console.log('turn', this.turn);
        if (this.turn > max + 10) {
            this.turn = 0;
        }
        if (this.turn < max) {
            this.turn = val + this.turn;
            this.testEgibFormGroup.get('turnover').setValue(this.turn + '%');
        }
    }
    decTurn(val, min, max) {
        console.log('turn', this.turn);
        if (this.turn < min + 10) {
            this.turn = 0;
        }
        if (this.turn > min) {
            this.turn = this.turn - val;
            this.testEgibFormGroup.get('turnover').setValue(this.turn + '%');
        }
    }
    changeEtatA() {
        this.showA = true;
        this.showB = false;
    }
    changeEtatB() {
        this.showA = false;
        this.showB = true;
    }
    incVisite(val, min, max) {
        console.log('turn', this.turn);
        if (this.turn > max + 10) {
            this.turn = 0;
        }
        if (this.turn < max) {
            this.turn = val + this.turn;
            this.testEgibFormGroup.get('Nvisite').setValue(this.turn + '%');
        }
    }
    decVisite(val, min, max) {
        console.log('turn', this.turn);
        if (this.turn < min + 10) {
            this.turn = 0;
        }
        if (this.turn > min) {
            this.turn = this.turn - val;
            this.testEgibFormGroup.get('Nvisite').setValue(this.turn + '%');
        }
    }
    incVente(val, min, max) {
        console.log('turn', this.turn);
        if (this.turn > max + 10) {
            this.turn = 0;
        }
        if (this.turn < max) {
            this.turn = val + this.turn;
            this.testEgibFormGroup.get('Nvente').setValue(this.turn + '%');
        }
    }
    decVente(val, min, max) {
        console.log('turn', this.turn);
        if (this.turn < min + 10) {
            this.turn = 0;
        }
        if (this.turn > min) {
            this.turn = this.turn - val;
            this.testEgibFormGroup.get('Nvente').setValue(this.turn + '%');
        }
    }
    incUser(val, min, max) {
        console.log('turn', this.turn);
        if (this.turn > max + 10) {
            this.turn = 0;
        }
        if (this.turn < max) {
            this.turn = val + this.turn;
            this.testEgibFormGroup.get('Nuser').setValue(this.turn + '%');
        }
    }
    decUser(val, min, max) {
        console.log('turn', this.turn);
        if (this.turn < min + 10) {
            this.turn = 0;
        }
        if (this.turn > min) {
            this.turn = this.turn - val;
            this.testEgibFormGroup.get('Nuser').setValue(this.turn + '%');
        }
    }
    /**********************************************Last turnover ******************************************/
    setLastTurnover(val) {
        const step = this.test.active.step;
        let range = [
            val[0],
            val[1]
        ];
        console.log('val', val);
        console.log('range', range);
        this.testService.getServiceTurnover(range).subscribe(response => {
            console.log('data', response);
            this.testEgibFormGroup.get('service').setValue(response.data.transition_id);
            this.testEgibFormGroup.get('lastTurnover').setValue(response.data.id);
            this.testEgibFormGroup.get('budget').setValue(Math.ceil(response.data.budget / 100) * 100);
            this.test.data[11].budget = Math.ceil(response.data.budget / 100);
            this.test.data[11].min = Math.ceil(response.data.budget_min / 100) * 100;
            this.test.data[11].target = Math.ceil(response.data.budget / 100) * 100;
            this.test.data[11].max = Math.ceil(response.data.budget_max / 100) * 100;
        });
    }
    /******************************************************** get naf with siret**********************************/
    getNafCompany(siret) {
        let siren = siret.substring(0, 9);
        this.testEgibFormGroup.get('siren').setValue(siren);
        this.test.data[this.test.active.step].loading = true;
        siret = this.testEgibFormGroup.value.siret;
        this.testService.getCompanySiren(this.testEgibFormGroup.value.siret).subscribe(response => {
            console.log('siren', response);
            this.test.data[this.test.active.step].loading = false;
            this.testEgibFormGroup.get('naf').setValue(response.ape);
            this.testEgibFormGroup.get('naf').setValue(response.ape);
            return true;
        }, error => {
            console.log(error);
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default.a.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'code siret est introuvable !',
            });
            return false;
        });
        return true;
    }
    /******************************************************** save client to database**********************************/
    setContactForm(formDatas) {
        this.testService.addContact({
            "address": {
                "advisorName": this.testEgibFormGroup.value.nomSoc,
                "line": this.testEgibFormGroup.value.adresse,
                "zipcode": this.testEgibFormGroup.value.codeP,
                "region": this.testEgibFormGroup.value.region,
                "departement": this.testEgibFormGroup.value.departement,
                "city": this.testEgibFormGroup.value.city,
                "country": this.testEgibFormGroup.value.country
            },
            "companies": {
                "name": this.testEgibFormGroup.value.nomSoc,
                "status": this.testEgibFormGroup.value.status,
                "activity": this.testEgibFormGroup.value.activite.id,
                "help": this.testEgibFormGroup.value.help,
                "salaries": this.testEgibFormGroup.value.personneSal,
                "siret": this.testEgibFormGroup.value.siret,
                "siren": this.testEgibFormGroup.value.siren,
                "naf": this.testEgibFormGroup.value.naf,
                "phone": this.testEgibFormGroup.value.phoneEntrep,
                "turnover": this.testEgibFormGroup.value.turnover,
                "lastTurnover": this.testEgibFormGroup.value.lastTurnover
            },
            "contacts": {
                "firstName": this.testEgibFormGroup.value.nom,
                "lastName": this.testEgibFormGroup.value.prenom,
                "email": this.testEgibFormGroup.value.email,
                "phone": this.testEgibFormGroup.value.phone,
                "position": this.testEgibFormGroup.value.post,
                "type": 3,
                "comment": ''
            },
            "development": {
                "haveWebsite": this.testEgibFormGroup.value.haveSite,
                "websiteType": this.testEgibFormGroup.value.typeSite,
                "websiteValue": this.testEgibFormGroup.value.siteVal,
                "websiteLink": this.testEgibFormGroup.value.liensite,
                "websiteDate": this.testEgibFormGroup.value.datesite,
                "haveCrm": this.testEgibFormGroup.value.haveCrm,
                "crmType": this.testEgibFormGroup.value.typeCRM,
                "crmDev": this.testEgibFormGroup.value.crmDev,
                "crmName": this.testEgibFormGroup.value.nomCrm,
                "erpName": this.testEgibFormGroup.value.nomErp,
                "crmDate": this.testEgibFormGroup.value.dateCrm,
                "agencyName": this.testEgibFormGroup.value.agence
            },
            "investment": {
                "service": this.testEgibFormGroup.value.service,
                "budget": this.testEgibFormGroup.value.budget,
                "digitalTransitions": ['test']
            },
            "contactID": '',
            "meetingType": ''
        })
            .subscribe(response => {
            console.log('contactid', response);
            if (response) {
                this.cid = response.cid;
            }
        });
    }
    /********************************************************status**********************************/
    getstatus(data) {
        console.log('activi', data);
        this.testEgibFormGroup.get('status').setValue(data);
    }
    /******************************************nextmodule  *************************************************/
    nextStep() {
        this.test.active.step += 1;
    }
    nextSubStep() {
        this.test.active.subStep += 1;
    }
    /******************************************prevmodule  *************************************************/
    prevStep() {
        this.test.active.step -= 1;
    }
    /**************************test elgible */
    elgiblTest() {
        this.eleg = null;
        this.nextStep();
    }
    /**********************is cpn **************/
    isCpn() {
        console.log("data result", this.testEgibFormGroup.value);
        let budget = this.testEgibFormGroup.value.budget;
        let service = this.testEgibFormGroup.value.service;
        let region = this.testEgibFormGroup.value.region;
        let naf = this.testEgibFormGroup.value.naf;
        console.log("isopen", this.test.result.isOpen);
        console.log("isCpn", this.test.result.isCpn);
        console.log("isLoading", this.test.result.isLoading);
        /**************************calcule cpn *******************/
        console.log("is open false");
        this.test.result.isOpen = true;
        this.test.result.isLoading = true;
        console.log("service", this.testEgibFormGroup.value.service);
        console.log("budget", this.testEgibFormGroup.value.budget);
        this.testService.cpnGrant(service, budget)
            .subscribe(response => {
            console.log("cpnGrant", response);
            this.test.result.cpn.id = response.id;
            this.test.result.cpn.amount = response.grants;
            this.test.result.cpn.originalPrice = response.original_price;
            this.test.result.cpn.sellPrice = response.sell_price;
            this.test.result.isLoading = false;
        });
        console.log("step", this.test.active.step);
        this.nextStep();
    }
    showResult() {
        console.log("data result", this.testEgibFormGroup.value);
        let budget = this.testEgibFormGroup.value.budget;
        let service = this.testEgibFormGroup.value.service;
        let region = this.testEgibFormGroup.value.region;
        let naf = this.testEgibFormGroup.value.naf;
        console.log("isopen", this.test.result.isOpen);
        console.log("isCpn", this.test.result.isCpn);
        console.log("isLoading", this.test.result.isLoading);
        /**************************calcule cpn *******************/
        console.log("is open false");
        this.test.result.isOpen = true;
        this.test.result.isLoading = true;
        this.testService.cpnGrant(service, budget)
            .subscribe(response => {
            console.log("cpnGrant", response);
            this.test.result.cpn.id = response.id;
            this.test.result.cpn.amount = response.grants;
            this.test.result.cpn.originalPrice = response.original_price;
            this.test.result.cpn.sellPrice = response.sell_price;
            this.test.result.isLoading = false;
        });
        console.log("step", this.test.active.step);
        switch (this.test.result.isOpen) {
            case true:
                console.log("is open true");
                console.log("is cpn true");
                this.test.result.isCpn = false;
                this.test.result.isLoading = true;
                this.testService.regionalGrant(region, budget, naf)
                    .subscribe(response => {
                    console.log("regionalGrant", response);
                    this.eleg = response.eligible;
                    if (response.eligible) {
                        this.test.result.regional.id = response.id;
                        this.test.result.regional.eligible = response.eligible;
                        this.test.result.regional.voucher = response.voucher;
                        this.test.result.regional.amount = response.amount;
                        this.test.result.regional.region = response.region;
                        console.log("is eligible", this.test.active.step);
                    }
                    else {
                        this.test.result.regional.eligible = response.eligible;
                        this.test.result.regional.voucher = null;
                        this.test.result.regional.amount = null;
                        console.log("is not eligi", this.test.active.step);
                    }
                    this.test.result.isLoading = false;
                });
                /*  .catch(error=>{
                    this.test.result.isLoading = false;
                    console.log(error)
                  });*/
                console.log("is cpn false");
                this.setContactForm(this.test.formData);
                this.test.result.isCpn = true;
                this.test.result.isOpen = false;
                this.nextStep();
                break;
        }
        console.log("test", this.test.result);
    }
    /******************************************************chekform **************************************/
    checkForm(step) {
        console.log('step', step);
        let subStep = this.test.active.subStep;
        let subStepCat = this.test.active.subStepCat;
        switch (step) {
            case 0:
                this.nextStep();
                break;
            case 1:
                if (this.testEgibFormGroup.value.codeP == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 2:
                if (this.testEgibFormGroup.value.activite == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 3:
                if (this.testEgibFormGroup.value.status == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 4:
                if (this.testEgibFormGroup.value.nomSoc == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 5:
                if (this.testEgibFormGroup.value.turnover == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 6:
                if (this.testEgibFormGroup.value.help == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 7:
                console.log("laste", Math.abs(this.max - this.min));
                if (this.min == 0 || this.max == 0 || (Math.abs(this.max - this.min) != 5000000 && Math.abs(this.max - this.min) != 500000 && Math.abs(this.max - this.min) != 100000 && Math.abs(this.max - this.min) != 50000 && Math.abs(this.max - this.min) != 45000)) {
                    alert("les deux valeur doit être très approcher");
                }
                else {
                    this.setLastTurnover([this.min, this.max]);
                    this.nextStep();
                }
                break;
            case 8:
                console.log('salair', this.testEgibFormGroup.value.personneSal);
                if (this.testEgibFormGroup.value.personneSal == "de 0 à 5 Personnes" || this.testEgibFormGroup.value.personneSal == "de 5 à 10 Personnes") {
                    this.test.active.subStepCat = 1;
                    this.test.active.subStep = 1;
                    this.nextStep();
                }
                else {
                    this.test.active.subStepCat = 2;
                    this.test.active.subStep = 1;
                    this.nextStep();
                }
                break;
            case 9:
                switch (subStepCat) {
                    case 1:
                        switch (subStep) {
                            case 1:
                                if (this.testEgibFormGroup.value.haveSite == "oui") {
                                    this.nextSubStep();
                                }
                                else {
                                    this.nextStep();
                                }
                                ;
                                break;
                            case 2:
                                this.nextSubStep();
                                break;
                            case 3:
                                this.nextSubStep();
                                break;
                            case 4:
                                this.nextSubStep();
                                break;
                            case 5:
                                this.nextStep();
                                break;
                        }
                        break;
                    case 2:
                        switch (subStep) {
                            case 1:
                                if (this.testEgibFormGroup.value.haveCrm == "oui") {
                                    this.nextSubStep();
                                }
                                else {
                                    this.test.active.subStepCat = 1;
                                }
                                ;
                                break;
                            case 2:
                                this.nextSubStep();
                                break;
                            case 3:
                                this.nextSubStep();
                                break;
                            case 4:
                                this.nextSubStep();
                                break;
                            case 5:
                                this.test.active.subStepCat = 1;
                                this.test.active.subStep = 1;
                                break;
                        }
                        break;
                }
                break;
            case 10:
                this.nextStep();
                break;
            case 11:
                if (this.testEgibFormGroup.value.budget == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 12:
                this.getNafCompany(this.testEgibFormGroup.value.siret);
                if (this.testEgibFormGroup.value.siret == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 13:
                if (this.testEgibFormGroup.value.adresse == '' && this.testEgibFormGroup.value.zipcode == '' && this.testEgibFormGroup.value.region == ''
                    && this.testEgibFormGroup.value.city == '' && this.testEgibFormGroup.value.country == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.nextStep();
                }
                break;
            case 14:
                if (this.testEgibFormGroup.value.nom == '' && this.testEgibFormGroup.value.prenom == '' && this.testEgibFormGroup.value.email == ''
                    && this.testEgibFormGroup.value.phone == '' && this.testEgibFormGroup.value.phoneEntrep == '') {
                    alert("champ est obligatoir");
                }
                else {
                    this.isCpn();
                }
                break;
            case 15:
                this.nextStep();
                break;
            case 16:
                this.nextStep();
                break;
            case 17:
                this.generateZoomLink();
                break;
        }
    }
    onUserChangeStart(changeContext) {
        console.log('start', changeContext);
        this.min = changeContext.highValue;
        this.max = changeContext.value;
    }
    onUserChange(changeContext) {
        console.log('use', changeContext);
        this.min = changeContext.highValue;
        this.max = changeContext.value;
    }
    onUserChangeEnd(changeContext) {
        this.min = changeContext.highValue;
        this.max = changeContext.value;
    }
    getChangeContextString(changeContext) {
        return; /*`{pointerType: ${changeContext.pointerType === PointerType.Min ? 'Min' : 'Max'}, ` +
               `value: ${changeContext.value}, ` +
               `highValue: ${changeContext.highValue}}`;*/
    }
}
TestComponent.ɵfac = function TestComponent_Factory(t) { return new (t || TestComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_services_cpn_test_egibilite_service__WEBPACK_IMPORTED_MODULE_4__["TestEgibiliteService"])); };
TestComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: TestComponent, selectors: [["app-test"]], decls: 50, vars: 24, consts: [[1, "container"], [1, "body"], [3, "formGroup"], ["class", "slide0", 4, "ngIf"], ["class", "slide1", 4, "ngIf"], ["class", "slide2", 4, "ngIf"], ["class", "slide3", 4, "ngIf"], ["class", "slide4", 4, "ngIf"], ["class", "slide5", 4, "ngIf"], ["class", "slide6", 4, "ngIf"], ["class", "slide7", 4, "ngIf"], ["class", "slide8", 4, "ngIf"], [4, "ngIf"], ["class", "slide27", 4, "ngIf"], ["class", "slide18", 4, "ngIf"], ["class", "slide19", 4, "ngIf"], ["class", "slide20", 4, "ngIf"], ["class", "slide26", 4, "ngIf"], ["class", "slide21", 4, "ngIf"], ["class", "slide24", 4, "ngIf"], ["class", "slide25", 4, "ngIf"], [1, "footer"], [1, "left"], [3, "routerLink"], ["src", "assets/cpnimages/logo/logo-cpn-blanc.png", "alt", ""], ["class", "center", 4, "ngIf"], ["id", "eventModal", "tabindex", "-1", "role", "dialog", 1, "modal", "fade", "text-left"], [1, "modal-dialog"], [1, "modal-content"], [1, "modal-header"], [1, "modal-title", "align-center"], [1, "modal-body"], [3, "formGroup", "ngSubmit"], [1, "row"], [1, "col-sm-12"], [1, "form-group"], ["placeholder", "cr\u00E9e un \u00E9v\u00E9nement", "type", "text", "formControlName", "title", 1, "titleinp", "form-control"], ["type", "datetime-local", "id", "meeting-time", "name", "meeting-time", "formControlName", "dateDebut", 1, "titleinp", "form-control"], ["dateDebut", ""], ["type", "submit", 1, "btn", "btn-primary"], [1, "slide0"], [1, "image"], ["src", "assets/cpnimages/test-egibilite/1.png", "height", "30%", "alt", ""], [1, "test"], ["mat-stroked-button", "", "color", "primary", 3, "click"], [1, "slide1"], [3, "myinputDep", "myOutput"], ["type", "text", "formControlName", "codeP", "placeholder", "entrer votre code postal"], [1, "slide2"], ["src", "assets/cpnimages/test-egibilite/4.png", "height", "30%", "alt", ""], ["bindLabel", "name", "placeholder", "Choisir un activit\u00E9", "formControlName", "activite", "autofocus", "", 3, "items", "change"], [1, "slide3"], ["src", "assets/cpnimages/test-egibilite/5.png", "height", "30%", "alt", ""], [1, "blockBtn"], [1, "sousBlock"], ["mat-stroked-button", "", 3, "ngClass", "click"], [1, "slide4"], ["src", "assets/cpnimages/test-egibilite/3.png", "height", "30%", "alt", ""], ["type", "text", "placeholder", "nom societe", "formControlName", "nomSoc"], [1, "slide5"], ["src", "assets/cpnimages/test-egibilite/6.png", "height", "30%", "alt", ""], [1, "qty", 3, "click"], [1, "baiss", 3, "ngClass"], ["type", "text", "formControlName", "turnover", 3, "readonly"], ["turnover", ""], [1, "qtyBtn"], [1, "augm", 3, "ngClass"], ["type", "radio", "formControlName", "turnover", "name", "turnover", "id", "turnover", "value", "0", 1, "form-check-input"], ["for", "inlineRadio1", 1, "form-check-label"], [1, "slide6"], ["src", "assets/cpnimages/test-egibilite/7.png", "alt", ""], ["name", "", "formControlName", "help"], ["value", "item", 4, "ngFor", "ngForOf"], ["value", "item"], [1, "slide7"], [1, "colum1"], ["src", "assets/cpnimages/test-egibilite/8.png", "height", "30%", "alt", ""], [1, "colum2"], [1, "__range", "__range-step"], [3, "value", "highValue", "options", "valueChange", "highValueChange", "userChangeStart", "userChange", "userChangeEnd"], [1, "slide8"], ["src", "assets/cpnimages/test-egibilite/9.png", "height", "30%", "alt", ""], ["placeholder", "choisir un Nombre de salari\u00E9s", "formControlName", "personneSal"], [4, "ngFor", "ngForOf"], ["class", "slide9", 4, "ngIf"], ["class", "slide10", 4, "ngIf"], ["class", "slide11", 4, "ngIf"], ["class", "slide12", 4, "ngIf"], ["class", "slide13", 4, "ngIf"], [1, "slide9"], ["src", "assets/cpnimages/test-egibilite/10.png", "height", "30%", "alt", ""], ["aria-label", "Select an option", "formControlName", "haveSite"], ["value", "oui"], ["value", "non"], [1, "slide10"], ["src", "assets/cpnimages/test-egibilite/11.png", "height", "30%", "alt", ""], ["aria-label", "Select an option", "formControlName", "typeSite"], ["value", "E-commerce"], ["value", "Vitrine"], ["value", "Market-place"], [1, "block"], [1, "qty"], ["type", "text", "formControlName", "Nvente", 3, "readonly"], ["Nvente", ""], ["type", "text", "formControlName", "Nvisite", 3, "readonly"], ["Nvisite", ""], ["type", "text", "formControlName", "Nuser", 3, "readonly"], ["Nuser", ""], [1, "slide11"], ["src", "assets/cpnimages/test-egibilite/12.png", "height", "30%", "alt", ""], ["type", "text", "formControlName", "liensite", "placeholder", "entre lien de votre site"], [1, "slide12"], ["src", "assets/cpnimages/test-egibilite/13.png", "alt", ""], ["placeholder", "choisir un date", "formControlName", "datesite"], [1, "slide13"], ["type", "text", "placeholder", "votre agnce svp!", "formControlName", "agence"], ["value", "Internet/Freelance"], ["class", "slide14", 4, "ngIf"], ["class", "slide15", 4, "ngIf"], ["class", "slide16", 4, "ngIf"], ["class", "slide17", 4, "ngIf"], [1, "slide14"], ["src", "assets/cpnimages/test-egibilite/17.png", "height", "30%", "alt", ""], ["aria-label", "Select an option", "formControlName", "haveCrm"], [1, "slide15"], ["name", "", "formControlName", "nomCrm"], ["value", "Sage", 4, "ngFor", "ngForOf"], ["value", "Sage"], ["src", "assets/cpnimages/test-egibilite/logo-sage.png", "height", "20px", "alt", ""], [1, "slide16"], ["src", "assets/cpnimages/test-egibilite/14.png", "height", "30%", "alt", ""], ["aria-label", "Select an option", "formControlName", "haveErp"], [1, "slide17"], ["name", "", "formControlName", "nomErp"], ["placeholder", "choisir un date", "formControlName", "dateCrm"], [1, "slide27"], [1, "bSearch"], ["for", ""], ["type", "text", 1, "search", 3, "keyup"], [1, "far", "fa-search"], [1, "contentTab"], ["class", "list-item", 4, "ngFor", "ngForOf"], [1, "list-item"], [1, "item-content"], [1, "block1"], [1, "fas", "fa-chess-rook"], [1, "block2"], [1, "block3"], ["type", "radio", "name", "service", "id", "service", 3, "click"], [1, "slide18"], ["name", "", "formControlName", "budget"], ["value", "300"], ["value", "400"], ["value", "500"], ["value", "600"], [1, "slide19"], ["src", "assets/cpnimages/test-egibilite/18.png", "height", "30%", "alt", ""], ["type", "text", "formControlName", "siret", "placeholder", "EX: 13168813881"], [1, "slide20"], ["src", "assets/cpnimages/test-egibilite/19.png", "alt", ""], ["type", "text", "formControlName", "adresse"], ["type", "text", "formControlName", "city"], ["type", "text", "formControlName", "codeP", 3, "readonly"], [1, "slide26"], ["src", "assets/cpnimages/test-egibilite/20.png", "alt", ""], ["type", "text", "formControlName", "nom"], ["type", "text", "formControlName", "prenom"], ["type", "text", "formControlName", "email"], ["type", "text", "formControlName", "phone"], ["type", "text", "formControlName", "phoneEntrep"], ["aria-label", "Select an option", "formControlName", "post"], ["value", "G\u00E9rant"], ["value", "Associ\u00E9"], ["value", "Dir\u00E9cteur"], ["value", "autre"], [1, "slide21"], ["src", "assets/cpnimages/test-egibilite/21.png", "alt", ""], ["src", "assets/cpnimages/test-egibilite/24.png", "alt", ""], ["src", "assets/cpnimages/test-egibilite/22.png", "alt", ""], [1, "nextIcon", 3, "click"], ["src", "assets/cpnimages/test-egibilite/23.png", "alt", ""], [1, "prix"], [1, "faild"], ["src", "assets/cpnimages/test-egibilite/25.png", "alt", ""], [1, "slide24"], ["src", "assets/cpnimages/test-egibilite/27.png", "height", "30%", "alt", ""], [1, "calend"], ["data-toggle", "modal", "data-target", "#eventModal", 2, "width", "100%", 3, "options"], ["fullcalendar", ""], ["aria-label", "Select an option", "formControlName", "meetingType"], ["value", "Entretien vid\u00E9o direct"], ["value", "Visite de courtoisie"], [1, "slide25"], ["src", "assets/cpnimages/test-egibilite/28.png", "alt", ""], [1, "nextIcon"], [1, "socialMedia"], ["src", "assets/cpnimages/test-egibilite/icone-Facebook.png", "alt", ""], ["src", "assets/cpnimages/test-egibilite/icone-Instagram.png", "alt", ""], ["src", "assets/cpnimages/test-egibilite/icone-Linkedin.png", "alt", ""], ["src", "assets/cpnimages/test-egibilite/icone-youtube.png", "alt", ""], [1, "center"], [3, "click"], [1, "far", "fa-chevron-left", "iconNex"], [1, "listP"], ["aria-hidden", "true", 1, "fas", "fa-circle", "point", 3, "ngClass"], [1, "far", "fa-chevron-right", "iconNex"]], template: function TestComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "form", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, TestComponent_div_3_Template, 8, 0, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, TestComponent_div_4_Template, 7, 1, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, TestComponent_div_5_Template, 8, 1, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, TestComponent_div_6_Template, 19, 15, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, TestComponent_div_7_Template, 7, 0, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](8, TestComponent_div_8_Template, 35, 8, "div", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](9, TestComponent_div_9_Template, 8, 1, "div", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, TestComponent_div_10_Template, 10, 3, "div", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, TestComponent_div_11_Template, 10, 1, "div", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](12, TestComponent_div_12_Template, 3, 2, "div", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](13, TestComponent_div_13_Template, 10, 1, "div", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, TestComponent_div_14_Template, 17, 0, "div", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](15, TestComponent_div_15_Template, 7, 0, "div", 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, TestComponent_div_16_Template, 17, 1, "div", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, TestComponent_div_17_Template, 36, 0, "div", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, TestComponent_div_18_Template, 21, 1, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](19, TestComponent_div_19_Template, 24, 1, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](20, TestComponent_div_20_Template, 12, 0, "div", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](21, TestComponent_div_21_Template, 14, 1, "div", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](22, TestComponent_div_22_Template, 20, 0, "div", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 22);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "a", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](26, "img", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](27, TestComponent_div_27_Template, 38, 48, "div", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 29);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "h4", 30);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, " Cr\u00E9er un \u00E9v\u00E9nement ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 31);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "form", 32);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngSubmit", function TestComponent_Template_form_ngSubmit_35_listener() { return ctx.sendRendvous(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 33);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "Titre \u00E9v\u00E9nement");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](41, "input", 36);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 34);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 35);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "label");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "select date");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](46, "input", 37, 38);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "button", 39);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "Envoyer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.testEgibFormGroup);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 15);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.eleg == true && ctx.test.active.step == 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.eleg == false && ctx.test.active.step == 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step == 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", "/home");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.test.active.step != 0 && ctx.test.active.step != 15 && ctx.test.active.step != 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx.addEventForm);
    } }, directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroupDirective"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterLinkWithHref"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControlName"], _angular_material_button__WEBPACK_IMPORTED_MODULE_7__["MatButton"], _map_french_map_french_component__WEBPACK_IMPORTED_MODULE_8__["MapFrenchComponent"], _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_9__["NgSelectComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgClass"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["RadioControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["SelectControlValueAccessor"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["ɵangular_packages_forms_forms_x"], _angular_slider_ngx_slider__WEBPACK_IMPORTED_MODULE_10__["ɵa"], _angular_material_radio__WEBPACK_IMPORTED_MODULE_11__["MatRadioGroup"], _angular_material_radio__WEBPACK_IMPORTED_MODULE_11__["MatRadioButton"], _fullcalendar_angular__WEBPACK_IMPORTED_MODULE_12__["FullCalendarComponent"]], styles: [".container[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    width: 100%;\r\n    height: 100%;\r\n    max-width: 100%;\r\n    margin: 0;\r\n    padding: 0;\r\n}\r\n.body[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    height: 100%;\r\n    display: flex;\r\n    flex-direction:column;\r\n    justify-content: center;\r\n}\r\n.footer[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n    max-height: 150px;\r\n    min-height: 150px;\r\n    height: 100%;\r\n    background-color:  #111D5Eff;\r\n    display: flex;\r\n    flex-direction: row;\r\n}\r\n.left[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: flex-start;\r\n    height: 100%;\r\n    align-items: center;\r\n    width: 10%;\r\n    margin-left: 10px;\r\n}\r\n.left[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    width: 50%;\r\n}\r\n.point[_ngcontent-%COMP%]{\r\n    font-size: 10px;\r\n    color:rgb(153, 153, 153);\r\n    margin-left: 10px;\r\n    \r\n}\r\ni[_ngcontent-%COMP%]{\r\n    color:rgb(153, 153, 153);\r\n}\r\ni[_ngcontent-%COMP%]:hover{\r\n    color: white;\r\n    cursor: pointer;\r\n}\r\n.iconNex[_ngcontent-%COMP%]{\r\n    font-size: 25px;\r\n    margin-top: 8px;\r\n    margin-left: 10px;\r\n}\r\n.checkIcon[_ngcontent-%COMP%]{\r\n    color: white;\r\n}\r\n.center[_ngcontent-%COMP%]{\r\n width: 100%;\r\n height: 100%;\r\n display: flex;\r\n flex-direction: row;\r\n justify-content: center;\r\n align-items: center;\r\n}\r\n\r\n.butttonREd[_ngcontent-%COMP%]{\r\n    background-color: rgb(206, 0, 0);\r\n    color: white;\r\n}\r\nbutton[_ngcontent-%COMP%]:hover{\r\n    background-color: rgb(145, 136, 136);\r\n    color: white;\r\n}\r\n\r\n.slide1[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide1[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  width: 70%;\r\n  height: 100%;\r\n}\r\n.slide1[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n order: 1;\r\n display: flex;\r\n flex-direction: column;\r\n   justify-content: center;\r\n   align-items:center;\r\nwidth: 40%;\r\nheight: 100%;\r\n}\r\n.slide1[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    width: 80%;\r\n    height: 40px;\r\n    border-color: rgb(212, 5, 5);\r\n       }\r\n\r\n.slide0[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide0[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  width: 52%;\r\n  height: 100%;\r\n}\r\n.slide0[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n   flex-direction: column;\r\n   justify-content: center;\r\n   align-items: center;\r\n   width:40%;\r\n   height: 100%;\r\n   }\r\n.slide0[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\nfont-size: 50px;\r\n   }\r\n.slide0[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\r\n    width: 150px;\r\n    height: 50px;\r\n    border-radius: 30px;\r\n    border-color: rgb(192, 3, 3);\r\n       }\r\n\r\n.slide2[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n        width: 100%;\r\n        height: 100%;\r\n    }\r\n.slide2[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n      order: 1;\r\n      width: 55%;\r\n      height: 100%;\r\n    }\r\n.slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n       flex-direction: column;\r\n       justify-content: center;\r\n       align-items: flex-start;\r\n       width:40%;\r\n       height: 100%;\r\n       }\r\n.slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 50px;\r\n       }\r\n.slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .ng-select[_ngcontent-%COMP%] {\r\n            border:0px;\r\n            min-height: 0px;\r\n            border-radius: 0;\r\n            width: 480px;\r\n            height: 40px;\r\n            border-color: rgb(212, 5, 5);\r\n        }\r\n.slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .ng-select[_ngcontent-%COMP%]   .ng-select-container[_ngcontent-%COMP%]  {            \r\n            min-height: 0px;\r\n            border-radius: 0;\r\n            width: 480px;\r\n            height: 40px;\r\n            border-color: rgb(212, 5, 5);\r\n        }\r\n.slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .ng-select[_ngcontent-%COMP%]     .ng-select-container  {            \r\n            min-height: 0px;\r\n            border-radius: 0;\r\n            width: 480px;\r\n            height: 40px;\r\n            border-color: rgb(212, 5, 5);\r\n        }\r\n.slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   ng-select.ng-invalid.ng-touched[_ngcontent-%COMP%]   .ng-select-container[_ngcontent-%COMP%] {\r\n            border-color: #dc3545;\r\n            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 0 3px #fde6e8;\r\n            width: 480px;\r\n            height: 40px;\r\n            border-color: rgb(212, 5, 5);\r\n        }\r\n\r\n.slide3[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n        width: 100%;\r\n        height: 100%;\r\n    }\r\n.slide3[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        width: 54%;\r\n        height: 100%;\r\n    }\r\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n        order: 1;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: space-around;\r\n        align-items: center;\r\n        margin-top: 50px;\r\n        width:40%;\r\n        height: 70%;\r\n        }\r\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n        font-size: 50px;\r\n        }\r\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]{\r\n           display: flex;\r\n           flex-direction: column;\r\n           justify-content: center;\r\n\r\n        }\r\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]{\r\n            display: flex;\r\n            flex-direction: row;\r\n            justify-content: center;\r\n \r\n         }\r\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\r\n        width: 310px;\r\n        height: 50px;\r\n        margin: 5px;\r\n        border-color: rgb(192, 3, 3);\r\n            }\r\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\r\n                width: 150px;\r\n                margin: 5px;\r\n                    }\r\n\r\n.slide4[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n        width: 100%;\r\n        height: 100%;\r\n    }\r\n.slide4[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n        margin-left: 50px;\r\n        order: 1;\r\n        width: 33%;\r\n        height: 100%;\r\n        display: flex;\r\n        z-index: 1;\r\n        margin-top: 55px;\r\n    }\r\n.slide4[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        align-items: flex-start;\r\n        width:40%;\r\n        height: 100%;\r\n        }\r\n.slide4[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 50px;\r\n        }\r\n.slide4[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n        width: 80%;\r\n        height: 40px;\r\n        border-color: rgb(212, 5, 5);\r\n            }\r\n\r\n.slide5[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n        width: 100%;\r\n        height: 100%;\r\n    }\r\n.slide5[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n        order: 1;\r\n        width: 50%;\r\n        height: 100%;\r\n        z-index: 1;\r\n        display: flex;\r\n        margin-top: 64px;\r\n    }\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        align-items: center;\r\n        width:50%;\r\n        height: 100%;\r\n        }\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n        font-size: 50px;\r\n        }\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]{\r\n           display: flex;\r\n           flex-direction: row;\r\n           justify-content: space-around;\r\n            width: 100%;\r\n            height: 20%;\r\n        }\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]{\r\n            display: flex;\r\n            flex-direction: row;\r\n            justify-content: flex-start;\r\n            align-items: center; \r\n         }\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]{\r\n            display: flex;\r\n            flex-direction: row;\r\n            justify-content: center;            \r\n            }\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        width: 80px;\r\n        height: 50px;\r\n        border: 4px solid rgb(212, 5, 5);\r\n        text-align: center;\r\n        }\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n        order: 1;\r\n        margin: 0 5px 0 0;\r\n        padding: 0;\r\n        display: flex;\r\n        align-items: center;\r\n        flex-direction: row;\r\n            }\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .baiss[_ngcontent-%COMP%]{ display: none;}\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .augm[_ngcontent-%COMP%]{ display: none;}\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .baissD[_ngcontent-%COMP%]{ display: contents;}\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .augmD[_ngcontent-%COMP%]{ display: contents;}\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .qtyBtn[_ngcontent-%COMP%]{\r\n        order: 3;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: flex-start;\r\n        }\r\n.slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .qtyBtn[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\r\n            border: none;\r\n            border-bottom: 1px solid white;\r\n            margin-bottom: 1px;\r\n            width: 5px;\r\n            border-radius: 0px;\r\n            height: 25px;\r\n            color: white;\r\n            background-color: rgb(212, 5, 5);\r\n            display: flex;\r\n            flex-direction: column;\r\n            font-size: 20px;\r\n            justify-content: center;\r\n            align-items: center;\r\n            }\r\n\r\n.slide6[_ngcontent-%COMP%]{\r\n                display: flex;\r\n                flex-direction: row;\r\n                justify-content: space-between;\r\n                width: 100%;\r\n                height: 100%;\r\n            }\r\n.slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n                order: 2;\r\n                display: flex;\r\n                flex-direction: column;\r\n                justify-content: center;\r\n                align-items:center;\r\n                width:53%;\r\n                height: 100%;\r\n            }\r\n.slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n                order: 2;\r\n                height: 25%;\r\n                width: 15%;\r\n                    }\r\n.slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n                order: 1;\r\n                font-size: 50px;\r\n                text-align: center;\r\n                margin-bottom: 20px;\r\n                    }\r\n.slide6[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n                order: 2;\r\n                display: flex;\r\n                flex-direction: column;\r\n                justify-content: center;\r\n                align-items: flex-start;\r\n                width:40%;\r\n                height: 100%;\r\n                }\r\n.slide6[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\r\n                width: 80%;\r\n                height: 40px;\r\n                border-color: rgb(212, 5, 5);\r\n                    }\r\n\r\n.slide7[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide7[_ngcontent-%COMP%]   .colum1[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-around;\r\n    width: 100%;\r\n    height: 100%;\r\n    margin-bottom: 20px;\r\n}\r\n.slide7[_ngcontent-%COMP%]   .colum2[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: center;\r\n    width: 100%;\r\n    margin-top: 20px;\r\n    height: 100%;\r\n}\r\n.slide7[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    margin-left: 50px;\r\n    order: 1;\r\n    width: 37%;\r\n    height: 100%;\r\n}\r\n.slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: flex-start;\r\n    width:40%;\r\n    height: 100%;\r\n    }\r\n.slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\nfont-size: 50px;\r\n    }\r\n.slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    width: 80%;\r\n    height: 40px;\r\n    border-color: rgb(212, 5, 5);\r\n        }\r\n.slide7[_ngcontent-%COMP%]   .progress[_ngcontent-%COMP%]{\r\n    order: 3;\r\n}\r\n.slider[_ngcontent-%COMP%] {\r\n    -webkit-appearance: none;\r\n    width: 100%;\r\n    height: 15px;\r\n    background: rgb(255, 255, 255);\r\n    outline: none;\r\n    border: 5px solid rgb(189, 8, 8);\r\n    border-radius: 8px;\r\n  }\r\n\r\n.slider[_ngcontent-%COMP%]::-webkit-slider-thumb {\r\n    -webkit-appearance: none;\r\n    appearance: none;\r\n    width: 20px;\r\n    height: 60px;\r\n    background: rgb(248, 224, 5);\r\n    cursor: pointer;\r\n    border: 5px solid rgb(248, 224, 5);\r\n    border-radius: 50px;\r\n  }\r\n\r\n.slider[_ngcontent-%COMP%]::-moz-range-thumb {\r\n    width: 20px;\r\n    height: 60px;\r\n    background: rgb(255, 255, 255);\r\n    cursor: pointer;\r\n    border: 5px solid rgb(189, 8, 8);\r\n    border-radius: 4px;\r\n  }\r\n.__range[_ngcontent-%COMP%]{\r\n    width: 80%;\r\n    height: 100%;\r\n}\r\n.__range-step[_ngcontent-%COMP%]{\r\n\tposition: relative;                \r\n}\r\n.__range-step[_ngcontent-%COMP%]{\r\n\tposition: relative;                \r\n}\r\n.__range-max[_ngcontent-%COMP%]{\r\n\tfloat: right;\r\n}\r\n.__range[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]::range-progress {\tbackground: rgb(189, 8, 8);\r\n}\r\n.slider[_ngcontent-%COMP%]   input[type=range][_ngcontent-%COMP%]::-moz-range-progress {\r\n    background-color: #c657a0;\r\n  }\r\n.__range-step[_ngcontent-%COMP%]   datalist[_ngcontent-%COMP%] {\r\n\tposition:relative;\r\n\tdisplay: flex;\r\n\tjustify-content: space-between;\r\n\theight: auto;\r\n\tbottom: 10px;\r\n\t\r\n\t-webkit-user-select: none;                   \r\n\tuser-select: none; \r\n\t\r\n\tpointer-events:none;  \r\n}\r\n.__range-step[_ngcontent-%COMP%]   datalist[_ngcontent-%COMP%]   option[_ngcontent-%COMP%] {\r\n\twidth: 10px;\r\n\theight: 10px;\r\n\tmin-height: 10px;\r\n\tborder-radius: 100px;\r\n\t\r\n\twhite-space: nowrap;       \r\n  padding:0;\r\n  line-height: 40px;\r\n}\r\n\r\n.slide8[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-around;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide8[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    align-items: center;\r\n    width: 37%;\r\n    height: 100%;\r\n}\r\n.slide8[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: flex-start;\r\n    width:40%;\r\n    height: 100%;\r\n    }\r\n.slide8[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\nfont-size: 50px;\r\n    }\r\n.slide8[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\r\n    width: 80%;\r\n    height: 40px;\r\n    border-color: rgb(212, 5, 5);\r\n        }\r\n\r\n.slide9[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-around;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide9[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    align-items: center;\r\n    width: 37%;\r\n    height: 100%;\r\n}\r\n.slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: flex-start;\r\n    width:40%;\r\n    height: 100%;\r\n    }\r\n.slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 50px;\r\n    }\r\n.slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\r\n        margin-top: 50px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-around;\r\n        width: 100%;\r\n    }\r\n\r\n.slide10[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-around;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide10[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    align-items: center;\r\n    width: 37%;\r\n    height: 100%;\r\n}\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: flex-start;\r\n    width:40%;\r\n    height: 100%;\r\n    }\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 50px;\r\n    }\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\r\n        margin-top: 50px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-around;\r\n        width: 100%;\r\n    }\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-around;\r\n        width: 100%;\r\n        margin-top: 20px;\r\n    }\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: space-around;\r\n        align-items: center; \r\n     }\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: center;            \r\n        }\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    width: 80px;\r\n    height: 50px;\r\n    border: 4px solid rgb(212, 5, 5);\r\n    text-align: center;\r\n    }\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    margin: 0 5px 0 0;\r\n    padding: 0;\r\n    display: flex;\r\n    align-items: center;\r\n    flex-direction: row;\r\n        }\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .qtyBtn[_ngcontent-%COMP%]{\r\n    order: 3;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-start;\r\n    }\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   .qtyBtn[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]{\r\n        border: none;\r\n        border-bottom: 1px solid white;\r\n        margin-bottom: 1px;\r\n        width: 5px;\r\n        border-radius: 0px;\r\n        height: 25px;\r\n        color: white;\r\n        background-color: rgb(212, 5, 5);\r\n        display: flex;\r\n        flex-direction: column;\r\n        font-size: 20px;\r\n        justify-content: center;\r\n        align-items: center;\r\n        }\r\n\r\n.slide11[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-around;\r\n        width: 100%;\r\n        height: 100%;\r\n    }\r\n.slide11[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n        align-items: center;\r\n        display: flex;\r\n        order: 1;\r\n        width: 37%;\r\n        height: 100%;\r\n    }\r\n.slide11[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        align-items: flex-start;\r\n        width:40%;\r\n        height: 100%;\r\n        }\r\n.slide11[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 50px;\r\n        }\r\n.slide11[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n        width: 80%;\r\n        height: 40px;\r\n        border-color: rgb(212, 5, 5);\r\n            }\r\n\r\n.slide12[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n        width: 100%;\r\n        height: 100%;\r\n    }\r\n.slide12[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        align-items:center;\r\n        width:53%;\r\n        height: 100%;\r\n    }\r\n.slide12[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        height: 15%;\r\n        width: 15%;\r\n            }\r\n.slide12[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n        order: 1;\r\n        font-size: 50px;\r\n        text-align: center;\r\n        margin-bottom: 20px;\r\n            }\r\n.slide12[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        align-items: flex-start;\r\n        width:40%;\r\n        height: 100%;\r\n        }\r\n.slide12[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\r\n        width: 80%;\r\n        height: 40px;\r\n        border-color: rgb(212, 5, 5);\r\n            }\r\n\r\n.slide13[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide13[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    margin-left: 50px;\r\n    order: 1;\r\n    display: flex;\r\n    align-items:center;\r\n    width: 37%;\r\n    height: 100%;\r\n}\r\n.slide13[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: center;\r\n    width:40%;\r\n    height: 100%;\r\n    }\r\n.slide13[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\nfont-size: 50px;\r\n    }\r\n.slide13[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    width: 80%;\r\n    height: 40px;\r\n    margin: 50px 0 50px 0;\r\n    border-color: rgb(212, 5, 5);\r\n        }\r\n\r\n.slide14[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-around;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide14[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    align-items: center;\r\n    width: 37%;\r\n    height: 100%;\r\n}\r\n.slide14[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: flex-start;\r\n    width:40%;\r\n    height: 100%;\r\n    }\r\n.slide14[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 50px;\r\n    }\r\n.slide14[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\r\n        margin-top: 50px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-around;\r\n        width: 100%;\r\n    }\r\n\r\n.slide15[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n        width: 100%;\r\n        height: 100%;\r\n    }\r\n.slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        align-items:center;\r\n        width:53%;\r\n        height: 100%;\r\n    }\r\n.slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n        order: 2;\r\n    \r\n            }\r\n.slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n        order: 1;\r\n        font-size: 50px;\r\n        text-align: center;\r\n        margin-bottom: 20px;\r\n            }\r\n.slide15[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        align-items: flex-start;\r\n        width:40%;\r\n        height: 100%;\r\n        }\r\n.slide15[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\r\n        width: 80%;\r\n        height: 40px;\r\n        border-color: rgb(212, 5, 5);\r\n            }\r\n\r\n.slide16[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-around;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide16[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    align-items: center;\r\n    width: 37%;\r\n    height: 100%;\r\n}\r\n.slide16[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: flex-start;\r\n    width:40%;\r\n    height: 100%;\r\n    }\r\n.slide16[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 50px;\r\n    }\r\n.slide16[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\r\n        margin-top: 50px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-around;\r\n        width: 100%;\r\n    }\r\n\r\n.slide17[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n        width: 100%;\r\n        height: 100%;\r\n    }\r\n.slide17[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        align-items:center;\r\n        width:53%;\r\n        height: 100%;\r\n    }\r\n.slide17[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n        order: 2;\r\n    \r\n            }\r\n.slide17[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n        order: 1;\r\n        font-size: 50px;\r\n        text-align: center;\r\n        margin-bottom: 20px;\r\n            }\r\n.slide17[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        align-items: flex-start;\r\n        width:40%;\r\n        height: 100%;\r\n        }\r\n.slide17[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\r\n        width: 80%;\r\n        height: 40px;\r\n        border-color: rgb(212, 5, 5);\r\n            }\r\n\r\n.slide18[_ngcontent-%COMP%]{\r\n            display: flex;\r\n            flex-direction: row;\r\n            justify-content: space-between;\r\n            width: 100%;\r\n            height: 100%;\r\n        }\r\n.slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n            order: 2;\r\n            display: flex;\r\n            flex-direction: column;\r\n            justify-content: center;\r\n            align-items:center;\r\n            width:53%;\r\n            height: 100%;\r\n        }\r\n.slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n            order: 2;\r\n            height: 30%;\r\n            width: 15%;\r\n                }\r\n.slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n            order: 2;\r\n            font-size: 50px;\r\n            text-align: center;\r\n            margin-bottom: 20px;\r\n                }\r\n.slide18[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n            order: 1;\r\n            display: flex;\r\n            flex-direction: column;\r\n            justify-content: center;\r\n            align-items: center;\r\n            width:40%;\r\n            height: 100%;\r\n            }\r\n.slide18[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   select[_ngcontent-%COMP%]{\r\n            width: 80%;\r\n            height: 40px;\r\n            border-color: rgb(212, 5, 5);\r\n                }\r\n\r\n.slide19[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n        width: 100%;\r\n        height: 100%;\r\n    }\r\n.slide19[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n        margin-left: 50px;\r\n        display: flex;\r\n        align-items: center;\r\n        order: 1;\r\n        width: 37%;\r\n        height: 100%;\r\n    }\r\n.slide19[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        align-items: flex-start;\r\n        width:40%;\r\n        height: 100%;\r\n        }\r\n.slide19[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 50px;\r\n        }\r\n.slide19[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n        width: 80%;\r\n        height: 40px;\r\n        border-color: rgb(212, 5, 5);\r\n            }\r\n\r\n.slide20[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    align-items: center;\r\n    justify-content: center;\r\n    order: 1;\r\n    width: 60%;\r\n    height: 80%;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  height: 80%;\r\n  width: 40%;\r\n  filter: drop-shadow(0.4rem 0.4rem 0.45rem rgba(0, 0, 30, 0.5));\r\n}\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: flex-start;\r\n    width:60%;\r\n    height: 100%;\r\n    }\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\nfont-size: 50px;\r\n    }\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    width: 90%;\r\n    height: 40px;\r\n    border-color: rgb(212, 5, 5);\r\n        }\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\n        margin-top: 20px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n        width: 90%;\r\n    }\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%] {\r\n        margin-top: 20px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n        width: 90%;\r\n    }\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n        width: 40%;\r\n    }\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        align-items: center;\r\n        margin: 0;\r\n    }\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n        width: 90%;\r\n    }\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        align-items: center;\r\n        margin: 0;\r\n    }\r\n\r\n.slide21[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: center;\r\n    align-items: center;\r\n    width: 100%;\r\n    height: 70%;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    align-items: flex-end;\r\n    justify-content: flex-end;\r\n    order: 1;\r\n    width: 22%;\r\n    height: 100%;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n  height: 70%;\r\n  width: 30%;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content:flex-start;\r\n    align-items: center;\r\n    width:70%;\r\n    height: 80%;\r\n    }\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n        line-height: normal;\r\n       color: green;\r\n       font-size: 50px;\r\n        }\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n          \r\n            font-size: 100px;\r\n            margin: 10px;\r\n                }\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n        line-height: normal;\r\n        color: rgb(0, 0, 133);\r\n        font-size: 30px;\r\n            }\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .prix[_ngcontent-%COMP%] {\r\n        color: red;\r\n        line-height: normal;\r\n            }\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .faild[_ngcontent-%COMP%]{\r\n        color: red;\r\n        line-height: normal;\r\n        font-size: 60px;\r\n            }\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n        width: 100px;\r\n            }\r\n.slide21[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]{\r\n    order: 3;\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: flex-start;\r\n    align-items: flex-end;\r\n    width: 22%;\r\n    height: 90%;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n color: green;\r\n width: 80px;\r\n}\r\n\r\n.slide24[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide24[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    margin-left: 50px;\r\n    order: 2;\r\n    display: flex;\r\n    align-items:flex-end;\r\n    width: 47%;\r\n    height: 100%;\r\n    margin-top: 10px;\r\n}\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: center;\r\n    width:40%;\r\n    height: 100%;\r\n    }\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 50px;\r\n    text-align: center;\r\n    }\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    width: 80%;\r\n    height: 40px;\r\n    margin: 50px 0 50px 0;\r\n    border-color: rgb(212, 5, 5);\r\n        }\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\r\n       display: flex;\r\n       flex-direction: row;\r\n       justify-content: space-around;\r\n       align-items: flex-start;\r\n       width: 100%;\r\n\r\n            }\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .calend[_ngcontent-%COMP%]{\r\n        width: 450px;\r\n        height: 450px;\r\n    }\r\n\r\n.slide25[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: center;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    align-items: flex-end;\r\n    justify-content: flex-end;\r\n    order: 1;\r\n    width: 22%;\r\n    height: 20%;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content:flex-start;\r\n    align-items: center;\r\n    width:70%;\r\n    height: 80%;\r\n    }\r\n.slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    width: 50px;\r\n        }\r\n.slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n    color: rgb(0, 0, 133);\r\n    font-size: 40px;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]{\r\n    order: 3;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n    text-align: center;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   .socialMedia[_ngcontent-%COMP%] {\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: center;\r\n   }\r\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   .socialMedia[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n width: 60px;\r\n}\r\n\r\n.slide26[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items:center;\r\n    width:53%;\r\n    height: 100%;\r\n}\r\n.slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    height: 30%;\r\n    width: 15%;\r\n        }\r\n.slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    font-size: 50px;\r\n    text-align: center;\r\n    margin-bottom: 20px;\r\n        }\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: flex-start;\r\n    width:60%;\r\n    height: 100%;\r\n    }\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\nfont-size: 50px;\r\n    }\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    width: 90%;\r\n    height: 40px;\r\n    border-color: rgb(212, 5, 5);\r\n        }\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\n        margin: 20px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n        align-items: flex-start;\r\n        width: 90%;\r\n    }\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%] {\r\n        margin-top: 20px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-around;\r\n        width: 100%;\r\n    }\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]  {\r\n        margin-top: 20px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-around;\r\n        width: 90%;\r\n    }\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n        width: 90%;\r\n    }\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        align-items: center;\r\n        margin: 0;\r\n    }\r\n\r\n.slide27[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide27[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items:center;\r\n    width:53%;\r\n    height: 100%;\r\n}\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-start;\r\n    width:40%;\r\n    height: 100%;\r\n    \r\n    }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .bSearch[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-flow: wrap;\r\n        justify-content: flex-start;\r\n        align-items: baseline;\r\n    }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .bSearch[_ngcontent-%COMP%]   .search[_ngcontent-%COMP%] {\r\n        margin-bottom: 15px;\r\n        margin-left: 15px;\r\n        width: 50%;\r\n        border-radius: 50px;\r\n        background-color: rgb(134, 134, 134);\r\n        color:white;\r\n      }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .bSearch[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\r\n        margin-left: -29px;\r\n        color: white;\r\n      }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .list-item[_ngcontent-%COMP%] {  \r\n\r\nborder: 3px solid rgb(255, 0, 0);\r\nborder-radius: 4px;\r\ncolor: rgb(153, 153, 153);\r\nline-height: 90px;\r\nfont-weight: 400;\r\nbackground-color: rgb(255, 255, 255);\r\nwidth: 88%;\r\n}\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%] {\r\n    height: 100%;\r\n    border: none;\r\n    color: rgb(153, 153, 153);\r\n    line-height: 45px;\r\n    background-color: rgb(255, 255, 255);\r\n    box-shadow: rgba(0,0,0,0.2) 0px 1px 2px 0px;\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    width: 100%;\r\n  }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: center;\r\n    width: 10%;\r\n  }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: center;\r\n    height: 100%;\r\n    width: -moz-available;\r\n    }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n        margin-top: 0;\r\n        margin-bottom: 1rem;\r\n        height: 29px;\r\n        }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]   .block3[_ngcontent-%COMP%]{\r\n    display: inline;\r\n    flex-direction: column;\r\n    width: 10%;\r\n  }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]   .block3[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    width: 100%;\r\n  }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]:hover{\r\n    background-color: rgb(218, 98, 98);\r\n  }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]:hover   p[_ngcontent-%COMP%]{\r\n    color: white;\r\n    }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]:hover   input[_ngcontent-%COMP%]{\r\n        color: white;\r\n    }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .item-content[_ngcontent-%COMP%]:hover   i[_ngcontent-%COMP%]{\r\n        color: white;\r\n    }\r\n.slide27[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .contentTab[_ngcontent-%COMP%]{\r\n          width: 100%;\r\n          height: 500px;\r\n          overflow-y: scroll;\r\n      }\r\n\r\n@media screen and (max-width: 768px) {\r\n   \r\n    .body[_ngcontent-%COMP%]{\r\n        width: 100%;\r\n        height: 100%;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-between;\r\n    }\r\n    .footer[_ngcontent-%COMP%]{\r\n        flex-direction: column;\r\n    }\r\n    .left[_ngcontent-%COMP%]{\r\n        justify-content: center;\r\n        align-items: center;\r\n        width: 100%;\r\n    }\r\n    .left[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n        width: 60px;\r\n    }\r\n    .point[_ngcontent-%COMP%]{\r\n        text-align: center;\r\n        font-size: 7px;\r\n    }\r\n     .center[_ngcontent-%COMP%]{\r\n      width: 90%;\r\n      margin-left: 20px;  \r\n     }\r\n    .center[_ngcontent-%COMP%]   .listP[_ngcontent-%COMP%]{\r\n   text-align: center;\r\n    }\r\n    \r\n    \r\n.slide0[_ngcontent-%COMP%]{\r\n    flex-direction: column;\r\n    justify-content: flex-start;\r\n    align-items: center ;\r\n    height: 100%;\r\n}\r\n.slide0[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    width: 80%;\r\n    height: 100%;\r\n  }\r\n\r\n.slide0[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n   width:100%;\r\n   }\r\n\r\n.slide0[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n   }\r\n\r\n\r\n\r\n.slide1[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    align-items: center;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide1[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  width: 90%;\r\n  height: 100%;\r\n}\r\n\r\n.slide1[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\nwidth: 100%;\r\n}\r\n.slide1[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    }\r\n\r\n  \r\n  .slide2[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content:space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide2[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n  order: 2;\r\n  width: 100%;\r\n  height: 100%;\r\n  display: flex;\r\n  flex-direction: column;\r\n  justify-content:flex-end;\r\n}\r\n.slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n   flex-direction: column;\r\n   justify-content: space-around;\r\n   align-items: center;\r\n   width:100%;\r\n   height: 100%;\r\n   }\r\n   .slide2[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n   }\r\n \r\n  \r\n  .slide3[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide3[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    width: 100%;\r\n    height: 100%;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-end;\r\n}\r\n.slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n    .slide3[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n    }\r\n \r\n\r\n\r\n.slide4[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content:space-between;\r\n    align-items: center;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide4[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    width: 80%;\r\n    height: 100%;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content:flex-end;\r\n}\r\n.slide4[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n    .slide4[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n    }\r\n\r\n    \r\n     \r\n     .slide5[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: space-between;\r\n        align-items: center;\r\n        width: 100%;\r\n        height: 100%;\r\n\r\n    }\r\n    .slide5[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        width: 100%;\r\n        height: 100%;\r\n        z-index: 1;\r\n     \r\n    }\r\n    .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n        order: 1;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: space-between;\r\n        align-items: center;\r\n        width:100%;\r\n        height: 100%;\r\n        }\r\n        .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n        font-size: 30px;\r\n        text-align: center;\r\n        margin-block: auto;\r\n        }\r\n        .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]{\r\n\r\n           display: flex;\r\n           flex-direction: column;\r\n           justify-content: center;\r\n            width: 100%;\r\n            height: 20%;\r\n        }\r\n        .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]{\r\n            display: flex;\r\n            flex-direction: column;\r\n            justify-content: center;\r\n            align-items: flex-end;\r\n            width: 75%;\r\n         }\r\n    \r\n         .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]{\r\n            display: flex;\r\n            flex-direction: row;\r\n            justify-content: center;  \r\n            margin: 10px;          \r\n            }\r\n            \r\n    .slide5[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .blockBtn[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]   .qty[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        width: 80px;\r\n        height: 50.5px;\r\n        border: 4px solid rgb(212, 5, 5);\r\n        text-align: center;\r\n        }\r\n   \r\n \r\n .slide6[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items:center;\r\n    width:100%;\r\n    height: 100%;\r\n}\r\n.slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    height: 15%;\r\n    width: 15%;\r\n        }\r\n.slide6[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    font-size: 30px;\r\n    text-align: center;\r\n    margin-bottom: 20px;\r\n        }\r\n\r\n\r\n.slide6[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-start;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n\r\n    \r\n\r\n.slide7[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    align-items: center;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide7[_ngcontent-%COMP%]   .colum1[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide7[_ngcontent-%COMP%]   .colum2[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: center;\r\n    align-items: flex-end;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide7[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    width: 90%;\r\n    height: 100%;\r\n}\r\n.slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-end;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    margin-top: 20px;\r\n    }\r\n    .slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n    }\r\n    .slide7[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    width: 80%;\r\n    height: 20px;\r\n    border-color: rgb(212, 5, 5);\r\n        }\r\n    .slide7[_ngcontent-%COMP%]   .progress[_ngcontent-%COMP%]{\r\n    order: 3;\r\n}\r\n\r\n.slider[_ngcontent-%COMP%] {\r\n    -webkit-appearance: none;\r\n    width: 100%;\r\n    height: 15px;\r\n    background: rgb(255, 255, 255);\r\n    outline: none;\r\n    border: 5px solid rgb(189, 8, 8);\r\n    border-radius: 8px;\r\n  }\r\n  \r\n  \r\n  \r\n  .slider[_ngcontent-%COMP%]::-webkit-slider-thumb {\r\n    -webkit-appearance: none;\r\n    appearance: none;\r\n    width: 10px;\r\n    height: 40px;\r\n    background: rgb(248, 224, 5);\r\n    cursor: pointer;\r\n    border: 5px solid rgb(248, 224, 5);\r\n    border-radius: 30px;\r\n  }\r\n  \r\n  \r\n  .slider[_ngcontent-%COMP%]::-moz-range-thumb {\r\n    width: 10px;\r\n    height: 30px;\r\n    background: rgb(255, 255, 255);\r\n    cursor: pointer;\r\n    border: 5px solid rgb(189, 8, 8);\r\n    border-radius: 4px;\r\n  }\r\n.__range[_ngcontent-%COMP%]{\r\n    width: 80%;\r\n    height: 100%;\r\n}\r\n.__range-step[_ngcontent-%COMP%]{\r\n\tposition: relative;                \r\n}\r\n.__range-step[_ngcontent-%COMP%]{\r\n\tposition: relative;                \r\n}\r\n\r\n.__range-max[_ngcontent-%COMP%]{\r\n\tfloat: right;\r\n}\r\n           \r\n\r\n\r\n.__range[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]::range-progress {\tbackground: rgb(189, 8, 8);\r\n}\r\n.slider[_ngcontent-%COMP%]   input[type=range][_ngcontent-%COMP%]::-moz-range-progress {\r\n    background-color: #c657a0;\r\n  }\r\n.__range-step[_ngcontent-%COMP%]   datalist[_ngcontent-%COMP%] {\r\n\tposition:relative;\r\n\tdisplay: flex;\r\n\tjustify-content: space-between;\r\n\theight: auto;\r\n\tbottom: 6px;\r\n\t\r\n\t-webkit-user-select: none;                   \r\n\tuser-select: none; \r\n\t\r\n\tpointer-events:none;  \r\n}\r\n.__range-step[_ngcontent-%COMP%]   datalist[_ngcontent-%COMP%]   option[_ngcontent-%COMP%] {\r\n\twidth: 10px;\r\n\theight: 10px;\r\n\tmin-height: 10px;\r\n\tborder-radius: 100px;\r\n\t\r\n\twhite-space: nowrap;       \r\n  padding:0;\r\n  line-height: 40px;\r\n}\r\n\r\n\r\n\r\n.slide8[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide8[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    align-items: flex-end;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide8[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n    .slide8[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n    }\r\n\r\n    \r\n.slide9[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 750px;\r\n}\r\n.slide9[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    align-items: center;\r\n    flex-direction: column;\r\n    justify-content: flex-end;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n    .slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n    }\r\n    .slide9[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\r\n        margin-top: 50px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-around;\r\n        width: 100%;\r\n    }\r\n\r\n\r\n\r\n.slide10[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 750px;\r\n}\r\n.slide10[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    align-items: center;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n    .slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n    }\r\n    .slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        width: 100%;\r\n        margin-top: 20px;\r\n    }\r\n\r\n    .slide10[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block[_ngcontent-%COMP%]   .sousBlock[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: space-around;\r\n        align-items: center; \r\n        margin-top: 10px;\r\n     }\r\n  \r\n     \r\n        \r\n\r\n.slide11[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height: 750px;\r\n}\r\n.slide11[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    align-items: center;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-end;\r\n    order: 2;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide11[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n    .slide11[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n    }\r\n\r\n  \r\n  .slide12[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content:center;\r\n    width: 100%;\r\n    height: 700px;\r\n}\r\n.slide12[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items:center;\r\n    width:100%;\r\n    height: 100%;\r\n}\r\n.slide12[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    font-size: 30px;\r\n    text-align: center;\r\n    margin-bottom: 20px;\r\n        }\r\n\r\n\r\n.slide12[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n \r\n\r\n\r\n.slide13[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nwidth: 100%;\r\nheight: 750px;\r\n}\r\n.slide13[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\norder: 2;\r\nwidth: 80%;\r\nheight: 100%;\r\n}\r\n.slide13[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\norder: 1;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth:100%;\r\nheight: 100%;\r\n}\r\n.slide13[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\nfont-size: 30px;\r\ntext-align: center;\r\n}\r\n\r\n\r\n.slide14[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: space-between;\r\nwidth: 100%;\r\nheight: 750px;\r\n}\r\n.slide14[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\norder: 2;\r\ndisplay: flex;\r\nalign-items: center;\r\njustify-content: flex-end;\r\nwidth: 100%;\r\nheight: 100%;\r\n}\r\n.slide14[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\norder: 1;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: space-around;\r\nalign-items: center;\r\nwidth:100%;\r\nheight: 100%;\r\n}\r\n.slide14[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\nfont-size: 30px;\r\ntext-align: center;\r\n}\r\n\r\n.slide15[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height:750px;\r\n}\r\n.slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items:center;\r\n    width:100%;\r\n    height: 100%;\r\n}\r\n.slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n    order: 2;\r\n\r\n        }\r\n.slide15[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    font-size: 30px;\r\n    text-align: center;\r\n    margin-bottom: 20px;\r\n        }\r\n\r\n\r\n\r\n.slide15[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-start;\r\n    align-items:center;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n \r\n\r\n\r\n\r\n.slide16[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: space-between;\r\nwidth: 100%;\r\nheight: 750px;\r\n}\r\n.slide16[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\norder: 2;\r\ndisplay: flex;\r\nalign-items: center;\r\njustify-content: flex-end;\r\nwidth: 100%;\r\nheight: 100%;\r\n}\r\n.slide16[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\norder: 1;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: flex-start;\r\nalign-items: center;\r\nwidth:100%;\r\nheight: 100%;\r\n}\r\n.slide16[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\nfont-size: 30px;\r\ntext-align: center;\r\n}\r\n\r\n\r\n.slide17[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    height:750px;\r\n}\r\n.slide17[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items:center;\r\n    width:100%;\r\n    height: 100%;\r\n}\r\n.slide17[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    font-size: 30px;\r\n    text-align: center;\r\n    margin-bottom: 20px;\r\n        }\r\n\r\n.slide17[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: flex-start;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n \r\n       \r\n       .slide18[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        width: 100%;\r\n        height: 100%;\r\n    }\r\n    .slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n        order: 1;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: center;\r\n        align-items:center;\r\n        width:100%;\r\n        height: 100%;\r\n    }\r\n    .slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        height: 20%;\r\n        width: 15%;\r\n            }\r\n    .slide18[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        font-size: 30px;\r\n        text-align: center;\r\n        margin-bottom: 20px;\r\n            }\r\n\r\n    .slide18[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n        order: 2;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: flex-start ;\r\n        align-items: center;\r\n        width:100%;\r\n        height: 100%;\r\n        }\r\n     \r\n    \r\n\r\n.slide19[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide19[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    margin-left: 50px;\r\n    display: flex;\r\n    align-items: center;\r\n    justify-content: center;\r\n    order: 2;\r\n    width: 90%;\r\n    height: 100%;\r\n}\r\n.slide19[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-around;\r\n    align-items: center;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n    .slide19[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    font-size: 30px;\r\n    text-align: center;\r\n    }\r\n\r\n        \r\n\r\n.slide20[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nwidth: 100%;\r\nheight: 100%;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nalign-items: center;\r\njustify-content: center;\r\norder: 1;\r\nwidth: 100%;\r\nheight: 80%;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\nheight: 60%;\r\nwidth: 40%;\r\nfilter: drop-shadow(0.4rem 0.4rem 0.45rem rgba(0, 0, 30, 0.5));\r\n}\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\norder: 2;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: flex-start;\r\nalign-items: center;\r\nwidth:100%;\r\nheight: 100%;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\nfont-size: 30px;\r\ntext-align: center;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\n    margin-top: 0px;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    width: 90%;\r\n}\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%] {\r\n    margin-top: 0px;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    width: 90%;\r\n    }\r\n\r\n.slide20[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    align-items: flex-start;\r\n    flex-direction: column;\r\n    margin-top: 10px;\r\n}\r\n\r\n\r\n.slide21[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 100%;\r\nheight: 100%;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\ndisplay:none;\r\nflex-direction: column;\r\nalign-items: center;\r\njustify-content: center;\r\norder: 1;\r\nwidth: 100%;\r\nheight: 10%;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\norder: 1;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content:center;\r\nalign-items: center;\r\nwidth:80%;\r\nheight: 80%;    \r\n}\r\n\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    line-height: normal;\r\n   color: green;\r\n   font-size: 30px;\r\n   text-align: center;\r\n    }\r\n    .slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n        line-height: normal;\r\n        font-size: 80px;\r\n        margin: 10px;\r\n            }\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n    line-height: normal;\r\n    color: rgb(0, 0, 133);\r\n    font-size: 30px;\r\n    text-align: center;\r\n        }\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .prix[_ngcontent-%COMP%] {\r\n    color: red;\r\n        }\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .faild[_ngcontent-%COMP%]{\r\n    line-height: normal;\r\n    width: 360px;\r\n    color: red;\r\n    font-size: 30px;\r\n    text-align: center;\r\n        }\r\n.slide21[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    width: 80px;\r\n        }\r\n\r\n.slide21[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]{\r\n    order: 3;\r\n    display: flex;\r\n    flex-direction: row;\r\n    justify-content: flex-end;\r\n    align-items: flex-end;\r\n    width:97%;\r\n    height: 15%;\r\n}\r\n.slide21[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    color: green;\r\n    width: 80px;\r\n}\r\n        \r\n\r\n\r\n\r\n.slide24[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nwidth: 100%;\r\nheight: 100%;\r\n}\r\n.slide24[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\nmargin-left: 50px;\r\norder: 2;\r\ndisplay: flex;\r\nalign-items:flex-end;\r\nwidth: 88%;\r\nheight: 100%;\r\n}\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\norder: 1;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: flex-end;\r\nalign-items: center;\r\nwidth:100%;\r\nheight: 100%;\r\nmargin-top: 20px;\r\n}\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\nfont-size: 30px;\r\ntext-align: center;\r\n}\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\nwidth: 80%;\r\nheight: 40px;\r\nmargin: 50px 0 50px 0;\r\nborder-color: rgb(212, 5, 5);\r\n    }\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]{\r\n   display: flex;\r\n   flex-direction: row;\r\n   justify-content: space-around;\r\n   align-items: flex-start;\r\n   width: 100%;\r\n   margin-top: 20px;\r\n\r\n        }\r\n.slide24[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .calend[_ngcontent-%COMP%]{\r\n    width: 350px;\r\n    height: 350px;\r\n}\r\n\r\n\r\n\r\n.slide25[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content: center;\r\nalign-items: center;\r\nwidth: 100%;\r\nheight: 100%;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\ndisplay: flex;\r\nalign-items: flex-end;\r\njustify-content: flex-end;\r\norder: 1;\r\nwidth: 22%;\r\nheight: 20%;\r\n}\r\n\r\n.slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\norder: 2;\r\ndisplay: flex;\r\nflex-direction: column;\r\njustify-content:flex-start;\r\nalign-items: center;\r\nwidth:100%;\r\nheight: 80%;\r\n}\r\n\r\n.slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\nwidth: 50px;\r\nmargin-bottom: 30px;\r\n    }\r\n.slide25[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\n    text-align: center;\r\ncolor: rgb(0, 0, 133);\r\nfont-size: 30px;\r\nline-height: normal;\r\n}\r\n\r\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]{\r\norder: 3;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   p[_ngcontent-%COMP%]{\r\ntext-align: center;\r\nline-height: normal;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   .socialMedia[_ngcontent-%COMP%] {\r\ndisplay: flex;\r\nflex-direction: row;\r\njustify-content: center;\r\n}\r\n.slide25[_ngcontent-%COMP%]   .nextIcon[_ngcontent-%COMP%]   .socialMedia[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\nwidth: 60px;\r\n}\r\n\r\n          \r\n\r\n.slide26[_ngcontent-%COMP%]{\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: space-between;\r\n    align-items: center;\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]{\r\n    order: 1;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items:center;\r\n    width:53%;\r\n    height: 100%;\r\n}\r\n.slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    height: 20%;\r\n    width: 15%;\r\n        }\r\n.slide26[_ngcontent-%COMP%]   .image[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    font-size: 30px;\r\n    text-align: center;\r\n    margin-bottom: 20px;\r\n        }\r\n\r\n.slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]{\r\n    order: 2;\r\n    display: flex;\r\n    flex-direction: column;\r\n    justify-content: center;\r\n    align-items: flex-start;\r\n    width:100%;\r\n    height: 100%;\r\n    }\r\n    .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   h1[_ngcontent-%COMP%]{\r\n     font-size: 50px;\r\n    }\r\n    .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n    width: 90%;\r\n    height: 40px;\r\n    border-color: rgb(212, 5, 5);\r\n    margin-left: 5px;\r\n        }\r\n\r\n    .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]{\r\n        margin: 20px;\r\n        display: flex;\r\n        flex-direction: column;\r\n        justify-content: space-around;\r\n        align-items: flex-start;\r\n        width: 90%;\r\n    }\r\n    .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%] {\r\n        margin-top: 20px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-around;\r\n        width: 100%;\r\n    }\r\n    .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block2[_ngcontent-%COMP%]   mat-radio-group[_ngcontent-%COMP%]  {\r\n        margin-top: 20px;\r\n        display: flex;\r\n        flex-direction: row;\r\n        justify-content: space-around;\r\n        width: 90%;\r\n    }\r\n\r\n    \r\n    .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]{\r\n        width: 90%;\r\n    }\r\n    .slide26[_ngcontent-%COMP%]   .test[_ngcontent-%COMP%]   .block1[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\r\n        display: flex;\r\n        align-items: center;\r\n        margin: 0;\r\n    }\r\n\r\n\r\n    }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdGVzdC90ZXN0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLFdBQVc7SUFDWCxZQUFZO0lBQ1osZUFBZTtJQUNmLFNBQVM7SUFDVCxVQUFVO0FBQ2Q7QUFDQTtJQUNJLFdBQVc7SUFDWCxZQUFZO0lBQ1osYUFBYTtJQUNiLHFCQUFxQjtJQUNyQix1QkFBdUI7QUFDM0I7QUFDQTtJQUNJLFdBQVc7SUFDWCxpQkFBaUI7SUFDakIsaUJBQWlCO0lBQ2pCLFlBQVk7SUFDWiw0QkFBNEI7SUFDNUIsYUFBYTtJQUNiLG1CQUFtQjtBQUN2QjtBQUNBO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiwyQkFBMkI7SUFDM0IsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsaUJBQWlCO0FBQ3JCO0FBQ0E7SUFDSSxVQUFVO0FBQ2Q7QUFDQTtJQUNJLGVBQWU7SUFDZix3QkFBd0I7SUFDeEIsaUJBQWlCOztBQUVyQjtBQUNBO0lBQ0ksd0JBQXdCO0FBQzVCO0FBQ0E7SUFDSSxZQUFZO0lBQ1osZUFBZTtBQUNuQjtBQUNBO0lBQ0ksZUFBZTtJQUNmLGVBQWU7SUFDZixpQkFBaUI7QUFDckI7QUFDQTtJQUNJLFlBQVk7QUFDaEI7QUFFQTtDQUNDLFdBQVc7Q0FDWCxZQUFZO0NBQ1osYUFBYTtDQUNiLG1CQUFtQjtDQUNuQix1QkFBdUI7Q0FDdkIsbUJBQW1CO0FBQ3BCO0FBRUEsK0VBQStFO0FBQy9FO0lBQ0ksZ0NBQWdDO0lBQ2hDLFlBQVk7QUFDaEI7QUFDQztJQUNHLG9DQUFvQztJQUNwQyxZQUFZO0FBQ2hCO0FBQ0EsNkVBQTZFO0FBQzdFO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiw4QkFBOEI7SUFDOUIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtFQUNFLFFBQVE7RUFDUixVQUFVO0VBQ1YsWUFBWTtBQUNkO0FBRUE7Q0FDQyxRQUFRO0NBQ1IsYUFBYTtDQUNiLHNCQUFzQjtHQUNwQix1QkFBdUI7R0FDdkIsa0JBQWtCO0FBQ3JCLFVBQVU7QUFDVixZQUFZO0FBQ1o7QUFFRztJQUNDLFVBQVU7SUFDVixZQUFZO0lBQ1osNEJBQTRCO09BQ3pCO0FBRVAsNkVBQTZFO0FBQzdFO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiw4QkFBOEI7SUFDOUIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtFQUNFLFFBQVE7RUFDUixVQUFVO0VBQ1YsWUFBWTtBQUNkO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtHQUNkLHNCQUFzQjtHQUN0Qix1QkFBdUI7R0FDdkIsbUJBQW1CO0dBQ25CLFNBQVM7R0FDVCxZQUFZO0dBQ1o7QUFDQTtBQUNILGVBQWU7R0FDWjtBQUNBO0lBQ0MsWUFBWTtJQUNaLFlBQVk7SUFDWixtQkFBbUI7SUFDbkIsNEJBQTRCO09BQ3pCO0FBRUEsNkVBQTZFO0FBQzdFO1FBQ0MsYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw4QkFBOEI7UUFDOUIsV0FBVztRQUNYLFlBQVk7SUFDaEI7QUFDQTtNQUNFLFFBQVE7TUFDUixVQUFVO01BQ1YsWUFBWTtJQUNkO0FBQ0E7UUFDSSxRQUFRO1FBQ1IsYUFBYTtPQUNkLHNCQUFzQjtPQUN0Qix1QkFBdUI7T0FDdkIsdUJBQXVCO09BQ3ZCLFNBQVM7T0FDVCxZQUFZO09BQ1o7QUFDQTtJQUNILGVBQWU7T0FDWjtBQUdJO1lBQ0MsVUFBVTtZQUNWLGVBQWU7WUFDZixnQkFBZ0I7WUFDaEIsWUFBWTtZQUNaLFlBQVk7WUFDWiw0QkFBNEI7UUFDaEM7QUFDQTtZQUNJLGVBQWU7WUFDZixnQkFBZ0I7WUFDaEIsWUFBWTtZQUNaLFlBQVk7WUFDWiw0QkFBNEI7UUFDaEM7QUFDQTtZQUNJLGVBQWU7WUFDZixnQkFBZ0I7WUFDaEIsWUFBWTtZQUNaLFlBQVk7WUFDWiw0QkFBNEI7UUFDaEM7QUFDQTtZQUNJLHFCQUFxQjtZQUNyQixtRUFBbUU7WUFDbkUsWUFBWTtZQUNaLFlBQVk7WUFDWiw0QkFBNEI7UUFDaEM7QUFFSCw2RUFBNkU7QUFDMUU7UUFDQSxhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDhCQUE4QjtRQUM5QixXQUFXO1FBQ1gsWUFBWTtJQUNoQjtBQUNBO1FBQ0ksUUFBUTtRQUNSLFVBQVU7UUFDVixZQUFZO0lBQ2hCO0FBQ0E7UUFDSSxRQUFRO1FBQ1IsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qiw2QkFBNkI7UUFDN0IsbUJBQW1CO1FBQ25CLGdCQUFnQjtRQUNoQixTQUFTO1FBQ1QsV0FBVztRQUNYO0FBQ0E7UUFDQSxlQUFlO1FBQ2Y7QUFDQTtXQUNHLGFBQWE7V0FDYixzQkFBc0I7V0FDdEIsdUJBQXVCOztRQUUxQjtBQUNBO1lBQ0ksYUFBYTtZQUNiLG1CQUFtQjtZQUNuQix1QkFBdUI7O1NBRTFCO0FBQ0Q7UUFDQSxZQUFZO1FBQ1osWUFBWTtRQUNaLFdBQVc7UUFDWCw0QkFBNEI7WUFDeEI7QUFDQTtnQkFDSSxZQUFZO2dCQUNaLFdBQVc7b0JBQ1A7QUFFcEIsNkVBQTZFO0FBQ3pFO1FBQ0ksYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw4QkFBOEI7UUFDOUIsV0FBVztRQUNYLFlBQVk7SUFDaEI7QUFDQTtRQUNJLGlCQUFpQjtRQUNqQixRQUFRO1FBQ1IsVUFBVTtRQUNWLFlBQVk7UUFDWixhQUFhO1FBQ2IsVUFBVTtRQUNWLGdCQUFnQjtJQUNwQjtBQUNBO1FBQ0ksUUFBUTtRQUNSLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsdUJBQXVCO1FBQ3ZCLHVCQUF1QjtRQUN2QixTQUFTO1FBQ1QsWUFBWTtRQUNaO0FBQ0E7SUFDSixlQUFlO1FBQ1g7QUFDQTtRQUNBLFVBQVU7UUFDVixZQUFZO1FBQ1osNEJBQTRCO1lBQ3hCO0FBR1AsNkVBQTZFO0FBQzdFO1FBQ0csYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw4QkFBOEI7UUFDOUIsV0FBVztRQUNYLFlBQVk7SUFDaEI7QUFDQTtRQUNJLFFBQVE7UUFDUixVQUFVO1FBQ1YsWUFBWTtRQUNaLFVBQVU7UUFDVixhQUFhO1FBQ2IsZ0JBQWdCO0lBQ3BCO0FBR0E7UUFDSSxRQUFRO1FBQ1IsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsbUJBQW1CO1FBQ25CLFNBQVM7UUFDVCxZQUFZO1FBQ1o7QUFDQTtRQUNBLGVBQWU7UUFDZjtBQUNBO1dBQ0csYUFBYTtXQUNiLG1CQUFtQjtXQUNuQiw2QkFBNkI7WUFDNUIsV0FBVztZQUNYLFdBQVc7UUFDZjtBQUNBO1lBQ0ksYUFBYTtZQUNiLG1CQUFtQjtZQUNuQiwyQkFBMkI7WUFDM0IsbUJBQW1CO1NBQ3RCO0FBRUE7WUFDRyxhQUFhO1lBQ2IsbUJBQW1CO1lBQ25CLHVCQUF1QjtZQUN2QjtBQUVSO1FBQ0ksUUFBUTtRQUNSLFdBQVc7UUFDWCxZQUFZO1FBQ1osZ0NBQWdDO1FBQ2hDLGtCQUFrQjtRQUNsQjtBQUNKO1FBQ0ksUUFBUTtRQUNSLGlCQUFpQjtRQUNqQixVQUFVO1FBQ1YsYUFBYTtRQUNiLG1CQUFtQjtRQUNuQixtQkFBbUI7WUFDZjtBQUNBLGdEQUFnRCxhQUFhLENBQUM7QUFDOUQsK0NBQStDLGFBQWEsQ0FBQztBQUM3RCxrREFBa0QsaUJBQWlCLENBQUM7QUFDcEUsZ0RBQWdELGlCQUFpQixDQUFDO0FBQzFFO1FBQ0ksUUFBUTtRQUNSLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsMkJBQTJCO1FBQzNCO0FBQ0E7WUFDSSxZQUFZO1lBQ1osOEJBQThCO1lBQzlCLGtCQUFrQjtZQUNsQixVQUFVO1lBQ1Ysa0JBQWtCO1lBQ2xCLFlBQVk7WUFDWixZQUFZO1lBQ1osZ0NBQWdDO1lBQ2hDLGFBQWE7WUFDYixzQkFBc0I7WUFDdEIsZUFBZTtZQUNmLHVCQUF1QjtZQUN2QixtQkFBbUI7WUFDbkI7QUFFQSw2RUFBNkU7QUFDN0U7Z0JBQ0ksYUFBYTtnQkFDYixtQkFBbUI7Z0JBQ25CLDhCQUE4QjtnQkFDOUIsV0FBVztnQkFDWCxZQUFZO1lBQ2hCO0FBQ0E7Z0JBQ0ksUUFBUTtnQkFDUixhQUFhO2dCQUNiLHNCQUFzQjtnQkFDdEIsdUJBQXVCO2dCQUN2QixrQkFBa0I7Z0JBQ2xCLFNBQVM7Z0JBQ1QsWUFBWTtZQUNoQjtBQUNBO2dCQUNJLFFBQVE7Z0JBQ1IsV0FBVztnQkFDWCxVQUFVO29CQUNOO0FBQ1I7Z0JBQ0ksUUFBUTtnQkFDUixlQUFlO2dCQUNmLGtCQUFrQjtnQkFDbEIsbUJBQW1CO29CQUNmO0FBR1I7Z0JBQ0ksUUFBUTtnQkFDUixhQUFhO2dCQUNiLHNCQUFzQjtnQkFDdEIsdUJBQXVCO2dCQUN2Qix1QkFBdUI7Z0JBQ3ZCLFNBQVM7Z0JBQ1QsWUFBWTtnQkFDWjtBQUVBO2dCQUNBLFVBQVU7Z0JBQ1YsWUFBWTtnQkFDWiw0QkFBNEI7b0JBQ3hCO0FBR3BCLDZFQUE2RTtBQUM3RTtJQUNJLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiw2QkFBNkI7SUFDN0IsV0FBVztJQUNYLFlBQVk7SUFDWixtQkFBbUI7QUFDdkI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QixXQUFXO0lBQ1gsZ0JBQWdCO0lBQ2hCLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGlCQUFpQjtJQUNqQixRQUFRO0lBQ1IsVUFBVTtJQUNWLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2Qix1QkFBdUI7SUFDdkIsU0FBUztJQUNULFlBQVk7SUFDWjtBQUNBO0FBQ0osZUFBZTtJQUNYO0FBQ0E7SUFDQSxVQUFVO0lBQ1YsWUFBWTtJQUNaLDRCQUE0QjtRQUN4QjtBQUNSO0lBQ0ksUUFBUTtBQUNaO0FBRUE7SUFDSSx3QkFBd0I7SUFDeEIsV0FBVztJQUNYLFlBQVk7SUFDWiw4QkFBOEI7SUFDOUIsYUFBYTtJQUNiLGdDQUFnQztJQUNoQyxrQkFBa0I7RUFDcEI7QUFHQSxzQkFBc0I7QUFDdEI7SUFDRSx3QkFBd0I7SUFDeEIsZ0JBQWdCO0lBQ2hCLFdBQVc7SUFDWCxZQUFZO0lBQ1osNEJBQTRCO0lBQzVCLGVBQWU7SUFDZixrQ0FBa0M7SUFDbEMsbUJBQW1CO0VBQ3JCO0FBRUEsZ0JBQWdCO0FBQ2hCO0lBQ0UsV0FBVztJQUNYLFlBQVk7SUFDWiw4QkFBOEI7SUFDOUIsZUFBZTtJQUNmLGdDQUFnQztJQUNoQyxrQkFBa0I7RUFDcEI7QUFDRjtJQUNJLFVBQVU7SUFDVixZQUFZO0FBQ2hCO0FBQ0E7Q0FDQyxrQkFBa0I7QUFDbkI7QUFDQTtDQUNDLGtCQUFrQjtBQUNuQjtBQUVBO0NBQ0MsWUFBWTtBQUNiO0FBSUEsaUNBQWlDLDBCQUEwQjtBQUMzRDtBQUNBO0lBQ0kseUJBQXlCO0VBQzNCO0FBQ0Y7Q0FDQyxpQkFBaUI7Q0FDakIsYUFBYTtDQUNiLDhCQUE4QjtDQUM5QixZQUFZO0NBQ1osWUFBWTtDQUNaLDJCQUEyQjtDQUMzQix5QkFBeUIsRUFBRSxXQUFXLEVBQ2QsWUFBWSxFQUNiLGVBQWU7Q0FDdEMsaUJBQWlCLEVBQUUsYUFBYTtDQUNoQyx5QkFBeUI7Q0FDekIsbUJBQW1CO0FBQ3BCO0FBQ0E7Q0FDQyxXQUFXO0NBQ1gsWUFBWTtDQUNaLGdCQUFnQjtDQUNoQixvQkFBb0I7Q0FDcEIsY0FBYztDQUNkLG1CQUFtQjtFQUNsQixTQUFTO0VBQ1QsaUJBQWlCO0FBQ25CO0FBR0EsNkVBQTZFO0FBQzdFO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiw2QkFBNkI7SUFDN0IsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsdUJBQXVCO0lBQ3ZCLFNBQVM7SUFDVCxZQUFZO0lBQ1o7QUFDQTtBQUNKLGVBQWU7SUFDWDtBQUNBO0lBQ0EsVUFBVTtJQUNWLFlBQVk7SUFDWiw0QkFBNEI7UUFDeEI7QUFFUiw0RUFBNEU7QUFDNUU7SUFDSSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDZCQUE2QjtJQUM3QixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2Qix1QkFBdUI7SUFDdkIsU0FBUztJQUNULFlBQVk7SUFDWjtBQUNBO0lBQ0EsZUFBZTtJQUNmO0FBQ0E7UUFDSSxnQkFBZ0I7UUFDaEIsYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw2QkFBNkI7UUFDN0IsV0FBVztJQUNmO0FBR0osNkVBQTZFO0FBQzdFO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiw2QkFBNkI7SUFDN0IsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsdUJBQXVCO0lBQ3ZCLFNBQVM7SUFDVCxZQUFZO0lBQ1o7QUFDQTtJQUNBLGVBQWU7SUFDZjtBQUNBO1FBQ0ksZ0JBQWdCO1FBQ2hCLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsNkJBQTZCO1FBQzdCLFdBQVc7SUFDZjtBQUVBO1FBQ0ksYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw2QkFBNkI7UUFDN0IsV0FBVztRQUNYLGdCQUFnQjtJQUNwQjtBQUVBO1FBQ0ksYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qiw2QkFBNkI7UUFDN0IsbUJBQW1CO0tBQ3RCO0FBQ0Q7UUFDSSxRQUFRO1FBQ1IsYUFBYTtRQUNiLG1CQUFtQjtRQUNuQix1QkFBdUI7UUFDdkI7QUFFUjtJQUNJLFFBQVE7SUFDUixXQUFXO0lBQ1gsWUFBWTtJQUNaLGdDQUFnQztJQUNoQyxrQkFBa0I7SUFDbEI7QUFDSjtJQUNJLFFBQVE7SUFDUixpQkFBaUI7SUFDakIsVUFBVTtJQUNWLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsbUJBQW1CO1FBQ2Y7QUFFUjtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDJCQUEyQjtJQUMzQjtBQUNBO1FBQ0ksWUFBWTtRQUNaLDhCQUE4QjtRQUM5QixrQkFBa0I7UUFDbEIsVUFBVTtRQUNWLGtCQUFrQjtRQUNsQixZQUFZO1FBQ1osWUFBWTtRQUNaLGdDQUFnQztRQUNoQyxhQUFhO1FBQ2Isc0JBQXNCO1FBQ3RCLGVBQWU7UUFDZix1QkFBdUI7UUFDdkIsbUJBQW1CO1FBQ25CO0FBR1IsOEVBQThFO0FBQzFFO1FBQ0ksYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw2QkFBNkI7UUFDN0IsV0FBVztRQUNYLFlBQVk7SUFDaEI7QUFDQTtRQUNJLG1CQUFtQjtRQUNuQixhQUFhO1FBQ2IsUUFBUTtRQUNSLFVBQVU7UUFDVixZQUFZO0lBQ2hCO0FBQ0E7UUFDSSxRQUFRO1FBQ1IsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsdUJBQXVCO1FBQ3ZCLFNBQVM7UUFDVCxZQUFZO1FBQ1o7QUFDQTtJQUNKLGVBQWU7UUFDWDtBQUNBO1FBQ0EsVUFBVTtRQUNWLFlBQVk7UUFDWiw0QkFBNEI7WUFDeEI7QUFFTiw4RUFBOEU7QUFDOUU7UUFDRSxhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDhCQUE4QjtRQUM5QixXQUFXO1FBQ1gsWUFBWTtJQUNoQjtBQUNBO1FBQ0ksUUFBUTtRQUNSLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsdUJBQXVCO1FBQ3ZCLGtCQUFrQjtRQUNsQixTQUFTO1FBQ1QsWUFBWTtJQUNoQjtBQUNBO1FBQ0ksUUFBUTtRQUNSLFdBQVc7UUFDWCxVQUFVO1lBQ047QUFDUjtRQUNJLFFBQVE7UUFDUixlQUFlO1FBQ2Ysa0JBQWtCO1FBQ2xCLG1CQUFtQjtZQUNmO0FBR1I7UUFDSSxRQUFRO1FBQ1IsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsdUJBQXVCO1FBQ3ZCLFNBQVM7UUFDVCxZQUFZO1FBQ1o7QUFFQTtRQUNBLFVBQVU7UUFDVixZQUFZO1FBQ1osNEJBQTRCO1lBQ3hCO0FBR1osOEVBQThFO0FBQzlFO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiw4QkFBOEI7SUFDOUIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGlCQUFpQjtJQUNqQixRQUFRO0lBQ1IsYUFBYTtJQUNiLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixTQUFTO0lBQ1QsWUFBWTtJQUNaO0FBQ0E7QUFDSixlQUFlO0lBQ1g7QUFDQTtJQUNBLFVBQVU7SUFDVixZQUFZO0lBQ1oscUJBQXFCO0lBQ3JCLDRCQUE0QjtRQUN4QjtBQUdSLDZFQUE2RTtBQUM3RTtJQUNJLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsNkJBQTZCO0lBQzdCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLHVCQUF1QjtJQUN2QixTQUFTO0lBQ1QsWUFBWTtJQUNaO0FBQ0E7SUFDQSxlQUFlO0lBQ2Y7QUFDQTtRQUNJLGdCQUFnQjtRQUNoQixhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDZCQUE2QjtRQUM3QixXQUFXO0lBQ2Y7QUFFQSw4RUFBOEU7QUFDOUU7UUFDSSxhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDhCQUE4QjtRQUM5QixXQUFXO1FBQ1gsWUFBWTtJQUNoQjtBQUNBO1FBQ0ksUUFBUTtRQUNSLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsdUJBQXVCO1FBQ3ZCLGtCQUFrQjtRQUNsQixTQUFTO1FBQ1QsWUFBWTtJQUNoQjtBQUNBO1FBQ0ksUUFBUTs7WUFFSjtBQUNSO1FBQ0ksUUFBUTtRQUNSLGVBQWU7UUFDZixrQkFBa0I7UUFDbEIsbUJBQW1CO1lBQ2Y7QUFJUjtRQUNJLFFBQVE7UUFDUixhQUFhO1FBQ2Isc0JBQXNCO1FBQ3RCLHVCQUF1QjtRQUN2Qix1QkFBdUI7UUFDdkIsU0FBUztRQUNULFlBQVk7UUFDWjtBQUVBO1FBQ0EsVUFBVTtRQUNWLFlBQVk7UUFDWiw0QkFBNEI7WUFDeEI7QUFHWiw2RUFBNkU7QUFDN0U7SUFDSSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDZCQUE2QjtJQUM3QixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2Qix1QkFBdUI7SUFDdkIsU0FBUztJQUNULFlBQVk7SUFDWjtBQUNBO0lBQ0EsZUFBZTtJQUNmO0FBQ0E7UUFDSSxnQkFBZ0I7UUFDaEIsYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw2QkFBNkI7UUFDN0IsV0FBVztJQUNmO0FBSUEsOEVBQThFO0FBQzlFO1FBQ0ksYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw4QkFBOEI7UUFDOUIsV0FBVztRQUNYLFlBQVk7SUFDaEI7QUFDQTtRQUNJLFFBQVE7UUFDUixhQUFhO1FBQ2Isc0JBQXNCO1FBQ3RCLHVCQUF1QjtRQUN2QixrQkFBa0I7UUFDbEIsU0FBUztRQUNULFlBQVk7SUFDaEI7QUFDQTtRQUNJLFFBQVE7O1lBRUo7QUFDUjtRQUNJLFFBQVE7UUFDUixlQUFlO1FBQ2Ysa0JBQWtCO1FBQ2xCLG1CQUFtQjtZQUNmO0FBSVI7UUFDSSxRQUFRO1FBQ1IsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsdUJBQXVCO1FBQ3ZCLFNBQVM7UUFDVCxZQUFZO1FBQ1o7QUFFQTtRQUNBLFVBQVU7UUFDVixZQUFZO1FBQ1osNEJBQTRCO1lBQ3hCO0FBR0QsOEVBQThFO0FBQzlFO1lBQ0MsYUFBYTtZQUNiLG1CQUFtQjtZQUNuQiw4QkFBOEI7WUFDOUIsV0FBVztZQUNYLFlBQVk7UUFDaEI7QUFDQTtZQUNJLFFBQVE7WUFDUixhQUFhO1lBQ2Isc0JBQXNCO1lBQ3RCLHVCQUF1QjtZQUN2QixrQkFBa0I7WUFDbEIsU0FBUztZQUNULFlBQVk7UUFDaEI7QUFDQTtZQUNJLFFBQVE7WUFDUixXQUFXO1lBQ1gsVUFBVTtnQkFDTjtBQUNSO1lBQ0ksUUFBUTtZQUNSLGVBQWU7WUFDZixrQkFBa0I7WUFDbEIsbUJBQW1CO2dCQUNmO0FBR1I7WUFDSSxRQUFRO1lBQ1IsYUFBYTtZQUNiLHNCQUFzQjtZQUN0Qix1QkFBdUI7WUFDdkIsbUJBQW1CO1lBQ25CLFNBQVM7WUFDVCxZQUFZO1lBQ1o7QUFFQTtZQUNBLFVBQVU7WUFDVixZQUFZO1lBQ1osNEJBQTRCO2dCQUN4QjtBQUloQiw4RUFBOEU7QUFDMUU7UUFDSSxhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDhCQUE4QjtRQUM5QixXQUFXO1FBQ1gsWUFBWTtJQUNoQjtBQUNBO1FBQ0ksaUJBQWlCO1FBQ2pCLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsUUFBUTtRQUNSLFVBQVU7UUFDVixZQUFZO0lBQ2hCO0FBQ0E7UUFDSSxRQUFRO1FBQ1IsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsdUJBQXVCO1FBQ3ZCLFNBQVM7UUFDVCxZQUFZO1FBQ1o7QUFDQTtJQUNKLGVBQWU7UUFDWDtBQUNBO1FBQ0EsVUFBVTtRQUNWLFlBQVk7UUFDWiw0QkFBNEI7WUFDeEI7QUFJWiw4RUFBOEU7QUFDOUU7SUFDSSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDhCQUE4QjtJQUM5QixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQix1QkFBdUI7SUFDdkIsUUFBUTtJQUNSLFVBQVU7SUFDVixXQUFXO0FBQ2Y7QUFDQTtFQUNFLFdBQVc7RUFDWCxVQUFVO0VBQ1YsOERBQThEO0FBQ2hFO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsdUJBQXVCO0lBQ3ZCLFNBQVM7SUFDVCxZQUFZO0lBQ1o7QUFDQTtBQUNKLGVBQWU7SUFDWDtBQUNBO0lBQ0EsVUFBVTtJQUNWLFlBQVk7SUFDWiw0QkFBNEI7UUFDeEI7QUFFSjtRQUNJLGdCQUFnQjtRQUNoQixhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDhCQUE4QjtRQUM5QixVQUFVO0lBQ2Q7QUFDQTtRQUNJLGdCQUFnQjtRQUNoQixhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDhCQUE4QjtRQUM5QixVQUFVO0lBQ2Q7QUFFQTtRQUNJLFVBQVU7SUFDZDtBQUNBO1FBQ0ksYUFBYTtRQUNiLG1CQUFtQjtRQUNuQixTQUFTO0lBQ2I7QUFDQTtRQUNJLFVBQVU7SUFDZDtBQUNBO1FBQ0ksYUFBYTtRQUNiLG1CQUFtQjtRQUNuQixTQUFTO0lBQ2I7QUFFSiw4RUFBOEU7QUFDOUU7SUFDSSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFdBQVc7QUFDZjtBQUNBO0lBQ0ksYUFBYTtJQUNiLHFCQUFxQjtJQUNyQix5QkFBeUI7SUFDekIsUUFBUTtJQUNSLFVBQVU7SUFDVixZQUFZO0FBQ2hCO0FBQ0E7RUFDRSxXQUFXO0VBQ1gsVUFBVTtBQUNaO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0QiwwQkFBMEI7SUFDMUIsbUJBQW1CO0lBQ25CLFNBQVM7SUFDVCxXQUFXO0lBQ1g7QUFFQTtRQUNJLG1CQUFtQjtPQUNwQixZQUFZO09BQ1osZUFBZTtRQUNkO0FBQ0E7O1lBRUksZ0JBQWdCO1lBQ2hCLFlBQVk7Z0JBQ1I7QUFDWjtRQUNJLG1CQUFtQjtRQUNuQixxQkFBcUI7UUFDckIsZUFBZTtZQUNYO0FBQ1I7UUFDSSxVQUFVO1FBQ1YsbUJBQW1CO1lBQ2Y7QUFDUjtRQUNJLFVBQVU7UUFDVixtQkFBbUI7UUFDbkIsZUFBZTtZQUNYO0FBQ1I7UUFDSSxZQUFZO1lBQ1I7QUFJWjtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLDJCQUEyQjtJQUMzQixxQkFBcUI7SUFDckIsVUFBVTtJQUNWLFdBQVc7QUFDZjtBQUNBO0NBQ0MsWUFBWTtDQUNaLFdBQVc7QUFDWjtBQUdBLDhFQUE4RTtBQUM5RTtJQUNJLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsOEJBQThCO0lBQzlCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxpQkFBaUI7SUFDakIsUUFBUTtJQUNSLGFBQWE7SUFDYixvQkFBb0I7SUFDcEIsVUFBVTtJQUNWLFlBQVk7SUFDWixnQkFBZ0I7QUFDcEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsU0FBUztJQUNULFlBQVk7SUFDWjtBQUNBO0lBQ0EsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQjtBQUNBO0lBQ0EsVUFBVTtJQUNWLFlBQVk7SUFDWixxQkFBcUI7SUFDckIsNEJBQTRCO1FBQ3hCO0FBQ0o7T0FDRyxhQUFhO09BQ2IsbUJBQW1CO09BQ25CLDZCQUE2QjtPQUM3Qix1QkFBdUI7T0FDdkIsV0FBVzs7WUFFTjtBQUNSO1FBQ0ksWUFBWTtRQUNaLGFBQWE7SUFDakI7QUFHSiw4RUFBOEU7QUFDOUU7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGFBQWE7SUFDYixxQkFBcUI7SUFDckIseUJBQXlCO0lBQ3pCLFFBQVE7SUFDUixVQUFVO0lBQ1YsV0FBVztBQUNmO0FBRUE7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0QiwwQkFBMEI7SUFDMUIsbUJBQW1CO0lBQ25CLFNBQVM7SUFDVCxXQUFXO0lBQ1g7QUFFSjtJQUNJLFdBQVc7UUFDUDtBQUNSO0lBQ0kscUJBQXFCO0lBQ3JCLGVBQWU7QUFDbkI7QUFFQTtJQUNJLFFBQVE7QUFDWjtBQUNBO0lBQ0ksa0JBQWtCO0FBQ3RCO0FBQ0E7SUFDSSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLHVCQUF1QjtHQUN4QjtBQUNIO0NBQ0MsV0FBVztBQUNaO0FBR0EsOEVBQThFO0FBQzlFO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiw4QkFBOEI7SUFDOUIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixrQkFBa0I7SUFDbEIsU0FBUztJQUNULFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixXQUFXO0lBQ1gsVUFBVTtRQUNOO0FBQ1I7SUFDSSxRQUFRO0lBQ1IsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixtQkFBbUI7UUFDZjtBQUVSO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLHVCQUF1QjtJQUN2QixTQUFTO0lBQ1QsWUFBWTtJQUNaO0FBQ0E7QUFDSixlQUFlO0lBQ1g7QUFDQTtJQUNBLFVBQVU7SUFDVixZQUFZO0lBQ1osNEJBQTRCO1FBQ3hCO0FBRUo7UUFDSSxZQUFZO1FBQ1osYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw4QkFBOEI7UUFDOUIsdUJBQXVCO1FBQ3ZCLFVBQVU7SUFDZDtBQUNBO1FBQ0ksZ0JBQWdCO1FBQ2hCLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsNkJBQTZCO1FBQzdCLFdBQVc7SUFDZjtBQUNBO1FBQ0ksZ0JBQWdCO1FBQ2hCLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsNkJBQTZCO1FBQzdCLFVBQVU7SUFDZDtBQUdBO1FBQ0ksVUFBVTtJQUNkO0FBQ0E7UUFDSSxhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLFNBQVM7SUFDYjtBQUVILDhFQUE4RTtBQUM5RTtJQUNHLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsOEJBQThCO0lBQzlCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxZQUFZO0FBQ2hCO0FBS0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0QiwyQkFBMkI7SUFDM0IsU0FBUztJQUNULFlBQVk7O0lBRVo7QUFDQTtRQUNJLGFBQWE7UUFDYixlQUFlO1FBQ2YsMkJBQTJCO1FBQzNCLHFCQUFxQjtJQUN6QjtBQUNBO1FBQ0ksbUJBQW1CO1FBQ25CLGlCQUFpQjtRQUNqQixVQUFVO1FBQ1YsbUJBQW1CO1FBQ25CLG9DQUFvQztRQUNwQyxXQUFXO01BQ2I7QUFDQTtRQUNFLGtCQUFrQjtRQUNsQixZQUFZO01BQ2Q7QUFFQTs7QUFFTixnQ0FBZ0M7QUFDaEMsa0JBQWtCO0FBQ2xCLHlCQUF5QjtBQUN6QixpQkFBaUI7QUFDakIsZ0JBQWdCO0FBQ2hCLG9DQUFvQztBQUNwQyxVQUFVO0FBQ1Y7QUFFRTtJQUNFLFlBQVk7SUFDWixZQUFZO0lBQ1oseUJBQXlCO0lBQ3pCLGlCQUFpQjtJQUNqQixvQ0FBb0M7SUFDcEMsMkNBQTJDO0lBQzNDLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsNkJBQTZCO0lBQzdCLG1CQUFtQjtJQUNuQixXQUFXO0VBQ2I7QUFHQTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixVQUFVO0VBQ1o7QUFFQTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1oscUJBQXFCO0lBQ3JCO0FBRUE7UUFDSSxhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLFlBQVk7UUFDWjtBQUVOO0lBQ0UsZUFBZTtJQUNmLHNCQUFzQjtJQUN0QixVQUFVO0VBQ1o7QUFDQTtJQUNFLFdBQVc7RUFDYjtBQUNBO0lBQ0Usa0NBQWtDO0VBQ3BDO0FBQ0E7SUFDRSxZQUFZO0lBQ1o7QUFDQTtRQUNJLFlBQVk7SUFDaEI7QUFDQTtRQUNJLFlBQVk7SUFDaEI7QUFHRTtVQUNJLFdBQVc7VUFDWCxhQUFhO1VBQ2Isa0JBQWtCO01BQ3RCO0FBTU4sMEdBQTBHO0FBQzFHOztJQUVJO1FBQ0ksV0FBVztRQUNYLFlBQVk7UUFDWixhQUFhO1FBQ2IsbUJBQW1CO1FBQ25CLDhCQUE4QjtJQUNsQztJQUNBO1FBQ0ksc0JBQXNCO0lBQzFCO0lBQ0E7UUFDSSx1QkFBdUI7UUFDdkIsbUJBQW1CO1FBQ25CLFdBQVc7SUFDZjtJQUNBO1FBQ0ksV0FBVztJQUNmO0lBQ0E7UUFDSSxrQkFBa0I7UUFDbEIsY0FBYztJQUNsQjtLQUNDO01BQ0MsVUFBVTtNQUNWLGlCQUFpQjtLQUNsQjtJQUNEO0dBQ0Qsa0JBQWtCO0lBQ2pCOztJQUVBLDZFQUE2RTtBQUNqRjtJQUNJLHNCQUFzQjtJQUN0QiwyQkFBMkI7SUFDM0Isb0JBQW9CO0lBQ3BCLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixVQUFVO0lBQ1YsWUFBWTtFQUNkOztBQUVGO0dBQ0csVUFBVTtHQUNWOztBQUVIO0lBQ0ksZUFBZTtHQUNoQjs7O0FBR0gsNkVBQTZFO0FBQzdFO0lBQ0ksYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw4QkFBOEI7SUFDOUIsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsVUFBVTtFQUNWLFlBQVk7QUFDZDs7QUFFQTtBQUNBLFdBQVc7QUFDWDtBQUNBO0lBQ0ksZUFBZTtJQUNmOztFQUVGLDZFQUE2RTtFQUM3RTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsNkJBQTZCO0lBQzdCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7RUFDRSxRQUFRO0VBQ1IsV0FBVztFQUNYLFlBQVk7RUFDWixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLHdCQUF3QjtBQUMxQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7R0FDZCxzQkFBc0I7R0FDdEIsNkJBQTZCO0dBQzdCLG1CQUFtQjtHQUNuQixVQUFVO0dBQ1YsWUFBWTtHQUNaO0dBQ0E7SUFDQyxlQUFlO0lBQ2Ysa0JBQWtCO0dBQ25COztFQUVELDZFQUE2RTtFQUM3RTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsOEJBQThCO0lBQzlCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsV0FBVztJQUNYLFlBQVk7SUFDWixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHlCQUF5QjtBQUM3QjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsNkJBQTZCO0lBQzdCLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsWUFBWTtJQUNaO0lBQ0E7SUFDQSxlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCOzs7QUFHSiw2RUFBNkU7QUFDN0U7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDZCQUE2QjtJQUM3QixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixVQUFVO0lBQ1YsWUFBWTtJQUNaLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsd0JBQXdCO0FBQzVCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw2QkFBNkI7SUFDN0IsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixZQUFZO0lBQ1o7SUFDQTtJQUNBLGVBQWU7SUFDZixrQkFBa0I7SUFDbEI7OztLQUdDLDZFQUE2RTtLQUM3RTtRQUNHLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsOEJBQThCO1FBQzlCLG1CQUFtQjtRQUNuQixXQUFXO1FBQ1gsWUFBWTs7SUFFaEI7SUFDQTtRQUNJLFFBQVE7UUFDUixXQUFXO1FBQ1gsWUFBWTtRQUNaLFVBQVU7O0lBRWQ7SUFDQTtRQUNJLFFBQVE7UUFDUixhQUFhO1FBQ2Isc0JBQXNCO1FBQ3RCLDhCQUE4QjtRQUM5QixtQkFBbUI7UUFDbkIsVUFBVTtRQUNWLFlBQVk7UUFDWjtRQUNBO1FBQ0EsZUFBZTtRQUNmLGtCQUFrQjtRQUNsQixrQkFBa0I7UUFDbEI7UUFDQTs7V0FFRyxhQUFhO1dBQ2Isc0JBQXNCO1dBQ3RCLHVCQUF1QjtZQUN0QixXQUFXO1lBQ1gsV0FBVztRQUNmO1FBQ0E7WUFDSSxhQUFhO1lBQ2Isc0JBQXNCO1lBQ3RCLHVCQUF1QjtZQUN2QixxQkFBcUI7WUFDckIsVUFBVTtTQUNiOztTQUVBO1lBQ0csYUFBYTtZQUNiLG1CQUFtQjtZQUNuQix1QkFBdUI7WUFDdkIsWUFBWTtZQUNaOztJQUVSO1FBQ0ksUUFBUTtRQUNSLFdBQVc7UUFDWCxjQUFjO1FBQ2QsZ0NBQWdDO1FBQ2hDLGtCQUFrQjtRQUNsQjs7Q0FFUCw2RUFBNkU7Q0FDN0U7SUFDRyxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLFdBQVc7SUFDWCxVQUFVO1FBQ047QUFDUjtJQUNJLFFBQVE7SUFDUixlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCLG1CQUFtQjtRQUNmOzs7QUFHUjtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDJCQUEyQjtJQUMzQixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7SUFDWjs7O0FBR0osNkVBQTZFO0FBQzdFO0lBQ0ksYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw4QkFBOEI7SUFDOUIsbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw4QkFBOEI7SUFDOUIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QixxQkFBcUI7SUFDckIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixVQUFVO0lBQ1YsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIseUJBQXlCO0lBQ3pCLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsWUFBWTtJQUNaLGdCQUFnQjtJQUNoQjtJQUNBO0lBQ0EsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQjtJQUNBO0lBQ0EsVUFBVTtJQUNWLFlBQVk7SUFDWiw0QkFBNEI7UUFDeEI7SUFDSjtJQUNBLFFBQVE7QUFDWjs7QUFFQTtJQUNJLHdCQUF3QjtJQUN4QixXQUFXO0lBQ1gsWUFBWTtJQUNaLDhCQUE4QjtJQUM5QixhQUFhO0lBQ2IsZ0NBQWdDO0lBQ2hDLGtCQUFrQjtFQUNwQjs7O0VBR0Esc0JBQXNCO0VBQ3RCO0lBQ0Usd0JBQXdCO0lBQ3hCLGdCQUFnQjtJQUNoQixXQUFXO0lBQ1gsWUFBWTtJQUNaLDRCQUE0QjtJQUM1QixlQUFlO0lBQ2Ysa0NBQWtDO0lBQ2xDLG1CQUFtQjtFQUNyQjs7RUFFQSxnQkFBZ0I7RUFDaEI7SUFDRSxXQUFXO0lBQ1gsWUFBWTtJQUNaLDhCQUE4QjtJQUM5QixlQUFlO0lBQ2YsZ0NBQWdDO0lBQ2hDLGtCQUFrQjtFQUNwQjtBQUNGO0lBQ0ksVUFBVTtJQUNWLFlBQVk7QUFDaEI7QUFDQTtDQUNDLGtCQUFrQjtBQUNuQjtBQUNBO0NBQ0Msa0JBQWtCO0FBQ25COztBQUVBO0NBQ0MsWUFBWTtBQUNiOzs7O0FBSUEsaUNBQWlDLDBCQUEwQjtBQUMzRDtBQUNBO0lBQ0kseUJBQXlCO0VBQzNCO0FBQ0Y7Q0FDQyxpQkFBaUI7Q0FDakIsYUFBYTtDQUNiLDhCQUE4QjtDQUM5QixZQUFZO0NBQ1osV0FBVztDQUNYLDJCQUEyQjtDQUMzQix5QkFBeUIsRUFBRSxXQUFXLEVBQ2QsWUFBWSxFQUNiLGVBQWU7Q0FDdEMsaUJBQWlCLEVBQUUsYUFBYTtDQUNoQyx5QkFBeUI7Q0FDekIsbUJBQW1CO0FBQ3BCO0FBQ0E7Q0FDQyxXQUFXO0NBQ1gsWUFBWTtDQUNaLGdCQUFnQjtDQUNoQixvQkFBb0I7Q0FDcEIsY0FBYztDQUNkLG1CQUFtQjtFQUNsQixTQUFTO0VBQ1QsaUJBQWlCO0FBQ25COzs7QUFHQSw2RUFBNkU7QUFDN0U7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixxQkFBcUI7SUFDckIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDZCQUE2QjtJQUM3QixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7SUFDWjtJQUNBO0lBQ0EsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQjs7SUFFQSw0RUFBNEU7QUFDaEY7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixXQUFXO0lBQ1gsYUFBYTtBQUNqQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsc0JBQXNCO0lBQ3RCLHlCQUF5QjtJQUN6QixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsNkJBQTZCO0lBQzdCLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsWUFBWTtJQUNaO0lBQ0E7SUFDQSxlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCO0lBQ0E7UUFDSSxnQkFBZ0I7UUFDaEIsYUFBYTtRQUNiLG1CQUFtQjtRQUNuQiw2QkFBNkI7UUFDN0IsV0FBVztJQUNmOzs7QUFHSiw2RUFBNkU7QUFDN0U7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixXQUFXO0lBQ1gsYUFBYTtBQUNqQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7SUFDWjtJQUNBO0lBQ0EsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQjtJQUNBO1FBQ0ksYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsV0FBVztRQUNYLGdCQUFnQjtJQUNwQjs7SUFFQTtRQUNJLGFBQWE7UUFDYixzQkFBc0I7UUFDdEIsNkJBQTZCO1FBQzdCLG1CQUFtQjtRQUNuQixnQkFBZ0I7S0FDbkI7Ozs7QUFJTCw4RUFBOEU7QUFDOUU7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixXQUFXO0lBQ1gsYUFBYTtBQUNqQjtBQUNBO0lBQ0ksbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIseUJBQXlCO0lBQ3pCLFFBQVE7SUFDUixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsNkJBQTZCO0lBQzdCLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsWUFBWTtJQUNaO0lBQ0E7SUFDQSxlQUFlO0lBQ2Ysa0JBQWtCO0lBQ2xCOztFQUVGLDhFQUE4RTtFQUM5RTtJQUNFLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsc0JBQXNCO0lBQ3RCLFdBQVc7SUFDWCxhQUFhO0FBQ2pCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qiw2QkFBNkI7SUFDN0Isa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixtQkFBbUI7UUFDZjs7O0FBR1I7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixZQUFZO0lBQ1o7OztBQUdKLDhFQUE4RTtBQUM5RTtBQUNBLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCLFdBQVc7QUFDWCxhQUFhO0FBQ2I7QUFDQTtBQUNBLFFBQVE7QUFDUixVQUFVO0FBQ1YsWUFBWTtBQUNaO0FBQ0E7QUFDQSxRQUFRO0FBQ1IsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CLFVBQVU7QUFDVixZQUFZO0FBQ1o7QUFDQTtBQUNBLGVBQWU7QUFDZixrQkFBa0I7QUFDbEI7O0FBRUEsNkVBQTZFO0FBQzdFO0FBQ0EsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qiw4QkFBOEI7QUFDOUIsV0FBVztBQUNYLGFBQWE7QUFDYjtBQUNBO0FBQ0EsUUFBUTtBQUNSLGFBQWE7QUFDYixtQkFBbUI7QUFDbkIseUJBQXlCO0FBQ3pCLFdBQVc7QUFDWCxZQUFZO0FBQ1o7QUFDQTtBQUNBLFFBQVE7QUFDUixhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLDZCQUE2QjtBQUM3QixtQkFBbUI7QUFDbkIsVUFBVTtBQUNWLFlBQVk7QUFDWjtBQUNBO0FBQ0EsZUFBZTtBQUNmLGtCQUFrQjtBQUNsQjtBQUNBLDhFQUE4RTtBQUM5RTtJQUNJLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsOEJBQThCO0lBQzlCLFdBQVc7SUFDWCxZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVixZQUFZO0FBQ2hCO0FBQ0E7SUFDSSxRQUFROztRQUVKO0FBQ1I7SUFDSSxRQUFRO0lBQ1IsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixtQkFBbUI7UUFDZjs7OztBQUlSO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsMkJBQTJCO0lBQzNCLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsWUFBWTtJQUNaOzs7O0FBSUosNkVBQTZFO0FBQzdFO0FBQ0EsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qiw4QkFBOEI7QUFDOUIsV0FBVztBQUNYLGFBQWE7QUFDYjtBQUNBO0FBQ0EsUUFBUTtBQUNSLGFBQWE7QUFDYixtQkFBbUI7QUFDbkIseUJBQXlCO0FBQ3pCLFdBQVc7QUFDWCxZQUFZO0FBQ1o7QUFDQTtBQUNBLFFBQVE7QUFDUixhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLDJCQUEyQjtBQUMzQixtQkFBbUI7QUFDbkIsVUFBVTtBQUNWLFlBQVk7QUFDWjtBQUNBO0FBQ0EsZUFBZTtBQUNmLGtCQUFrQjtBQUNsQjs7QUFFQSw4RUFBOEU7QUFDOUU7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1YsWUFBWTtBQUNoQjtBQUNBO0lBQ0ksUUFBUTtJQUNSLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsbUJBQW1CO1FBQ2Y7O0FBRVI7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLHNCQUFzQjtJQUN0QiwyQkFBMkI7SUFDM0IsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixZQUFZO0lBQ1o7O09BRUcsOEVBQThFO09BQzlFO1FBQ0MsYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qix1QkFBdUI7UUFDdkIsV0FBVztRQUNYLFlBQVk7SUFDaEI7SUFDQTtRQUNJLFFBQVE7UUFDUixhQUFhO1FBQ2Isc0JBQXNCO1FBQ3RCLHVCQUF1QjtRQUN2QixrQkFBa0I7UUFDbEIsVUFBVTtRQUNWLFlBQVk7SUFDaEI7SUFDQTtRQUNJLFFBQVE7UUFDUixXQUFXO1FBQ1gsVUFBVTtZQUNOO0lBQ1I7UUFDSSxRQUFRO1FBQ1IsZUFBZTtRQUNmLGtCQUFrQjtRQUNsQixtQkFBbUI7WUFDZjs7SUFFUjtRQUNJLFFBQVE7UUFDUixhQUFhO1FBQ2Isc0JBQXNCO1FBQ3RCLDRCQUE0QjtRQUM1QixtQkFBbUI7UUFDbkIsVUFBVTtRQUNWLFlBQVk7UUFDWjs7O0FBR1IsOEVBQThFO0FBQzlFO0lBQ0ksYUFBYTtJQUNiLHNCQUFzQjtJQUN0Qix1QkFBdUI7SUFDdkIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGlCQUFpQjtJQUNqQixhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QixRQUFRO0lBQ1IsVUFBVTtJQUNWLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDZCQUE2QjtJQUM3QixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLFlBQVk7SUFDWjtJQUNBO0lBQ0EsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQjs7O0FBR0osOEVBQThFO0FBQzlFO0FBQ0EsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qix1QkFBdUI7QUFDdkIsV0FBVztBQUNYLFlBQVk7QUFDWjtBQUNBO0FBQ0EsYUFBYTtBQUNiLG1CQUFtQjtBQUNuQix1QkFBdUI7QUFDdkIsUUFBUTtBQUNSLFdBQVc7QUFDWCxXQUFXO0FBQ1g7QUFDQTtBQUNBLFdBQVc7QUFDWCxVQUFVO0FBQ1YsOERBQThEO0FBQzlEO0FBQ0E7QUFDQSxRQUFRO0FBQ1IsYUFBYTtBQUNiLHNCQUFzQjtBQUN0QiwyQkFBMkI7QUFDM0IsbUJBQW1CO0FBQ25CLFVBQVU7QUFDVixZQUFZO0FBQ1o7QUFDQTtBQUNBLGVBQWU7QUFDZixrQkFBa0I7QUFDbEI7QUFDQTtJQUNJLGVBQWU7SUFDZixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixVQUFVO0FBQ2Q7QUFDQTtJQUNJLGVBQWU7SUFDZixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixVQUFVO0lBQ1Y7O0FBRUo7SUFDSSxhQUFhO0lBQ2IsdUJBQXVCO0lBQ3ZCLHNCQUFzQjtJQUN0QixnQkFBZ0I7QUFDcEI7O0FBRUEsOEVBQThFO0FBQzlFO0FBQ0EsYUFBYTtBQUNiLHNCQUFzQjtBQUN0Qix1QkFBdUI7QUFDdkIsbUJBQW1CO0FBQ25CLFdBQVc7QUFDWCxZQUFZO0FBQ1o7QUFDQTtBQUNBLFlBQVk7QUFDWixzQkFBc0I7QUFDdEIsbUJBQW1CO0FBQ25CLHVCQUF1QjtBQUN2QixRQUFRO0FBQ1IsV0FBVztBQUNYLFdBQVc7QUFDWDtBQUNBO0FBQ0EsUUFBUTtBQUNSLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsc0JBQXNCO0FBQ3RCLG1CQUFtQjtBQUNuQixTQUFTO0FBQ1QsV0FBVztBQUNYOztBQUVBO0lBQ0ksbUJBQW1CO0dBQ3BCLFlBQVk7R0FDWixlQUFlO0dBQ2Ysa0JBQWtCO0lBQ2pCO0lBQ0E7UUFDSSxtQkFBbUI7UUFDbkIsZUFBZTtRQUNmLFlBQVk7WUFDUjtBQUNaO0lBQ0ksbUJBQW1CO0lBQ25CLHFCQUFxQjtJQUNyQixlQUFlO0lBQ2Ysa0JBQWtCO1FBQ2Q7QUFDUjtJQUNJLFVBQVU7UUFDTjtBQUNSO0lBQ0ksbUJBQW1CO0lBQ25CLFlBQVk7SUFDWixVQUFVO0lBQ1YsZUFBZTtJQUNmLGtCQUFrQjtRQUNkO0FBQ1I7SUFDSSxXQUFXO1FBQ1A7O0FBRVI7SUFDSSxRQUFRO0lBQ1IsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQix5QkFBeUI7SUFDekIscUJBQXFCO0lBQ3JCLFNBQVM7SUFDVCxXQUFXO0FBQ2Y7QUFDQTtJQUNJLFlBQVk7SUFDWixXQUFXO0FBQ2Y7Ozs7QUFJQSw4RUFBOEU7QUFDOUU7QUFDQSxhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLHVCQUF1QjtBQUN2QixXQUFXO0FBQ1gsWUFBWTtBQUNaO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakIsUUFBUTtBQUNSLGFBQWE7QUFDYixvQkFBb0I7QUFDcEIsVUFBVTtBQUNWLFlBQVk7QUFDWjtBQUNBO0FBQ0EsUUFBUTtBQUNSLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIseUJBQXlCO0FBQ3pCLG1CQUFtQjtBQUNuQixVQUFVO0FBQ1YsWUFBWTtBQUNaLGdCQUFnQjtBQUNoQjtBQUNBO0FBQ0EsZUFBZTtBQUNmLGtCQUFrQjtBQUNsQjtBQUNBO0FBQ0EsVUFBVTtBQUNWLFlBQVk7QUFDWixxQkFBcUI7QUFDckIsNEJBQTRCO0lBQ3hCO0FBQ0o7R0FDRyxhQUFhO0dBQ2IsbUJBQW1CO0dBQ25CLDZCQUE2QjtHQUM3Qix1QkFBdUI7R0FDdkIsV0FBVztHQUNYLGdCQUFnQjs7UUFFWDtBQUNSO0lBQ0ksWUFBWTtJQUNaLGFBQWE7QUFDakI7OztBQUdBLDhFQUE4RTtBQUM5RTtBQUNBLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsdUJBQXVCO0FBQ3ZCLG1CQUFtQjtBQUNuQixXQUFXO0FBQ1gsWUFBWTtBQUNaO0FBQ0E7QUFDQSxhQUFhO0FBQ2IscUJBQXFCO0FBQ3JCLHlCQUF5QjtBQUN6QixRQUFRO0FBQ1IsVUFBVTtBQUNWLFdBQVc7QUFDWDs7QUFFQTtBQUNBLFFBQVE7QUFDUixhQUFhO0FBQ2Isc0JBQXNCO0FBQ3RCLDBCQUEwQjtBQUMxQixtQkFBbUI7QUFDbkIsVUFBVTtBQUNWLFdBQVc7QUFDWDs7QUFFQTtBQUNBLFdBQVc7QUFDWCxtQkFBbUI7SUFDZjtBQUNKO0lBQ0ksa0JBQWtCO0FBQ3RCLHFCQUFxQjtBQUNyQixlQUFlO0FBQ2YsbUJBQW1CO0FBQ25COztBQUVBO0FBQ0EsUUFBUTtBQUNSO0FBQ0E7QUFDQSxrQkFBa0I7QUFDbEIsbUJBQW1CO0FBQ25CO0FBQ0E7QUFDQSxhQUFhO0FBQ2IsbUJBQW1CO0FBQ25CLHVCQUF1QjtBQUN2QjtBQUNBO0FBQ0EsV0FBVztBQUNYOzs7QUFHQSw4RUFBOEU7QUFDOUU7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixrQkFBa0I7SUFDbEIsU0FBUztJQUNULFlBQVk7QUFDaEI7QUFDQTtJQUNJLFFBQVE7SUFDUixXQUFXO0lBQ1gsVUFBVTtRQUNOO0FBQ1I7SUFDSSxRQUFRO0lBQ1IsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixtQkFBbUI7UUFDZjs7QUFFUjtJQUNJLFFBQVE7SUFDUixhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2Qix1QkFBdUI7SUFDdkIsVUFBVTtJQUNWLFlBQVk7SUFDWjtJQUNBO0tBQ0MsZUFBZTtJQUNoQjtJQUNBO0lBQ0EsVUFBVTtJQUNWLFlBQVk7SUFDWiw0QkFBNEI7SUFDNUIsZ0JBQWdCO1FBQ1o7O0lBRUo7UUFDSSxZQUFZO1FBQ1osYUFBYTtRQUNiLHNCQUFzQjtRQUN0Qiw2QkFBNkI7UUFDN0IsdUJBQXVCO1FBQ3ZCLFVBQVU7SUFDZDtJQUNBO1FBQ0ksZ0JBQWdCO1FBQ2hCLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsNkJBQTZCO1FBQzdCLFdBQVc7SUFDZjtJQUNBO1FBQ0ksZ0JBQWdCO1FBQ2hCLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsNkJBQTZCO1FBQzdCLFVBQVU7SUFDZDs7O0lBR0E7UUFDSSxVQUFVO0lBQ2Q7SUFDQTtRQUNJLGFBQWE7UUFDYixtQkFBbUI7UUFDbkIsU0FBUztJQUNiOzs7SUFHQSIsImZpbGUiOiJzcmMvYXBwL3Rlc3QvdGVzdC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNvbnRhaW5lcntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBtYXgtd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG59XHJcbi5ib2R5e1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246Y29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuLmZvb3RlcntcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWF4LWhlaWdodDogMTUwcHg7XHJcbiAgICBtaW4taGVpZ2h0OiAxNTBweDtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICAjMTExRDVFZmY7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxufVxyXG4ubGVmdHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOiAxMCU7XHJcbiAgICBtYXJnaW4tbGVmdDogMTBweDtcclxufVxyXG4ubGVmdCBpbWd7XHJcbiAgICB3aWR0aDogNTAlO1xyXG59XHJcbi5wb2ludHtcclxuICAgIGZvbnQtc2l6ZTogMTBweDtcclxuICAgIGNvbG9yOnJnYigxNTMsIDE1MywgMTUzKTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgXHJcbn1cclxuaXtcclxuICAgIGNvbG9yOnJnYigxNTMsIDE1MywgMTUzKTtcclxufVxyXG5pOmhvdmVye1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcbi5pY29uTmV4e1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG4gICAgbWFyZ2luLXRvcDogOHB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbn1cclxuLmNoZWNrSWNvbntcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLmNlbnRlcntcclxuIHdpZHRoOiAxMDAlO1xyXG4gaGVpZ2h0OiAxMDAlO1xyXG4gZGlzcGxheTogZmxleDtcclxuIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKmJ0bioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5idXR0dG9uUkVke1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDIwNiwgMCwgMCk7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn1cclxuIGJ1dHRvbjpob3ZlcntcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigxNDUsIDEzNiwgMTM2KTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMSAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMXtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMSAuaW1hZ2V7XHJcbiAgb3JkZXI6IDI7XHJcbiAgd2lkdGg6IDcwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuXHJcbi5zbGlkZTEgLnRlc3R7XHJcbiBvcmRlcjogMTtcclxuIGRpc3BsYXk6IGZsZXg7XHJcbiBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgYWxpZ24taXRlbXM6Y2VudGVyO1xyXG53aWR0aDogNDAlO1xyXG5oZWlnaHQ6IDEwMCU7XHJcbn1cclxuXHJcbiAgIC5zbGlkZTEgLnRlc3QgaW5wdXR7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxuICAgICAgIH1cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAwICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGUwe1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUwIC5pbWFnZXtcclxuICBvcmRlcjogMjtcclxuICB3aWR0aDogNTIlO1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUwIC50ZXN0e1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgd2lkdGg6NDAlO1xyXG4gICBoZWlnaHQ6IDEwMCU7XHJcbiAgIH1cclxuICAgLnNsaWRlMCAudGVzdCBoMXtcclxuZm9udC1zaXplOiA1MHB4O1xyXG4gICB9XHJcbiAgIC5zbGlkZTAgLnRlc3QgYnV0dG9ue1xyXG4gICAgd2lkdGg6IDE1MHB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuICAgIGJvcmRlci1jb2xvcjogcmdiKDE5MiwgMywgMyk7XHJcbiAgICAgICB9XHJcbiAgIFxyXG4gICAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDIgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgICAgICAuc2xpZGUye1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMiAuaW1hZ2V7XHJcbiAgICAgIG9yZGVyOiAxO1xyXG4gICAgICB3aWR0aDogNTUlO1xyXG4gICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUyIC50ZXN0e1xyXG4gICAgICAgIG9yZGVyOiAyO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgICAgIHdpZHRoOjQwJTtcclxuICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgIH1cclxuICAgICAgIC5zbGlkZTIgLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICAgICB9XHJcbiAgICAgICBcclxuICAgICAgIFxyXG4gICAgICAgICAgIC5zbGlkZTIgLnRlc3QgLm5nLXNlbGVjdCB7XHJcbiAgICAgICAgICAgIGJvcmRlcjowcHg7XHJcbiAgICAgICAgICAgIG1pbi1oZWlnaHQ6IDBweDtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMDtcclxuICAgICAgICAgICAgd2lkdGg6IDQ4MHB4O1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgICAgICAgIGJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zbGlkZTIgLnRlc3QgIC5uZy1zZWxlY3QgLm5nLXNlbGVjdC1jb250YWluZXIgIHsgICAgICAgICAgICBcclxuICAgICAgICAgICAgbWluLWhlaWdodDogMHB4O1xyXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgICAgICAgICB3aWR0aDogNDgwcHg7XHJcbiAgICAgICAgICAgIGhlaWdodDogNDBweDtcclxuICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnNsaWRlMiAudGVzdCAgLm5nLXNlbGVjdCA6Om5nLWRlZXAgLm5nLXNlbGVjdC1jb250YWluZXIgIHsgICAgICAgICAgICBcclxuICAgICAgICAgICAgbWluLWhlaWdodDogMHB4O1xyXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgICAgICAgICB3aWR0aDogNDgwcHg7XHJcbiAgICAgICAgICAgIGhlaWdodDogNDBweDtcclxuICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnNsaWRlMiAudGVzdCBuZy1zZWxlY3QubmctaW52YWxpZC5uZy10b3VjaGVkIC5uZy1zZWxlY3QtY29udGFpbmVyIHtcclxuICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiAjZGMzNTQ1O1xyXG4gICAgICAgICAgICBib3gtc2hhZG93OiBpbnNldCAwIDFweCAxcHggcmdiYSgwLCAwLCAwLCAwLjA3NSksIDAgMCAwIDNweCAjZmRlNmU4O1xyXG4gICAgICAgICAgICB3aWR0aDogNDgwcHg7XHJcbiAgICAgICAgICAgIGhlaWdodDogNDBweDtcclxuICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDMgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgICAgICAgLnNsaWRlM3tcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTMgLmltYWdle1xyXG4gICAgICAgIG9yZGVyOiAyO1xyXG4gICAgICAgIHdpZHRoOiA1NCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMyAudGVzdHtcclxuICAgICAgICBvcmRlcjogMTtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tdG9wOiA1MHB4O1xyXG4gICAgICAgIHdpZHRoOjQwJTtcclxuICAgICAgICBoZWlnaHQ6IDcwJTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnNsaWRlMyAudGVzdCBoMXtcclxuICAgICAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zbGlkZTMgLnRlc3QgLmJsb2NrQnRue1xyXG4gICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuXHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zbGlkZTMgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2t7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gXHJcbiAgICAgICAgIH1cclxuICAgICAgICAuc2xpZGUzIC50ZXN0IC5ibG9ja0J0biBidXR0b257XHJcbiAgICAgICAgd2lkdGg6IDMxMHB4O1xyXG4gICAgICAgIGhlaWdodDogNTBweDtcclxuICAgICAgICBtYXJnaW46IDVweDtcclxuICAgICAgICBib3JkZXItY29sb3I6IHJnYigxOTIsIDMsIDMpO1xyXG4gICAgICAgICAgICB9ICBcclxuICAgICAgICAgICAgLnNsaWRlMyAudGVzdCAuYmxvY2tCdG4gLnNvdXNCbG9jayBidXR0b257XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMTUwcHg7XHJcbiAgICAgICAgICAgICAgICBtYXJnaW46IDVweDtcclxuICAgICAgICAgICAgICAgICAgICB9ICAgXHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgNCAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuICAgIC5zbGlkZTR7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGU0IC5pbWFnZXtcclxuICAgICAgICBtYXJnaW4tbGVmdDogNTBweDtcclxuICAgICAgICBvcmRlcjogMTtcclxuICAgICAgICB3aWR0aDogMzMlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIHotaW5kZXg6IDE7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogNTVweDtcclxuICAgIH1cclxuICAgIC5zbGlkZTQgLnRlc3R7XHJcbiAgICAgICAgb3JkZXI6IDI7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAgICAgIHdpZHRoOjQwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zbGlkZTQgLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zbGlkZTQgLnRlc3QgaW5wdXR7XHJcbiAgICAgICAgd2lkdGg6IDgwJTtcclxuICAgICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgXHJcbiAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDUgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgICAgLnNsaWRlNXtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTUgLmltYWdle1xyXG4gICAgICAgIG9yZGVyOiAxO1xyXG4gICAgICAgIHdpZHRoOiA1MCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIHotaW5kZXg6IDE7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBtYXJnaW4tdG9wOiA2NHB4O1xyXG4gICAgfVxyXG5cclxuICAgIFxyXG4gICAgLnNsaWRlNSAudGVzdHtcclxuICAgICAgICBvcmRlcjogMjtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICB3aWR0aDo1MCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2xpZGU1IC50ZXN0IGgxe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogNTBweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnNsaWRlNSAudGVzdCAuYmxvY2tCdG57XHJcbiAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAyMCU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zbGlkZTUgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2t7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjsgXHJcbiAgICAgICAgIH1cclxuICAgIFxyXG4gICAgICAgICAuc2xpZGU1IC50ZXN0IC5ibG9ja0J0biAuc291c0Jsb2NrIC5xdHl7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyOyAgICAgICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICBcclxuICAgIC5zbGlkZTUgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2sgLnF0eSBpbnB1dHtcclxuICAgICAgICBvcmRlcjogMjtcclxuICAgICAgICB3aWR0aDogODBweDtcclxuICAgICAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICAgICAgYm9yZGVyOiA0cHggc29saWQgcmdiKDIxMiwgNSwgNSk7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIH1cclxuICAgIC5zbGlkZTUgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2sgLnF0eSBwe1xyXG4gICAgICAgIG9yZGVyOiAxO1xyXG4gICAgICAgIG1hcmdpbjogMCA1cHggMCAwO1xyXG4gICAgICAgIHBhZGRpbmc6IDA7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLnNsaWRlNSAudGVzdCAuYmxvY2tCdG4gLnNvdXNCbG9jayAucXR5IC5iYWlzc3sgZGlzcGxheTogbm9uZTt9XHJcbiAgICAgICAgICAgIC5zbGlkZTUgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2sgLnF0eSAuYXVnbXsgZGlzcGxheTogbm9uZTt9XHJcbiAgICAgICAgICAgIC5zbGlkZTUgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2sgLnF0eSAgLmJhaXNzRHsgZGlzcGxheTogY29udGVudHM7fVxyXG4gICAgICAgICAgICAuc2xpZGU1IC50ZXN0IC5ibG9ja0J0biAuc291c0Jsb2NrIC5xdHkgLmF1Z21EeyBkaXNwbGF5OiBjb250ZW50czt9XHJcbiAgICAuc2xpZGU1IC50ZXN0IC5ibG9ja0J0biAuc291c0Jsb2NrIC5xdHkgLnF0eUJ0bntcclxuICAgICAgICBvcmRlcjogMztcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2xpZGU1IC50ZXN0IC5ibG9ja0J0biAuc291c0Jsb2NrIC5xdHkgLnF0eUJ0biBidXR0b257XHJcbiAgICAgICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgICAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHdoaXRlO1xyXG4gICAgICAgICAgICBtYXJnaW4tYm90dG9tOiAxcHg7XHJcbiAgICAgICAgICAgIHdpZHRoOiA1cHg7XHJcbiAgICAgICAgICAgIGJvcmRlci1yYWRpdXM6IDBweDtcclxuICAgICAgICAgICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigyMTIsIDUsIDUpO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgNiAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuICAgICAgICAgICAgLnNsaWRlNntcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLnNsaWRlNiAuaW1hZ2V7XHJcbiAgICAgICAgICAgICAgICBvcmRlcjogMjtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDo1MyU7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLnNsaWRlNiAuaW1hZ2UgaW1ne1xyXG4gICAgICAgICAgICAgICAgb3JkZXI6IDI7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDI1JTtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAxNSU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAuc2xpZGU2IC5pbWFnZSBoMXtcclxuICAgICAgICAgICAgICAgIG9yZGVyOiAxO1xyXG4gICAgICAgICAgICAgICAgZm9udC1zaXplOiA1MHB4O1xyXG4gICAgICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICAgICAgLnNsaWRlNiAudGVzdHtcclxuICAgICAgICAgICAgICAgIG9yZGVyOiAyO1xyXG4gICAgICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAgICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6NDAlO1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAuc2xpZGU2IC50ZXN0IHNlbGVjdHtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiA4MCU7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgICAgICAgICAgICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICBcclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDcgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTd7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlNyAuY29sdW0xe1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59XHJcbi5zbGlkZTcgLmNvbHVtMntcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGU3IC5pbWFnZXtcclxuICAgIG1hcmdpbi1sZWZ0OiA1MHB4O1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICB3aWR0aDogMzclO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTcgLnRlc3R7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgIHdpZHRoOjQwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTcgLnRlc3QgaDF7XHJcbmZvbnQtc2l6ZTogNTBweDtcclxuICAgIH1cclxuICAgIC5zbGlkZTcgLnRlc3QgaW5wdXR7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxuICAgICAgICB9XHJcbi5zbGlkZTcgLnByb2dyZXNze1xyXG4gICAgb3JkZXI6IDM7XHJcbn1cclxuXHJcbi5zbGlkZXIge1xyXG4gICAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDE1cHg7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2IoMjU1LCAyNTUsIDI1NSk7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgYm9yZGVyOiA1cHggc29saWQgcmdiKDE4OSwgOCwgOCk7XHJcbiAgICBib3JkZXItcmFkaXVzOiA4cHg7XHJcbiAgfVxyXG4gIFxyXG4gIFxyXG4gIC8qIGZvciBjaHJvbWUvc2FmYXJpICovXHJcbiAgLnNsaWRlcjo6LXdlYmtpdC1zbGlkZXItdGh1bWIge1xyXG4gICAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lO1xyXG4gICAgYXBwZWFyYW5jZTogbm9uZTtcclxuICAgIHdpZHRoOiAyMHB4O1xyXG4gICAgaGVpZ2h0OiA2MHB4O1xyXG4gICAgYmFja2dyb3VuZDogcmdiKDI0OCwgMjI0LCA1KTtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIGJvcmRlcjogNXB4IHNvbGlkIHJnYigyNDgsIDIyNCwgNSk7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gIH1cclxuICBcclxuICAvKiBmb3IgZmlyZWZveCAqL1xyXG4gIC5zbGlkZXI6Oi1tb3otcmFuZ2UtdGh1bWIge1xyXG4gICAgd2lkdGg6IDIwcHg7XHJcbiAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2IoMjU1LCAyNTUsIDI1NSk7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBib3JkZXI6IDVweCBzb2xpZCByZ2IoMTg5LCA4LCA4KTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICB9XHJcbi5fX3Jhbmdle1xyXG4gICAgd2lkdGg6IDgwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uX19yYW5nZS1zdGVwe1xyXG5cdHBvc2l0aW9uOiByZWxhdGl2ZTsgICAgICAgICAgICAgICAgXHJcbn1cclxuLl9fcmFuZ2Utc3RlcHtcclxuXHRwb3NpdGlvbjogcmVsYXRpdmU7ICAgICAgICAgICAgICAgIFxyXG59XHJcblxyXG4uX19yYW5nZS1tYXh7XHJcblx0ZmxvYXQ6IHJpZ2h0O1xyXG59XHJcbiAgICAgICAgICAgXHJcblxyXG5cclxuLl9fcmFuZ2UgaW5wdXQ6OnJhbmdlLXByb2dyZXNzIHtcdGJhY2tncm91bmQ6IHJnYigxODksIDgsIDgpO1xyXG59XHJcbi5zbGlkZXIgaW5wdXRbdHlwZT1yYW5nZV06Oi1tb3otcmFuZ2UtcHJvZ3Jlc3Mge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2M2NTdhMDtcclxuICB9XHJcbi5fX3JhbmdlLXN0ZXAgZGF0YWxpc3Qge1xyXG5cdHBvc2l0aW9uOnJlbGF0aXZlO1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0anVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG5cdGhlaWdodDogYXV0bztcclxuXHRib3R0b206IDEwcHg7XHJcblx0LyogZGlzYWJsZSB0ZXh0IHNlbGVjdGlvbiAqL1xyXG5cdC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7IC8qIFNhZmFyaSAqLyAgICAgICAgXHJcblx0LW1vei11c2VyLXNlbGVjdDogbm9uZTsgLyogRmlyZWZveCAqL1xyXG5cdC1tcy11c2VyLXNlbGVjdDogbm9uZTsgLyogSUUxMCsvRWRnZSAqLyAgICAgICAgICAgICAgICBcclxuXHR1c2VyLXNlbGVjdDogbm9uZTsgLyogU3RhbmRhcmQgKi9cclxuXHQvKiBkaXNhYmxlIGNsaWNrIGV2ZW50cyAqL1xyXG5cdHBvaW50ZXItZXZlbnRzOm5vbmU7ICBcclxufVxyXG4uX19yYW5nZS1zdGVwIGRhdGFsaXN0IG9wdGlvbiB7XHJcblx0d2lkdGg6IDEwcHg7XHJcblx0aGVpZ2h0OiAxMHB4O1xyXG5cdG1pbi1oZWlnaHQ6IDEwcHg7XHJcblx0Ym9yZGVyLXJhZGl1czogMTAwcHg7XHJcblx0LyogaGlkZSB0ZXh0ICovXHJcblx0d2hpdGUtc3BhY2U6IG5vd3JhcDsgICAgICAgXHJcbiAgcGFkZGluZzowO1xyXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xyXG59XHJcblxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDggKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTh7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlOCAuaW1hZ2V7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6IDM3JTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGU4IC50ZXN0e1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICB3aWR0aDo0MCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGU4IC50ZXN0IGgxe1xyXG5mb250LXNpemU6IDUwcHg7XHJcbiAgICB9XHJcbiAgICAuc2xpZGU4IC50ZXN0IHNlbGVjdHtcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xyXG4gICAgICAgIH1cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSA5KioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTl7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlOSAuaW1hZ2V7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6IDM3JTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGU5IC50ZXN0e1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICB3aWR0aDo0MCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGU5IC50ZXN0IGgxe1xyXG4gICAgZm9udC1zaXplOiA1MHB4O1xyXG4gICAgfVxyXG4gICAgLnNsaWRlOSAudGVzdCBtYXQtcmFkaW8tZ3JvdXB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogNTBweDtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcblxyXG4gICAgXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxMCoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGUxMHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxMCAuaW1hZ2V7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6IDM3JTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxMCAudGVzdHtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAgd2lkdGg6NDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMTAgLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUxMCAudGVzdCBtYXQtcmFkaW8tZ3JvdXB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogNTBweDtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcblxyXG4gICAgLnNsaWRlMTAgLnRlc3QgLmJsb2Nre1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5zbGlkZTEwIC50ZXN0IC5ibG9jayAuc291c0Jsb2Nre1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyOyBcclxuICAgICB9XHJcbiAgICAuc2xpZGUxMCAudGVzdCAuYmxvY2sgLnNvdXNCbG9jayAucXR5e1xyXG4gICAgICAgIG9yZGVyOiAyO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjsgICAgICAgICAgICBcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbi5zbGlkZTEwIC50ZXN0IC5ibG9jayAuc291c0Jsb2NrIC5xdHkgaW5wdXR7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIHdpZHRoOiA4MHB4O1xyXG4gICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgYm9yZGVyOiA0cHggc29saWQgcmdiKDIxMiwgNSwgNSk7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcbi5zbGlkZTEwIC50ZXN0IC5ibG9jayAuc291c0Jsb2NrICBwe1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBtYXJnaW46IDAgNXB4IDAgMDtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgfVxyXG5cclxuLnNsaWRlMTAgLnRlc3QgLmJsb2NrIC5zb3VzQmxvY2sgLnF0eSAucXR5QnRue1xyXG4gICAgb3JkZXI6IDM7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgIH1cclxuICAgIC5zbGlkZTEwIC50ZXN0IC5ibG9jayAuc291c0Jsb2NrIC5xdHkgLnF0eUJ0biBidXR0b257XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCB3aGl0ZTtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxcHg7XHJcbiAgICAgICAgd2lkdGg6IDVweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgfVxyXG4gICAgXHJcbiAgICAgICAgXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxMSAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuICAgIC5zbGlkZTExe1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUxMSAuaW1hZ2V7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIG9yZGVyOiAxO1xyXG4gICAgICAgIHdpZHRoOiAzNyU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMTEgLnRlc3R7XHJcbiAgICAgICAgb3JkZXI6IDI7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAgICAgIHdpZHRoOjQwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zbGlkZTExIC50ZXN0IGgxe1xyXG4gICAgZm9udC1zaXplOiA1MHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2xpZGUxMSAudGVzdCBpbnB1dHtcclxuICAgICAgICB3aWR0aDogODAlO1xyXG4gICAgICAgIGhlaWdodDogNDBweDtcclxuICAgICAgICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMTIgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgICAgIC5zbGlkZTEye1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMTIgLmltYWdle1xyXG4gICAgICAgIG9yZGVyOiAyO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgICAgICAgd2lkdGg6NTMlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTEyIC5pbWFnZSBpbWd7XHJcbiAgICAgICAgb3JkZXI6IDI7XHJcbiAgICAgICAgaGVpZ2h0OiAxNSU7XHJcbiAgICAgICAgd2lkdGg6IDE1JTtcclxuICAgICAgICAgICAgfVxyXG4gICAgLnNsaWRlMTIgLmltYWdlIGgxe1xyXG4gICAgICAgIG9yZGVyOiAxO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogNTBweDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICAgICAgICAgICAgfVxyXG5cclxuXHJcbiAgICAuc2xpZGUxMiAudGVzdHtcclxuICAgICAgICBvcmRlcjogMjtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICAgICAgd2lkdGg6NDAlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICB9XHJcbiAgICAgXHJcbiAgICAgICAgLnNsaWRlMTIgLnRlc3Qgc2VsZWN0e1xyXG4gICAgICAgIHdpZHRoOiA4MCU7XHJcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgICAgIGJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICBcclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDEzICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGUxM3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTMgLmltYWdle1xyXG4gICAgbWFyZ2luLWxlZnQ6IDUwcHg7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgICB3aWR0aDogMzclO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTEzIC50ZXN0e1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOjQwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTEzIC50ZXN0IGgxe1xyXG5mb250LXNpemU6IDUwcHg7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUxMyAudGVzdCBpbnB1dHtcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICBtYXJnaW46IDUwcHggMCA1MHB4IDA7XHJcbiAgICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xyXG4gICAgICAgIH1cclxuXHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMTQqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMTR7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTQgLmltYWdle1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOiAzNyU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTQgLnRlc3R7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgIHdpZHRoOjQwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTE0IC50ZXN0IGgxe1xyXG4gICAgZm9udC1zaXplOiA1MHB4O1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMTQgLnRlc3QgbWF0LXJhZGlvLWdyb3Vwe1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDUwcHg7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxNSAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuICAgIC5zbGlkZTE1e1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMTUgLmltYWdle1xyXG4gICAgICAgIG9yZGVyOiAyO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgICAgICAgd2lkdGg6NTMlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTE1IC5pbWFnZSBwe1xyXG4gICAgICAgIG9yZGVyOiAyO1xyXG4gICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgIC5zbGlkZTE1IC5pbWFnZSBoMXtcclxuICAgICAgICBvcmRlcjogMTtcclxuICAgICAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgICAgICAgICAgIH1cclxuXHJcblxyXG5cclxuICAgIC5zbGlkZTE1IC50ZXN0e1xyXG4gICAgICAgIG9yZGVyOiAyO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgICAgICB3aWR0aDo0MCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIH1cclxuICAgICBcclxuICAgICAgICAuc2xpZGUxNSAudGVzdCBzZWxlY3R7XHJcbiAgICAgICAgd2lkdGg6IDgwJTtcclxuICAgICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxNioqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGUxNntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxNiAuaW1hZ2V7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6IDM3JTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxNiAudGVzdHtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAgd2lkdGg6NDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMTYgLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUxNiAudGVzdCBtYXQtcmFkaW8tZ3JvdXB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogNTBweDtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIFxyXG4gICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDE3ICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gICAgLnNsaWRlMTd7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUxNyAuaW1hZ2V7XHJcbiAgICAgICAgb3JkZXI6IDI7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOmNlbnRlcjtcclxuICAgICAgICB3aWR0aDo1MyU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMTcgLmltYWdlIHB7XHJcbiAgICAgICAgb3JkZXI6IDI7XHJcbiAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgLnNsaWRlMTcgLmltYWdlIGgxe1xyXG4gICAgICAgIG9yZGVyOiAxO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogNTBweDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICAgICAgICAgICAgfVxyXG5cclxuXHJcblxyXG4gICAgLnNsaWRlMTcgLnRlc3R7XHJcbiAgICAgICAgb3JkZXI6IDI7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAgICAgIHdpZHRoOjQwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgfVxyXG4gICAgIFxyXG4gICAgICAgIC5zbGlkZTE3IC50ZXN0IHNlbGVjdHtcclxuICAgICAgICB3aWR0aDogODAlO1xyXG4gICAgICAgIGhlaWdodDogNDBweDtcclxuICAgICAgICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG5cclxuICAgICAgICAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMTggKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgICAgICAgICAgLnNsaWRlMTh7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnNsaWRlMTggLmltYWdle1xyXG4gICAgICAgICAgICBvcmRlcjogMjtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOmNlbnRlcjtcclxuICAgICAgICAgICAgd2lkdGg6NTMlO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zbGlkZTE4IC5pbWFnZSBpbWd7XHJcbiAgICAgICAgICAgIG9yZGVyOiAyO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDMwJTtcclxuICAgICAgICAgICAgd2lkdGg6IDE1JTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAuc2xpZGUxOCAuaW1hZ2UgaDF7XHJcbiAgICAgICAgICAgIG9yZGVyOiAyO1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcblxyXG4gICAgICAgIC5zbGlkZTE4IC50ZXN0e1xyXG4gICAgICAgICAgICBvcmRlcjogMTtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIHdpZHRoOjQwJTtcclxuICAgICAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgIFxyXG4gICAgICAgICAgICAuc2xpZGUxOCAudGVzdCBzZWxlY3R7XHJcbiAgICAgICAgICAgIHdpZHRoOiA4MCU7XHJcbiAgICAgICAgICAgIGhlaWdodDogNDBweDtcclxuICAgICAgICAgICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcblxyXG4gICAgICAgIFxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMTkgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgICAuc2xpZGUxOXtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTE5IC5pbWFnZXtcclxuICAgICAgICBtYXJnaW4tbGVmdDogNTBweDtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgb3JkZXI6IDE7XHJcbiAgICAgICAgd2lkdGg6IDM3JTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUxOSAudGVzdHtcclxuICAgICAgICBvcmRlcjogMjtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICAgICAgd2lkdGg6NDAlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnNsaWRlMTkgLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zbGlkZTE5IC50ZXN0IGlucHV0e1xyXG4gICAgICAgIHdpZHRoOiA4MCU7XHJcbiAgICAgICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgICAgIGJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcblxyXG4gICAgICAgICAgICBcclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDIwICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGUyMHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMjAgLmltYWdle1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgd2lkdGg6IDYwJTtcclxuICAgIGhlaWdodDogODAlO1xyXG59XHJcbi5zbGlkZTIwIC5pbWFnZSBpbWd7XHJcbiAgaGVpZ2h0OiA4MCU7XHJcbiAgd2lkdGg6IDQwJTtcclxuICBmaWx0ZXI6IGRyb3Atc2hhZG93KDAuNHJlbSAwLjRyZW0gMC40NXJlbSByZ2JhKDAsIDAsIDMwLCAwLjUpKTtcclxufVxyXG4uc2xpZGUyMCAudGVzdHtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAgd2lkdGg6NjAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMjAgLnRlc3QgaDF7XHJcbmZvbnQtc2l6ZTogNTBweDtcclxuICAgIH1cclxuICAgIC5zbGlkZTIwIC50ZXN0IGlucHV0e1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIGJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIC5zbGlkZTIwIC50ZXN0IC5ibG9jazF7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUyMCAudGVzdCAuYmxvY2syIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgIH1cclxuXHJcbiAgICAuc2xpZGUyMCAudGVzdCAuYmxvY2syIGlucHV0e1xyXG4gICAgICAgIHdpZHRoOiA0MCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUyMCAudGVzdCAuYmxvY2syIGxhYmVse1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW46IDA7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUyMCAudGVzdCAuYmxvY2sxIGlucHV0e1xyXG4gICAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUyMCAudGVzdCAuYmxvY2sxIGxhYmVse1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW46IDA7XHJcbiAgICB9XHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMjEgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTIxe1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogNzAlO1xyXG59XHJcbi5zbGlkZTIxIC5pbWFnZXtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICB3aWR0aDogMjIlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTIxIC5pbWFnZSBpbWd7XHJcbiAgaGVpZ2h0OiA3MCU7XHJcbiAgd2lkdGg6IDMwJTtcclxufVxyXG4uc2xpZGUyMSAudGVzdHtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6ZmxleC1zdGFydDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDo3MCU7XHJcbiAgICBoZWlnaHQ6IDgwJTtcclxuICAgIH1cclxuXHJcbiAgICAuc2xpZGUyMSAudGVzdCBoMXtcclxuICAgICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xyXG4gICAgICAgY29sb3I6IGdyZWVuO1xyXG4gICAgICAgZm9udC1zaXplOiA1MHB4O1xyXG4gICAgICAgIH1cclxuICAgICAgICAuc2xpZGUyMSAudGVzdCBpbWd7XHJcbiAgICAgICAgICBcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMDBweDtcclxuICAgICAgICAgICAgbWFyZ2luOiAxMHB4O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgLnNsaWRlMjEgLnRlc3QgcHtcclxuICAgICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xyXG4gICAgICAgIGNvbG9yOiByZ2IoMCwgMCwgMTMzKTtcclxuICAgICAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICAgICAgICAgIH1cclxuICAgIC5zbGlkZTIxIC50ZXN0IC5wcml4IHtcclxuICAgICAgICBjb2xvcjogcmVkO1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgICAgIH1cclxuICAgIC5zbGlkZTIxIC50ZXN0IC5mYWlsZHtcclxuICAgICAgICBjb2xvcjogcmVkO1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgZm9udC1zaXplOiA2MHB4O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAuc2xpZGUyMSAudGVzdCBpbWd7XHJcbiAgICAgICAgd2lkdGg6IDEwMHB4O1xyXG4gICAgICAgICAgICB9XHJcblxyXG5cclxuXHJcbi5zbGlkZTIxIC5uZXh0SWNvbntcclxuICAgIG9yZGVyOiAzO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XHJcbiAgICB3aWR0aDogMjIlO1xyXG4gICAgaGVpZ2h0OiA5MCU7XHJcbn1cclxuLnNsaWRlMjEgLm5leHRJY29uIGltZ3tcclxuIGNvbG9yOiBncmVlbjtcclxuIHdpZHRoOiA4MHB4O1xyXG59XHJcblxyXG4gICBcclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDI0ICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGUyNHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMjQgLmltYWdle1xyXG4gICAgbWFyZ2luLWxlZnQ6IDUwcHg7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczpmbGV4LWVuZDtcclxuICAgIHdpZHRoOiA0NyU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG59XHJcbi5zbGlkZTI0IC50ZXN0e1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOjQwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTI0IC50ZXN0IGgxe1xyXG4gICAgZm9udC1zaXplOiA1MHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMjQgLnRlc3QgaW5wdXR7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgbWFyZ2luOiA1MHB4IDAgNTBweCAwO1xyXG4gICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxuICAgICAgICB9XHJcbiAgICAuc2xpZGUyNCAudGVzdCBtYXQtcmFkaW8tZ3JvdXB7XHJcbiAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICAgICB3aWR0aDogMTAwJTtcclxuXHJcbiAgICAgICAgICAgIH1cclxuICAgIC5zbGlkZTI0IC50ZXN0IC5jYWxlbmR7XHJcbiAgICAgICAgd2lkdGg6IDQ1MHB4O1xyXG4gICAgICAgIGhlaWdodDogNDUwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAyNSAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMjV7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTI1IC5pbWFnZXtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICB3aWR0aDogMjIlO1xyXG4gICAgaGVpZ2h0OiAyMCU7XHJcbn1cclxuXHJcbi5zbGlkZTI1IC50ZXN0e1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDpmbGV4LXN0YXJ0O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOjcwJTtcclxuICAgIGhlaWdodDogODAlO1xyXG4gICAgfVxyXG5cclxuLnNsaWRlMjUgLnRlc3QgaW1ne1xyXG4gICAgd2lkdGg6IDUwcHg7XHJcbiAgICAgICAgfVxyXG4uc2xpZGUyNSAudGVzdCBwe1xyXG4gICAgY29sb3I6IHJnYigwLCAwLCAxMzMpO1xyXG4gICAgZm9udC1zaXplOiA0MHB4O1xyXG59XHJcblxyXG4uc2xpZGUyNSAubmV4dEljb257XHJcbiAgICBvcmRlcjogMztcclxufVxyXG4uc2xpZGUyNSAubmV4dEljb24gcHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4uc2xpZGUyNSAubmV4dEljb24gLnNvY2lhbE1lZGlhIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgIH1cclxuLnNsaWRlMjUgLm5leHRJY29uIC5zb2NpYWxNZWRpYSBpbWd7XHJcbiB3aWR0aDogNjBweDtcclxufVxyXG5cclxuICAgICAgICAgICBcclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDI2ICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGUyNntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMjYgLmltYWdle1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6Y2VudGVyO1xyXG4gICAgd2lkdGg6NTMlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTI2IC5pbWFnZSBpbWd7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGhlaWdodDogMzAlO1xyXG4gICAgd2lkdGg6IDE1JTtcclxuICAgICAgICB9XHJcbi5zbGlkZTI2IC5pbWFnZSBoMXtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZm9udC1zaXplOiA1MHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICAgICAgICB9XHJcblxyXG4uc2xpZGUyNiAudGVzdHtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAgd2lkdGg6NjAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMjYgLnRlc3QgaDF7XHJcbmZvbnQtc2l6ZTogNTBweDtcclxuICAgIH1cclxuICAgIC5zbGlkZTI2IC50ZXN0IGlucHV0e1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIGJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIC5zbGlkZTI2IC50ZXN0IC5ibG9jazF7XHJcbiAgICAgICAgbWFyZ2luOiAyMHB4O1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTI2IC50ZXN0IC5ibG9jazIge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDIwcHg7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMjYgLnRlc3QgLmJsb2NrMiBtYXQtcmFkaW8tZ3JvdXAgIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgICAgICB3aWR0aDogOTAlO1xyXG4gICAgfVxyXG5cclxuICAgIFxyXG4gICAgLnNsaWRlMjYgLnRlc3QgLmJsb2NrMSBpbnB1dHtcclxuICAgICAgICB3aWR0aDogOTAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMjYgLnRlc3QgLmJsb2NrMSBsYWJlbHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luOiAwO1xyXG4gICAgfVxyXG5cclxuIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAyNyAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuIC5zbGlkZTI3e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUyNyAuaW1hZ2V7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgICB3aWR0aDo1MyU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuXHJcblxyXG5cclxuXHJcbi5zbGlkZTI3IC50ZXN0e1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgIHdpZHRoOjQwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIFxyXG4gICAgfVxyXG4gICAgLnNsaWRlMjcgLnRlc3QgLmJTZWFyY2h7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWZsb3c6IHdyYXA7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBiYXNlbGluZTtcclxuICAgIH1cclxuICAgIC5zbGlkZTI3IC50ZXN0IC5iU2VhcmNoIC5zZWFyY2gge1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDE1cHg7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDE1cHg7XHJcbiAgICAgICAgd2lkdGg6IDUwJTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigxMzQsIDEzNCwgMTM0KTtcclxuICAgICAgICBjb2xvcjp3aGl0ZTtcclxuICAgICAgfVxyXG4gICAgICAuc2xpZGUyNyAudGVzdCAuYlNlYXJjaCBpIHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogLTI5cHg7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICB9XHJcbiAgICAgIFxyXG4gICAgICAuc2xpZGUyNyAudGVzdCAubGlzdC1pdGVtIHsgIFxyXG5cclxuYm9yZGVyOiAzcHggc29saWQgcmdiKDI1NSwgMCwgMCk7XHJcbmJvcmRlci1yYWRpdXM6IDRweDtcclxuY29sb3I6IHJnYigxNTMsIDE1MywgMTUzKTtcclxubGluZS1oZWlnaHQ6IDkwcHg7XHJcbmZvbnQtd2VpZ2h0OiA0MDA7XHJcbmJhY2tncm91bmQtY29sb3I6IHJnYigyNTUsIDI1NSwgMjU1KTtcclxud2lkdGg6IDg4JTtcclxufVxyXG4gIFxyXG4gIC5zbGlkZTI3IC50ZXN0IC5pdGVtLWNvbnRlbnQge1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgY29sb3I6IHJnYigxNTMsIDE1MywgMTUzKTtcclxuICAgIGxpbmUtaGVpZ2h0OiA0NXB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDI1NSwgMjU1LCAyNTUpO1xyXG4gICAgYm94LXNoYWRvdzogcmdiYSgwLDAsMCwwLjIpIDBweCAxcHggMnB4IDBweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG4gIFxyXG4gICBcclxuICAuc2xpZGUyNyAudGVzdCAuaXRlbS1jb250ZW50IC5ibG9jazF7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOiAxMCU7XHJcbiAgfVxyXG5cclxuICAuc2xpZGUyNyAudGVzdCAuaXRlbS1jb250ZW50IC5ibG9jazJ7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHdpZHRoOiAtbW96LWF2YWlsYWJsZTtcclxuICAgIH1cclxuICBcclxuICAgIC5zbGlkZTI3IC50ZXN0IC5pdGVtLWNvbnRlbnQgLmJsb2NrMiBwe1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDA7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMXJlbTtcclxuICAgICAgICBoZWlnaHQ6IDI5cHg7XHJcbiAgICAgICAgfVxyXG4gIFxyXG4gIC5zbGlkZTI3IC50ZXN0IC5pdGVtLWNvbnRlbnQgLmJsb2NrM3tcclxuICAgIGRpc3BsYXk6IGlubGluZTtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICB3aWR0aDogMTAlO1xyXG4gIH1cclxuICAuc2xpZGUyNyAudGVzdCAuaXRlbS1jb250ZW50IC5ibG9jazMgaW5wdXR7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbiAgLnNsaWRlMjcgLnRlc3QgLml0ZW0tY29udGVudDpob3ZlcntcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigyMTgsIDk4LCA5OCk7XHJcbiAgfVxyXG4gIC5zbGlkZTI3IC50ZXN0IC5pdGVtLWNvbnRlbnQ6aG92ZXIgcHtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIH1cclxuICAgIC5zbGlkZTI3IC50ZXN0IC5pdGVtLWNvbnRlbnQ6aG92ZXIgaW5wdXR7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMjcgLnRlc3QgLml0ZW0tY29udGVudDpob3ZlciBpe1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIH1cclxuIFxyXG4gICAgICBcclxuICAgICAgLnNsaWRlMjcgLnRlc3QgLmNvbnRlbnRUYWJ7XHJcbiAgICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAgIGhlaWdodDogNTAwcHg7XHJcbiAgICAgICAgICBvdmVyZmxvdy15OiBzY3JvbGw7XHJcbiAgICAgIH1cclxuICBcclxuXHJcblxyXG5cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnJlc3BvbnNpdmUgY3NzICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XHJcbiAgIFxyXG4gICAgLmJvZHl7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICB9XHJcbiAgICAuZm9vdGVye1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICB9XHJcbiAgICAubGVmdHtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLmxlZnQgaW1ne1xyXG4gICAgICAgIHdpZHRoOiA2MHB4O1xyXG4gICAgfVxyXG4gICAgLnBvaW50e1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBmb250LXNpemU6IDdweDtcclxuICAgIH1cclxuICAgICAuY2VudGVye1xyXG4gICAgICB3aWR0aDogOTAlO1xyXG4gICAgICBtYXJnaW4tbGVmdDogMjBweDsgIFxyXG4gICAgIH1cclxuICAgIC5jZW50ZXIgLmxpc3RQe1xyXG4gICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAwICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGUwe1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXIgO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTAgLmltYWdle1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gIH1cclxuXHJcbi5zbGlkZTAgLnRlc3R7XHJcbiAgIHdpZHRoOjEwMCU7XHJcbiAgIH1cclxuXHJcbi5zbGlkZTAgLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgIH1cclxuXHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMSAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMXtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTEgLmltYWdle1xyXG4gIG9yZGVyOiAyO1xyXG4gIHdpZHRoOiA5MCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG4uc2xpZGUxIC50ZXN0e1xyXG53aWR0aDogMTAwJTtcclxufVxyXG4uc2xpZGUxIC50ZXN0IGgxe1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgfVxyXG5cclxuICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMiAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuICAuc2xpZGUye1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6c3BhY2UtYmV0d2VlbjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTIgLmltYWdle1xyXG4gIG9yZGVyOiAyO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAganVzdGlmeS1jb250ZW50OmZsZXgtZW5kO1xyXG59XHJcbi5zbGlkZTIgLnRlc3R7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICB3aWR0aDoxMDAlO1xyXG4gICBoZWlnaHQ6IDEwMCU7XHJcbiAgIH1cclxuICAgLnNsaWRlMiAudGVzdCBoMXtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgfVxyXG4gXHJcbiAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDMgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgLnNsaWRlM3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMyAuaW1hZ2V7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG59XHJcbi5zbGlkZTMgLnRlc3R7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTMgLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcbiBcclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSA0ICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGU0e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6c3BhY2UtYmV0d2VlbjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGU0IC5pbWFnZXtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgd2lkdGg6IDgwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OmZsZXgtZW5kO1xyXG59XHJcbi5zbGlkZTQgLnRlc3R7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTQgLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgXHJcbiAgICAgLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDUgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgICAgLnNsaWRlNXtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG5cclxuICAgIH1cclxuICAgIC5zbGlkZTUgLmltYWdle1xyXG4gICAgICAgIG9yZGVyOiAyO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICB6LWluZGV4OiAxO1xyXG4gICAgIFxyXG4gICAgfVxyXG4gICAgLnNsaWRlNSAudGVzdHtcclxuICAgICAgICBvcmRlcjogMTtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgd2lkdGg6MTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zbGlkZTUgLnRlc3QgaDF7XHJcbiAgICAgICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tYmxvY2s6IGF1dG87XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC5zbGlkZTUgLnRlc3QgLmJsb2NrQnRue1xyXG5cclxuICAgICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgICAgICBoZWlnaHQ6IDIwJTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLnNsaWRlNSAudGVzdCAuYmxvY2tCdG4gLnNvdXNCbG9ja3tcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcclxuICAgICAgICAgICAgd2lkdGg6IDc1JTtcclxuICAgICAgICAgfVxyXG4gICAgXHJcbiAgICAgICAgIC5zbGlkZTUgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2sgLnF0eXtcclxuICAgICAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7ICBcclxuICAgICAgICAgICAgbWFyZ2luOiAxMHB4OyAgICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBcclxuICAgIC5zbGlkZTUgLnRlc3QgLmJsb2NrQnRuIC5zb3VzQmxvY2sgLnF0eSBpbnB1dHtcclxuICAgICAgICBvcmRlcjogMjtcclxuICAgICAgICB3aWR0aDogODBweDtcclxuICAgICAgICBoZWlnaHQ6IDUwLjVweDtcclxuICAgICAgICBib3JkZXI6IDRweCBzb2xpZCByZ2IoMjEyLCA1LCA1KTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgfVxyXG4gICBcclxuIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSA2ICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4gLnNsaWRlNntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGU2IC5pbWFnZXtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOmNlbnRlcjtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlNiAuaW1hZ2UgaW1ne1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBoZWlnaHQ6IDE1JTtcclxuICAgIHdpZHRoOiAxNSU7XHJcbiAgICAgICAgfVxyXG4uc2xpZGU2IC5pbWFnZSBoMXtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICAgICAgICB9XHJcblxyXG5cclxuLnNsaWRlNiAudGVzdHtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuXHJcbiAgICBcclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDcgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTd7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGU3IC5jb2x1bTF7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlNyAuY29sdW0ye1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlNyAuaW1hZ2V7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIHdpZHRoOiA5MCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlNyAudGVzdHtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgfVxyXG4gICAgLnNsaWRlNyAudGVzdCBoMXtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuICAgIC5zbGlkZTcgLnRlc3QgaW5wdXR7XHJcbiAgICB3aWR0aDogODAlO1xyXG4gICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgYm9yZGVyLWNvbG9yOiByZ2IoMjEyLCA1LCA1KTtcclxuICAgICAgICB9XHJcbiAgICAuc2xpZGU3IC5wcm9ncmVzc3tcclxuICAgIG9yZGVyOiAzO1xyXG59XHJcblxyXG4uc2xpZGVyIHtcclxuICAgIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxNXB4O1xyXG4gICAgYmFja2dyb3VuZDogcmdiKDI1NSwgMjU1LCAyNTUpO1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIGJvcmRlcjogNXB4IHNvbGlkIHJnYigxODksIDgsIDgpO1xyXG4gICAgYm9yZGVyLXJhZGl1czogOHB4O1xyXG4gIH1cclxuICBcclxuICBcclxuICAvKiBmb3IgY2hyb21lL3NhZmFyaSAqL1xyXG4gIC5zbGlkZXI6Oi13ZWJraXQtc2xpZGVyLXRodW1iIHtcclxuICAgIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcclxuICAgIGFwcGVhcmFuY2U6IG5vbmU7XHJcbiAgICB3aWR0aDogMTBweDtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIGJhY2tncm91bmQ6IHJnYigyNDgsIDIyNCwgNSk7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBib3JkZXI6IDVweCBzb2xpZCByZ2IoMjQ4LCAyMjQsIDUpO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuICB9XHJcbiAgXHJcbiAgLyogZm9yIGZpcmVmb3ggKi9cclxuICAuc2xpZGVyOjotbW96LXJhbmdlLXRodW1iIHtcclxuICAgIHdpZHRoOiAxMHB4O1xyXG4gICAgaGVpZ2h0OiAzMHB4O1xyXG4gICAgYmFja2dyb3VuZDogcmdiKDI1NSwgMjU1LCAyNTUpO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgYm9yZGVyOiA1cHggc29saWQgcmdiKDE4OSwgOCwgOCk7XHJcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbiAgfVxyXG4uX19yYW5nZXtcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLl9fcmFuZ2Utc3RlcHtcclxuXHRwb3NpdGlvbjogcmVsYXRpdmU7ICAgICAgICAgICAgICAgIFxyXG59XHJcbi5fX3JhbmdlLXN0ZXB7XHJcblx0cG9zaXRpb246IHJlbGF0aXZlOyAgICAgICAgICAgICAgICBcclxufVxyXG5cclxuLl9fcmFuZ2UtbWF4e1xyXG5cdGZsb2F0OiByaWdodDtcclxufVxyXG4gICAgICAgICAgIFxyXG5cclxuXHJcbi5fX3JhbmdlIGlucHV0OjpyYW5nZS1wcm9ncmVzcyB7XHRiYWNrZ3JvdW5kOiByZ2IoMTg5LCA4LCA4KTtcclxufVxyXG4uc2xpZGVyIGlucHV0W3R5cGU9cmFuZ2VdOjotbW96LXJhbmdlLXByb2dyZXNzIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNjNjU3YTA7XHJcbiAgfVxyXG4uX19yYW5nZS1zdGVwIGRhdGFsaXN0IHtcclxuXHRwb3NpdGlvbjpyZWxhdGl2ZTtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG5cdGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHRoZWlnaHQ6IGF1dG87XHJcblx0Ym90dG9tOiA2cHg7XHJcblx0LyogZGlzYWJsZSB0ZXh0IHNlbGVjdGlvbiAqL1xyXG5cdC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7IC8qIFNhZmFyaSAqLyAgICAgICAgXHJcblx0LW1vei11c2VyLXNlbGVjdDogbm9uZTsgLyogRmlyZWZveCAqL1xyXG5cdC1tcy11c2VyLXNlbGVjdDogbm9uZTsgLyogSUUxMCsvRWRnZSAqLyAgICAgICAgICAgICAgICBcclxuXHR1c2VyLXNlbGVjdDogbm9uZTsgLyogU3RhbmRhcmQgKi9cclxuXHQvKiBkaXNhYmxlIGNsaWNrIGV2ZW50cyAqL1xyXG5cdHBvaW50ZXItZXZlbnRzOm5vbmU7ICBcclxufVxyXG4uX19yYW5nZS1zdGVwIGRhdGFsaXN0IG9wdGlvbiB7XHJcblx0d2lkdGg6IDEwcHg7XHJcblx0aGVpZ2h0OiAxMHB4O1xyXG5cdG1pbi1oZWlnaHQ6IDEwcHg7XHJcblx0Ym9yZGVyLXJhZGl1czogMTAwcHg7XHJcblx0LyogaGlkZSB0ZXh0ICovXHJcblx0d2hpdGUtc3BhY2U6IG5vd3JhcDsgICAgICAgXHJcbiAgcGFkZGluZzowO1xyXG4gIGxpbmUtaGVpZ2h0OiA0MHB4O1xyXG59XHJcblxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDggKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTh7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTggLmltYWdle1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlOCAudGVzdHtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlOCAudGVzdCBoMXtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuXHJcbiAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgOSoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGU5e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogNzUwcHg7XHJcbn1cclxuLnNsaWRlOSAuaW1hZ2V7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGU5IC50ZXN0e1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGU5IC50ZXN0IGgxe1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlOSAudGVzdCBtYXQtcmFkaW8tZ3JvdXB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogNTBweDtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICB9XHJcblxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDEwKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTEwe1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogNzUwcHg7XHJcbn1cclxuLnNsaWRlMTAgLmltYWdle1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTEwIC50ZXN0e1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUxMCAudGVzdCBoMXtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuICAgIC5zbGlkZTEwIC50ZXN0IC5ibG9ja3tcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgIH1cclxuXHJcbiAgICAuc2xpZGUxMCAudGVzdCAuYmxvY2sgLnNvdXNCbG9ja3tcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjsgXHJcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICB9XHJcbiAgXHJcbiAgICAgXHJcbiAgICAgICAgXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxMSAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMTF7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiA3NTBweDtcclxufVxyXG4uc2xpZGUxMSAuaW1hZ2V7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxMSAudGVzdHtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMTEgLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcblxyXG4gIC8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxMiAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuICAuc2xpZGUxMntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OmNlbnRlcjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiA3MDBweDtcclxufVxyXG4uc2xpZGUxMiAuaW1hZ2V7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTEyIC5pbWFnZSBoMXtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgZm9udC1zaXplOiAzMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICAgICAgICB9XHJcblxyXG5cclxuLnNsaWRlMTIgLnRlc3R7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgd2lkdGg6MTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIH1cclxuIFxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDEzICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGUxM3tcclxuZGlzcGxheTogZmxleDtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbndpZHRoOiAxMDAlO1xyXG5oZWlnaHQ6IDc1MHB4O1xyXG59XHJcbi5zbGlkZTEzIC5pbWFnZXtcclxub3JkZXI6IDI7XHJcbndpZHRoOiA4MCU7XHJcbmhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxMyAudGVzdHtcclxub3JkZXI6IDE7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG53aWR0aDoxMDAlO1xyXG5oZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTMgLnRlc3QgaDF7XHJcbmZvbnQtc2l6ZTogMzBweDtcclxudGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMTQqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMTR7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2Vlbjtcclxud2lkdGg6IDEwMCU7XHJcbmhlaWdodDogNzUwcHg7XHJcbn1cclxuLnNsaWRlMTQgLmltYWdle1xyXG5vcmRlcjogMjtcclxuZGlzcGxheTogZmxleDtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuanVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxud2lkdGg6IDEwMCU7XHJcbmhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxNCAudGVzdHtcclxub3JkZXI6IDE7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG53aWR0aDoxMDAlO1xyXG5oZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTQgLnRlc3QgaDF7XHJcbmZvbnQtc2l6ZTogMzBweDtcclxudGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxNSAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMTV7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0Ojc1MHB4O1xyXG59XHJcbi5zbGlkZTE1IC5pbWFnZXtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOmNlbnRlcjtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTUgLmltYWdlIHB7XHJcbiAgICBvcmRlcjogMjtcclxuXHJcbiAgICAgICAgfVxyXG4uc2xpZGUxNSAuaW1hZ2UgaDF7XHJcbiAgICBvcmRlcjogMTtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgICAgICAgfVxyXG5cclxuXHJcblxyXG4uc2xpZGUxNSAudGVzdHtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gXHJcblxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDE2KioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTE2e1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbndpZHRoOiAxMDAlO1xyXG5oZWlnaHQ6IDc1MHB4O1xyXG59XHJcbi5zbGlkZTE2IC5pbWFnZXtcclxub3JkZXI6IDI7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbmp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbndpZHRoOiAxMDAlO1xyXG5oZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTYgLnRlc3R7XHJcbm9yZGVyOiAxO1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbndpZHRoOjEwMCU7XHJcbmhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxNiAudGVzdCBoMXtcclxuZm9udC1zaXplOiAzMHB4O1xyXG50ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAxNyAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMTd7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0Ojc1MHB4O1xyXG59XHJcbi5zbGlkZTE3IC5pbWFnZXtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOmNlbnRlcjtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMTcgLmltYWdlIGgxe1xyXG4gICAgb3JkZXI6IDE7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG4gICAgICAgIH1cclxuXHJcbi5zbGlkZTE3IC50ZXN0e1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gXHJcbiAgICAgICAvKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMTggKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgICAgICAuc2xpZGUxOHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMTggLmltYWdle1xyXG4gICAgICAgIG9yZGVyOiAxO1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczpjZW50ZXI7XHJcbiAgICAgICAgd2lkdGg6MTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUxOCAuaW1hZ2UgaW1ne1xyXG4gICAgICAgIG9yZGVyOiAyO1xyXG4gICAgICAgIGhlaWdodDogMjAlO1xyXG4gICAgICAgIHdpZHRoOiAxNSU7XHJcbiAgICAgICAgICAgIH1cclxuICAgIC5zbGlkZTE4IC5pbWFnZSBoMXtcclxuICAgICAgICBvcmRlcjogMjtcclxuICAgICAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAuc2xpZGUxOCAudGVzdHtcclxuICAgICAgICBvcmRlcjogMjtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0IDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIHdpZHRoOjEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIH1cclxuICAgICBcclxuICAgIFxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMTkgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTE5e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTE5IC5pbWFnZXtcclxuICAgIG1hcmdpbi1sZWZ0OiA1MHB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIG9yZGVyOiAyO1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUxOSAudGVzdHtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB3aWR0aDoxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgfVxyXG4gICAgLnNsaWRlMTkgLnRlc3QgaDF7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgICAgIFxyXG4vKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqc2xpZGUgMjAgKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi5zbGlkZTIwe1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxud2lkdGg6IDEwMCU7XHJcbmhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUyMCAuaW1hZ2V7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5vcmRlcjogMTtcclxud2lkdGg6IDEwMCU7XHJcbmhlaWdodDogODAlO1xyXG59XHJcbi5zbGlkZTIwIC5pbWFnZSBpbWd7XHJcbmhlaWdodDogNjAlO1xyXG53aWR0aDogNDAlO1xyXG5maWx0ZXI6IGRyb3Atc2hhZG93KDAuNHJlbSAwLjRyZW0gMC40NXJlbSByZ2JhKDAsIDAsIDMwLCAwLjUpKTtcclxufVxyXG4uc2xpZGUyMCAudGVzdHtcclxub3JkZXI6IDI7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxud2lkdGg6MTAwJTtcclxuaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTIwIC50ZXN0IGgxe1xyXG5mb250LXNpemU6IDMwcHg7XHJcbnRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4uc2xpZGUyMCAudGVzdCAuYmxvY2sxe1xyXG4gICAgbWFyZ2luLXRvcDogMHB4O1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIHdpZHRoOiA5MCU7XHJcbn1cclxuLnNsaWRlMjAgLnRlc3QgLmJsb2NrMiB7XHJcbiAgICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICAgIH1cclxuXHJcbi5zbGlkZTIwIC50ZXN0IC5ibG9jazIgbGFiZWx7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxufVxyXG5cclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDIxICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGUyMXtcclxuZGlzcGxheTogZmxleDtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbndpZHRoOiAxMDAlO1xyXG5oZWlnaHQ6IDEwMCU7XHJcbn1cclxuLnNsaWRlMjEgLmltYWdle1xyXG5kaXNwbGF5Om5vbmU7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5vcmRlcjogMTtcclxud2lkdGg6IDEwMCU7XHJcbmhlaWdodDogMTAlO1xyXG59XHJcbi5zbGlkZTIxIC50ZXN0e1xyXG5vcmRlcjogMTtcclxuZGlzcGxheTogZmxleDtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OmNlbnRlcjtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxud2lkdGg6ODAlO1xyXG5oZWlnaHQ6IDgwJTsgICAgXHJcbn1cclxuXHJcbi5zbGlkZTIxIC50ZXN0IGgxe1xyXG4gICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcclxuICAgY29sb3I6IGdyZWVuO1xyXG4gICBmb250LXNpemU6IDMwcHg7XHJcbiAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuICAgIC5zbGlkZTIxIC50ZXN0IGltZ3tcclxuICAgICAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogODBweDtcclxuICAgICAgICBtYXJnaW46IDEwcHg7XHJcbiAgICAgICAgICAgIH1cclxuLnNsaWRlMjEgLnRlc3QgcHtcclxuICAgIGxpbmUtaGVpZ2h0OiBub3JtYWw7XHJcbiAgICBjb2xvcjogcmdiKDAsIDAsIDEzMyk7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgfVxyXG4uc2xpZGUyMSAudGVzdCAucHJpeCB7XHJcbiAgICBjb2xvcjogcmVkO1xyXG4gICAgICAgIH1cclxuLnNsaWRlMjEgLnRlc3QgLmZhaWxke1xyXG4gICAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcclxuICAgIHdpZHRoOiAzNjBweDtcclxuICAgIGNvbG9yOiByZWQ7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgfVxyXG4uc2xpZGUyMSAudGVzdCBpbWd7XHJcbiAgICB3aWR0aDogODBweDtcclxuICAgICAgICB9XHJcblxyXG4uc2xpZGUyMSAubmV4dEljb257XHJcbiAgICBvcmRlcjogMztcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICAgIGFsaWduLWl0ZW1zOiBmbGV4LWVuZDtcclxuICAgIHdpZHRoOjk3JTtcclxuICAgIGhlaWdodDogMTUlO1xyXG59XHJcbi5zbGlkZTIxIC5uZXh0SWNvbiBpbWd7XHJcbiAgICBjb2xvcjogZ3JlZW47XHJcbiAgICB3aWR0aDogODBweDtcclxufVxyXG4gICAgICAgIFxyXG5cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAyNCAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMjR7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG53aWR0aDogMTAwJTtcclxuaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTI0IC5pbWFnZXtcclxubWFyZ2luLWxlZnQ6IDUwcHg7XHJcbm9yZGVyOiAyO1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5hbGlnbi1pdGVtczpmbGV4LWVuZDtcclxud2lkdGg6IDg4JTtcclxuaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTI0IC50ZXN0e1xyXG5vcmRlcjogMTtcclxuZGlzcGxheTogZmxleDtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuYWxpZ24taXRlbXM6IGNlbnRlcjtcclxud2lkdGg6MTAwJTtcclxuaGVpZ2h0OiAxMDAlO1xyXG5tYXJnaW4tdG9wOiAyMHB4O1xyXG59XHJcbi5zbGlkZTI0IC50ZXN0IGgxe1xyXG5mb250LXNpemU6IDMwcHg7XHJcbnRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4uc2xpZGUyNCAudGVzdCBpbnB1dHtcclxud2lkdGg6IDgwJTtcclxuaGVpZ2h0OiA0MHB4O1xyXG5tYXJnaW46IDUwcHggMCA1MHB4IDA7XHJcbmJvcmRlci1jb2xvcjogcmdiKDIxMiwgNSwgNSk7XHJcbiAgICB9XHJcbi5zbGlkZTI0IC50ZXN0IG1hdC1yYWRpby1ncm91cHtcclxuICAgZGlzcGxheTogZmxleDtcclxuICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICB3aWR0aDogMTAwJTtcclxuICAgbWFyZ2luLXRvcDogMjBweDtcclxuXHJcbiAgICAgICAgfVxyXG4uc2xpZGUyNCAudGVzdCAuY2FsZW5ke1xyXG4gICAgd2lkdGg6IDM1MHB4O1xyXG4gICAgaGVpZ2h0OiAzNTBweDtcclxufVxyXG5cclxuXHJcbi8qKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipzbGlkZSAyNSAqKioqKioqKioqKioqKioqKioqKioqKi9cclxuLnNsaWRlMjV7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5hbGlnbi1pdGVtczogY2VudGVyO1xyXG53aWR0aDogMTAwJTtcclxuaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTI1IC5pbWFnZXtcclxuZGlzcGxheTogZmxleDtcclxuYWxpZ24taXRlbXM6IGZsZXgtZW5kO1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xyXG5vcmRlcjogMTtcclxud2lkdGg6IDIyJTtcclxuaGVpZ2h0OiAyMCU7XHJcbn1cclxuXHJcbi5zbGlkZTI1IC50ZXN0e1xyXG5vcmRlcjogMjtcclxuZGlzcGxheTogZmxleDtcclxuZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuanVzdGlmeS1jb250ZW50OmZsZXgtc3RhcnQ7XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbndpZHRoOjEwMCU7XHJcbmhlaWdodDogODAlO1xyXG59XHJcblxyXG4uc2xpZGUyNSAudGVzdCBpbWd7XHJcbndpZHRoOiA1MHB4O1xyXG5tYXJnaW4tYm90dG9tOiAzMHB4O1xyXG4gICAgfVxyXG4uc2xpZGUyNSAudGVzdCBwe1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5jb2xvcjogcmdiKDAsIDAsIDEzMyk7XHJcbmZvbnQtc2l6ZTogMzBweDtcclxubGluZS1oZWlnaHQ6IG5vcm1hbDtcclxufVxyXG5cclxuLnNsaWRlMjUgLm5leHRJY29ue1xyXG5vcmRlcjogMztcclxufVxyXG4uc2xpZGUyNSAubmV4dEljb24gcHtcclxudGV4dC1hbGlnbjogY2VudGVyO1xyXG5saW5lLWhlaWdodDogbm9ybWFsO1xyXG59XHJcbi5zbGlkZTI1IC5uZXh0SWNvbiAuc29jaWFsTWVkaWEge1xyXG5kaXNwbGF5OiBmbGV4O1xyXG5mbGV4LWRpcmVjdGlvbjogcm93O1xyXG5qdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG4uc2xpZGUyNSAubmV4dEljb24gLnNvY2lhbE1lZGlhIGltZ3tcclxud2lkdGg6IDYwcHg7XHJcbn1cclxuXHJcbiAgICAgICAgICBcclxuLyoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKnNsaWRlIDI2ICoqKioqKioqKioqKioqKioqKioqKioqL1xyXG4uc2xpZGUyNntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi5zbGlkZTI2IC5pbWFnZXtcclxuICAgIG9yZGVyOiAxO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOmNlbnRlcjtcclxuICAgIHdpZHRoOjUzJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxufVxyXG4uc2xpZGUyNiAuaW1hZ2UgaW1ne1xyXG4gICAgb3JkZXI6IDI7XHJcbiAgICBoZWlnaHQ6IDIwJTtcclxuICAgIHdpZHRoOiAxNSU7XHJcbiAgICAgICAgfVxyXG4uc2xpZGUyNiAuaW1hZ2UgaDF7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgICAgICAgfVxyXG5cclxuLnNsaWRlMjYgLnRlc3R7XHJcbiAgICBvcmRlcjogMjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUyNiAudGVzdCBoMXtcclxuICAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUyNiAudGVzdCBpbnB1dHtcclxuICAgIHdpZHRoOiA5MCU7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICBib3JkZXItY29sb3I6IHJnYigyMTIsIDUsIDUpO1xyXG4gICAgbWFyZ2luLWxlZnQ6IDVweDtcclxuICAgICAgICB9XHJcblxyXG4gICAgLnNsaWRlMjYgLnRlc3QgLmJsb2NrMXtcclxuICAgICAgICBtYXJnaW46IDIwcHg7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBmbGV4LXN0YXJ0O1xyXG4gICAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICB9XHJcbiAgICAuc2xpZGUyNiAudGVzdCAuYmxvY2syIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTI2IC50ZXN0IC5ibG9jazIgbWF0LXJhZGlvLWdyb3VwICB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbiAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgIH1cclxuXHJcbiAgICBcclxuICAgIC5zbGlkZTI2IC50ZXN0IC5ibG9jazEgaW5wdXR7XHJcbiAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgIH1cclxuICAgIC5zbGlkZTI2IC50ZXN0IC5ibG9jazEgbGFiZWx7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbjogMDtcclxuICAgIH1cclxuXHJcblxyXG4gICAgfSJdfQ== */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](TestComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-test',
                templateUrl: './test.component.html',
                styleUrls: ['./test.component.css']
            }]
    }], function () { return [{ type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] }, { type: _services_cpn_test_egibilite_service__WEBPACK_IMPORTED_MODULE_4__["TestEgibiliteService"] }]; }, null); })();


/***/ }),

/***/ "./src/app/video/video.component.ts":
/*!******************************************!*\
  !*** ./src/app/video/video.component.ts ***!
  \******************************************/
/*! exports provided: VideoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoComponent", function() { return VideoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


class VideoComponent {
    constructor() { }
    ngOnInit() {
    }
}
VideoComponent.ɵfac = function VideoComponent_Factory(t) { return new (t || VideoComponent)(); };
VideoComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: VideoComponent, selectors: [["app-video"]], decls: 7, vars: 1, consts: [[1, "all"], ["autoplay", "", "loop", "", "id", "myVideo", "autoplay", "", 3, "muted"], ["src", "assets/cpnimages/video/cpn_media.mp4", "type", "video/mp4"], [1, "button", 2, "overflow", "hidden", "padding", "10px 0", "position", "absolute", "bottom", "0", "left", "0", "width", "100%", "display", "flex", "justify-content", "center"], ["href", "/home", 1, "goToWebsite"]], template: function VideoComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "video", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "source", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, " Your browser does not support HTML5 video. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "a", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Acc\u00E9der au site");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("muted", "muted");
    } }, styles: ["#myVideo[_ngcontent-%COMP%] {\n  position: fixed;\n  right: 0;\n  bottom: 0;\n  width: 100%;\n}\n\n\n\n.content[_ngcontent-%COMP%] {\n  position: fixed;\n  bottom: 0;\n  background: rgba(0, 0, 0, 0.5);\n  color: #f1f1f1;\n  width: 100%;\n  padding: 20px;\n}\n\n\n\n#myBtn[_ngcontent-%COMP%] {\n  width: 200px;\n  font-size: 18px;\n  padding: 10px;\n  border: none;\n  background: #000;\n  color: #fff;\n  cursor: pointer;\n}\n\n#myBtn[_ngcontent-%COMP%]:hover {\n  background: #ddd;\n  color: black;\n}\n\n.goToWebsite[_ngcontent-%COMP%] {\n  text-decoration: none;\n  transform: translate(0, 100px);\n  border: none;\n  background: red;\n  padding: 10px;\n  border-radius: 5px;\n  color: white;\n  margin-bottom: 30px;\n}\n\n.all[_ngcontent-%COMP%]:hover   .goToWebsite[_ngcontent-%COMP%] {\n  transition: 0.3s;\n  transform: translate(0px, 0px);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlkZW8vdmlkZW8uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxlQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0FBQ0o7O0FBSUUscURBQUE7O0FBQ0E7RUFDRSxlQUFBO0VBQ0EsU0FBQTtFQUNBLDhCQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0FBREo7O0FBSUUsa0RBQUE7O0FBQ0E7RUFDRSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGFBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBQURKOztBQUlFO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0FBREo7O0FBSUU7RUFDRSxxQkFBQTtFQUNDLDhCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0QsbUJBQUE7QUFESjs7QUFNTTtFQUFlLGdCQUFBO0VBQ2pCLDhCQUFBO0FBRkoiLCJmaWxlIjoic3JjL2FwcC92aWRlby92aWRlby5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNteVZpZGVvIHtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIHJpZ2h0OiAwO1xyXG4gICAgYm90dG9tOiAwO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgIC8vIG1pbi13aWR0aDogNTAlO1xyXG4gICAvLyBtaW4taGVpZ2h0OiA3MCU7XHJcbiAgfVxyXG4gIFxyXG4gIC8qIEFkZCBzb21lIGNvbnRlbnQgYXQgdGhlIGJvdHRvbSBvZiB0aGUgdmlkZW8vcGFnZSAqL1xyXG4gIC5jb250ZW50IHtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIGJvdHRvbTogMDtcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC41KTtcclxuICAgIGNvbG9yOiAjZjFmMWYxO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBwYWRkaW5nOiAyMHB4O1xyXG4gIH1cclxuICBcclxuICAvKiBTdHlsZSB0aGUgYnV0dG9uIHVzZWQgdG8gcGF1c2UvcGxheSB0aGUgdmlkZW8gKi9cclxuICAjbXlCdG4ge1xyXG4gICAgd2lkdGg6IDIwMHB4O1xyXG4gICAgZm9udC1zaXplOiAxOHB4O1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGJhY2tncm91bmQ6ICMwMDA7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICB9XHJcbiAgXHJcbiAgI215QnRuOmhvdmVyIHtcclxuICAgIGJhY2tncm91bmQ6ICNkZGQ7XHJcbiAgICBjb2xvcjogYmxhY2s7XHJcbiAgfSBcclxuXHJcbiAgLmdvVG9XZWJzaXRle1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOm5vbmU7XHJcbiAgICAgdHJhbnNmb3JtOnRyYW5zbGF0ZSgwLDEwMHB4KTtcclxuICAgICBib3JkZXI6bm9uZTtcclxuICAgICBiYWNrZ3JvdW5kOnJlZDtcclxuICAgICBwYWRkaW5nOjEwcHg7XHJcbiAgICAgYm9yZGVyLXJhZGl1czo1cHg7XHJcbiAgICAgY29sb3I6d2hpdGU7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG4gIH1cclxuXHJcbiAgICAuYWxsOmhvdmVyICB7XHJcblxyXG4gICAgICAuZ29Ub1dlYnNpdGV7ICB0cmFuc2l0aW9uOiAwLjNzO1xyXG4gICAgdHJhbnNmb3JtIDogdHJhbnNsYXRlKDBweCwwcHgpO1xyXG4gIH19Il19 */"] });
/*@__PURE__*/ (function () { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](VideoComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
                selector: 'app-video',
                templateUrl: './video.component.html',
                styleUrls: ['./video.component.scss']
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var src_app_baseUrl__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/baseUrl */ "./src/app/baseUrl.ts");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    production: false,
    apiUrl: src_app_baseUrl__WEBPACK_IMPORTED_MODULE_0__["baseUrl"],
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\utilisateur\Music\cpn\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map